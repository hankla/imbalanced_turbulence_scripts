import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import comparison, get_comparison_Avar_as_Bvar, get_comparison_stats
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe
from fractions import Fraction

"""
This script will plot the perpendicular magnetic energy spectrum
for each simulation in the category. The spectrum can be taken at a single
time or averaged over a number of times (the same for each simulation).

The spectrum can be compensated to k^compensate_index, which is given by a Fraction.
Annotations include power laws of k^{-2}, k^{-5/3} in the inertial range and
k^{-4} in the kinetic range. The largest magnetic energy spectrum is also plotted
(ph_biggest), and the simulation/time can be adjusted.

This script assumes access to the raw data or a reduced data file
pspectrum for each time step.

KWARGS:
- time_to_plot: the *index* of the desired time.
- timestep_to_plot: the timestep to plot.
- tLC_initial and tLC_final: the times in light-crossing to average over

TO DO:
- clean up time selection
"""
small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)


def compare_plot_magnetic_energy_spectrum(category, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    time_to_plot = kwargs.get("time_to_plot", 76)
    timestep_to_plot = kwargs.get("timestep_to_plot", None)
    to_average = kwargs.get("to_average", False)
    plot_elsasser = kwargs.get("plot_elsasser", True)
    if to_average and not (time_to_plot == "teq"):
        tLC_initial = kwargs.get("tLC_initial", 10.0)
        tLC_final = kwargs.get("tLC_final", 11.0)
        timestep_to_plot = None
        time_to_plot = None
    # time_in_LC_to_plot = kwargs.get("time_in_LC_to_plot", None)
    to_compensate = kwargs.get("to_compensate", False)
    if to_compensate:
        compensate_index = kwargs.get("compensate_index", Fraction(5,3))

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    times = retrieve_time_values(ph0)

    ph_biggest = path_handler("data_1536cube_track_64ppc", system_config)
    timestep_biggest = 24000
    biggest_times = retrieve_time_values(ph_biggest)
    biggest_time_to_plot = (np.abs(biggest_times[:, 0] - timestep_biggest)).argmin()
    biggest_time_in_LC = biggest_times[biggest_time_to_plot, 2]
    biggest_spectrum = retrieve_perpendicular_spectrum(ph_biggest, "magnetic_energy", timestep_biggest)
    if not os.path.exists(ph_biggest.path_to_reduced_data + "larmor_radius.dat"):
        plot_length_scales(ph_biggest)
    biggest_larmor_evolution = np.loadtxt(ph_biggest.path_to_reduced_data + "larmor_radius.dat", delimiter=",", skiprows=1)
    biggest_larmor_ind = (np.abs(biggest_larmor_evolution[:, 0] - biggest_time_in_LC)).argmin()
    biggest_larmor_radius_in_cells = biggest_larmor_evolution[biggest_larmor_ind, 1]
    bnx = 1536
    bnm = int(bnx/3)
    bperp_k_vals = (2.0*np.pi/bnx)*np.arange(1, bnm + 1)*biggest_larmor_radius_in_cells

    tequivalent = False
    # NOTE this times must be indices! Since this script plots the same times.
    # The L/c should be the same but the timestep will be different for box sizes
    if "final_Einj" in kwargs and time_to_plot == "teq":
        print("Plotting equivalent times.")
        tequivalent = True
        time_string = "teq"
        final_Einj = kwargs.get("final_Einj")
    else:
        if timestep_to_plot is not None:
            time_string = "ts{:d}".format(int(timestep_to_plot))
        else:
            if to_average:
                tinitial_ind = (np.abs(times[:,2] - tLC_initial)).argmin()
                tfinal_ind = (np.abs(times[:,2] - tLC_final)).argmin()
                time_string = "tAvg{:d}-{:d}".format(tinitial_ind, tfinal_ind)
            else:
                time_string = "t{:d}".format(int(time_to_plot))

    figdir = ph0.path_to_figures + "../compare/" + category + "/magnetic_energy_spectrum/"
    if to_average:
        figdir += "tAvg/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "magnetic-energy-spectrum_" + time_string
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    if to_compensate:
        comp_str = str(compensate_index).replace('/','')
        figname += "_compensated" + comp_str
    if plot_elsasser:
        figname += "_withElsasser"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    labels = comparison(category).labels
    colors = itertools.cycle(comparison(category).colors)
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    plt.figure()
    plt.xlabel(r"$k_\perp\rho_e(t)$")
    ylabel = r"$P_{\rm mag}(k_\perp)$"
    if to_compensate:
        ylabel += r"$k_\perp^{" + str(Fraction(compensate_index)) + "}$"
    plt.ylabel(ylabel)
    plt.yscale('log')
    plt.xscale('log')
    plt.gca().grid(color='.9', ls='--')
    plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    if ylims is not None:
        plt.ylim(ylims)
    if xlims is not None:
        plt.xlim(xlims)
    title = "Magnetic energy spectrum \n"
    equiv_string = "at equivalent times "

    for i, sim_name in enumerate(configs):
        print(sim_name)
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, system_config)

        times_in_LC = retrieve_time_values(ph)[:, 2]
        timesteps = retrieve_time_values(ph)[:, 0]
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        B0 = params["B0"]
        mag_energy_density = B0**2.0/(8.0*np.pi)
        # Get equivalent time step
        if tequivalent:
            einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
            einj_cum = np.cumsum(einj)
            dt_in_s = params["dt"]
            Lx = params["xmax"] - params["xmin"]
            Ly = params["ymax"] - params["ymin"]
            Lz = params["zmax"] - params["zmin"]
            volume = Lx*Ly*Lz
            einj_norm = einj_cum/volume/mag_energy_density
            dt_in_LC = dt_in_s*rdu.Consts.c/Lz
            einj_times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
            einj_times_in_LC = np.insert(einj_times_in_LC, 0, 0)
            einj_times_in_LC = einj_times_in_LC[:-1]
            equivalent_time_ind = (np.abs(-1.0*einj_norm - final_Einj)).argmin()
            einj_equiv_time = einj_times_in_LC[equivalent_time_ind]
            equivalent_time_ind = (np.abs(times_in_LC - einj_equiv_time)).argmin()
            time_to_plot = equivalent_time_ind
            time_in_LC = einj_equiv_time
            equiv_string += "{:.2f}, ".format(times_in_LC[equivalent_time_ind])
        else:
            if not to_average:
                if timestep_to_plot is not None:
                    time_to_plot = (np.abs(timesteps - timestep_to_plot)).argmin()
                time_in_LC = times_in_LC[time_to_plot]

        coords = retrieve_coords(ph)
        nx = coords[0].size - 1
        ny = coords[1].size - 1
        # number of modes comes from reduce_perp_spectrum
        number_of_modes = int(np.min([nx, ny])/3)
        # Load rho(t)
        larmor_radius_time_evolution = np.loadtxt(ph.path_to_reduced_data + "larmor_radius.dat", delimiter=",", skiprows=1)
        pspectrum = None
        pspectrum_all = None
        epspectrum = None
        emspectrum = None
        epspectrum_all = None
        emspectrum_all = None

        if to_average:
            # Remember larmor radius is output at every time step, so will have different
            # times indices!
            larmor_initial_ind = (np.abs(larmor_radius_time_evolution[:, 0] - tLC_initial)).argmin()
            larmor_final_ind = (np.abs(larmor_radius_time_evolution[:, 0] - tLC_final)).argmin()
            larmor_radius_in_cells = larmor_radius_time_evolution[larmor_initial_ind:larmor_final_ind + 1, 1].mean()
            tind_size = tfinal_ind - tinitial_ind + 1
            etind_size = tind_size
            for tind in np.arange(tinitial_ind, tfinal_ind + 1):

                timestep_to_load = int(timesteps[tind])
                pspectrum_at_t = retrieve_perpendicular_spectrum(ph, "magnetic_energy", timestep_to_load)
                if plot_elsasser and "im00" in ph.sim_name:
                    epspectrum_at_t = retrieve_perpendicular_spectrum(ph, "elsasserPlus", timestep_to_load)
                    emspectrum_at_t = retrieve_perpendicular_spectrum(ph, "elsasserMinus", timestep_to_load)
                    if epspectrum_at_t is None or emspectrum_at_t is None:
                        etind_size = etind_size - 1

                    if epspectrum_all is None:
                        epspectrum_all = epspectrum_at_t
                        emspectrum_all = emspectrum_at_t
                    else:
                        epspectrum_all = np.vstack([epspectrum_all, epspectrum_at_t])
                        emspectrum_all = np.vstack([emspectrum_all, emspectrum_at_t])

                if pspectrum_at_t is None:
                    print("Spectrum for " + ph.sim_name + " at time {:d} does not exist.".format(timestep_to_load))
                    tind_size = tind_size - 1
                else:
                    if pspectrum_all is None:
                        pspectrum_all = pspectrum_at_t
                    else:
                        pspectrum_all = np.vstack([pspectrum_all, pspectrum_at_t])
                        # pspectrum += pspectrum_at_t

        else:
            timestep_to_load = timesteps[time_to_plot]
            larmor_ind = (np.abs(larmor_radius_time_evolution[:, 0] - time_in_LC)).argmin()
            larmor_radius_in_cells = larmor_radius_time_evolution[larmor_ind, 1]
            pspectrum = retrieve_perpendicular_spectrum(ph, "magnetic_energy", timestep_to_load)
            # larmor_radius_in_cells = 3.0/2.0 # = NOM_LENGTH_SCALE
        perp_k_vals = (2.0*np.pi/nx)*np.arange(1, number_of_modes + 1)*larmor_radius_in_cells
        if pspectrum_all is None and pspectrum is None:
            next(colors)
            next(markers)
            next(linestyles)
            continue
        if to_compensate:
            if to_average:
                if pspectrum_all.ndim > 1:
                    pspectrum_all = pspectrum_all*(perp_k_vals)**(compensate_index)
                    pspectrum = pspectrum_all.mean(axis=0)
                    pspectrum_stds = np.std(pspectrum_all, axis=0, dtype=float)
                else:
                    pspectrum = pspectrum_all*(perp_k_vals)**(compensate_index)
                    pspectrum_stds = np.zeros(pspectrum.shape)
                if plot_elsasser and "im00" in ph.sim_name:
                    if epspectrum_all.ndim > 1:
                        epspectrum_all = epspectrum_all*(perp_k_vals[:-1])**(compensate_index)
                        emspectrum_all = emspectrum_all*(perp_k_vals[:-1])**(compensate_index)
                        epspectrum = epspectrum_all.mean(axis=0)
                        emspectrum = emspectrum_all.mean(axis=0)
                        epspectrum_stds = np.std(epspectrum_all, axis=0, dtype=float)
                        emspectrum_stds = np.std(emspectrum_all, axis=0, dtype=float)
                    else:
                        epspectrum = epspectrum_all*(perp_k_vals[:-1])**(compensate_index)
                        emspectrum = emspectrum_all*(perp_k_vals[:-1])**(compensate_index)
                        epspectrum_stds = np.zeros(epspectrum.shape)
                        emspectrum_stds = np.zeros(emspectrum.shape)
            else:
                pspectrum = pspectrum*(perp_k_vals)**(compensate_index)
        else:
            if to_average:
                if pspectrum_all.ndim > 1:
                    pspectrum = pspectrum_all.mean(axis=0)
                    pspectrum_stds = np.std(pspectrum_all, axis=0, dtype=float)
                else:
                    pspectrum = pspectrum_all
                    # pspectrum = pspectrum_all*(perp_k_vals)**(compensate_index)
                    pspectrum_stds = np.zeros(pspectrum.shape)
                if plot_elsasser and "im00" in ph.sim_name:
                    if epspectrum_all.ndim > 1:
                        epspectrum = epspectrum_all.mean(axis=0)
                        emspectrum = emspectrum_all.mean(axis=0)
                        epspectrum_stds = np.std(epspectrum_all, axis=0, dtype=float)
                        emspectrum_stds = np.std(emspectrum_all, axis=0, dtype=float)
                    else:
                        epspectrum = epspectrum_all
                        emspectrum = emspectrum_all
                        epspectrum_stds = np.zeros(epspectrum.shape)
                        emspectrum_stds = np.zeros(emspectrum.shape)
        color = next(colors)
        plt.plot(perp_k_vals, pspectrum, color=color, label=labels[i], ls=next(linestyles), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
        # plt.plot(perp_k_vals, pspectrum, marker=next(markers), markersize=4, color=next(colors), label=labels[i], ls=next(linestyles))
        if to_average:
            pspectrum = pspectrum.astype(np.float)
            pspectrum_stds = pspectrum_stds.astype(np.float)
            plt.fill_between(perp_k_vals, pspectrum - pspectrum_stds, pspectrum + pspectrum_stds, color=color, alpha=0.3)
            # plt.fill_between(perp_k_vals, pspectrum - pspectrum_stds, pspectrum + pspectrum_stds, color='blue', label=labels[i], ls=next(linestyles), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
            if plot_elsasser and "im00" in ph.sim_name:
                epspectrum = epspectrum.astype(np.float)*mag_energy_density*2.0
                emspectrum = emspectrum.astype(np.float)*mag_energy_density*2.0
                plt.plot(perp_k_vals[:-1], epspectrum, color='tab:red', ls='-.', alpha=0.5)
                plt.plot(perp_k_vals[:-1], emspectrum, color='tab:red', ls='-.', alpha=1.0)
                # plt.plot(perp_k_vals[:-1], epspectrum, color='red', ls='-.', path_effects=[mpe.Stroke(linewidth=2, foreground='r'), mpe.Normal()], alpha=0.2)
                # plt.plot(perp_k_vals[:-1], emspectrum, color='red', ls='-.', path_effects=[mpe.Stroke(linewidth=2, foreground='r'), mpe.Normal()], alpha=0.7)
                plt.fill_between(perp_k_vals[:-1], epspectrum - epspectrum_stds, epspectrum + epspectrum_stds, color='tab:red', alpha=0.2)
                plt.fill_between(perp_k_vals[:-1], emspectrum - emspectrum_stds, emspectrum + emspectrum_stds, color='tab:red', alpha=0.2)
    if to_compensate:
        plt.gca().axhline([2e6], ls=':', color='black')
        # Next line is specifically the -5/3 at the -2 compensation
        if compensate_index == Fraction(2, 1):
            short_k_vals = perp_k_vals[6:18]
            plt.plot(short_k_vals, short_k_vals**(-5/3+2)*1.1e6, 'k--')

            short_bperp_k_vals = bperp_k_vals#[4:90]
            short_bspectrum = biggest_spectrum#[4:90]
            plt.plot(short_bperp_k_vals, 0.55*short_bspectrum*(short_bperp_k_vals)**(compensate_index), 'k-', lw=2, alpha=0.6)
        # plt.text(0.4, 1e8, r'$\propto k^{-5/3}$')
    else:
        short_k_vals = perp_k_vals[8:35]
        plt.plot(short_k_vals, 2.9e7*(short_k_vals)**(-5.0/3.0), ls='--', color='black')
        plt.text(0.35, 2e8, r'$\propto k_\perp^{-5/3}$')
        short_k_vals = perp_k_vals[6:33]
        plt.plot(short_k_vals, 4e5*(short_k_vals)**(-2.0), ls=':', color='black')
        plt.text(0.2, 1e6, r'$\propto k_\perp^{-2}$')

        short_k_vals = perp_k_vals[50:200]
        plt.plot(short_k_vals, 2e7*(short_k_vals)**(-4.0), ls='-.', color='black')
        plt.text(1.6, 3e6, r'$\propto k_\perp^{-4}$')

        # short_k_vals = perp_k_vals[70:200]
        # plt.plot(short_k_vals, 9e6*(short_k_vals)**(-16.0/3.0), ls='-.', color='black')
        # plt.text(1.5, 3e6, r'$\propto k_\perp^{-16/3}$')

        short_bperp_k_vals = bperp_k_vals#[10:140]
        short_bspectrum = biggest_spectrum#[10:140]
        plt.plot(short_bperp_k_vals, 10*short_bspectrum, 'k-', lw=2, alpha=0.8)
    if tequivalent:
        title += equiv_string
    else:
        if to_average:
            title += r"Averaged from $t=${:.2f}-{:.2f} $L/c$".format(times[tinitial_ind,2], times[tfinal_ind,2])
        else:
            title += r"$t=${:.2f} $L/c$".format(time_in_LC)
    title += "\n" + category
    plt.title(title, fontsize=12)

    sim_set = comparison(category)
    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False, ncol=sim_set.ncol)
    else:
        plt.gca().legend(handles=legend_elements, frameon=False, ncol=sim_set.ncol)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()



if __name__ == '__main__':
    # ------- inputs ---------
    # category = "xiAllboxSizeAll-comparison"
    category = "768Imbalance-comparison"
    # category = "384Imbalance-comparison"
    # category = "xi0boxSize-comparison"
    # category = "768vvz-comparison"
    stampede2 = False
    system_config = "lia_hp"
    plot_elsasser = True
    plot_teq = False
    to_average = True
    if to_average:
        tLC_initial = 08.9
        tLC_final = 10.0
    to_compensate = True
    compensate_index = None
    compensate_index = Fraction(5,3)
    compensate_index = Fraction(2,1)
    # compensate_index = Fraction(4,1)
    if to_compensate:
        if compensate_index == Fraction(5,3):
            xlims = [4.0e-2, 2]
            ylims = [8.0e5, 2e7]
            # xlims = [1.0e-1, 2]
            # ylims = [1.0e5, 5e7]
            # xlims = [1.0e-1, 2]
            # ylims = [1.0e6, 5e8]
        elif compensate_index == Fraction(2, 1):
            # xlims = [0.04, 3]
            # ylims = [5e5, 1.8e7]
            xlims = [0.04, 3]
            ylims = [3e5, 1.8e7]
        else:
            xlims = [5e-1, 5]
            ylims = [1.0e4, 1e8]
    else:
        if compensate_index == Fraction(5,3):
            xlims = [1.0e-1, 2]
            ylims = [1.0e5, 5e9]
        elif compensate_index == Fraction(4,1):
            xlims = [5e-1, 5]
            ylims = [1.0e3, 1e8]
        else:
            xlims = [4e-2, 4]
            ylims = [7e3, 5e9]

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["time_to_plot"] = 34
    # kwargs["timestep_to_plot"] = 12000
    # kwargs["time_in_LC_to_plot"] = 7.8
    # kwargs["timestep_to_plot"] = 10500
    # kwargs["timestep_to_plot"] = 5400
    kwargs["to_average"] = to_average
    if to_average:
        kwargs["tLC_initial"] = tLC_initial
        kwargs["tLC_final"] = tLC_final
    kwargs["to_compensate"] = to_compensate
    kwargs["compensate_index"] = compensate_index
    kwargs["stampede2"] = stampede2
    kwargs["system_config"] = system_config
    kwargs["plot_elsasser"] = plot_elsasser
    kwargs["reduced_data_path"] = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/data_reduced/"

    # get minimum final Einj
    if plot_teq:
        stats = get_comparison_stats(category)
        final_einj_densities = stats.loc["einj_density_at_20_Lc"]
        final_Einj = final_einj_densities.min()
        kwargs["final_Einj"] = final_Einj
        kwargs["time_to_plot"] = "teq"
    # compare_plot_magnetic_energy_spectrum(category, **kwargs)
    compare_plot_magnetic_energy_spectrum(category, **kwargs, xlims=xlims, ylims=ylims)
