import numpy as np
import sys
sys.path.append("./modules/")
sys.path.append("./modules/zPy/for3D/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from utils import get_times
import zfileUtil as zf
import pickle

stampede2 = True
# config = "zeltron_96cube_balanced_s0"
# config = "zeltron_96cube_single-mode_s0"
config = "zeltron_96cube_single-mode_no-forcing_travelling"
# config = "zeltron_96cube_single-mode_no-forcing_standing"
# config = "zeltron_96cube_single-mode_no-forcing_dcorr"
# config = "zeltron_96cube_balanced_master"
# config = "zeltron_96cube_single-mode_master"

overwrite = True

if stampede2:
    datapath = "/scratch/06165/ahankla/imbalanced_turbulence/" + config + "/data/"
    datarpath = "/work/06165/ahankla/stampede2/imbalanced_turbulence/data_reduced/" + config + "/"
    figpath = "/work/06165/ahankla/stampede2/imbalanced_turbulence/figures/" + config + "/"
else:
    datapath = "/mnt/d/imbalanced_turbulence/data/" + config + "/"
    datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    figpath = "/mnt/d/imbalanced_turbulence/figures/" + config + "/"
ch_means_p = datarpath +  "cross-helicity_vavg_means.p"
ch_stds_p = datarpath + "cross-helicity_vavg_stds.p"
chf_means_p = datarpath +  "cross-helicity-flucs_vavg_means.p"
chvf_means_p = datarpath +  "cross-helicity-vflucs_vavg_means.p"
chv_means_p = datarpath +  "cross-helicity-v_vavg_means.p"
chf_stds_p = datarpath + "cross-helicity-flucs_vavg_stds.p"
te_means_p = datarpath +  "total-energy_vavg_means.p"
te_stds_p = datarpath + "total-energy_vavg_stds.p"
tef_means_p = datarpath +  "total-energy-flucs_vavg_means.p"
tevf_means_p = datarpath +  "total-energy-vflucs_vavg_means.p"
tev_means_p = datarpath +  "total-energy-v_vavg_means.p"
tef_stds_p = datarpath + "total-energy-flucs_vavg_stds.p"
field = "cross-helicity"


times = [int(t) for t in get_times(datapath)]

os.chdir(datapath)

if overwrite or not os.path.exists(ch_means_p):
    to_calc = True
    mean_cross_helicity = {}
    mean_cross_helicity["vol_avg"] = []
    mean_cross_helicity["xy_avg"] = []
    mean_cross_helicity["xz_avg"] = []
    mean_cross_helicity["yz_avg"] = []
    mean_cross_helicity_f = {}
    mean_cross_helicity_f["vol_avg"] = []
    mean_cross_helicity_vf = {}
    mean_cross_helicity_vf["vol_avg"] = []
    mean_cross_helicity_v = {}
    mean_cross_helicity_v["vol_avg"] = []
    std_cross_helicity = {}
    std_cross_helicity["vol_avg"] = []
    std_cross_helicity["xy_avg"] = []
    std_cross_helicity["xz_avg"] = []
    std_cross_helicity["yz_avg"] = []
    mean_total_energy = {}
    mean_total_energy["vol_avg"] = []
    mean_total_energy["xy_avg"] = []
    mean_total_energy["xz_avg"] = []
    mean_total_energy["yz_avg"] = []
    mean_total_energy_f = {}
    mean_total_energy_f["vol_avg"] = []
    mean_total_energy_vf = {}
    mean_total_energy_vf["vol_avg"] = []
    mean_total_energy_v = {}
    mean_total_energy_v["vol_avg"] = []
    std_total_energy = {}
    std_total_energy["vol_avg"] = []
    std_total_energy["xy_avg"] = []
    std_total_energy["xz_avg"] = []
    std_total_energy["yz_avg"] = []
else:
    mean_cross_helicity = pickle.load(open(ch_means_p, "rb"))
    std_cross_helicity = pickle.load(open(ch_stds_p, "rb"))
    mean_cross_helicity_f = pickle.load(open(chf_means_p, "rb"))
    std_cross_helicity_f = pickle.load(open(chf_stds_p, "rb"))
    mean_cross_helicity_vf = pickle.load(open(chvf_means_p, "rb"))
    mean_cross_helicity_v = pickle.load(open(chv_means_p, "rb"))
    mean_total_energy = pickle.load(open(te_means_p, "rb"))
    std_total_energy = pickle.load(open(te_stds_p, "rb"))
    mean_total_energy_f = pickle.load(open(tef_means_p, "rb"))
    mean_total_energy_vf = pickle.load(open(tevf_means_p, "rb"))
    mean_total_energy_v = pickle.load(open(tev_means_p, "rb"))
    std_total_energy_f = pickle.load(open(tef_stds_p, "rb"))
    to_calc = False

# calculate normalization factor
params = np.loadtxt(datapath + "phys_params.dat", skiprows=1)
consts = zf.Consts
B0 = params[2]
sigma = params[6]
va = consts.c*np.sqrt(sigma/(sigma+4./3.))

# if normstr == "B0c":
    # norm = B0*c
    # normlab = r"[$B_0c$]"
# elif normstr == "B0va":
    # norm = B0*va
    # normlab = r"[$B_0v_A$]"
# else:
    # print("Error.")

# Loop through at each time
if to_calc:
    for time in times:
        # get magnetic field
        # Bx = zf.getExtField("Bx", time)[0]
        (Bx, t, step, coords) = zf.getExtField("Bx", time)
        By = zf.getExtField("By", time)[0]
        Bz = zf.getExtField("Bz", time)[0]
        bz = Bz - B0

        # normalize to alfven velocity
        # Bx = Bx*va/B0
        # By = By*va/B0
        # Bz = Bz*va/B0
        # bz = bz*va/B0

        # calculate center of mass velocity, assuming same mass for electrons and ions (positrons)
        velx = zf.getExtField("velx_total", time)[0]*consts.c
        vely = zf.getExtField("vely_total", time)[0]*consts.c
        velz = zf.getExtField("velz_total", time)[0]*consts.c

        velxf = velx - np.mean(velx)
        velyf = vely - np.mean(vely)
        velzf = velz - np.mean(velz)

        # calculate cross-helicity B dot v
        cross_helicity = Bx*velx + By*vely + Bz*velz
        cross_helicity_f = Bx*velx + By*vely + bz*velz
        cross_helicity_vf = Bx*velxf + By*velyf + bz*velzf
        cross_helicity_v = Bx*velxf + By*velyf + Bz*velzf

        # Calculate total energy
        total_energy = 0.25*(velx**2 + vely**2 + velz**2 + Bx**2 + By**2 + Bz**2)
        total_energy_f = 0.25*(velx**2 + vely**2 + velz**2 + Bx**2 + By**2 + bz**2)
        total_energy_vf = 0.25*(velxf**2 + velyf**2 + velzf**2 + Bx**2 + By**2 + bz**2)
        total_energy_v = 0.25*(velxf**2 + velyf**2 + velzf**2 + Bx**2 + By**2 + Bz**2)

        # take some averages and stds
        def tintegrate(data, coordinates):
            integrated_data = np.trapz(data, x=coords[2], axis=2)
            integrated_data = np.trapz(integrated_data, x=coords[1], axis=1)
            integrated_data = np.trapz(integrated_data, x=coords[0], axis=0)
            return integrated_data

        mch = tintegrate(cross_helicity, coords)
        mchf = tintegrate(cross_helicity_f, coords)
        mchvf = tintegrate(cross_helicity_vf, coords)
        mchv = tintegrate(cross_helicity_v, coords)
        mte = tintegrate(total_energy, coords)
        mtef = tintegrate(total_energy_f, coords)
        mtevf = tintegrate(total_energy_vf, coords)
        mtev = tintegrate(total_energy_v, coords)
        # print(mch)
        # print(np.mean(cross_helicity))
        # print(mte)
        # print(np.mean(total_energy))
        # print(mch/mte)
        # print(np.mean(cross_helicity)/np.mean(total_energy))
        mean_cross_helicity["vol_avg"].append(mch)
        mean_cross_helicity["xy_avg"].append(np.mean(cross_helicity, axis=(0, 1)))
        mean_cross_helicity["xz_avg"].append(np.mean(cross_helicity, axis=(0, 2)))
        mean_cross_helicity["yz_avg"].append(np.mean(cross_helicity, axis=(1, 2)))
        mean_cross_helicity_f["vol_avg"].append(mchf)
        mean_cross_helicity_vf["vol_avg"].append(mchvf)
        mean_cross_helicity_v["vol_avg"].append(mchv)
        std_cross_helicity["vol_avg"].append(np.std(cross_helicity))
        std_cross_helicity["xy_avg"].append(np.std(cross_helicity, axis=(0, 1)))
        std_cross_helicity["xz_avg"].append(np.std(cross_helicity, axis=(0, 2)))
        std_cross_helicity["yz_avg"].append(np.std(cross_helicity, axis=(1, 2)))
        mean_total_energy["vol_avg"].append(mte)
        mean_total_energy["xy_avg"].append(np.mean(total_energy, axis=(0, 1)))
        mean_total_energy["xz_avg"].append(np.mean(total_energy, axis=(0, 2)))
        mean_total_energy["yz_avg"].append(np.mean(total_energy, axis=(1, 2)))
        mean_total_energy_f["vol_avg"].append(mtef)
        mean_total_energy_vf["vol_avg"].append(mtevf)
        mean_total_energy_v["vol_avg"].append(mtev)
        std_total_energy["vol_avg"].append(np.std(total_energy))
        std_total_energy["xy_avg"].append(np.std(total_energy, axis=(0, 1)))
        std_total_energy["xz_avg"].append(np.std(total_energy, axis=(0, 2)))
        std_total_energy["yz_avg"].append(np.std(total_energy, axis=(1, 2)))

        # save to hdf5 file

        # make figures
        # print(np.mean(cross_helicity))

    pickle.dump(mean_cross_helicity, open(ch_means_p, "wb"))
    pickle.dump(mean_cross_helicity_f, open(chf_means_p, "wb"))
    pickle.dump(mean_cross_helicity_vf, open(chvf_means_p, "wb"))
    pickle.dump(mean_cross_helicity_v, open(chv_means_p, "wb"))
    pickle.dump(std_cross_helicity, open(ch_stds_p, "wb"))
    pickle.dump(mean_total_energy, open(te_means_p, "wb"))
    pickle.dump(mean_total_energy_f, open(tef_means_p, "wb"))
    pickle.dump(mean_total_energy_vf, open(tevf_means_p, "wb"))
    pickle.dump(mean_total_energy_v, open(tev_means_p, "wb"))
    pickle.dump(std_total_energy, open(te_stds_p, "wb"))



# -----------------------------------------
# Plot volume-averaged normalized cross-helicity over time
norm_cross_helicity = np.array(mean_cross_helicity["vol_avg"])/np.array(mean_total_energy["vol_avg"])
tlcpath = datarpath + "timeInLc.p"
if not os.path.exists(tlcpath):
    print("ERROR: PICKLE TIME VALUES FIRST!!")
else:
    timesLC = np.array(pickle.load(open(tlcpath, "rb")))

plt.figure()
plt.xlabel("$tc/L$")
plt.ylabel("$H_c/E$")
plt.title(config + "\n Average over time: {:.2e}$\pm${:.2e} ".format(np.mean(norm_cross_helicity), np.std(norm_cross_helicity)))
plt.plot(timesLC, norm_cross_helicity, marker='o')
plt.gca().axhline([0], color="black", linestyle="--")
plt.tight_layout()
figdir = figpath + "volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + field + "_norm_" + config
print(figname)
plt.savefig(figname)

# Plot volume-averaged normalized cross-helicity fluctuations over time
norm_cross_helicity_f = np.array(mean_cross_helicity_f["vol_avg"])/np.array(mean_total_energy_f["vol_avg"])
plt.figure()
plt.xlabel("$tc/L$")
plt.ylabel("$H_c/E$")
plt.title(config + "\n Average over time: {:.2e}$\pm${:.2e} ".format(np.mean(norm_cross_helicity_f), np.std(norm_cross_helicity_f)))
plt.plot(timesLC, norm_cross_helicity_f, marker='o')
plt.gca().axhline([0], color="black", linestyle="--")
plt.tight_layout()
figdir = figpath + "volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + field + "-flucs_norm_" + config
print(figname)
plt.savefig(figname)

# Plot slice-averaged cross-helicity over time
plt.figure()
mean_cross_helicity["yz_avg"] = np.array(mean_cross_helicity["yz_avg"])
mean_total_energy["yz_avg"] = np.array(mean_total_energy["yz_avg"])
vmin = np.min(mean_cross_helicity["yz_avg"]/mean_total_energy["yz_avg"])
vmax = np.max(mean_cross_helicity["yz_avg"]/mean_total_energy["yz_avg"])
vmin = np.min([-np.abs(vmin), -np.abs(vmax)])
vmax = np.max([np.abs(vmin), np.abs(vmax)])
plt.imshow(np.transpose(mean_cross_helicity["yz_avg"]/mean_total_energy["yz_avg"]), vmin=vmin, vmax=vmax, cmap="RdBu")
plt.xlabel('Time')
plt.ylabel('x')
plt.title(config)
cbar = plt.colorbar()
cbar.set_label("$H_c/E$")
plt.gca().set_aspect('auto')
plt.tight_layout()
figdir = figpath + "sliceAvg-over-time/" + "x-slice/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + field + "_norm_" + config
plt.savefig(figname)

plt.figure()
mean_cross_helicity["xy_avg"] = np.array(mean_cross_helicity["xy_avg"])
mean_total_energy["xy_avg"] = np.array(mean_total_energy["xy_avg"])
vmin = np.min(mean_cross_helicity["xy_avg"]/mean_total_energy["xy_avg"])
vmax = np.max(mean_cross_helicity["xy_avg"]/mean_total_energy["xy_avg"])
vmin = np.min([-np.abs(vmin), -np.abs(vmax)])
vmax = np.max([np.abs(vmin), np.abs(vmax)])
plt.imshow(np.transpose(mean_cross_helicity["xy_avg"]/mean_total_energy["xy_avg"]), vmin=vmin, vmax=vmax, cmap="RdBu")
plt.xlabel('Time')
plt.ylabel('z')
plt.title(config)
cbar = plt.colorbar()
cbar.set_label("$H_c/E$")
plt.gca().set_aspect('auto')
plt.tight_layout()
figdir = figpath + "sliceAvg-over-time/" + "z-slice/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + field + "_norm_" + config
plt.savefig(figname)

plt.figure()
mean_cross_helicity["xz_avg"] = np.array(mean_cross_helicity["xz_avg"])
mean_total_energy["xz_avg"] = np.array(mean_total_energy["xz_avg"])
vmin = np.min(mean_cross_helicity["xz_avg"]/mean_total_energy["xz_avg"])
vmax = np.max(mean_cross_helicity["xz_avg"]/mean_total_energy["xz_avg"])
vmin = np.min([-np.abs(vmin), -np.abs(vmax)])
vmax = np.max([np.abs(vmin), np.abs(vmax)])
plt.imshow(np.transpose(mean_cross_helicity["xz_avg"]/mean_total_energy["xz_avg"]), vmin=vmin, vmax=vmax, cmap="RdBu")
plt.xlabel('Time')
plt.ylabel('y')
plt.title(config)
cbar = plt.colorbar()
cbar.set_label("$H_c/E$")
plt.gca().set_aspect('auto')
plt.tight_layout()
figdir = figpath + "sliceAvg-over-time/" + "y-slice/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + field + "_norm_" + config
plt.savefig(figname)
