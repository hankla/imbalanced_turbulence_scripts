"""
This script will plot slices of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - from create_slices-2d_cross-helicity: need to implement subtracting vel/B means
"""
import sys
sys.path.append("./modules/")
import shutil
import multiprocessing as mp
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from save_elsaesser_quantities import save_elsaesser_quantities
from plot_length_scales_over_time import plot_length_scales


def parallel_plot(simulations, system_config, **kwargs):
    pool = mp.Pool(len(simulations))
    arg = [(sim, system_config, kwargs) for sim in simulations]
    pool.map(plotting_wrapper, arg)


def plotting_wrapper(arg):
    sim, system_config, kwargs = arg
    return plot_single_simulation(sim, system_config, **kwargs)


def plot_single_simulation(sim_name, system_config, **kwargs):
    print("******************************************")
    print("******************************************")
    print("Plotting simulation " + sim_name)

    # -------------------------------------------
    # Set paths
    ph = path_handler(sim_name, system_config)

    # -------------------------------------------
    # Next step is to make sure the simulation parameters
    # are accessible from the reduced data path
    files_to_copy = ["phys_params.dat", "input_params.dat", "Eem.dat", "Eifluid.dat", "Eefluid.dat", "Einj.dat", "Ekin_ions_bg.dat", "Ekin_electrons_bg.dat", "divE.dat"]
    if stampede2:
        for file1 in files_to_copy:
            shutil.copyfile(ph.path_to_raw_data + file1, ph.path_to_reduced_data + file1)
        print("Files copied to reduced data folder.")

    if not os.path.exists(ph.path_to_reduced_data + "magnetization.dat"):
        plot_length_scales(ph, **kwargs)
    save_elsaesser_quantities(ph, **kwargs)


if __name__ == "__main__":
    # ----------------------------------
    #    INPUTS
    # ----------------------------------
    stampede2 = True
    system_config = "lia_hp"
    overwrite_data = False
    overwrite_vol_avg_values = True

    # times_to_plot = [0]
    times_to_plot = None

    if stampede2:
        scratch_dir = "/scratch/06165/ahankla/imbalanced_turbulence/"
        system_config = "stampede2"
    else:
        scratch_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
        system_config = "lia_hp"

    list_of_sims = []
    list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im00"])
    list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im05"])
    list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im075"])
    list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im10"])
    # simulations = list_of_sims
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "im00"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "im025"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "im05"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "im075"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "im10"])
    simulations = list_of_sims
    # simulations = ["zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30", "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60", "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"]
    # simulations = rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "im05"])

    # ----------------------------------
    #    PLOTTING (NO EDITS REQUIRED)
    # ----------------------------------
    kwargs = {}
    kwargs["overwrite_data"] = overwrite_data
    kwargs["times_to_plot"] = times_to_plot
    kwargs["overwrite_vol_avg_values"] = overwrite_vol_avg_values

    parallel_plot(simulations, system_config, **kwargs)
