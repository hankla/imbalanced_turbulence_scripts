import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from scipy.fftpack import fft, fftfreq, fftshift
from compare_handler import comparison
import itertools
"""
This script will plot the spatial profilesof every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

"""

def compare_plot_FT_profiles_temporal(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    cut_initial = kwargs.get("cut_initial", False)
    match_times = kwargs.get("match_times", True)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = kwargs.get("fields_to_profile", ["Ex", "By", "vely_total"])
    log_yscale = kwargs.get("log_yscale", True)
    # NOTE: plot vlines only plots the LAST omegaD! If omegaD is different, won't work. 
    plot_vlines = kwargs.get("plot_vlines", True)
    max_scaling = kwargs.get("max_scaling", False)
    average_2d = kwargs.get("average_2d", False)

    configs = comparison(category).configs
    labels = comparison(category).labels
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figbase = ph0.path_to_figures + "../compare/" + category + "/profiles-1d_temporal/"
    if average_2d:
        figbase += "2d_averaged/"
    figbase += "FT/"
    if match_times:
        figbase += "matched_times/"
    if log_yscale:
        figbase += "log_yscale/"
    if max_scaling:
        figbase += "max_scaling/"
    figdir = figbase
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    zVars = rdu.Zvariables.options
    # fields_to_plot = zVars
    time_steps = retrieve_time_values(ph0)[:, 0]
    times_to_plot = time_steps[:]

    colors = comparison(category).colors

    smallest_end_time = None
    omegaDvalues = comparison(category).omegaD

    # First, need to find smallest time.
    for sim_name in configs:
        ph = path_handler(sim_name, system_config)

        times_in_LC = retrieve_time_values(ph)[:, 2]
        if smallest_end_time is None or times_in_LC[-1] < smallest_end_time:
            smallest_end_time = times_in_LC[-1]

    # Now, loop through fields and simulations.
    for field in fields_to_plot:
        linestyles = itertools.cycle(comparison(category).linestyles)
        markers = itertools.cycle(comparison(category).markers)
        norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.figure()
        plt.xlabel(r"Angular Frequency $\omega~[c/L]$")
        plt.ylabel("Power [arb. units]")

        for i, sim_name in enumerate(configs):
            ph = path_handler(sim_name, system_config)
            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            sigmaDict = rdu.getSimSigmas(params)
            vA = sigmaDict["vAoverC"]
            omegaA = 2*np.pi*vA
            if "omegaD" in params:
                omegaD = params["omegaD"]
            else:
                omegaD = omegaDvalues[i]*omegaA

            times_in_LC = retrieve_time_values(ph)[:, 2]
            time_interval = times_in_LC[1] - times_in_LC[0]

            profile_data = retrieve_profiles_temporal(ph, fields_to_profile=[field], average_2d=average_2d)
            if average_2d:
                profile = np.array(profile_data[field]["xAyAz0"])/norm_value[0]
            else:
                profile = np.array(profile_data[field]["x0y0z0"])/norm_value[0]

            smallest_ind = (np.abs(times_in_LC - smallest_end_time)).argmin()
            if match_times:
                times_in_LC = times_in_LC[:smallest_ind+1]
                profile = profile[:smallest_ind+1]

            # Now do FT stuff
            N = profile.size
            frequencies = fftfreq(N, time_interval)
            profile_FT = fft(profile)
            # profile_FT = fft(test_data)
            frequencies = 2*np.pi*fftshift(frequencies)
            profile_FT_abs_norm = 1.0/N * np.abs(fftshift(profile_FT))**2.0

            if max_scaling:
                max_value = np.max(profile_FT_abs_norm[N//2:])
                profile_FT_abs_norm = profile_FT_abs_norm/max_value

            plt.plot(frequencies[N//2:], profile_FT_abs_norm[N//2:], label=labels[i], marker=next(markers), ls=next(linestyles), markersize=2, color=colors[i] )

        if plot_vlines:
            plt.gca().axvline([omegaA], color="black", ls=":", label=r"$\omega_A=${:.2f} $c/L$".format(omegaA))
            plt.gca().axvline([omegaD], color="black", ls=":", label=r"$\omega_D=${:.2f} $c/L$".format(omegaD))
        plt.legend()
        if log_yscale:
            plt.yscale('log')
        plt.xscale('log')
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        figure_name = figdir + field + ".png"
        titstr = "\n FT of " + field_abbrev + norm_value[1] + " profile at "
        if average_2d:
            titstr += "$z=0$, averaged over $x$ and $y$"
        else:
            titstr += "$(x,y,z)=(0,0,0)$"
        titstr += "\n"
        if match_times:
            titstr += " with times matched up to ${:.2f}~L/c$".format(smallest_end_time)
        else:
            titstr += " over all available times (might not be same for all sims)"
        if max_scaling:
            titstr += ", normed to max vals"
        titstr += "\n" + category

        plt.title(titstr)
        plt.tight_layout()
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches="tight")
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
        plt.close()


if __name__ == '__main__':
    category = "im10-00-comparison"
    system_config = "lia_hp"
    stampede2 = False
    match_times = True
    average_2d = False

    # -------------------------------------------------
    kwargs = {}
    kwargs["system_config"] = system_config
    kwargs["match_times"] = match_times
    kwargs["average_2d"] = average_2d

    compare_plot_FT_profiles_temporal(category, **kwargs)
