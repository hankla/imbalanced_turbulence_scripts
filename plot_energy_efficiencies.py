import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plot_length_scales_over_time import plot_length_scales
import itertools
from scipy import optimize
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""
small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)

def plot_energy_efficiencies(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    fields_to_plot = ["internalEnergyDensity", "electromagneticEnergyDensity", "turbulentEnergyDensity", "netEnergyDensity"]
    overwrite_data = kwargs.get("overwrite_data", False)
    overwrite_Eheat = kwargs.get("overwrite_Eheat", False)
    kwargs_backup = kwargs.get("fields_to_vavg", None)
    fit_decay = kwargs.get("fit_decay", True)
    fit_start_time = kwargs.get("fit_start_time", 7.5)
    yscale = kwargs.get("yscale", False)

    if overwrite_Eheat and not overwrite_data:
        kwargs["fields_to_vavg"] = ["heatEnergyDensity", "netEnergyDensity", "turbulentEnergyDensity"]
        kwargs["overwrite_data"] = True
        retrieve_variables_vol_means_stds(ph, **kwargs)
        kwargs["overwrite_Eheat"] = False
        kwargs["overwrite_data"] = False
    kwargs["fields_to_vavg"] = fields_to_plot
    # colors =
    markers = itertools.cycle(["o", "s", "^", "d"])

    (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
    if kwargs_backup is not None:
        kwargs["fields_to_vavg"] = kwargs_backup
    times_in_LC = retrieve_time_values(ph)[:, 2]
    sim_name = ph.sim_name

    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    consts = rdu.Consts
    Lz = params["zmax"] - params["zmin"]
    Ly = params["ymax"] - params["ymin"]
    Lx = params["xmax"] - params["xmin"]
    volume = Lx*Ly*Lz
    B0 = params["B0"]
    norm_value = B0**2.0/(8.0*np.pi)

    # Load in injected energy
    if os.path.exists(ph.path_to_raw_data + "Einj.dat"):
        einj = np.loadtxt(ph.path_to_raw_data + "Einj.dat")
    elif os.path.exists(ph.path_to_reduced_data + "Einj.dat"):
        einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
    else:
        print("Einj.dat not available. Injected energy NOT plotted!")
        return

    fdump = int(params["FDUMP"])
    # Now make Einj the same size as times_in_LC.
    # Use the times_in_LC size since it's possible that einj was output but
    # the main .h5 files were not.
    # the -1 is because the first time step is skipped (einj=0)
    num_time_steps = times_in_LC.size - 1

    # reshape and sum
    # np.resize will repeat entries if need be--BAD! Use array.resize instead
    # because extra values will be filled with zero, which won't contribute to the sum
    einj.resize((num_time_steps, fdump))
    # initial injected energy is zero. must append?
    einj = np.sum(einj, axis=1)
    einj = np.insert(einj, 0, 0)
    # convert to energy density instead of energy
    einj = einj/volume
    einj = -einj

    einj_cum = np.cumsum(einj)

    print("Plotting energy efficiencies for " + ph.sim_name)
    # -------------------------------------
    # First plot
    # -------------------------------------
    # set up figure info
    figdir = ph.path_to_figures + "energy-partition/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "energy_evolution_minusT0"
    if yscale:
        figname += "_log"
    figname += ".png"
    # figname = figdir + "initial_delta_energy_evolution.png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    # Body of the plot.
    plt.figure()

    # ----------------------------------------------
    # Now plot
    for field in fields_to_plot:
        field_mean = vol_means[field]
        Delta_field_mean = field_mean - field_mean[0]
        field_abbrev = rdu.getFieldAbbrev(field, True)


        plt.plot(times_in_LC, Delta_field_mean/norm_value, markersize=4, marker=next(markers), label=r"$\Delta$" + field_abbrev)

    # plt.legend(frameon=False)
    plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    plt.gca().set_xlabel("$tc/L$")
    plt.gca().set_ylabel(r"$\Delta\mathcal{E}/(B_0^2/8\pi)$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    # plt.title(sim_name)
    plt.title(sim_name + "\n")
    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.title('')
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')

    plt.close()

    # -------------------------------------
    # Next plot
    # -------------------------------------
    # set up figure info
    # figname = figdir + "initial_delta_energy_efficiency_evolution.png"
    figname = figdir + "efficiency_evolution_minusT0"
    if fit_decay:
        figname += "_fit"
        figname += "{:d}".format(int(fit_start_time*10))
    if yscale:
        figname += "_log"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    # Body of the plot.
    plt.figure()

    # ----------------------------------------------
    # Now plot
    for field in fields_to_plot:
        field_mean = vol_means[field]
        Delta_field_mean = field_mean - field_mean[0]
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.plot(times_in_LC, Delta_field_mean/einj_cum, markersize=4, marker=next(markers), label=r"$\Delta$" + field_abbrev)

        # Fit to 1/t decay
        if fit_decay:
            if not field == "netEnergyDensity":
                # Cut values to start at fit_start_time
                cut_index = (np.abs(fit_start_time - times_in_LC)).argmin()
                cut_times = times_in_LC[cut_index:]
                cut_values = Delta_field_mean[cut_index:]/einj_cum[cut_index:]
                fine_cut_times = np.linspace(cut_times[0], cut_times[-1], 1000)
                fit_params, fit_e = optimize.curve_fit(inverse_time_decay, cut_times, cut_values, bounds=([-np.inf, 0], [np.inf, np.inf]))
                # print("Final value: {:.3f}".format(cut_values[-1]))
                fitted_values = inverse_time_decay(fine_cut_times, *fit_params)
                plt.plot(fine_cut_times, fitted_values, 'k--')

    plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    # plt.legend(frameon=False)
    plt.gca().set_xlabel("$tc/L$")
    plt.gca().set_ylabel(r"$\Delta\mathcal{E}/\mathcal{E}_{\rm inj}$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name + "\n")
    if yscale:
        plt.yscale('log')
        plt.ylim([1.0e-3, 1.0])
    else:
        plt.ylim([-1.0, 1.0])
    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.title('')
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()

    # --------------------------------------------------------
    # second plot: calculating the change in each time step.

    field_sum = None

    # set up figure info
    # figname = figdir + "timestep_delta_energy_evolution.png"
    figname = figdir + "energy_evolution_minusTimestep.png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    # Body of the plot.
    plt.figure()

    for field in fields_to_plot:
        field_mean = vol_means[field]
        delta_field = np.diff(field_mean)
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.plot(times_in_LC[1:], delta_field/norm_value, markersize=4, marker=next(markers), label=r"$\delta$" + field_abbrev)

        if field_sum is None:
            field_sum = delta_field
        else:
            field_sum = field_sum + delta_field

    # plt.plot(times_in_LC, field_sum/norm_value, markersize=4, marker=next(markers), label=r"$\sum\delta\mathcal{E}_i$")
    # plt.plot(times_in_LC, -einj/norm_value, markersize=4, marker=next(markers), label=r"$-\delta \mathcal{E}_{\rm inj}$", color='black', ls='--')

    # plt.legend(frameon=False)
    plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    plt.gca().set_xlabel("$t_f~c/L$")
    plt.gca().set_ylabel(r"$\delta\mathcal{E}/(B_0^2/8\pi)$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    # plt.title(sim_name)
    plt.title(sim_name + "\n")
    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        plt.title('')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()

    # print(np.allclose(field_sum, -einj))

    # -------------------------------------------------------
    # Now it's efficiencies time
    # -------------------------------------------------------
    # set up figure info
    # figname = figdir + "timestep_delta_energy_efficiency_evolution.png"
    figname = figdir + "efficiency_evolution_minusTimestep.png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    # Body of the plot.
    plt.figure()

    for field in fields_to_plot:
        field_mean = vol_means[field]
        delta_field = np.diff(field_mean)
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.plot(times_in_LC[1:], delta_field/einj[1:], markersize=4, marker=next(markers), label=r"$\delta$" + field_abbrev + r"$/\delta \mathcal{E}_{\rm inj}$")


    # plt.plot(times_in_LC, field_sum/norm_value, markersize=4, marker=next(markers), label=r"$\sum\delta\mathcal{E}_i$")
    # plt.plot(times_in_LC, -einj/norm_value, markersize=4, marker=next(markers), label=r"$-\delta \mathcal{E}_{\rm inj}$", color='black', ls='--')

    plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    # plt.legend(frameon=False)
    plt.gca().set_xlabel("$tc/L$")
    plt.gca().set_ylabel(r"$\delta\mathcal{E}/(-\delta\mathcal{E}_{\rm inj})$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    # plt.title(sim_name)
    plt.title(sim_name + "\n")
    plt.ylim([-10, 10])
    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.title('')
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()

    # print(np.allclose(field_sum, -einj))


def inverse_time_decay(time_values, amplitude, offset):
    return amplitude*1.0/time_values + offset

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"
    overwrite_data = False
    red_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
    fit_decay = True
    fit_start_time = 5.0

    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    sim_name = rdu.get_list_of_sim_names_with_phrases(red_dir, ["im10", "768cube", "dcorr029", "A075"])[0]
    # sim_name = rdu.get_list_of_sim_names_with_phrases(red_dir, ["im00", "384cube", "dcorr029", "A075", "8xyz"])[1]

    if stampede2:
        system_config = "stampede2"

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["overwrite_Eheat"] = False
    kwargs["fit_decay"] = fit_decay
    kwargs["fit_start_time"] = fit_start_time

    # --------------------
    ph = path_handler(sim_name, system_config)
    plot_energy_efficiencies(ph, **kwargs)
    plot_energy_efficiencies(ph, **kwargs, yscale=True)
