import numpy as np
# from compare_plot_profiles_spatial import compare_plot_profiles_spatial
# from compare_plot_profiles_temporal import compare_plot_profiles_temporal
from compare_plot_vol_avg_over_time import compare_plot_vol_avg_over_time
# from compare_plot_FT_vol_avg_over_time import compare_plot_FT_vol_avg_over_time
# from compare_plot_FT_profiles_temporal import compare_plot_FT_profiles_temporal
# from compare_plot_FT_profiles_spatial import compare_plot_FT_profiles_spatial
from compare_plot_initial_energy_partition import compare_plot_initial_energy_partition
from compare_plot_particle_energy_spectrum_same_time import compare_plot_particle_energy_spectrum_same_time
from compare_plot_scatterplot_volAvg import compare_plot_scatterplot_volAvg
import sys
sys.path.append("./modules/")
from compare_handler import *
from path_handler_class import path_handler
import raw_data_utils as rdu
from reduce_data_utils import *

#
category = "384cube-seed-comparison"
category = "768Imbalance-comparison"
category = "384im00seed-comparison"
category = "xiExtremesboxSizeAll-comparison"
# category = "xiAllboxSizeExtremes-comparison"
# category = "xiAllboxSizeAll-comparison"
# category = "384im10seed-comparison"
# category = "8xyzIm00Mf-comparison"
# category = "384seedstudy5-comparison"

system_config = "lia_hp"
stampede2 = False
match_times = False
to_show = False
reload_sims = False

do_error_bars = False
plot_final_Einj = False
cut_at_tequivalent = False
tinitial = 05.0 # L/c
tfinal = 14.0 # L/c

plot_scaling = True
plot_vol_avg = False
plot_spatial_profiles = False
plot_temporal_profiles = False
plot_spectra_same_time = False
plot_FT = False
plot_scatterplot = False
to_fit = False
error_bar_type = "statistical"
error_bar_type = "both"
# error_bar_type = None

# fields_to_vavg = ["deltaBrms2", "deltaBrms", "deltaBrms2vA", "Uprms", "velz_total", "VpBp_total", "deltaBrms", "internalEnergyDensity", "ExB_z", "turbulentEnergyDensity", "netEnergyDensity", "electromagneticEnergyDensity"]
fields_to_vavg = ["turbulentEnergyDensity", "magneticEnergyDensity", "internalEnergyDensity", "turbulentEnergyDensity", "netEnergyDensity", "electromagneticEnergyDensity", "internalEnergyDensity_efficiency", "turbulentEnergyDensity_efficiency", "netEnergyDensity_efficiency", "electromagneticEnergyDensity_efficiency", "cumInjectedEnergyDensity", "ExB_z", "cumInjectedEnergyDensity"]
# fields_to_vavg = ["magneticEnergyDensity"]

field_means_to_scale = ["electromagneticEnergyDensity", "magneticEnergyDensity", "electromagneticEnergyDensity", "turbulentEnergyDensity", "netEnergyDensity", "internalEnergyDensity_efficiency", "Upz_total", "ExB_z", "turbulentEnergyDensity_efficiency", "netEnergyDensity_efficiency", "electromagneticEnergyDensity_efficiency"]
# field_means_to_scale = ["internalEnergyDensity"] # "internalEnergyDensity_efficiency", "netEnergyDensity_efficiency", "turbulentEnergyDensity", "electromagneticEnergyDensity", "electricTotalRatio"]
field_means_to_scale = ["turbulentEnergyDensity", "magneticEnergyDensity", "relativisticVz2", "Upz_total", "netEnergyDensity_efficiency", "magneticEnergyDensity", "ExB_z", "electromagneticEnergyDensity", "turbulentEnergyDensity"]
# field_means_to_scale = field_means_to_scale[-1:]
# field_means_to_scale = field_means_to_scale + ["intInjSlopeRatio", "cumInjectedEnergyDensity"]
field_means_to_scale = field_means_to_scale[:2]

print_error_bars = ["Upz_total_mean", "internalEnergyDensity_slope", "turbulentEnergyDensity_mean", "electromagneticEnergyDensity_mean", "netEnergyDensity_efficiency_mean", "ExB_z_mean", "electromagneticEnergyDensity_efficiency_mean", "internalEnergyDensity_efficiency_mean"]

fields_to_linfit = get_fields_to_linear_fit()
fields_to_linfit = field_means_to_scale
# fields_to_linfit.append("internalEnergyDensity")
# fields_to_linfit.append("internalEnergyDensity_efficiency")

fields_to_profile = ["velz_total", "Ex", "By", "Bx", "Bz", "vely_total", "velx_total"]
times_to_profile = [30, 2100, 3330]
times_to_spectra = [] # [0, 10, 20, 30, 50, 65, 76]
# -------------------------------------------------
kwargs = {}
kwargs["system_config"] = system_config
kwargs["match_times"] = match_times
kwargs["system_config"] = system_config
kwargs["stampede2"] = stampede2
kwargs["reload_sims"] = reload_sims
kwargs["fields_to_vavg"] = fields_to_vavg
kwargs["fields_to_profile"] = fields_to_profile
kwargs["fields_to_linear_fit"] = fields_to_linfit
kwargs["times_to_profile"] = times_to_profile
kwargs["tinitial"] = tinitial
kwargs["tfinal"] = tfinal
# kwargs["ylims"] = [0.0, 1.0]
# kwargs["ylims"] = [-4.0, 4.0]
# kwargs["ylims"] = [-0.45, 0.45]
# kwargs["ylims"] = [0.0, 1.2]
kwargs["ylims"] = [0.0, 2.0]
# kwargs["ylims"] = [0.0, 0.3]
kwargs["error_bars"] = do_error_bars
kwargs["error_bar_type"] = error_bar_type
if to_show:
    kwargs["figure_name"] = "show"

# The nuclear option
kwargs["reload_all_sims"] = True

print(comparison(category).configs)
reduced_data_path = "/mnt/d/imbalanced_turbulence/data_reduced/"

# get minimum final Einj
if plot_final_Einj:
    stats = get_comparison_stats(category, **kwargs)
    final_einj_densities = stats.loc["einj_density_at_20_Lc"]
    final_Einj = final_einj_densities.min()
    kwargs["final_Einj"] = final_Einj
    kwargs["tequivalent"] = cut_at_tequivalent

if plot_scaling:
    kwargs["configs"] = comparison(category).configs

    xvariable = comparison(category).variable
    for field in field_means_to_scale:
        print("plotting " + field + " scaling with " + xvariable)
        plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs)
        plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, subtract_initial_value=True)
        # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, subtract_initial_value=False)
#
        if field in fields_to_linfit and to_fit:
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=True, subtract_initial_value=True)
            plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, predicted_fit=True, subtract_initial_value=False)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=True, predicted_fit=True, subtract_initial_value=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=True, subtract_initial_value=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=True, constant_fit=True, subtract_initial_value=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=True, quadratic_fit=True, subtract_initial_value=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=False, quadratic_fit=True, subtract_initial_value=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, linear_fit=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, quadratic_fit=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, subtract_initial_value=True, linear_fit=True)
            # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, subtract_initial_value=True, quadratic_fit=True)
            # # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_slope", **kwargs, subtract_initial_value=True, predicted_fit=True)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_slope", **kwargs, subtract_initial_value=True)

    # xvariable = "NX"
    # for field in field_means_to_scale:
        # print("plotting " + field + " scaling with " + xvariable)
        # plot_comparison_Avar_as_Bvar(category, xvariable, field + "_mean", **kwargs, subtract_initial_value=True)

    # plot_comparison_Avar_as_Bvar(category, xvariable, "vely_total_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "deltaBrms_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "deltaBrms2_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "deltaBrms2vA_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "VpBp_total_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "ExB_z_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "turbulentEnergyDensity_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "netEnergyDensity_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "electromagneticEnergyDensity_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_efficiency_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "turbulentEnergyDensity_efficiency_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "netEnergyDensity_efficiency_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "electromagneticEnergyDensity_efficiency_mean", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, xvariable, "einj_density_at_20_Lc", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, xvariable, "einj_density_at_20_Lc", **kwargs, error_bars=False)
    # # plot_comparison_Avar_as_Bvar(category, "internalEnergyDensity_slope", "velz_total_mean", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, "internalEnergyDensity_slope", "einj_density_at_20_Lc", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, "deltaBrms_mean", "velz_total_mean", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, "deltaBrms_mean", "velz_total_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, "NX", "velz_total_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, "NX", "internalEnergyDensity_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, "NX", "VpBp_total_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, "NX", "deltaBrms_mean", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, xvariable, "pelsaesserMinusEnergy_mean", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, xvariable, "pelsaesserPlusEnergy_mean", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, xvariable, "pelsaesserEnergyRatio", **kwargs)
    # # plot_comparison_Avar_as_Bvar(category, xvariable, "pelsaesserDifference_mean", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "turbulentEnergyDensity_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "electromagneticEnergyDensity_slope", **kwargs)
    # plot_comparison_Avar_as_Bvar(category, xvariable, "netEnergyDensity_slope", **kwargs)
#
    # if to_fit:
        # kwargs["quadratic_fit"] = True
        # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_slope", **kwargs)
        # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_mean", **kwargs)
        # kwargs["quadratic_fit"] = False
        # kwargs["linear_fit"] = True
        # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_mean", **kwargs)
#
        # kwargs["quadratic_fit"] = True
        # # plot_comparison_Avar_as_Bvar(category, xvariable, "velx_total_slope", **kwargs)
        # # plot_comparison_Avar_as_Bvar(category, xvariable, "vely_total_slope", **kwargs)
        # # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_slope", **kwargs)
        # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_mean", **kwargs)
        # # plot_comparison_Avar_as_Bvar(category, xvariable, "velx_total_intercept", **kwargs)
        # # plot_comparison_Avar_as_Bvar(category, xvariable, "vely_total_intercept", **kwargs)
        # # plot_comparison_Avar_as_Bvar(category, xvariable, "velz_total_intercept", **kwargs)
        # plot_comparison_Avar_as_Bvar(category, xvariable, "deltaBrms_mean", **kwargs)
        # plot_comparison_Avar_as_Bvar(category, xvariable, "VpBp_total_mean", **kwargs)
        # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_slope", **kwargs)
        # plot_comparison_Avar_as_Bvar(category, "deltaBrms_mean", "internalEnergyDensity_slope", **kwargs)
#
        # kwargs["predicted_fit"] = True
        # plot_comparison_Avar_as_Bvar(category, xvariable, "deltaBrms_mean", **kwargs)
        # kwargs["quadratic_fit"] = False
        # kwargs["linear_fit"] = False
        # plot_comparison_Avar_as_Bvar(category, xvariable, "internalEnergyDensity_slope", **kwargs)



if plot_vol_avg:
    kwargs["linear_fit"] = False
    compare_plot_vol_avg_over_time(category, **kwargs)
    compare_plot_vol_avg_over_time(category, log_yscale=True, **kwargs)
    if plot_FT:
        compare_plot_FT_vol_avg_over_time(category, **kwargs, log_yscale=False)
        compare_plot_FT_vol_avg_over_time(category, **kwargs, log_yscale=True)
        compare_plot_FT_vol_avg_over_time(category, **kwargs, log_yscale=True, only_before_decay=True)
        compare_plot_FT_vol_avg_over_time(category, **kwargs, log_yscale=True, only_after_decay=True)
        compare_plot_FT_vol_avg_over_time(category, **kwargs, log_yscale=True, max_scaling=True)

    kwargs["fields_to_vavg"] = fields_to_linfit
    kwargs["linear_fit"] = True
    compare_plot_vol_avg_over_time(category, **kwargs)
    kwargs["fields_to_vavg"] = fields_to_vavg

    compare_plot_initial_energy_partition(category, **kwargs)

if plot_scatterplot:
    compare_plot_scatterplot_volAvg(category, **kwargs)

if plot_spectra_same_time:
    for time in times_to_spectra:
        compare_plot_particle_energy_spectrum_same_time(category, **kwargs, time_to_plot=time, species="electrons")
        compare_plot_particle_energy_spectrum_same_time(category, **kwargs, time_to_plot=time, species="electrons", xlims=[10, 1e5], ylims=[1.0e-3, 1])
    if cut_at_tequivalent:
        compare_plot_particle_energy_spectrum_same_time(category, **kwargs, time_to_plot="teq", species="electrons")
        compare_plot_particle_energy_spectrum_same_time(category, **kwargs, time_to_plot="teq", species="electrons", xlims=[10, 1e5], ylims=[1.0e-3, 1])

if plot_spatial_profiles:
    # compare_plot_profiles_spatial(category, **kwargs)
    compare_plot_profiles_spatial(category, **kwargs, average_2d=True)
    # compare_plot_FT_profiles_spatial(category, **kwargs)
    compare_plot_FT_profiles_spatial(category, **kwargs, average_2d=True)

if plot_temporal_profiles:
    # compare_plot_profiles_temporal(category, **kwargs)
    compare_plot_profiles_temporal(category, **kwargs, average_2d=True)
    # compare_plot_profiles_temporal(category, **kwargs, max_scaling=True)
    # compare_plot_FT_profiles_temporal(category, **kwargs, log_yscale=True, plot_vlines=True)
    compare_plot_FT_profiles_temporal(category, **kwargs, log_yscale=True, plot_vlines=True, average_2d=True)
    # compare_plot_FT_profiles_temporal(category, **kwargs, log_yscale=True, plot_vlines=True, max_scaling=True)
    compare_plot_FT_profiles_temporal(category, **kwargs, log_yscale=True, plot_vlines=True, average_2d=True, max_scaling=True)
