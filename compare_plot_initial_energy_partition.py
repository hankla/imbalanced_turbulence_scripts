import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import scipy.fftpack as fftpack
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
import itertools
from compare_handler import comparison
import matplotlib.patheffects as mpe

"""
This script will plot the different energy efficiencies and the differences
in energy between each time step.

I believe this script is defunct after the implementation of energy efficiencies into
the volume average code. However, since it is in the compare_simulations.py script,
I'll leave it for now.
"""

def compare_plot_initial_energy_partition(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    ylog = kwargs.get("ylog", False)
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True

    sim_set = comparison(category)

    fields_to_plot = ["internalEnergyDensity", "netEnergyDensity", "turbulentEnergyDensity", "electromagneticEnergyDensity", "cumInjectedEnergyDensity"]
    kwargs["fields_to_vavg"] = fields_to_plot

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    figdir = ph0.path_to_figures + "../compare/" + category + "/energy-partition/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)

    for field in fields_to_plot:
        field_abbrev = rdu.getFieldAbbrev(field, True)
        labels = comparison(category).labels
        colors = comparison(category).colors
        linestyles = itertools.cycle(comparison(category).linestyles)
        markers = itertools.cycle(comparison(category).markers)
        figname = figdir + field + "_initial_delta_energy_evolution"
        if t_limited:
            figname += t_save_str
        if ylog:
            figname += "_log"
        figname += ".png"
        print(figname)
        figure_name = figname

        if figure_name == "show":
            matplotlib.use("TkAgg")
        plt.figure()
        plt.xlabel('$tc/L$')
        plt.ylabel(r'$\Delta$' + field_abbrev + r'$/(B_0^2/8\pi)$')

        for i, sim in enumerate(configs):
            ph = path_handler(sim, system_config)
            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            B0 = params["B0"]
            mag_energy_density = B0**2/(8*np.pi)
            Lx = params["xmax"] - params["xmin"]
            Ly = params["ymax"] - params["ymin"]
            Lz = params["zmax"] - params["zmin"]
            volume = Lz*Ly*Lz
            norm_value = B0**2.0/(8.0*np.pi)

            times_in_LC = retrieve_time_values(ph)[:, 2]
            (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
            field_mean = vol_means[field]
            if field_mean is None:
                continue
            Delta_field_mean = field_mean - field_mean[0]
            # temporary fix since changed sign of einj
            if field == "cumInjectedEnergyDensity":
                if (Delta_field_mean <= 0).any():
                    Delta_field_mean = np.abs(Delta_field_mean)

            plt.plot(times_in_LC, Delta_field_mean/norm_value, ls=next(linestyles), marker=next(markers), label=labels[i], color=colors[i], markevery=int(times_in_LC.size/10), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])

        if t_limited:
            plt.xlim([tinitial, tfinal])
        if ylog:
            plt.yscale('log')
            plt.ylim([1.0e-1, 1])
        if sim_set.cbar_label is not None:
            colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
            colorbar1.set_label(sim_set.cbar_label)
            colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
        legend_elements = sim_set.legend_elements
        if not legend_elements:
            plt.legend(frameon=False, ncol=sim_set.ncol)
        else:
            plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)
        titstr = category
        plt.title(titstr + "\n")
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()

        if figure_name == "show":
            plt.show()
        else:
            print("Saving figure " + figure_name)
            plt.savefig(figure_name, bbox_inches='tight')
            root_name = figure_name.split("/")[-1]
            if not os.path.isdir(figdir + "pdfs/"):
                os.makedirs(figdir + "pdfs/")
            plt.title('')
            plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

            plt.gca().grid(False, which='both')
            if not os.path.isdir(figdir + "pdfs/nogrids/"):
                os.makedirs(figdir + "pdfs/nogrids/")
            plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
            if not os.path.isdir(figdir + "nogrids/"):
                os.makedirs(figdir + "nogrids/")
            plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
        plt.close()

    # -------------------------------------
    # Next plot
    # -------------------------------------

    for field in fields_to_plot:
        field_abbrev = rdu.getFieldAbbrev(field, True)
        labels = comparison(category).labels
        colors = comparison(category).colors
        linestyles = itertools.cycle(comparison(category).linestyles)
        markers = itertools.cycle(comparison(category).markers)
        figname = figdir + field + "_initial_delta_energy_efficiency_evolution"
        if t_limited:
            figname += t_save_str
        if ylog:
            figname += "_log"
        figname += ".png"
        print("Saving: " + figname)
        figure_name = figname

        if figure_name == "show":
            matplotlib.use("TkAgg")
        plt.figure()
        plt.xlabel('$tc/L$')
        ylabel = r'$\Delta$' + field_abbrev + r'$/\mathcal{E}_{\rm inj}$'
        if field == "netEnergyDensity":
            ylabel = r"$5\times $" + ylabel
        plt.ylabel(ylabel)

        for i, sim in enumerate(configs):
            ph = path_handler(sim, system_config)
            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            B0 = params["B0"]
            Lx = params["xmax"] - params["xmin"]
            Ly = params["ymax"] - params["ymin"]
            Lz = params["zmax"] - params["zmin"]
            volume = Lz*Ly*Lz
            norm_value = B0**2.0/(8.0*np.pi)

            times_in_LC = retrieve_time_values(ph)[:, 2]
            (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
            field_mean = vol_means[field]
            if field == "cumInjectedEnergyDensity":
                field_mean = -1.0*field_mean
            if field_mean is None:
                continue

            Delta_field_mean = field_mean - field_mean[0]
            einj_cum = vol_means["cumInjectedEnergyDensity"]
            # temporary fix since changed sign of einj
            if (einj_cum <= 0).any():
                einj_cum = np.abs(1.0*einj_cum)
            if field == "cumInjectedEnergyDensity":
                if (Delta_field_mean <= 0).any():
                    Delta_field_mean = np.abs(Delta_field_mean)

            if field == "netEnergyDensity":
                Delta_field_mean = Delta_field_mean * 5.0
            plt.plot(times_in_LC, Delta_field_mean/einj_cum, ls=next(linestyles), marker=next(markers), label=labels[i], color=colors[i], markevery=int(times_in_LC.size/10), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()] )

        if t_limited:
            plt.xlim([tinitial, tfinal])
        if ylog:
            plt.yscale('log')
            plt.ylim([1.0e-1, 1])
        else:
            plt.ylim([-0.01, 1])
        if sim_set.cbar_label is not None:
            colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
            colorbar1.set_label(sim_set.cbar_label)
            colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
        legend_elements = sim_set.legend_elements
        if not legend_elements:
            plt.legend(frameon=False, ncol=sim_set.ncol)
        else:
            plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)
        titstr = category
        plt.title(titstr)
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()

        if figure_name == "show":
            plt.show()
        else:
            print("Saving figure " + figure_name)
            plt.savefig(figure_name, bbox_inches='tight')
            plt.title('')
            root_name = figure_name.split("/")[-1]
            if not os.path.isdir(figdir + "pdfs/"):
                os.makedirs(figdir + "pdfs/")
            plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

            plt.gca().grid(False, which='both')
            if not os.path.isdir(figdir + "pdfs/nogrids/"):
                os.makedirs(figdir + "pdfs/nogrids/")
            plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
            if not os.path.isdir(figdir + "nogrids/"):
                os.makedirs(figdir + "nogrids/")
            plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
        plt.close()


    # Now plot the sum of net and internal energy efficiencies
    labels = comparison(category).labels
    colors = comparison(category).colors
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)
    figname = figdir + "sum_net-internal_efficiency_evolution"
    if t_limited:
        figname += t_save_str
    if ylog:
        figname += "_log"
    figname += ".png"
    print("Saving: " + figname)
    figure_name = figname

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel('$tc/L$')
    plt.ylabel(r'$(\Delta\mathcal{E}_{\rm int}+\Delta\mathcal{E}_{\rm net})/\mathcal{E}_{\rm inj}$')

    for i, sim in enumerate(configs):
        ph = path_handler(sim, system_config)
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        B0 = params["B0"]
        Lx = params["xmax"] - params["xmin"]
        Ly = params["ymax"] - params["ymin"]
        Lz = params["zmax"] - params["zmin"]
        volume = Lz*Ly*Lz
        norm_value = B0**2.0/(8.0*np.pi)

        times_in_LC = retrieve_time_values(ph)[:, 2]
        kwargs["fields_to_vavg"] = ["netEnergyDensity", "internalEnergyDensity", "cumInjectedEnergyDensity"]
        field_means = retrieve_variables_vol_means_stds(ph, **kwargs)[0]
        netEnergy = field_means["netEnergyDensity"]
        intEnergy = field_means["internalEnergyDensity"]
        einj_cum = field_means["cumInjectedEnergyDensity"]

        Delta_net = netEnergy - netEnergy[0]
        Delta_int = intEnergy - intEnergy[0]
        # temporary fix since changed sign of einj
        if (einj_cum <= 0).any():
            einj_cum = np.abs(1.0*einj_cum)

        summed_efficiency = (Delta_net + Delta_int)/einj_cum
        plt.plot(times_in_LC, summed_efficiency, ls=next(linestyles), marker=next(markers), label=labels[i], color=colors[i], markevery=int(times_in_LC.size/10), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()] )

    if t_limited:
        plt.xlim([tinitial, tfinal])
    if ylog:
        plt.yscale('log')
        plt.ylim([1.0e-1, 1])
    else:
        plt.ylim([0, 1])
    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False, ncol=sim_set.ncol)
    else:
        plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)
    titstr = category
    plt.title(titstr + "\n")
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()

if __name__ == '__main__':
    # category = "xiAllboxSizeAll-comparison"
    category = "768Imbalance-comparison"
    # category = "xiAllboxSizeExtremes-comparison"

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    # kwargs["figure_name"] = "show"
    kwargs["tinitial"] = 0.0
    kwargs["tfinal"] = 20.0

    compare_plot_initial_energy_partition(category, **kwargs)
    compare_plot_initial_energy_partition(category, **kwargs, ylog=True)
