import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from scipy.fftpack import fft, fftfreq, fftshift
from matplotlib.ticker import FixedLocator

"""
This script will plot the fourier transform of temporal profiles at the location (0, 0, 0)
of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - extend to other locations
"""

def plot_FT_volume_averages_over_time(ph, **kwargs):
    # ------------------------------------------
    fields_to_plot = kwargs.get("fields_to_vavg", ["velz_total", "deltaBrms", "VpBp_total"])
    sim_name = ph.sim_name
    overwrite = kwargs.get("overwrite", False)
    to_show = kwargs.get("to_show", False)
    log_yscale = kwargs.get("log_yscale", False)
    detrend = kwargs.get("detrend", False)
    only_after_decay = kwargs.get("only_after_decay", False)
    only_before_decay = kwargs.get("only_before_decay", False)
    ind_times_to_FT = kwargs.get("ind_times_to_FT", None) # a tuple with starting/ending indices.
    # ------------------------------------------
    if to_show:
        matplotlib.use('TkAgg')
    figdir = ph.path_to_figures + "volAvg-over-time/FT/"
    if detrend:
        figdir += "detrended/"
    if log_yscale:
        figdir += "log_yscale/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    os.chdir(ph.path_to_reduced_data)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    # Get times to transform.
    tit_add = ""
    time_steps = retrieve_time_values(ph)[:, 0]
    if ind_times_to_FT is None:
        if "NDECAY" not in params:
            start_ind = 0
            end_ind = -1
            start_time_step = 0
            end_time_step = int(time_steps[-1])
            tit_add = " (entire duration)"
        elif only_after_decay:
            NDECAY = params["NDECAY"]
            start_ind = (np.abs(time_steps - NDECAY)).argmin()
            end_ind = -1
            start_time_step = int(NDECAY)
            end_time_step = int(time_steps[-1])
            tit_add = " (only after decay)"
        elif only_before_decay:
            start_ind = 0
            NDECAY = params["NDECAY"]
            end_ind = (np.abs(time_steps - NDECAY)).argmin()
            start_time_step = 0
            end_time_step = int(NDECAY)
            tit_add = " (only before decay)"
        else:
            start_ind = 0
            start_time_step = 0
            end_time_step = int(time_steps[-1])
            end_ind = -1
            tit_add = " (entire duration)"
            # tit_add = " between $t=${:.2f} and {:.2f} $L/c$".format(times_in_LC[start_ind], times_in_LC[end_ind])
    else:
        start_ind = ind_times_to_FT[0]
        end_ind = ind_times_to_FT[1]
    times_in_LC = times_in_LC[start_ind:end_ind]

    consts = rdu.Consts
    sigmaDict = rdu.getSimSigmas(params)
    vA = sigmaDict["vAoverC"]
    omegaA = 2*np.pi*vA
    if "omegaD" in params:
        omegaD = params["omegaD"]
    else:
        omegaD = omegaA*kwargs.get("omegaD", 0.6/np.sqrt(3.0))
    minorlocator = [omegaA, omegaD]
    # minorloclab = [r"$\omega_A$", r"$\omega_D$"]
    minorloclab = ["", ""]

    print("Plotting " + sim_name + " Fourier transform of volume average over time")
    vol_means = retrieve_variables_vol_means_stds(ph, **kwargs)[0]
    time_interval = times_in_LC[1] - times_in_LC[0]

    for field in fields_to_plot:
        titstr = sim_name
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        profile = np.array(vol_means[field])/norm_value[0]
        profile = profile[start_ind:end_ind]
        figname = figdir + field + "_FT-in-time{:d}-{:d}_vol-avg.png".format(start_time_step, end_time_step)

        if detrend:
            # Now, detrend the profile data by fitting to a line:
            linear_model = np.polyfit(times_in_LC, profile, 1)
            linear_model_vals = np.poly1d(linear_model)(times_in_LC)
            titstr += "\n Linear increase slope: {:.2e}".format(linear_model[0])
            profile = profile - linear_model_vals

        N = profile.size
        sample_frequencies = fftfreq(N, time_interval)
        profile_FT = fft(profile)
        profile_power = np.abs(profile_FT)**2
        frequencies = 2*np.pi*fftshift(sample_frequencies)[N//2:]
        profile_FT_abs_norm = 1.0/N * (np.abs(fftshift(profile_FT))**2.0)[N//2:]

        if omegaA > frequencies[-1]:
            print("Warning: FDUMP was too large to capture the Alfven frequency.")
        from scipy.signal import argrelextrema
        maxima_inds = argrelextrema(profile_FT_abs_norm, np.greater)
        max_freq_1 = frequencies[maxima_inds[0][0]]
        max_freq_2 = frequencies[maxima_inds[0][1]]

        plt.figure()
        plt.ylabel("Power [arb. units]")
        plt.xlabel(r"Angular Frequency $\omega~[c/L]$")
        if detrend:
            frequencies = frequencies[1:]
            profile_FT_abs_norm = profile_FT_abs_norm[1:]
        plt.plot(frequencies, profile_FT_abs_norm, marker="o", lw=2, markersize=2)
        if log_yscale:
            plt.yscale('log')
        plt.xscale('log')
        titstr += "\n FT of "
        if detrend:
            titstr += "detrended "
        titstr += " volume average\n" + r"$\langle$" + field_abbrev + norm_value[1] + r"$\rangle$"
        titstr += " between times {:.2f} and {:.2f} $L/c$".format(times_in_LC[0], times_in_LC[-1])
        titstr += tit_add
        plt.title(titstr)
        # Add labels
        ax = plt.gca()
        ax.axvline([omegaA], color="black", ls=":", label=r"$\omega_A=${:.2f} $c/L$".format(omegaA))
        ax.axvline([omegaD], color="black", ls="-.", label=r"$\omega_D=${:.2f} $c/L$".format(omegaD))
        ax.axvline([2.0*omegaD], color="tab:orange", ls="--", label=r"$2\omega_D=${:.2f} $c/L$".format(omegaD*2.0))
        ax.axvline([2.0*omegaA], color="tab:green", ls="--", label=r"$2\omega_A=${:.2f} $c/L$".format(omegaA*2.0))
        ax.axvline([omegaA-omegaD], color="tab:purple", ls="--", label=r"$\omega_A-\omega_D=${:.2f} $c/L$".format(omegaA-omegaD))
        ax.axvline([omegaA+omegaD], color="tab:pink", ls="--", label=r"$\omega_A+\omega_D=${:.2f} $c/L$".format(omegaA+omegaD))
        # ax.axvline(max_freq_1, color="black", ls=":")
        # ax.axvline(max_freq_2, color="black", ls=":")
        ax.tick_params('x', which='minor', labeltop=True, labelbottom=False, top=True)
        plt.legend()

        # minorlocator.append(max_freq_1)
        # minorlocator.append(max_freq_2)
        # minorloclab.append("${:.2f}$".format(max_freq_1))
        # minorloclab.append("${:.2f}$".format(max_freq_2))

        ax.xaxis.set_minor_locator(FixedLocator(minorlocator))
        ax.set_xticklabels(minorloclab, minor=True)
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')

        sorted_inds = np.argsort(profile_FT_abs_norm)
        highest_freqs = frequencies[sorted_inds]

        plt.tight_layout()

        print(figname)
        if to_show:
            plt.show()
        else:
            print("Saving figure: " + figname)
            plt.savefig(figname, bbox_inches="tight")
            plt.close()




if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"

    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30-decay2500"
    omegaD = 0.6/np.sqrt(3.0)

    # kwargs
    overwrite = False
    to_show = False

    # NOTE: <Ex>, <By> etc don't mean anything! Only vz does.
    fields_to_plot = ["velz_total", "velx_total", "vely_total", "deltaBrms", "VpBp_total"]

    if stampede2:
        system_config = "stampede2"
    ph = path_handler(sim_name, system_config)

    kwargs = {}
    kwargs["fields_to_vavg"] = fields_to_plot
    kwargs["to_show"] = to_show
    kwargs["overwrite"] = overwrite
    # kwargs["only_after_decay"] = True
    # kwargs["ind_times_to_FT"] = (10, 100)
    if omegaD is not None: kwargs["omegaD"] = omegaD
    plot_FT_volume_averages_over_time(ph, **kwargs, log_yscale=True)
    plot_FT_volume_averages_over_time(ph, **kwargs, log_yscale=True, only_after_decay=True)
    plot_FT_volume_averages_over_time(ph, **kwargs, log_yscale=True, only_before_decay=True)
    # plot_FT_volume_averages_over_time(ph, **kwargs, log_yscale=False)
    # plot_FT_volume_averages_over_time(ph, **kwargs, log_yscale=True, detrend=True)
