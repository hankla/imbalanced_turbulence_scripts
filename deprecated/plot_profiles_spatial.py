import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *

"""
This script will plot profiles of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
The particular slices to be taken can be specified with ix, iy, iz.

TO DO:
    - retrieve data instead of reading entire slices
"""

def plot_profiles_spatial(ph, times_to_plot, **kwargs):
    # ------------------------------------------
    fields_to_plot = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    sim_name = ph.sim_name
    overwrite = kwargs.get("overwrite_data", False)
    ix = kwargs.get("ix", 0)
    iy = kwargs.get("iy", 0)
    iz = kwargs.get("iz", 0)
    to_show = kwargs.get("to_show", False)
    to_fit = kwargs.get("to_fit", False)
    to_output = kwargs.get("to_output", False)
    average_2d = kwargs.get("average_2d", False)
    # ------------------------------------------
    if to_show:
        matplotlib.use('TkAgg')

    os.chdir(ph.path_to_reduced_data)
    figpath = ph.path_to_figures + "profiles-1d_spatial/"
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    consts = rdu.Consts
    rho = consts.c/params["omegac"]

    print("Plotting " + sim_name + " profiles")

    profile_data = retrieve_profiles_spatial(ph, times_to_plot, **kwargs)
    coords = retrieve_coords(ph)
    profiles = {}
    x_axis = {}

    for time in times_to_plot:
        values_to_text = [time]
        for field in fields_to_plot:
            norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
            field_abbrev = rdu.getFieldAbbrev(field, True)
            if average_2d:
                profiles["z"] = profile_data[field]["{:d}".format(int(time))]["xAyA"]
                profiles["y"] = profile_data[field]["{:d}".format(int(time))]["xAzA"]
                profiles["x"] = profile_data[field]["{:d}".format(int(time))]["yAzA"]
            else:
                profiles["z"] = profile_data[field]["{:d}".format(int(time))]["x0y0"]
                profiles["y"] = profile_data[field]["{:d}".format(int(time))]["x0z0"]
                profiles["x"] = profile_data[field]["{:d}".format(int(time))]["y0z0"]

            for i, dim in enumerate(["x", "y", "z"]):
                x_axis["z"] = coords[2]
                x_axis["y"] = coords[1]
                x_axis["x"] = coords[0]

                prof_mean = np.mean(profiles[dim]/norm_value[0])
                prof_std = np.std(profiles[dim]/norm_value[0])

                if profiles[dim].size != len(x_axis[dim]):
                    if len(x_axis[dim]) == params["N" + dim.capitalize()]:
                        x_axis[dim] = coords[i][1:]
                # Figure paths
                figdir = figpath + dim + "-profile/" + field + "/"
                if to_fit and dim == "z":
                    figdir += "fits/"
                if average_2d:
                    figdir += "2d-averaged/"
                    if dim == "x": profstr = "yAzA"
                    elif dim == "y": profstr = "xAzA"
                    elif dim == "z": profstr = "xAyA"
                else:
                    if dim == "x": profstr = "iy{}iz{}".format(iy, iz)
                    elif dim == "y": profstr = "ix{}iz{}".format(ix, iz)
                    elif dim == "z": profstr = "ix{}iy{}".format(ix, iy)
                if not os.path.isdir(figdir):
                    os.makedirs(figdir)
                figname = figdir + field + "_t{}".format(int(time)) + "_" + profstr + ".png"

                titstr = sim_name + "\n" + field + " " + dim + "-profile"
                if average_2d:
                    titstr += ", 2d averaged"
                else:
                    if dim == "x": titstr += " at y={}, z={}".format(iy, iz)
                    elif dim == "y": titstr += " at x={}, z={}".format(ix, iz)
                    elif dim == "z": titstr += " at x={}, y={}".format(ix, iy)
                titstr += ", t={}".format(time)
                titstr += "\n Mean: {:.2e}. Std: {:.2e}".format(prof_mean, prof_std)

                plt.figure()
                plt.plot(x_axis[dim]/rho, profiles[dim]/norm_value[0], markersize=2, marker='o')

                if to_fit and dim=="z":
                    from scipy.optimize import curve_fit
                    def sin_func(x, a, phi, c):
                        return a*np.sin(x*2*np.pi/coords[2][-1]+phi)+c
                    bounds = ([0, 0, -np.inf], [np.inf, 2.0*np.pi, np.inf])
                    fit_params, cov = curve_fit(f=sin_func, xdata=x_axis[dim]/rho, ydata=profiles[dim]/norm_value[0], bounds=bounds)
                    titstr += "\n Fit parameters: amp={:.2f}, phase={:.2f}, offset={:.2f}".format(*fit_params)
                    plt.plot(x_axis[dim]/rho, sin_func(x_axis[dim]/rho, *fit_params), 'k--', label="Fit")
                    if field == "Ex" or field == "By" or field == "vely_total":
                        values_to_text.append(fit_params[0])
                        values_to_text.append(fit_params[1])

                plt.title(titstr)
                plt.xlabel("$" + dim + r"/\rho$") # units
                ylabel = field_abbrev + norm_value[1]
                plt.ylabel(ylabel)
                plt.gca().grid(color='.9', ls='--')
                plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                plt.tight_layout()

                if not to_show:
                    print("Saving figure: " + figname)
                    plt.savefig(figname, bbox_inches="tight")
                    plt.close()

            if to_show:
                plt.show()

        if to_fit and to_output:
            # output values to txt file. First load, check for repeated values.
            values_to_text = np.reshape(values_to_text, (1, 7))
            output_values_path = ph.path_to_reduced_data + "Ex-By-vy_profile_fits.txt"
            header = "Time step, Ex amp, Ex phase, By amp, By phase, vy amp, vy phase"
            if os.path.exists(output_values_path):
                output_values = np.loadtxt(output_values_path, skiprows=1, delimiter=",")
                if output_values.ndim == 1:
                    output_values = np.reshape(output_values, (1, 7))
                if (output_values[:, 0] == time).any():
                    if overwrite:
                        print("Replacing repeated value at time {}".format(time))
                        repeat_ind = np.where(output_values[:, 0] == time)[0][0]
                        output_values[repeat_ind, :] = values_to_text
                else:
                    output_values = np.vstack((output_values, values_to_text))
            else:
                output_values = values_to_text
                np.savetxt(output_values_path, output_values, header=header, delimiter=",")
            np.savetxt(output_values_path, output_values, header=header, delimiter=",")

if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    sim_name = "test_data"
    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"

    # kwargs
    ix = 0; iy = 0; iz = 0
    overwrite = False
    to_show = False
    to_fit = False
    average_2d = True

    zVars = rdu.Zvariables.options
    fields_to_plot = ["By", "vely_total"]
    # fields_to_plot = ["pelsaesserMinusEnergy", "pelsaesserPlusEnergy", "elsaesserMinusy", "elsaesserPlusy", "elsaesserMinusx", "elsaesserPlusx", "velx_total", "pelsaesserDifference", "pelsaesserEnergy"]
    # fields_to_plot = fields_to_plot[:2]
    # fields_to_plot = ["velx_electrons", "velx_ions"]
    # fields_to_plot = zVars

    # ------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    time_steps = retrieve_time_values(ph)[:, 0]
    times_to_plot = time_steps[:10]
    times_to_plot = time_steps
    kwargs = {}
    kwargs["fields_to_profile"] = fields_to_plot
    kwargs["to_show"] = to_show
    kwargs["overwrite"] = overwrite
    kwargs["ix"] = ix
    kwargs["iy"] = iy
    kwargs["iz"] = iz
    kwargs["to_fit"] = False
    kwargs["average_2d"] = average_2d
    plot_profiles_spatial(ph, times_to_plot, **kwargs)
