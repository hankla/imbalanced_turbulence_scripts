import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from scipy.fft import fft, fftfreq, fftshift, ifft, rfft, irfft, rfftfreq
from matplotlib.ticker import FixedLocator

"""
This script will plot the fourier transform of temporal profiles at the location (0, 0, 0)
of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - extend to other locations
"""

def find_nearest(arr, element):
    return np.argmin(np.abs(arr - element))

def plot_vz_vol_avg_FT_pred(ph, **kwargs):
    # ------------------------------------------
    sim_name = ph.sim_name
    overwrite = kwargs.get("overwrite", False)
    to_show = kwargs.get("to_show", False)
    nmodes = int(kwargs.get("nmodes_to_show", 6))
    plot_predicted = kwargs.get("plot_predicted", True)
    if plot_predicted: nmodes = 4
    # ------------------------------------------
    fields_to_plot = ["velz_total"]
    if to_show:
        matplotlib.use('TkAgg')
    figdir = ph.path_to_figures + "volAvg-over-time/FT/vz-pred/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    os.chdir(ph.path_to_reduced_data)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    consts = rdu.Consts
    sigmaDict = rdu.getSimSigmas(params)
    vA = sigmaDict["vAoverC"]
    omegaA = 2*np.pi*vA
    omegaD = kwargs.get("omegaD", omegaA*0.6/np.sqrt(3.0))

    print("Plotting " + sim_name + " Fourier transform of volume average over time")

    vol_means = retrieve_variables_vol_means_stds(ph, **kwargs)[0]
    time_interval = times_in_LC[1] - times_in_LC[0]

    for field in fields_to_plot:
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        profile = np.array(vol_means[field])/norm_value[0]
        figname = figdir + field + "_FT-in-time_vol-avg_modes-"
        if plot_predicted:
            figname += "predicted"
        else:
            figname += "{:d}".format(nmodes)
            figname += ".png"

        N = profile.size
        sample_frequencies = rfftfreq(N, time_interval)
        profile_FT = rfft(profile)
        profile_power = np.abs(profile_FT)**2.0
        # Create a simple FT that only has a few of the modes
        simple_FT = np.zeros(profile_FT.shape, dtype=complex)

        if plot_predicted:
            inds = []
            inds.append(find_nearest(2*np.pi*sample_frequencies, 0))
            inds.append(find_nearest(2*np.pi*sample_frequencies, omegaA*2.0))
            inds.append(find_nearest(2*np.pi*sample_frequencies, omegaD*2.0))
            inds.append(find_nearest(2*np.pi*sample_frequencies, omegaA - omegaD))
            inds.append(find_nearest(2*np.pi*sample_frequencies, omegaA + omegaD))
            inds_to_plot = np.array(inds)
            label = "Four predicted modes"
        else:
            # just take sorted in terms of power
            # argsort default is to take the max REAL portion,
            # so need to sort the power instead of the complex FT
            # also, sort from largest to smallest
            sorted_inds = np.argsort(profile_power)[::-1]
            inds_to_plot = sorted_inds[:nmodes+1]
            label = "{:d} modes with greatest power".format(nmodes+1)

        # print(inds_to_plot)
        # print(2*np.pi*sample_frequencies[inds_to_plot])

        simple_FT[inds_to_plot] = profile_FT[inds_to_plot]
        plt.figure()
        plt.xlabel(r"$tc/L$")
        ylabel = r"$\langle$" + field_abbrev + norm_value[1] + r"$\rangle$"
        plt.ylabel(ylabel)
        plt.plot(times_in_LC, profile, label="Data", lw=2)
        simple_profile = irfft(simple_FT, n=profile.size)
        plt.plot(times_in_LC, simple_profile, ls="--", label=label, lw=2, marker="s")
        plt.legend()
        plt.title(sim_name + "\n Temporal profile taken at (x,y,z)=(0,0,0)\n" + field_abbrev + norm_value[1])
        plt.legend()
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()

        if not to_show:
            print("Saving figure: " + figname)
            plt.savefig(figname, bbox_inches="tight")

        plt.figure()
        figdir = ph.path_to_figures + "spectrum_vol-avg_over_time/vz_FT-pred/extrapolated/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)
        figname = figdir + field + "_FT-in-time_vol-avg_modes-"
        if plot_predicted:
            figname += "predicted"
        else:
            figname += "{:d}".format(nmodes)
        figname += sim_name + ".png"
        plt.xlabel(r"$tc/L$")
        ylabel = r"$\langle$" + field_abbrev + norm_value[1] + r"$\rangle$"
        plt.ylabel(ylabel)

        plt.plot(times_in_LC, profile, label="Data")
        # plt.plot(times_in_LC, simple_profile, label="Inverse FFT", ls=linestyles[0])
        times_extrapolated = np.arange(times_in_LC[0], 2.0*times_in_LC[-1], time_interval)
        offset = np.abs(profile_FT[inds_to_plot[0]])/N
        added_sines = np.zeros(times_extrapolated.shape)
        added_sines += offset

        for nm in np.arange(1, nmodes+1):
            frequency = sample_frequencies[inds_to_plot[nm]]
            magnitude = 2.0*np.abs(profile_FT[inds_to_plot[nm]])/N
            angle = np.angle(profile_FT[inds_to_plot[nm]])
            sine_to_add =  magnitude*np.cos(2*np.pi*frequency * times_extrapolated + angle)
            added_sines += sine_to_add

        if plot_predicted:
            label = "Reconstructed with predicted modes"
        else:
            label = "Reconstructed with {:d} modes".format(nmodes)

        plt.plot(times_extrapolated, added_sines, label = label, ls="--")
        plt.title(sim_name + "\n Temporal profile taken at (x,y,z)=(0,0,0)\n" + field_abbrev + norm_value[1])
        plt.legend()
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()


        if to_show:
            plt.show()
        else:
            print("Saving figure: " + figname)
            plt.savefig(figname, bbox_inches="tight")




if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False

    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05omega025A025_s0-PPC32-Fd50"

    # kwargs
    omegaD = 1.79
    omegaD = None
    overwrite = False
    to_show = False
    plot_predicted = True
    nmodes = 4
    # nmodes_start = 0
    # nmodes_end = 8

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    kwargs = {}
    kwargs["to_show"] = to_show
    kwargs["overwrite"] = overwrite
    kwargs["nmodes_to_show"] = nmodes
    kwargs["plot_predicted"] = plot_predicted
    if omegaD is not None: kwargs["omegaD"] = omegaD

    plot_vz_vol_avg_FT_pred(ph, **kwargs)

    # for nm in np.arange(nmodes_start, nmodes_end):
        # kwargs["nmodes_to_show"] = nm
        # plot_vz_vol_avg_FT_pred(ph, **kwargs)
