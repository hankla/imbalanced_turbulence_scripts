import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
This script will plot the spatial profilesof every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

"""

overwrite = False
stampede2 = False
system_config = "lia_hp"
# system_config = "jila_laptop"

sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
dim = "z"
ix = 0; iy = 0; iz = 0

if stampede2:
    ph = path_handler(sim_name, "stampede2")
else:
    ph = path_handler(sim_name, system_config)

figbase = ph.path_to_figures + "profiles-1d_spatial/compare_vx_species/" + dim + "-profile/"
if not os.path.isdir(figbase):
    os.makedirs(figbase)
fields_to_plot = ["velx_electrons", "velx_ions", "velx_total"]
time_steps = retrieve_time_values(ph)[:, 0]
times_to_plot = time_steps[:]

colors = ["tab:green", "tab:orange", "tab:blue"]
linestyles = ["--", ":", "-"]
markers = ["s", "x", "o"]

coords = retrieve_coords(ph)
x_axes = {}
x_axes["z"] = coords[2]
x_axes["y"] = coords[1]
x_axes["x"] = coords[0]
x_axis = np.array(x_axes[dim])
if dim == "x": profstr = "iy{}iz{}".format(iy,iz)
elif dim == "y": profstr = "ix{}iz{}".format(ix, iz)
elif dim == "z": profstr = "ix{}iy{}".format(ix, iy)
(params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
consts = rdu.Consts
rho = consts.c/params["omegac"]

profile_data = retrieve_profiles_spatial(ph, times_to_plot, fields_to_profile=fields_to_plot)
vxe_norm_value = rdu.getNormValue("velx_electrons", simDir=ph.path_to_reduced_data)
vxe_field_abbrev = rdu.getFieldAbbrev("velx_electrons", True)
vxi_norm_value = rdu.getNormValue("velx_ions", simDir=ph.path_to_reduced_data)
vxi_field_abbrev = rdu.getFieldAbbrev("velx_ions", True)
vxtot_norm_value = rdu.getNormValue("velx_total", simDir=ph.path_to_reduced_data)
vxtot_field_abbrev = rdu.getFieldAbbrev("velx_total", True)

for time in times_to_plot:
    plt.figure()
    plt.xlabel("$" + dim + r"/\rho$") # units

    vxe_profile = profile_data["velx_electrons"]["{:d}".format(int(time))][profstr.replace("i", '')]
    vxi_profile = profile_data["velx_ions"]["{:d}".format(int(time))][profstr.replace("i", '')]
    vxtot_profile = profile_data["velx_total"]["{:d}".format(int(time))][profstr.replace("i", '')]
    if vxe_profile.size != x_axis.size:
        if x_axis.size == params["N" + dim.capitalize()]:
            x_axis_vel = x_axis[1:]

    plt.plot(x_axis_vel/rho, vxe_profile/vxe_norm_value[0], label=vxe_field_abbrev+vxe_norm_value[1], marker=markers[0], ls=linestyles[0], markersize=4, markevery=int(vxe_profile.size/10), color=colors[0] )
    plt.plot(x_axis_vel/rho, vxi_profile/vxi_norm_value[0], label=vxi_field_abbrev+vxi_norm_value[1], marker=markers[1], ls=linestyles[1], markersize=4, markevery=int(vxi_profile.size/10), color=colors[1] )
    plt.plot(x_axis_vel/rho, vxtot_profile/vxtot_norm_value[0], label=vxtot_field_abbrev+vxtot_norm_value[1], marker=markers[2], ls=linestyles[2], markersize=4, markevery=int(vxtot_profile.size/10), color=colors[2] )

    plt.legend()
    plt.gca().grid(color='.9', ls='--')
    figname = figbase + "vx-species_comparison_t{}".format(int(time)) + "_" + profstr + ".png"
    titstr = sim_name + "\n" + dim + "-profile at "
    if dim == "x": titstr += "y={}, z={}".format(iy, iz)
    elif dim == "y": titstr += "x={}, z={}".format(ix, iz)
    elif dim == "z": titstr += "x={}, y={}".format(ix, iy)
    titstr += ", t={}".format(time)
    plt.title(titstr)
    plt.tight_layout()
    print(figname)
    plt.savefig(figname, bbox_inches="tight")
    plt.close()

