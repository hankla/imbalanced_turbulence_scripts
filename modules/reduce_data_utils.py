import numpy as np
import os
import csv
import raw_data_utils as rdu
from path_handler_class import *
from scipy import interpolate
import datetime
import pickle
import h5py


def reduce_time_values(path_handler):
    """
    Get a list of times straight from the raw data.
    Must have access to the raw data to do this.
    Saves a list of time steps (as in the integer of the output), the time values in seconds, and the time values in light-crossing times.
    """
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
        return None
    else:
        datapath = path_handler.path_to_raw_data
        fieldpath = datapath + "densities"

    import fnmatch
    time_steps = []

    # uses the magnetic field files to grab times
    files = fnmatch.filter(os.listdir(fieldpath), 'Ue_ions_bg_*.h5')
    for file in files:
        time_steps.append(int(file.split('_')[3].split(".")[0]))
    time_steps.sort()
    # time_steps = [str(ti) for ti in time_steps]

    # -----------------------------------
    # Now extract simulation time in seconds
    # and in light-crossing times.
    consts = rdu.Consts
    (params, units) = rdu.getSimParams(simDir=datapath)
    tLc = (params["zmax"] - params["zmin"])/consts.c

    time_in_sec = []
    time_in_lc = []

    os.chdir(datapath)

    for time in time_steps:
        fileName = rdu.getFieldFilename("Ue_ions_bg_%i" % time)
        ts = rdu.readArrayFromHdf5(fileName, "time")
        time_in_sec.append(ts)
        time_in_lc.append(ts/tLc)
    # -----------------------------------
    # Now dump to txt file.
    times = np.transpose(np.array([time_steps, time_in_sec, time_in_lc]))
    header = "time step, time in s, time in light-crossing Lz/c"
    git_commit = path_handler.get_current_commit()
    header += "\n" + path_handler.sim_name + "\nGit commit: " + git_commit
    header += "\nSaved at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("Saving times to " + path_handler.times_path)
    np.savetxt(path_handler.times_path, times, delimiter=",", header=header)

    return


def retrieve_time_values(path_handler, overwrite=False):
    """
    This function does not need access to the raw data as long as
    reduce_time_value's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_time_values.
    """
    if overwrite or not os.path.exists(path_handler.times_path):
        reduce_time_values(path_handler)
    time_values = np.loadtxt(path_handler.times_path, delimiter=",", skiprows=4)
    return time_values

def reduce_coords(path_handler):
    """
    Get a list of coordinates straight from the raw data.
    Must have access to the raw data to do this.
    Saves a list of coordinates in an array, i.e.
    xvals = coords[0], yvals = coords[1], zvals = coords[2]
    """
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce coords: path to raw data doesn't exist!!")
        return None
    else:
        datapath = path_handler.path_to_raw_data
        os.chdir(datapath)
        fieldpath = datapath + "fields"

    import fnmatch

    # uses the magnetic field files to grab a sample file
    files = fnmatch.filter(os.listdir(fieldpath), 'Bx_*.h5')
    filename = files[0].strip('.h5')
    coords = rdu.getField(filename)[3]

    # -----------------------------------
    # Now dump to txt file.
    git_commit = path_handler.get_current_commit()
    header = "values of x, y, z coordinates"
    header += "\n" + path_handler.sim_name + "\nGit commit: " + git_commit
    header += "\nSaved at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    np.savetxt(path_handler.coords_path, coords, delimiter=",", header=header)
    return


def retrieve_coords(path_handler, overwrite=False):
    """
    This function does not need access to the raw data as long as
    reduce_coords's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_coords.
    """
    if overwrite or not os.path.exists(path_handler.coords_path):
        reduce_coords(path_handler)
    coord_values = np.loadtxt(path_handler.coords_path, delimiter=",", skiprows=4)
    return coord_values


def reduce_variables_vol_means_stds(path_handler, **kwargs):
    """
    For the variables output by Zeltron, calculate
        - volume averages over time
        - volume standard deviations over time
    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_output_variables_vol_avg instead.

    KWARGS:
        - overwrite: if True, will overwrite the data.
             Useful for re-calculating variables.
        - vars_to_calc: default is all. Can set to specific
             variable with overwrite to shorten re-compute time.
             should be an array with variable names.
    """
    overwrite = kwargs.get("overwrite_data", False)
    output_variables = kwargs.get("fields_to_vavg", rdu.Zvariables.options)

    if len(output_variables) == 1 and output_variables[0] == "cumInjectedEnergyDensity":
        print("Reducing cumulative injected energy density")
    elif len(output_variables) == 1 and "efficiency" in output_variables[0]:
        print("Reducing " + output_variables[0])
    elif not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce vol means: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
        return None
    else:
        datapath = path_handler.path_to_raw_data
        os.chdir(datapath)


    # already sorted, but double make sure
    time_steps = np.sort(retrieve_time_values(path_handler)[:, 0])

    consts = rdu.Consts
    (params, units) = rdu.getSimParams(simDir=path_handler.path_to_reduced_data)

    # load from file
    if not os.path.exists(path_handler.output_vars_vol_means_path):
        vol_means = {}
        vol_stds = {}
    else:
        vol_means = pickle.load(open(path_handler.output_vars_vol_means_path, "rb"))
        vol_stds = pickle.load(open(path_handler.output_vars_vol_stds_path, "rb"))

    for field in output_variables:
        if field not in rdu.Zvariables.options and field.replace("_efficiency", "") not in rdu.Zvariables.options:
            print(field + " is not a calculable quantity. Skipping...")
        elif overwrite or field not in vol_means or field not in vol_stds:
            print("Reducing " + path_handler.sim_name + " volume averages for " + field + " over time steps {:d} to {:d}".format(int(time_steps[0]), int(time_steps[-1])))
            vol_means[field] = []
            vol_stds[field] = []

            if field == "cumInjectedEnergyDensity":
                # Load in injected energy
                if os.path.exists(path_handler.path_to_raw_data + "Einj.dat"):
                    einj = np.loadtxt(path_handler.path_to_raw_data + "Einj.dat")
                elif os.path.exists(path_handler.path_to_reduced_data + "Einj.dat"):
                    einj = np.loadtxt(path_handler.path_to_reduced_data + "Einj.dat")
                else:
                    print("Einj.dat not available. Injected energy NOT plotted!")
                    vol_means[field] = None
                    vol_stds[field] = None
                times_in_LC = retrieve_time_values(path_handler)[:, 2]
                fdump = int(params["FDUMP"])
                Lx = params["xmax"] - params["xmin"]
                Ly = params["ymax"] - params["ymin"]
                Lz = params["zmax"] - params["zmin"]
                volume = Lz*Ly*Lz
                # Now make Einj the same size as times_in_LC.
                # Use the times_in_LC size since it's possible that einj was output but
                # the main .h5 files were not.
                # the -1 is because the first time step is skipped (einj=0)
                num_time_steps = times_in_LC.size - 1

                # reshape and sum
                # np.resize will repeat entries if need be--BAD! Use array.resize instead
                # because extra values will be filled with zero, which won't contribute to the sum
                einj.resize((num_time_steps, fdump))
                # initial injected energy is zero. must append?
                einj = np.sum(einj, axis=1)
                einj = np.insert(einj, 0, 0)
                # convert to energy density instead of energy
                einj_density = einj/volume
                einj_density_cum = np.cumsum(einj_density)

                vol_means[field] = -1.0*einj_density_cum
                vol_stds[field] = None
            elif field == "relativisticVz1":
                if "netEnergyDensity" not in vol_means:
                    print("Error: get net energy density first.")
                elif "internalEnergyDensity" not in vol_means:
                    print("Error: get internal energy density first.")
                else:
                    vol_means[field] = np.sqrt(2.0*vol_means["netEnergyDensity"]/vol_means["internalEnergyDensity"])
                    vol_stds[field] = None
            elif field == "relativisticVz2":
                if "Upz_total" not in vol_means:
                    print("Error: get Upz_total first.")
                    print(path_handler.sim_name)
                    vol_means[field] = None
                elif "internalEnergyDensity" not in vol_means:
                    print("Error: get internal energy density first.")
                    print(path_handler.sim_name)
                    vol_means[field] = None
                else:
                    vol_means[field] = vol_means["Upz_total"]/vol_means["internalEnergyDensity"]*rdu.Consts.c
                    vol_stds[field] = None
            elif "efficiency" in field:
                field_name = field.replace("_efficiency", "")
                (vol_means, vol_stds) = retrieve_variables_vol_means_stds(path_handler, fields_to_vavg=[field_name, "cumInjectedEnergyDensity"])
                field_mean = vol_means[field_name]
                einj_cum_density = vol_means["cumInjectedEnergyDensity"]
                if field_mean is None:
                    vol_means[field] = None
                    vol_stds[field] = None
                else:
                    Delta_field_mean = field_mean - field_mean[0]
                    vol_means[field] = Delta_field_mean/einj_cum_density
                    vol_stds[field] = None

            else:
                for time in time_steps:
                    if field == "heatEnergyDensity":
                        vol_avg = rdu.get_heatEnergyDensity(time)
                        vol_std = None
                    elif field == "netEnergyDensity":
                        if "heatEnergyDensity" not in vol_means:
                            heatEnergyDensity = rdu.get_heatEnergyDensity(time)
                        else:
                            times = retrieve_time_values(path_handler)
                            time_index = (np.abs(times[:, 0] - time)).argmin()
                            # time_index = np.where(times == time)[0][0]
                            heatEnergyDensity = vol_means["heatEnergyDensity"][time_index]
                        if "fluidEnergyDensity" not in vol_means:
                            fluidEnergyDensity = rdu.getExtField("fluidEnergyDensity", time)[0]
                            fluidEnergyDensity_mean = np.nanmean(fluidEnergyDensity)
                        else:
                            times = retrieve_time_values(path_handler)
                            time_index = (np.abs(times[:, 0] - time)).argmin()
                            # time_index = np.where(times == time)[0][0]
                            fluidEnergyDensity_mean = vol_means["fluidEnergyDensity"][time_index]

                        vol_avg = fluidEnergyDensity_mean - heatEnergyDensity
                        vol_std = None

                    elif field == "turbulentEnergyDensity":
                        print(time)
                        if "heatEnergyDensity" not in vol_means:
                            print("Retrieving heat energy density")
                            heatEnergyDensity = rdu.get_heatEnergyDensity(time)
                        else:
                            print("Retrieving heat energy density from vol means")
                            times = retrieve_time_values(path_handler)
                            # time_index = np.where(times == time)[0][0]
                            time_index = (np.abs(times[:, 0] - time)).argmin()
                            heatEnergyDensity = vol_means["heatEnergyDensity"][time_index]
                        if "internalEnergyDensity" not in vol_means:
                            print("Retrieving internal energy density")
                            internalEnergyDensity = rdu.getExtField("internalEnergyDensity", time)[0]
                            internalEnergyDensity_mean = np.nanmean(internalEnergyDensity)
                        else:
                            print("Retrieving internal energy density from vol means")
                            times = retrieve_time_values(path_handler)
                            # time_index = np.where(times == time)[0][0]
                            time_index = (np.abs(times[:, 0] - time)).argmin()
                            print(time_index)
                            internalEnergyDensity_mean = vol_means["internalEnergyDensity"][time_index]
                        vol_avg = heatEnergyDensity - internalEnergyDensity_mean
                        vol_std = None
                    elif field == "magneticEnergyDensity":
                        if "Brms2" not in vol_means:
                            magneticEnergyDensity = rdu.getExtField("magneticEnergyDensity", time)[0]
                            vol_avg = np.nanmean(magneticEnergyDensity)
                            vol_std = np.nanstd(magneticEnergyDensity)
                        else:
                            times = retrieve_time_values(path_handler)
                            # time_index = np.where(times == time)[0][0]
                            time_index = (np.abs(times[:, 0] - time)).argmin()
                            Brms2_mean = vol_means["Brms2"][time_index]
                            vol_avg = Brms2_mean/(8.0*np.pi)
                            vol_std = vol_stds["Brms2"][time_index]/(8.0*np.pi)
                    elif field == "electricEnergyDensity":
                        if "Erms2" not in vol_means:
                            electricEnergyDensity = rdu.getExtField("electricEnergyDensity", time)[0]
                            vol_avg = np.nanmean(electricEnergyDensity)
                            vol_std = np.nanstd(electricEnergyDensity)
                        else:
                            times = retrieve_time_values(path_handler)
                            # time_index = np.where(times == time)[0][0]
                            time_index = (np.abs(times[:, 0] - time)).argmin()
                            Erms2_mean = vol_means["Erms2"][time_index]
                            vol_avg = Erms2_mean/(8.0*np.pi)
                            vol_std = vol_stds["Erms2"][time_index]/(8.0*np.pi)

                    else:
                        print(field)
                        print(time)
                        fielddata = rdu.getExtField(field, time)[0]
                        # Volume average is same as mean
                        vol_avg = np.nanmean(fielddata)
                        vol_std = np.nanstd(fielddata)
                    vol_means[field].append(vol_avg)
                    vol_stds[field].append(vol_std)
            if vol_means[field] is not None:
                vol_means[field] = np.asarray(vol_means[field])
            if vol_stds[field] is not None:
                vol_stds[field] = np.asarray(vol_stds[field])
            # dump after every variable in case gets cut off
            pickle.dump(vol_means, open(path_handler.output_vars_vol_means_path, "wb"))
            pickle.dump(vol_stds, open(path_handler.output_vars_vol_stds_path, "wb"))
    return

def retrieve_variables_vol_means_stds(path_handler, **kwargs):
    """
    This function does not need access to the raw data as long as
    reduce_output_variables_vol_means_stds's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_output_variables_vol_means_stds.

    OUTPUTS:
       - vol_means: a dictionary with "field" as the keys
            e.g. vol_means["Bx"]
       - vol_stds: a dictionary with "field" as the keys
            e.g. vol_stds["Bx"]
    """
    overwrite = kwargs.get("overwrite_data", False)
    selected_fields = kwargs.get("fields_to_vavg", rdu.Zvariables.options)

    if overwrite or not os.path.exists(path_handler.output_vars_vol_means_path):
        reduce_variables_vol_means_stds(path_handler, **kwargs)

    vol_means = pickle.load(open(path_handler.output_vars_vol_means_path, "rb"))
    vol_stds = pickle.load(open(path_handler.output_vars_vol_stds_path, "rb"))

    fields_to_none = []
    for field in selected_fields:
        if field not in vol_means:
            print(field + " not in file.")
            print(path_handler.sim_name)
            if field == "cumInjectedEnergyDensity":
                fields_to_vavg_bak = kwargs.get("fields_to_vavg", None)
                kwargs["fields_to_vavg"] = ["cumInjectedEnergyDensity"]
                reduce_variables_vol_means_stds(path_handler, **kwargs)
                if fields_to_vavg_bak is not None:
                    kwargs["fields_to_vavg"] = fields_to_vavg_bak
                vol_means = pickle.load(open(path_handler.output_vars_vol_means_path, "rb"))
                vol_stds = pickle.load(open(path_handler.output_vars_vol_stds_path, "rb"))
            elif "efficiency" in field:
                fields_to_vavg_bak = kwargs.get("fields_to_vavg", None)
                kwargs["fields_to_vavg"] = [field]
                reduce_variables_vol_means_stds(path_handler, **kwargs)
                if fields_to_vavg_bak is not None:
                    kwargs["fields_to_vavg"] = fields_to_vavg_bak
                vol_means = pickle.load(open(path_handler.output_vars_vol_means_path, "rb"))
                vol_stds = pickle.load(open(path_handler.output_vars_vol_stds_path, "rb"))
            elif field == "relativisticVz1":
                if "netEnergyDensity" not in vol_means:
                    print("Error: get net energy density first.")
                elif "internalEnergyDensity" not in vol_means:
                    print("Error: get internal energy density first.")
                else:
                    vol_means[field] = np.sqrt(2.0*vol_means["netEnergyDensity"]/vol_means["internalEnergyDensity"])
                    vol_stds[field] = None
            elif field == "relativisticVz2":
                if "Upz_total" not in vol_means:
                    print("Error: get Upz_total first.")
                    print(path_handler.sim_name)
                    vol_means[field] = None
                elif "internalEnergyDensity" not in vol_means:
                    print("Error: get internal energy density first.")
                    print(path_handler.sim_name)
                    vol_means[field] = None
                else:
                    vol_means[field] = vol_means["Upz_total"]/vol_means["internalEnergyDensity"]*rdu.Consts.c
                vol_stds[field] = None

            # Only reduce if path exists
            elif os.path.exists(path_handler.path_to_raw_data):
                print("Reducing...")
                reduce_variables_vol_means_stds(path_handler, **kwargs)
                vol_means = pickle.load(open(path_handler.output_vars_vol_means_path, "rb"))
                vol_stds = pickle.load(open(path_handler.output_vars_vol_stds_path, "rb"))
            else:
                print("Path to raw data does not exist. Setting " + field + " values to None")
                fields_to_none.append(field)

    for field in fields_to_none:
        vol_means[field] = None
        vol_stds[field] = None
    return (vol_means, vol_stds)

def reduce_profiles_temporal_average(path_handler, **kwargs):
    """
    Calculate temporal profiles averaged over two spatial dimensions for
    selected Zeltron output fields.
    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_profiles_temporal_average instead.

    KWARGS:
        - overwrite: if True, will overwrite the data.
             Useful for re-calculating variables.
        - selected_fields: default is all. Can set to specific
             variable with overwrite to shorten re-compute time.
             should be an array with variable names.
        - dim: default is z.
    OUTPUTS:
       - temporal_profiles: a dictionary with "field" as the keys.
            Each field is also a dictionary of locations.
            e.g. the profile over time of Bx at location (0,0,0)
            is: temporal_profiles["Bx"]["x0y0z0"]

    TO DO:
        - support profiles not at index = 0
    """
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce temporal profiles: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
    else:
        datapath = path_handler.path_to_raw_data

    overwrite = kwargs.get("overwrite", False)
    profile_variables = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    average_2d = kwargs.get("average_2d", True)
    if kwargs is None:
        kwargs = {}
        kwargs["average_2d"] = True
    time_steps = retrieve_time_values(path_handler)[:, 0]

    # load from file
    if not os.path.exists(path_handler.temporal_profiles_avg_path):
        temporal_profiles = {}
    else:
        temporal_profiles = pickle.load(open(path_handler.temporal_profiles_avg_path, "rb"))

    os.chdir(datapath)
    for field in profile_variables:
        if field not in rdu.Zvariables.options:
            print(field + " is not a calculable quantity. Skipping...")
        elif overwrite or field not in temporal_profiles:
            print("Reducing " + path_handler.sim_name + " temporal profiles for " + field)
            temporal_profiles[field] = {}

        for dims in ["x0yAzA", "xAy0zA", "xAyAz0"]:
            dim_dict = {"x0yAzA":"yAzA", "xAy0zA":"xAzA", "xAyAz0":"xAyA"}
            if dims not in temporal_profiles[field]:
                temporal_profiles[field][dims] = []

                for time in time_steps:
                    time = int(time)
                    spatial_profiles = retrieve_profiles_spatial(path_handler, [time], fields_to_profile=[field], average_2d=True)

                    x0y0z0_value = spatial_profiles[field]["{:d}".format(int(time))][dim_dict[dims]][0]
                    temporal_profiles[field][dims].append(x0y0z0_value)

                # dump after every variable in case gets cut off
                pickle.dump(temporal_profiles, open(path_handler.temporal_profiles_avg_path, "wb"))
            # else: # do nothing because profile is already there!
    return


def reduce_profiles_temporal(path_handler, **kwargs):
    """
    Calculate temporal profiles at (0,0,0) for
    selected Zeltron output fields.
    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_profiles_temporal instead.

    KWARGS:
        - overwrite: if True, will overwrite the data.
             Useful for re-calculating variables.
        - selected_fields: default is all. Can set to specific
             variable with overwrite to shorten re-compute time.
             should be an array with variable names.
    OUTPUTS:
       - temporal_profiles: a dictionary with "field" as the keys.
            Each field is also a dictionary of locations.
            e.g. the profile over time of Bx at location (0,0,0)
            is: temporal_profiles["Bx"]["x0y0z0"]

    TO DO:
        - support profiles not at index = 0
    """
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce temporal profiles: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
    else:
        datapath = path_handler.path_to_raw_data

    overwrite = kwargs.get("overwrite", False)
    profile_variables = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    ix = 0; iy = 0; iz = 0
    time_steps = retrieve_time_values(path_handler)[:, 0]

    # load from file
    if not os.path.exists(path_handler.temporal_profiles_path):
        temporal_profiles = {}
    else:
        temporal_profiles = pickle.load(open(path_handler.temporal_profiles_path, "rb"))

    os.chdir(datapath)
    for field in profile_variables:
        if field not in rdu.Zvariables.options:
            print(field + " is not a calculable quantity. Skipping...")
        elif overwrite or field not in temporal_profiles:
            print("Reducing " + path_handler.sim_name + " temporal profiles for " + field)
            temporal_profiles[field] = {}

        if "x0y0z0" not in temporal_profiles[field]:
            temporal_profiles[field]["x0y0z0"] = []

            for time in time_steps:
                time = int(time)
                spatial_profiles = retrieve_profiles_spatial(path_handler, [time], fields_to_profile=[field])
                x0y0z0_value = spatial_profiles[field]["{:d}".format(int(time))]["x0y0"][0]
                temporal_profiles[field]["x0y0z0"].append(x0y0z0_value)


            # dump after every variable in case gets cut off
            pickle.dump(temporal_profiles, open(path_handler.temporal_profiles_path, "wb"))
        # else: # do nothing because profile is already there!
    return

def reduce_profiles_spatial_average(path_handler, time_steps, **kwargs):
    """
    For the variables output by Zeltron, calculate
    spatial profiles averaged over two dimensions.
    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_profiles_temporal instead.

    KWARGS:
        - overwrite: if True, will overwrite the data.
             Useful for re-calculating variables.
        - selected_fields: default is all. Can set to specific
             variable with overwrite to shorten re-compute time.
             should be an array with variable names.
    OUTPUTS:
       - spatial_profiles: a dictionary with "field" as the keys.
            Each field is also a dictionary of times, and each
            time is a dictionary of profiles. e.g. the z-profile
            taken at y=0, x=0 of Bx at timestep 100 is:
            spatial_profiles["Bx"][100]["y0x0"]. the y-profile
            taken at z=5 [cells], x=0 of Ey at timestep 100 is:
            spatial_profiles["Ey"][100]["x0z5"]
    """
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce profiles spatial: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
        return None
    else:
        datapath = path_handler.path_to_raw_data

    overwrite = kwargs.get("overwrite_data", False)
    profile_variables = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    ix = kwargs.get("ix", 0)
    iy = kwargs.get("iy", 0)
    iz = kwargs.get("iz", 0)

    # load from file
    if not os.path.exists(path_handler.spatial_profiles_avg_path):
        spatial_profiles = {}
    else:
        spatial_profiles = pickle.load(open(path_handler.spatial_profiles_avg_path, "rb"))

    os.chdir(datapath)
    for field in profile_variables:
        if field not in rdu.Zvariables.options and field not in rdu.Zvariables.restFrame_options:
            print(field + " is not a calculable quantity. Skipping...")
            break
        elif overwrite or field not in spatial_profiles:
            times_to_calc = time_steps
            spatial_profiles[field] = {}
        else:
            times_to_calc = []
            for time in time_steps:
                time = "{:d}".format(int(time))
                if time not in spatial_profiles[field]:
                    times_to_calc.append(time)
                elif "xAyA" not in spatial_profiles[field][time]:
                    times_to_calc.append(time)
                elif "yAzA" not in spatial_profiles[field][time]:
                    times_to_calc.append(time)
                elif "xAzA" not in spatial_profiles[field][time]:
                    times_to_calc.append(time)
        # print(times_to_calc)
        # if len(times_to_calc) > 0:
            # print("Reducing " + path_handler.sim_name + " averaged spatial profiles for " + field + " at timestep {}".format(times_to_calc[0]))
        for time in times_to_calc:
            time = "{:d}".format(int(time))
            spatial_profiles[field][time] = {}
            (fielddata, fieldTime, step, coords) = rdu.getExtField(field, int(time))
            zprof = np.mean(fielddata, axis=(0, 1))
            yprof = np.mean(fielddata, axis=(0, 2))
            xprof = np.mean(fielddata, axis=(1, 2))

            spatial_profiles[field][time]["xAyA"] = zprof
            spatial_profiles[field][time]["xAzA"] = yprof
            spatial_profiles[field][time]["yAzA"] = xprof
            # dump after every time in case gets cut off
            pickle.dump(spatial_profiles, open(path_handler.spatial_profiles_avg_path, "wb"))
    return


def reduce_profiles_spatial(path_handler, time_steps, **kwargs):
    """
    For the variables output by Zeltron, calculate
    spatial profiles.
    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_profiles_temporal instead.

    KWARGS:
        - overwrite: if True, will overwrite the data.
             Useful for re-calculating variables.
        - selected_fields: default is all. Can set to specific
             variable with overwrite to shorten re-compute time.
             should be an array with variable names.
    OUTPUTS:
       - spatial_profiles: a dictionary with "field" as the keys.
            Each field is also a dictionary of times, and each
            time is a dictionary of profiles. e.g. the z-profile
            taken at y=0, x=0 of Bx at timestep 100 is:
            spatial_profiles["Bx"][100]["y0x0"]. the y-profile
            taken at z=5 [cells], x=0 of Ey at timestep 100 is:
            spatial_profiles["Ey"][100]["x0z5"]
    """
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce profiles spatial: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
        return None
    else:
        datapath = path_handler.path_to_raw_data

    overwrite = kwargs.get("overwrite_data", False)
    profile_variables = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    ix = kwargs.get("ix", 0)
    iy = kwargs.get("iy", 0)
    iz = kwargs.get("iz", 0)

    # load from file
    if not os.path.exists(path_handler.spatial_profiles_path):
        spatial_profiles = {}
    else:
        spatial_profiles = pickle.load(open(path_handler.spatial_profiles_path, "rb"))

    os.chdir(datapath)
    for field in profile_variables:
        if field not in rdu.Zvariables.options and field not in rdu.Zvariables.restFrame_options:
            print(field + " is not a calculable quantity. Skipping...")
            break
        elif overwrite or field not in spatial_profiles:
            times_to_calc = time_steps
            spatial_profiles[field] = {}
        else:
            times_to_calc = []
            for time in time_steps:
                time = "{:d}".format(int(time))
                if time not in spatial_profiles[field]:
                    times_to_calc.append(time)
                elif "x0y0" not in spatial_profiles[field][time]:
                    times_to_calc.append(time)
                elif "y0z0" not in spatial_profiles[field][time]:
                    times_to_calc.append(time)
                elif "x0z0" not in spatial_profiles[field][time]:
                    times_to_calc.append(time)
        # print(times_to_calc)
        if len(times_to_calc) > 0:
            print("Reducing " + path_handler.sim_name + " spatial profiles for " + field + " at timestep {}".format(times_to_calc[0]))
        for time in times_to_calc:
            time = "{:d}".format(int(time))
            spatial_profiles[field][time] = {}
            (fielddata, fieldTime, step, coords) = rdu.getExtField(field, int(time))
            zprof = fielddata[ix, iy, :]
            yprof = fielddata[ix, :, iz]
            xprof = fielddata[:, iy, iz]

            spatial_profiles[field][time]["x0y0"] = zprof
            spatial_profiles[field][time]["x0z0"] = yprof
            spatial_profiles[field][time]["y0z0"] = xprof
            # dump after every time in case gets cut off
            pickle.dump(spatial_profiles, open(path_handler.spatial_profiles_path, "wb"))
    return


def retrieve_profiles_temporal(path_handler, **kwargs):
    """
    This function does not need access to the raw data as long as
    reduce_profiles_temporal's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_profiles_temporal.

    OUTPUTS:
       - temporal_profiles: a dictionary with "field" as the keys.
            Each field is also a dictionary of locations.
            e.g. the profile over time of Bx at location (0,0,0)
            is: temporal_profiles["Bx"]["x0y0z0"]

    TO DO:
        - support profiles not at index = 0
    """
    overwrite = kwargs.get("overwrite", False)
    selected_fields = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    average_2d = kwargs.get("average_2d", False)

    if average_2d:
        temporal_profiles_path = path_handler.temporal_profiles_avg_path
    else:
        temporal_profiles_path = path_handler.temporal_profiles_path

    if overwrite or not os.path.exists(temporal_profiles_path):
        if average_2d:
            reduce_profiles_temporal_average(path_handler, **kwargs)
        else:
            reduce_profiles_temporal(path_handler, **kwargs)

    temporal_profiles = pickle.load(open(temporal_profiles_path, "rb"))
    for field in selected_fields:
        if field not in temporal_profiles:
            if average_2d:
                print(field + " not in temporal avg profile file. Reducing...")
                reduce_profiles_temporal_average(path_handler, **kwargs)
                break
            else:
                print(field + " not in temporal profile file. Reducing...")
                reduce_profiles_temporal(path_handler, **kwargs)
                break

    temporal_profiles = pickle.load(open(temporal_profiles_path, "rb"))

    return temporal_profiles



def retrieve_profiles_spatial(path_handler, time_steps, **kwargs):
    """
    This function does not need access to the raw data as long as
    reduce_profiles_spatial's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_profiles_spatial.

    OUTPUTS:
       - spatial_profiles: a dictionary with "field" as the keys.
            Each field is also a dictionary of times, and each
            time is a dictionary of profiles. e.g. the z-profile
            taken at y=0, x=0 of Bx at timestep 100 is:
            spatial_profiles["Bx"][100]["y0x0"]. the y-profile
            taken at z=5 [cells], x=0 of Ey at timestep 100 is:
            spatial_profiles["Ey"][100]["x0z5"].

    TO DO:
        - support profiles not at index = 0
    """
    overwrite = kwargs.get("overwrite_data", False)
    selected_fields = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    average_2d = kwargs.get("average_2d", False)

    if average_2d:
        spatial_profiles_path = path_handler.spatial_profiles_avg_path
    else:
        spatial_profiles_path = path_handler.spatial_profiles_path

    if overwrite or not os.path.exists(spatial_profiles_path):
        if average_2d:
            reduce_profiles_spatial_average(path_handler, time_steps, **kwargs)
        else:
            reduce_profiles_spatial(path_handler, time_steps, **kwargs)

    spatial_profiles = pickle.load(open(spatial_profiles_path, "rb"))
    for time in time_steps:
        for field in selected_fields:
            if field not in spatial_profiles or "{:d}".format(int(time)) not in spatial_profiles[field]:
                if average_2d:
                    reduce_profiles_spatial_average(path_handler, time_steps, **kwargs)
                else:
                    reduce_profiles_spatial(path_handler, time_steps, **kwargs)
                spatial_profiles = pickle.load(open(spatial_profiles_path, "rb"))
        spatial_profiles = pickle.load(open(spatial_profiles_path, "rb"))
    spatial_profiles = pickle.load(open(spatial_profiles_path, "rb"))

    return spatial_profiles

def retrieve_pvxy_fraction_over_time(path_handler, overwrite_data=False):
    """
    This function does not need access to the raw data as long as
    reduce_pvz_fraction's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_pvz_fraction.

    OUTPUTS:
       - pvxy_fraction_over_time: a dictionary with fraction of particles with vx, vy>0
         at each time step
    """

    # Reduce data if necessary
    if overwrite_data or not os.path.exists(path_handler.get_pvxy_fraction_over_time_path()):
        reduce_pvxy_fraction_over_time(path_handler)

    pvxy_fraction_over_time = pickle.load(open(path_handler.get_pvxy_fraction_over_time_path(), "rb"))
    return pvxy_fraction_over_time


def reduce_pvxy_fraction_over_time(path_handler):
    print("Reducing positive vxy fraction for " + path_handler.sim_name)
    os.chdir(path_handler.path_to_raw_data)

    time_values = retrieve_time_values(path_handler)
    time_indices = time_values[:, 0]
    pvxy_dict = {}

    # Grab coordinates
    # NOTE these angles are in degrees
    # polar_angle runs from -90 to 90 and has half as many steps as azi_angle; also called phi
    # azi_angle runs from -180 to 180; also called lambda
    (N_electrons, u_edges, polar_angle, azi_angle, step) = rdu.getPtclAngularSpectrum(int(time_indices[0]), 'electrons', 'bg')

    gamma_edges = np.sqrt(1. + u_edges**2)
    gamma_centers = 0.5*(gamma_edges[1:] + gamma_edges[:-1])
    dgamma = np.diff(gamma_edges)
    np.savetxt(path_handler.get_gamma_centers_path(), gamma_centers, delimiter=',')
    # convert to radians
    polar_angle = np.pi/180.0*polar_angle
    azi_angle = np.pi/180.0*azi_angle
    # Get differences (width of bins)
    dazi = np.diff(azi_angle)
    sin_polar = np.sin(polar_angle)
    sin_polar_centers = 0.5*(sin_polar[1:] + sin_polar[:-1])
    dsin_polar = np.diff(sin_polar)

    # Now extract the needed indices:
    # -90 to 90 for vx > 0
    pvx_ind1 = (np.abs(azi_angle + np.pi/2.0)).argmin()
    pvx_ind2 = (np.abs(azi_angle - np.pi/2.0)).argmin()

    average_gamma_over_time = []

    for timestep in time_indices:
        timestep = int(timestep)
        N_electrons = rdu.getPtclAngularSpectrum(timestep, 'electrons', 'bg')[0]
        N_ions = rdu.getPtclAngularSpectrum(timestep, 'ions', 'bg')[0]
        N_particles = N_electrons + N_ions

        # Get total number of particles. Integrate with angular element
        integrand = N_particles*dsin_polar[None, :, None]
        integrand *= dazi[None, None, :]
        total_particles_over_energy = (integrand.sum(axis=2).sum(axis=1)*dgamma).sum()

        # Calculate average gamma
        average_gamma = ((integrand*gamma_centers[:, None, None]).sum(axis=2).sum(axis=1)*dgamma).sum()/total_particles_over_energy
        average_gamma_over_time.append(average_gamma)

        num_particles_over_energy = 0
        for i, gamma in enumerate(gamma_centers):
            energy_slice = N_particles[i, :, :]

            # Particles with vx > 0.
            plus_x_particles = energy_slice[:, pvx_ind1:pvx_ind2]
            # Have to integrate over solid angle
            integrand = plus_x_particles*dsin_polar[None, :, None]*dazi[None, None, pvx_ind1:pvx_ind2]
            total_plus_x_particles = (integrand*dgamma[i]).sum()

            # Get total particles with this energy
            total_particles = (energy_slice*dsin_polar[None, :, None]*dazi[None, None, :]*dgamma[i]).sum()

            num_particles_over_energy += total_particles
            array_to_add = np.array([total_plus_x_particles, total_particles])
            gamma_str = "gamma{:d}".format(i)
            if gamma_str not in pvxy_dict:
                pvxy_dict[gamma_str] = array_to_add
            else:
                pvxy_dict[gamma_str] = np.vstack((pvxy_dict[gamma_str], array_to_add))

        if not np.allclose(total_particles_over_energy, num_particles_over_energy):
            print("Total particles at this time step {} don't match sum over energy bins {}".format(total_particles_over_energy, num_particles_over_energy))
    pvxy_dict["average_gamma_over_time"] = average_gamma_over_time
    pickle.dump(pvxy_dict, open(path_handler.get_pvxy_fraction_over_time_path(), "wb"))

def retrieve_pvz_fraction_over_time(path_handler, overwrite_data=False):
    """
    This function does not need access to the raw data as long as
    reduce_pvz_fraction's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_pvz_fraction.

    OUTPUTS:
       - pvz_fraction_over_time: a dictionary with fraction of particles with vz>0
         at each time step
    """

    # Reduce data if necessary
    if overwrite_data or not os.path.exists(path_handler.get_pvz_fraction_over_time_path()):
        if not os.path.exists(path_handler.path_to_raw_data):
            return None
        reduce_pvz_fraction_over_time(path_handler)

    pvz_fraction_over_time = pickle.load(open(path_handler.get_pvz_fraction_over_time_path(), "rb"))
    return pvz_fraction_over_time


def reduce_pvz_fraction_over_time(path_handler):
    print("Reducing positive vz fraction for " + path_handler.sim_name)
    os.chdir(path_handler.path_to_raw_data)

    time_values = retrieve_time_values(path_handler)
    time_indices = time_values[:, 0]
    pvz_dict = {}

    # Grab coordinates
    # NOTE these angles are in degrees
    # polar_angle runs from -90 to 90 and has half as many steps as azi_angle; also called phi
    # azi_angle runs from -180 to 180; also called lambda
    (N_electrons, u_edges, polar_angle, azi_angle, step) = rdu.getPtclAngularSpectrum(int(time_indices[0]), 'electrons', 'bg')

    gamma_edges = np.sqrt(1. + u_edges**2)
    gamma_centers = 0.5*(gamma_edges[1:] + gamma_edges[:-1])
    dgamma = np.diff(gamma_edges)
    np.savetxt(path_handler.get_gamma_centers_path(), gamma_centers, delimiter=',')
    # convert to radians
    polar_angle = np.pi/180.0*polar_angle
    azi_angle = np.pi/180.0*azi_angle
    # Get differences (width of bins)
    dazi = np.diff(azi_angle)
    sin_polar = np.sin(polar_angle)
    sin_polar_centers = 0.5*(sin_polar[1:] + sin_polar[:-1])
    dsin_polar = np.diff(sin_polar)

    # Now extract the needed indices
    polar_zero_ind = (np.abs(polar_angle + 0.0)).argmin()
    polar_PiHalf_ind = (np.abs(polar_angle - np.pi/2.0)).argmin()

    average_gamma_over_time = []
    average_zgamma_over_time = []

    for timestep in time_indices:
        timestep = int(timestep)
        N_electrons = rdu.getPtclAngularSpectrum(timestep, 'electrons', 'bg')[0]
        N_ions = rdu.getPtclAngularSpectrum(timestep, 'ions', 'bg')[0]
        N_particles = N_electrons + N_ions

        # Get total number of particles. Integrate with angular element
        integrand = N_particles*dsin_polar[None, :, None]
        integrand *= dazi[None, None, :]
        total_particles_over_energy = (integrand.sum(axis=2).sum(axis=1)*dgamma).sum()

        # Calculate average gamma
        average_gamma = ((integrand*gamma_centers[:, None, None]).sum(axis=2).sum(axis=1)*dgamma).sum()/total_particles_over_energy
        average_gamma_over_time.append(average_gamma)

        # Calculate average gammaz, which is gamma*sin_polar
        # (remember polar angle is not from the z-axis but rather the x-y plane...
        # latitude instead of co-latitude, the physics norm)
        average_zgamma = ((integrand*sin_polar_centers[None, :, None]*gamma_centers[:, None, None]).sum(axis=2).sum(axis=1)*dgamma).sum()/total_particles_over_energy
        average_zgamma_over_time.append(average_zgamma)

        num_particles_over_energy = 0
        for i, gamma in enumerate(gamma_centers):
            energy_slice = N_particles[i, :, :]

            # Particles with vz > 0.
            plus_z_particles = energy_slice[polar_zero_ind:polar_PiHalf_ind, :]
            # Have to integrate over solid angle
            integrand = plus_z_particles*dsin_polar[None, polar_zero_ind:polar_PiHalf_ind, None]*dazi[None, None, :]
            total_plus_z_particles = (integrand*dgamma[i]).sum()

            # Get total particles with this energy
            total_particles = (energy_slice*dsin_polar[None, :, None]*dazi[None, None, :]*dgamma[i]).sum()

            num_particles_over_energy += total_particles
            array_to_add = np.array([total_plus_z_particles, total_particles])
            gamma_str = "gamma{:d}".format(i)
            if gamma_str not in pvz_dict:
                pvz_dict[gamma_str] = array_to_add
            else:
                pvz_dict[gamma_str] = np.vstack((pvz_dict[gamma_str], array_to_add))

        if not np.allclose(total_particles_over_energy, num_particles_over_energy):
            print("Total particles at this time step {} don't match sum over energy bins {}".format(total_particles_over_energy, num_particles_over_energy))
    pvz_dict["average_gamma_over_time"] = average_gamma_over_time
    pvz_dict["average_zgamma_over_time"] = average_zgamma_over_time
    pickle.dump(pvz_dict, open(path_handler.get_pvz_fraction_over_time_path(), "wb"))


def load_gamma_centers(ph):
    if not os.path.exists(ph.get_gamma_centers_path()):
        if not os.path.exists(ph.path_to_raw_data):
            return None
        os.chdir(ph.path_to_raw_data)
        u_centers = rdu.getPtclAngularSpectrum(0, 'electrons', 'bg')[1]
        gamma_centers = np.sqrt(1. + u_centers**2)
        np.savetxt(ph.get_gamma_centers_path(), gamma_centers, delimiter=",")
    return np.loadtxt(ph.get_gamma_centers_path(), delimiter=",")


def reduce_flux_variable(path_handler, quantity="ExB_", ix=None, iy=None, iz=0):
    """
    For a vector quantity output by Zeltron, calculate the flux
    through the surface [xyz] = i[xyz]. For example, the slice perpendicular
    to the initial uniform magnetic field would have iz=0 (or 1, etc.).
    Note you can only specify ONE of either ix, iy, or iz.

    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_output_variables_vol_avg instead.
    """
    # ------- Error catching -------------
    allowed_flux_quantities = rdu.Zvariables.flux_options
    if quantity not in allowed_flux_quantities:
        print("ERROR: " + quantity + " is not an allowed quantity!")
        print("Allowed quantities are: " + ', '.join(allowed_quantities))
        return None
    if not os.path.exists(path_handler.path_to_raw_data):
        print("ERROR in reduce flux variable: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
        return None
    else:
        datapath = path_handler.path_to_raw_data
        os.chdir(datapath)

    field = quantity
    # make sure only one of ix, iy, iz is specified
    numNone = 0
    if ix is None:
        numNone += 1
    else:
        sliceDir = 0
        iSlice = ix
        field += "x"
    if iy is None:
        numNone += 1
    else:
        sliceDir = 1
        iSlice = iy
        field += "y"
    if iz is None:
        numNone += 1
    else:
        sliceDir = 2
        iSlice = iz
        field += "z"
    if numNone != 2:
        msg = "Exactly one of ix, iy, and iz must be specified (i.e., not None)"
        msg += ": (ix, iy, iz) = "
        msg += repr((ix, iy, iz))
        raise ValueError(msg)
    if ix is None:
        hAxis = 0
    else:
        hAxis = 2
    if hAxis == sliceDir:
        msg = "hAxis cannot be the fixed direction specified by "
        msg += "keyword i" + "xyz"[hAxis]
        raise ValueError(msg)
    vAxis = (hAxis+1) % 3
    if vAxis == hAxis or vAxis == sliceDir:
        vAxis = (vAxis + 1) % 3
    if vAxis == sliceDir:
        msg = "vAxis cannot be the fixed direction specified by "
        msg += "keyword i" + "xyz"[hAxis]
        raise ValueError(msg)
    if vAxis == hAxis:
        msg = "vAxis cannot be the same as hAxis (%i)" % vAxis
        raise ValueError(msg)

    hSlice = slice(None); vSlice = slice(None)
    ihLo = None; ihHi = None; ivLo = None; ivHi = None
    reduceRes = 1
    hSlice = slice(ihLo, ihHi, reduceRes)
    vSlice = slice(ivLo, ivHi, reduceRes)
    slices = (hSlice, vSlice)

    # --------------------------------------------
    # Initial/constant values
    (params, units) = rdu.getSimParams(simDir=path_handler.path_to_reduced_data)
    time_steps = retrieve_time_values(path_handler)[:, 0]
    flux_over_time = []

    # ------- Retrieve data at every time step ---------
    for time in time_steps:
        time = int(time)
        fieldFile = field + "_%i" % time

        (data, fieldTime, step, coords) = rdu.getFieldSlice(
            fieldFile, axis0 = hAxis, axis1 = vAxis, i2 = iSlice,
            i2minusPlus = 0, slices = slices, layerFieldKws=None)

        # Calculate fluxes: integrate over surface
        if sliceDir == 0: # x surface. da = dydz
            flux = np.trapz(data, axis=0, x=coords[1])
            flux = np.trapz(flux, axis=0, x=coords[2])
        elif sliceDir == 1: # y surface. da = dxdz
            flux = np.trapz(data, axis=0, x=coords[0])
            flux = np.trapz(flux, axis=0, x=coords[2])
        elif sliceDir == 2: # z surface. da = dxdy
            flux = np.trapz(data, axis=0, x=coords[0])
            flux = np.trapz(flux, axis=0, x=coords[1])

        flux_over_time.append(flux)

    header = "time step, " + field
    np.savetxt(path_handler.get_flux_variable_path(quantity, ix, iy, iz), flux_over_time, delimiter=",", header=header)
    return


def retrieve_flux_variable(path_handler, quantity, ix=None, iy=None, iz=0, overwrite=False):
    """
    This function does not need access to the raw data as long as
    reduce_flux_variable's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_flux_variable.

    OUTPUTS:
       - flux_over_time: an array with values of the flux at each time step
    """
    # make sure only one of ix, iy, iz is specified
    numNone = 0
    if ix is None:
        numNone += 1
    if iy is None:
        numNone += 1
    if iz is None:
        numNone += 1
    if numNone != 2:
        msg = "Exactly one of ix, iy, and iz must be specified (i.e., not None)"
        msg += ": (ix, iy, iz) = "
        msg += repr((ix, iy, iz))
        raise ValueError(msg)

    # Reduce data if necessary
    if overwrite or not os.path.exists(path_handler.get_flux_variable_path(quantity, ix, iy, iz)):
        reduce_flux_variable(path_handler, quantity, ix, iy, iz)

    flux_over_time = np.loadtxt(path_handler.get_flux_variable_path(quantity, ix, iy, iz), delimiter=",", skiprows=1)
    return flux_over_time




def retrieve_perpendicular_spectrum(path_handler, quantity, time, overwrite=False, cpu_count=None):
    """
    This function does not need access to the raw data as long as
    reduce_perpendicular_spectrum's data can be found.
    If the reduced data cannot be found, it will attempt to create
    the reduced data by calling reduce_perpendicular_spectrum.
    """
    spectrum_path = path_handler.get_pspectrum_path(quantity, time)
    # print(spectrum_path)
    if overwrite or not os.path.exists(spectrum_path):
        return reduce_perpendicular_spectrum(path_handler, quantity, time, cpu_count)
    else:
        return np.loadtxt(spectrum_path, delimiter=",", skiprows=4)

def integrate_over_phi(args):
    (ph, time_ind, kk, overwrite, quantity) = args
    # Uncomment saving intermediately if need to
    if quantity == "magnetic_energy":
        power_dir = ph.path_to_reduced_data + "ME_spectrum_intermediate/"
        power_path = power_dir + "power_at_t{:d}kk{:d}".format(time_ind, kk)
    elif quantity == "elsasserPlus":
        power_dir = ph.path_to_reduced_data + "elsasserPlus_spectrum_intermediate/"
        power_path = power_dir + "EP_power_at_t{:d}kk{:d}".format(time_ind, kk)
    elif quantity == "elsasserMinus":
        power_dir = ph.path_to_reduced_data + "elsasserMinus_spectrum_intermediate/"
        power_path = power_dir + "EM_power_at_t{:d}kk{:d}".format(time_ind, kk)
    else:
        print("ERROR: quantity " + quantity + " is not defined!")

    # First, check if reduced file exists!
    if not os.path.exists(power_path) or overwrite:
        os.chdir(ph.path_to_raw_data)
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        B0 = params["B0"]

        # slices ordering is x,y,z, unlike the array indexing
        # which is z, y, x
        slices = [slice(None), slice(None), slice(kk,kk+1)]
        if quantity == "magnetic_energy":
            (quantity_x, t, n, coords) = rdu.getFieldSliceXY("Bx_{:d}".format(time_ind), kk)
            quantity_y = rdu.getFieldSliceXY("By_{:d}".format(time_ind), kk)[0]
            quantity_z = rdu.getFieldSliceXY("Bz_{:d}".format(time_ind), kk)[0] - B0
            xvals = coords[0]; yvals = coords[1]

        if "elsasser" in quantity:
            elsaesser_path_base = ph.path_to_raw_data + "elsasserFields/"
            elsaesser_path = elsaesser_path_base + "elsaesserEnergies_{:d}.h5".format(time_ind)
            if not os.path.exists(elsaesser_path):
                print("ERROR: run save_elsaesser_quantities first to reduce the" +
                " elsaesser variables. Exiting...")
                return
            with h5py.File(elsaesser_path, "r") as hf:
                if "Plus" in quantity:
                    group = hf.get('elsaesserEnergyPlus')
                    # indexing is z, y, x
                    quantity_x = np.array(group.get("epx")[kk, :, :])
                    quantity_y = np.array(group.get("epy")[kk, :, :])
                    quantity_z = np.array(group.get("epz")[kk, :, :])
                elif "Minus" in quantity:
                    group = hf.get('elsaesserEnergyMinus')
                    quantity_x = np.array(group.get("emx")[kk, :, :])
                    quantity_y = np.array(group.get("emy")[kk, :, :])
                    quantity_z = np.array(group.get("emz")[kk, :, :])
                else:
                    print(quantity + " not implemented.")
            yvals = retrieve_coords(ph)[1][:-1]
            xvals = retrieve_coords(ph)[0][:-1]

        zvals = retrieve_coords(ph)[2]
        nx = xvals.size - 1; ny = yvals.size - 1; # nz = zvals.size - 1

        number_of_modes = int(np.min([nx, ny])/3)
        # possible perpendicular wavenumbers
        k_perp_vals = np.arange(number_of_modes) + 1.0

        # location of the box's center
        x_center = nx/2 - 1
        y_center = ny/2 - 1

        # note go to nx instead of nx-1 because IDL includes
        # the endpoint but python does not
        qx_fft = np.fft.fftn(quantity_x)
        qy_fft = np.fft.fftn(quantity_y)
        qz_fft = np.fft.fftn(quantity_z)
        # 1/N Normalization is included in IDL but
        # not in np's forward fft
        qx_fft = qx_fft/np.size(qx_fft)
        qy_fft = qy_fft/np.size(qy_fft)
        qz_fft = qz_fft/np.size(qz_fft)

        # unintegrated spectrum
        Et = (np.abs(qx_fft)**2 + np.abs(qy_fft)**2 + np.abs(qz_fft)**2)
        if quantity == "magnetic_energy":
            Et = Et/(8*np.pi)
        Et2 = np.fft.fftshift(Et)

        # function to find values of Et2 at any specified xnew, ynew
        interpolating_function = interpolate.interp2d(xvals, yvals, Et2, kind="linear")

        # --------------------------------
        # ring-integrated spectrum
        # --------------------------------
        power_at_z = []
        # Loop through each perpendicular mode and add
        # the phi-integrated spectrum
        for nn in np.arange(0, number_of_modes):
            kn = k_perp_vals[nn]
            # number of sample points
            # n_sample = int(2*np.pi*nx)
            # angular separation between points
            # dtheta = 1.0/nx

            n_sample = 4*kn
            dtheta = 2.0*np.pi/n_sample

            # grid on which to sample the unintegrated spectrum
            # remember to center origin in middle of box
            x_new = x_center + kn*np.cos(np.arange(n_sample)*dtheta)
            y_new = y_center + kn*np.sin(np.arange(n_sample)*dtheta)

            # evaluated as tuples: (x_new[i], y_new[i])
            # interpolated_values = interpolating_function(x_new, y_new, grid=False)
            # interpolated on a grid, so loop instead:
            interpolated_values = []
            for (x, y) in zip(x_new, y_new):
                interpolated_values.append(interpolating_function(x, y))

            interpolated_values = np.array(interpolated_values)
            # remove negative values, since Et2 is strictly positive
            # the negative values are spline overshoots.
            if np.min(interpolated_values) < 0:
                # print("negative value encountered!")
                interpolated_values = interpolated_values.clip(min=0)

            normalized_values = 2.0*np.pi*interpolated_values/n_sample
            phi_integrated = kn * np.sum(normalized_values)
            power_at_z.append(phi_integrated)

            # plt.figure()
            # ax = plt.gca()
            # ax.scatter(x_center, y_center)
            # sc = ax.scatter(x_new, y_new, c=interpolated_values, cmap=plt.cm.get_cmap('jet'), marker='o')
            # ax.grid(color='.9', ls='--')
            # ax.set_title('radius {}; # samples: {}'.format(nn, n_sample))
            # plt.colorbar(sc)
            # plt.figure()
            # some_xs = np.linspace(41, 55, 100)
            # some_ys = np.ones((100))*48
            # yvalsf = interpolating_function(some_xs, some_ys)
            # plt.gca().axhline([0], color='black')
            # plt.plot(some_xs, yvalsf)
            # plt.show()
        # Save to file
        git_commit = ph.get_current_commit()
        header = "Power at z for kk={:d} at time index {:d}".format(kk, time_ind)
        header += "\n Intermediate step for perpendicular spectrum"
        header += "\n" + ph.sim_name + "\nGit commit: " + git_commit
        header += "\nSaved at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        np.savetxt(power_path, power_at_z, delimiter=",", header=header)
    else:
        # Load file
        power_at_z = np.loadtxt(power_path, delimiter=",", skiprows=5)

    return power_at_z


def reduce_perpendicular_spectrum(path_handler, quantity, time_ind, cpu_count = None, overwrite=False):
    """
    Calculate the spectrum as a function of perpendicular wavenumber for:
        - magnetic energy density: "magnetic_energy"
    This function must have access to the raw data.
    It should not be called by a user script: use
    retrieve_perpendicular_spectrum instead.
    """
    import matplotlib
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    import multiprocessing as mp

    if cpu_count is None:
        cpu_count = int(mp.cpu_count()/4)
        if "256" in path_handler.sim_name:
            cpu_count = 60
        elif "384" in path_handler.sim_name:
            cpu_count = 40
        elif "512" in path_handler.sim_name:
            cpu_count = 20
        elif "768" in path_handler.sim_name:
            cpu_count = 12
        elif "1024" in path_handler.sim_name:
            cpu_count = 2
        else:
            cpu_count = 1
    print("Using cpu count = {}".format(cpu_count))

    if not os.path.exists(path_handler.path_to_raw_data) or not os.path.exists(path_handler.path_to_raw_data + "densities"):
        print("ERROR in reduce perpendicular spectrum: path to raw data doesn't exist!!")
        print(path_handler.path_to_raw_data)
        return None
    else:
        datapath = path_handler.path_to_raw_data

    print("Starting " + datetime.datetime.now().strftime("%H:%M:%S"))
    print("Using cpu count = {}".format(cpu_count))
    coords = retrieve_coords(path_handler)
    nz = coords[2].size - 1

    time_ind = int(time_ind)
    if quantity == "magnetic_energy":
        print("Reducing " + path_handler.sim_name + " perpendicular magnetic energy spectrum at t={:d}".format(time_ind))
        power_dir = path_handler.path_to_reduced_data + "ME_spectrum_intermediate/"
        if not os.path.exists(power_dir):
            os.makedirs(power_dir)
    elif quantity == "elsasserPlus":
        print("Reducing " + path_handler.sim_name + " perpendicular elsasser Plus energy spectrum at t={:d}".format(time_ind))
        power_dir = path_handler.path_to_reduced_data + "elsasserPlus_spectrum_intermediate/"
        if not os.path.exists(power_dir):
            os.makedirs(power_dir)
    elif quantity == "elsasserMinus":
        power_dir = path_handler.path_to_reduced_data + "elsasserMinus_spectrum_intermediate/"
        if not os.path.exists(power_dir):
            os.makedirs(power_dir)
        print("Reducing " + path_handler.sim_name + " perpendicular elsasser Minus energy spectrum at t={:d}".format(time_ind))
    else:
        print("ERROR: quantity " + quantity + " is not defined!")

    pool = mp.Pool(cpu_count)
    all_arguments = []
    for kk in np.arange(0, nz):
        if quantity in ["magnetic_energy", "elsasserPlus", "elsasserMinus"]:
            arguments = (path_handler, time_ind, kk, overwrite, quantity)
        else:
            print(quantity + " spectrum not implemented yet.")
        all_arguments.append(arguments)
    results = pool.map(integrate_over_phi, all_arguments)
    pool.close()
    integrated_perp_spectrum = np.sum(np.array(results), axis=0)/nz

    # save to file
    spectrum_path = path_handler.get_pspectrum_path(quantity, time_ind)
    git_commit = path_handler.get_current_commit()
    header = quantity + " as a function of perpendicular wavenumber (1, 2, 3, ...)"
    header += "\n" + path_handler.sim_name + "\nGit commit: " + git_commit
    header += "\nSaved at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    np.savetxt(spectrum_path, integrated_perp_spectrum, delimiter=",", header=header)
    print("Ending: " + datetime.datetime.now().strftime("%H:%M:%S"))

    return integrated_perp_spectrum
