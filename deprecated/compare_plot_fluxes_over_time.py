import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
TO DO: introduce fig category as in vol-avg script
"""

overwrite = False
stampede2 = False

ix = None; iy = 0; iz = None

# configs = ["zeltron_96cube_single-mode_no-forcing_standing", "zeltron_96cube_single-mode_no-forcing_travelling", "zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_s0"]
# labels = ["Single (standing) mode, sine forcing", "Single (travelling) mode, sine forcing", "Single mode, OLA forcing", "Balanced (8 modes), OLA forcing"]
# configs = ["zeltron_96cube_single-mode_no-forcing_travelling", "zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_s0"]
# labels = ["Single mode, sine forcing", "Single mode, OLA forcing", "Balanced (8 modes), OLA forcing"]
configs = ["zeltron_96cube_single-mode_master", "zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_master", "zeltron_96cube_balanced_s0"]
labels = ["Single mode (run 1)", "Single mode (run 2)", "Balanced (run 1)", "Balanced (run 2)"]
# configs = ["zeltron_96cube_single-mode_master", "zeltron_96cube_balanced_master"]
# labels = ["Single mode, OLA forcing", "Balanced (8 modes), OLA forcing"]

sim_name0 = "zeltron_96cube_single-mode_master"
if stampede2:
    ph0 = path_handler(sim_name0, "stampede2")
else:
    ph0 = path_handler(sim_name0, "lia_hp")

figdir = ph0.path_to_figures + "../compare/flux-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
zVars = rdu.Zvariables.flux_options
quantities_to_plot = zVars

colors = ["C01", "C01", "C02", "C02"]
linestyles = ["-", ":", "-.", "--"]
markers = ["o", "x", "s", "d"]
params, units = rdu.getSimParams(simDir=ph0.path_to_reduced_data)

Lx = params["xmax"] - params["xmin"]
Ly = params["ymax"] - params["ymin"]
Lz = params["zmax"] - params["zmin"]


for quantity in quantities_to_plot:
    # -------------------------------
    # Set strings according to quantity
    if ix is not None:
        quantity_str = quantity + "x"
        area = Ly*Lz
        dxstr = "$\mathrm{dydz}$"
        normstr = "$L_yL_z$"
        titstr = "x = {}".format(ix)
    elif iy is not None:
        quantity_str = quantity + "y"
        area = Lx*Lz
        dxstr = "$\mathrm{dxdz}$"
        normstr = "$L_xL_z$"
        titstr = "y = {}".format(iy)
    elif iz is not None:
        quantity_str = quantity + "z"
        area = Lx*Ly
        dxstr = "$\mathrm{dxdy}$"
        normstr = "$L_xL_y$"
        titstr = "z = {}".format(iz)

    norm_value = rdu.getNormValue(quantity_str, simDir=ph0.path_to_reduced_data)
    ylabel = r"$\int$" + rdu.getFieldAbbrev(quantity_str) + dxstr + norm_value[1] + normstr

    # --------------------------------------
    # Plotting
    plt.figure()
    plt.xlabel("$tc/L$")
    plt.ylabel(ylabel)
    plt.gca().axhline([0], color="black", linestyle="--")
    figure_name = figdir + quantity_str.replace("_", "")

    for i, sim_name in enumerate(configs):
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, "lia_hp")

        times_in_LC = retrieve_time_values(ph)[:, 2]
        flux_over_time = retrieve_flux_variable(ph, quantity, ix, iy, iz, overwrite=overwrite)

        plt.plot(times_in_LC, flux_over_time/norm_value[0]/area, markersize=4, marker=markers[i], label=labels[i], ls=linestyles[i], color=colors[i])

    plt.ylabel(ylabel)
    plt.xlabel("$tc/L$")
    plt.title(ph.sim_name + "\n" + titstr)
    plt.legend()
    plt.gca().grid(color='.9', ls='--')
    plt.tight_layout()
    print("Saving " + figure_name)
    plt.savefig(figure_name)
    root_name = figure_name.split("/")[-1]
    if not os.path.isdir(figdir + "pdfs/"):
        os.makedirs(figdir + "pdfs/")
    plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

    plt.gca().grid(False, which='both')
    if not os.path.isdir(figdir + "pdfs/nogrids/"):
        os.makedirs(figdir + "pdfs/nogrids/")
    plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
    if not os.path.isdir(figdir + "nogrids/"):
        os.makedirs(figdir + "nogrids/")
    plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()





# titstr += r"$4\langle v\cdot B\rangle/\langle v^2+B^2\rangle$" + " \n"
# 
# plt.title(titstr)
# plt.legend()
# plt.tight_layout()
# figdir = figpath + "volAvg-over-time/"
# if not os.path.isdir(figdir):
    # os.makedirs(figdir)
# figname = figdir + "compare_" + field
# print("saving " + figname)
# plt.savefig(figname)
# 
# 
# plt.figure()
# plt.xlabel("$tc/L$")
# plt.ylabel("$H_c/E$")
# # plt.ylabel("Cross-helicity normalized to maximum")
# plt.gca().axhline([0], color="black", linestyle="--")
# titstr = ""
# for i, config in enumerate(configs):
    # print("Loading " + config)
    # datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    # chf_means_p = datarpath +  "cross-helicity-flucs_vavg_means.p"
    # tef_means_p = datarpath +  "total-energy-flucs_vavg_means.p"
    # if os.path.exists(ch_means_p):
        # mean_cross_helicity_f = pickle.load(open(chf_means_p, "rb"))
        # mean_total_energy_f = pickle.load(open(tef_means_p, "rb"))
    # else:
        # print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    # tlcpath = datarpath + "timeInLc.p"
    # if not os.path.exists(tlcpath):
        # print("ERROR: PICKLE TIME VALUES FIRST!!")
    # else:
        # timesLC = np.array(pickle.load(open(tlcpath, "rb")))
# 
    # normed_chf = np.array(mean_cross_helicity_f["vol_avg"])/np.array(mean_total_energy_f["vol_avg"])
    # meanchf = np.mean(normed_chf)
    # titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanchf)
    # plt.plot(timesLC, normed_chf, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))
# 
# 
# 
# titstr += r"$4\langle v\cdot (B-B_0)\rangle/\langle v^2+(B-B_0)^2\rangle$" + " \n"
# 
# plt.title(titstr)
# plt.legend()
# plt.tight_layout()
# figdir = figpath + "volAvg-over-time/"
# if not os.path.isdir(figdir):
    # os.makedirs(figdir)
# figname = figdir + "compare_" + field + "-flucs"
# print("saving " + figname)
# plt.savefig(figname)
# 
# plt.figure()
# plt.xlabel("$tc/L$")
# plt.ylabel("$H_c/E$")
# # plt.ylabel("Cross-helicity normalized to maximum")
# plt.gca().axhline([0], color="black", linestyle="--")
# titstr = ""
# for i, config in enumerate(configs):
    # print("Loading " + config)
    # datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    # chvf_means_p = datarpath +  "cross-helicity-vflucs_vavg_means.p"
    # tevf_means_p = datarpath +  "total-energy-vflucs_vavg_means.p"
    # if os.path.exists(chvf_means_p):
        # mean_cross_helicity_vf = pickle.load(open(chvf_means_p, "rb"))
        # mean_total_energy_vf = pickle.load(open(tevf_means_p, "rb"))
    # else:
        # print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    # tlcpath = datarpath + "timeInLc.p"
    # if not os.path.exists(tlcpath):
        # print("ERROR: PICKLE TIME VALUES FIRST!!")
    # else:
        # timesLC = np.array(pickle.load(open(tlcpath, "rb")))
# 
    # normed_chvf = np.array(mean_cross_helicity_vf["vol_avg"])/np.array(mean_total_energy_vf["vol_avg"])
    # meanchvf = np.mean(normed_chvf)
    # titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanchvf)
    # plt.plot(timesLC, normed_chvf, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))
# 
# 
# 
# titstr += r"$4\langle (v-\langle v\rangle) \cdot (B-B_0)\rangle/\langle (v-\langle v\rangle)^2+(B-B_0)^2\rangle$" + " \n"
# 
# plt.title(titstr)
# plt.legend()
# plt.tight_layout()
# figdir = figpath + "volAvg-over-time/"
# if not os.path.isdir(figdir):
    # os.makedirs(figdir)
# figname = figdir + "compare_" + field + "-vflucs"
# print("saving " + figname)
# plt.savefig(figname)
# 
# plt.figure()
# plt.xlabel("$tc/L$")
# plt.ylabel("$H_c/E$")
# # plt.ylabel("Cross-helicity normalized to maximum")
# plt.gca().axhline([0], color="black", linestyle="--")
# titstr = ""
# for i, config in enumerate(configs):
    # print("Loading " + config)
    # datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    # chv_means_p = datarpath +  "cross-helicity-v_vavg_means.p"
    # tev_means_p = datarpath +  "total-energy-v_vavg_means.p"
    # if os.path.exists(chv_means_p):
        # mean_cross_helicity_v = pickle.load(open(chv_means_p, "rb"))
        # mean_total_energy_v = pickle.load(open(tev_means_p, "rb"))
    # else:
        # print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    # tlcpath = datarpath + "timeInLc.p"
    # if not os.path.exists(tlcpath):
        # print("ERROR: PICKLE TIME VALUES FIRST!!")
    # else:
        # timesLC = np.array(pickle.load(open(tlcpath, "rb")))
# 
    # normed_chv = np.array(mean_cross_helicity_v["vol_avg"])/np.array(mean_total_energy_v["vol_avg"])
    # meanchv = np.mean(normed_chv)
    # titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanchv)
    # plt.plot(timesLC, normed_chv, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))
# 
# 
# 
# titstr += r"$4\langle (v-\langle v\rangle) \cdot B\rangle/\langle (v-\langle v\rangle)^2+B^2\rangle$" + " \n"
# 
# plt.title(titstr)
# plt.legend()
# plt.tight_layout()
# figdir = figpath + "volAvg-over-time/"
# if not os.path.isdir(figdir):
    # os.makedirs(figdir)
# figname = figdir + "compare_" + field + "-v"
# print("saving " + figname)
# plt.savefig(figname)
