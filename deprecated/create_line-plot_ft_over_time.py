import numpy as np
import sys
sys.path.append("./modules/")
sys.path.append("./modules/zPy/for3D/")
import os
import matplotlib
matplotlib.use('Agg')
# matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from utils import get_times
import zfileUtil as zf
import pickle
from scipy import optimize

# config = "zeltron_96cube_single-mode_s0"
config = "zeltron_96cube_balanced_s0"
# config = "zeltron_96cube_single-mode_no-forcing_travelling"
# config = "zeltron_96cube_balanced_master"
# config = "zeltron_96cube_single-mode_master"

stampede2 = True
plot_components = True
do_extra = False
ix = 0; iy = 0; iz = 0

if stampede2:
    datapath = "/scratch/06165/ahankla/imbalanced_turbulence/" + config + "/data/"
    figpath = "/work/06165/ahankla/stampede2/imbalanced_turbulence/figures/" + config + "/profiles-1d/"
else:
    datapath = "/mnt/d/imbalanced_turbulence/data/" + config + "/"
    datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    figpath = "/mnt/d/imbalanced_turbulence/figures/" + config + "/profiles-1d/"

times = get_times(datapath)
# field_components = ["Bx", "By", "Bz", "Jx_electrons", "Jy_electrons", "Jz_electrons", "Jx_ions", "Jy_ions", "Jz_ions", "velx_electrons", "velx_ions", "vely_electrons", "vely_ions", "velz_electrons", "velz_ions"]
field_components = ["Bz", "By", "Bx"]

consts = zf.Consts
(params, units) = zf.getSimParams(simDir=datapath)
rho=consts.c/params["omegac"]
B0=params["B0"]
norm = {"Bz": B0, "By":B0, "Bx":B0, "magnetic_energy":B0**2/2.0, "Bz_flucs":B0}
ylabels = {"Bz":"$B_z/B_0$", "By":"$B_y/B_0$", "Bx":"$B_x/B_0$", "magnetic_energy":"$B^2/B_0^2$", "Bz_flucs":"$(B_z-B_0)/B_0$"}

Ls, Ns = zf.getSimLengths(params = params)

os.chdir(datapath)

if plot_components:
    for field in field_components:
        for time in times:

            # --- z profile
            (xslice, fieldTime, step, coords) = zf.getFieldSlice(field, int(time), axis0=1, axis1=2, i2=ix)
            # prep for FT
            zvals = coords[2]; Nz = len(zvals); dz = Ls[2]/Nz
            zprof = xslice[iy, :]

            # Now Fourier Transform
            zprof_fft = np.abs(np.fft.fft(zprof)/Nz)
            wavenumbers = np.linspace(0, 1.0/dz, Nz)
            wavelengths = 1.0/wavenumbers

            figdir = figpath + "z-profile/ft_" + field + "/"
            if not os.path.isdir(figdir):
                os.makedirs(figdir)
            figname = figdir + "ft_" + field + "_t{}".format(time) + "_ix{}iy{}_".format(ix, iy) + config
            plt.plot(wavelengths, zprof_fft/np.max(np.abs(zprof_fft)), marker="o", ls="")
            titstr = config + "\n" + field + " Fourier transformed z-profile at x={}, y={}, t={}\n Normalized to maximum".format(ix, iy, time)
            plt.title(titstr)
            plt.xlabel("Wavelength [cells]")
            plt.savefig(figname, bbox_inches="tight")
            plt.close()

            # --- y profile
            # prep for FT
            yvals = coords[1]; Ny = len(yvals); dy = Ls[1]/Ny
            yprof = xslice[:, iz]

            # Now Fourier Transform
            yprof_fft = np.abs(np.fft.fft(yprof)/Ny)
            wavenumbers = np.linspace(0, 1.0/dy, Ny)
            wavelengths = 1.0/wavenumbers
            figdir = figpath + "y-profile/ft_" + field + "/"
            if not os.path.isdir(figdir):
                os.makedirs(figdir)
            figname = figdir + "ft_" + field + "_t{}".format(time) + "_ix{}iz{}_".format(ix, iz) + config
            plt.plot(wavelengths, yprof_fft/np.max(np.abs(yprof_fft)), marker="o", ls="")
            titstr = config + "\n" + field + " Fourier transformed y-profile at x={}, z={}, t={}\n Normalized to maximum".format(ix, iz, time)
            plt.title(titstr)
            plt.xlabel("Wavelength [cells]")
            plt.savefig(figname, bbox_inches="tight")
            plt.close()

            # --- x profile
            (yslice, fieldTime, step, coords) = zf.getFieldSlice(field, int(time), axis0=0, axis1=2, i2=iy)
            # prep for FT
            xvals = coords[0]; Nx = len(xvals); dx = Ls[0]/Nx
            xprof = yslice[:, iz]
            # Now Fourier Transform
            xprof_fft = np.abs(np.fft.fft(xprof)/Nx)
            wavenumbers = np.linspace(0, 1.0/dx, Nx)
            wavelengths = 1.0/wavenumbers

            figdir = figpath + "x-profile/ft_" + field + "/"
            if not os.path.isdir(figdir):
                os.makedirs(figdir)
            figname = figdir + "ft_" + field + "_t{}".format(time) + "_iy{}iz{}_".format(iy, iz) + config
            plt.plot(wavelengths, xprof_fft/np.max(np.abs(xprof_fft)), marker="o", ls="")
            titstr = config + "\n" + field + " Fourier transformed x-profile at y={}, z={}, t={}".format(iy, iz, time)
            plt.title(titstr)
            plt.xlabel("Wavelength [cells]")
            plt.tight_layout()
            plt.savefig(figname, bbox_inches="tight")
            plt.close()
