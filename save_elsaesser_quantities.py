import numpy as np
import pickle
import h5py
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
This script will compare the volume-averaged conserved quantities:
energy and cross-helicity. From Schekochihin 2020 Eq. 3.4
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""

def save_elsaesser_quantities(ph, **kwargs):
    """
    Will save the elsaesserEnergyPlus and elsaesserEnergyMinus variables
    to a new .h5 file that also has an attribute that is the mean.
    """

    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    overwrite_vol_avg_values = kwargs.get("overwrite_vol_avg_values", False)
    times_to_plot = kwargs.get("times_to_plot", None)
    elsasser_reduced_path = ph.path_to_reduced_data + "vol_avg_elsasser_variables.p"
    to_plot = kwargs.get("to_plot", True)

    times_values = retrieve_time_values(ph)
    if times_to_plot is None:
        times_to_plot = times_values[:, 0]
    times_in_LC = times_values[:, 2]
    sim_name = ph.sim_name

    # -----------------------------------------
    # Load magnetization in case it's needed
    # -----------------------------------------
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    B0 = params["B0"]
    mag_values = np.loadtxt(ph.path_to_reduced_data + "magnetization.dat", delimiter=",", skiprows=1)
    magnetization = mag_values[:,1]

    # should be able to jump every FDUMP outputs
    jump = int(params["FDUMP"])
    magnetization = magnetization[::jump]
    # in case of going longer than needed
    magnetization = magnetization[:times_in_LC.size]
    alfven_in_c = np.sqrt(magnetization/(magnetization + 1.0))

    all_ep_means = []
    all_em_means = []
    all_pep_means = []
    all_pem_means = []

    if overwrite_vol_avg_values:
        print("Overwriting values in " + elsasser_reduced_path)

    # -----------------------------------------
    # For a specific time step, load everything only once.
    # -----------------------------------------
    if os.path.exists(elsasser_reduced_path):
        elsasser_reduced_dict = pickle.load(open(elsasser_reduced_path,
                                                 "rb"))
    else:
        elsasser_reduced_dict = {}
        elsasser_reduced_dict["elsasserEnergiesPlus"] = {}
        elsasser_reduced_dict["elsasserEnergiesMinus"] = {}

    for t in times_to_plot:
        step = int(t)
        elsaesser_path_base = ph.path_to_raw_data + "elsasserFields/"
        elsaesser_path = elsaesser_path_base + "elsaesserEnergies_{:d}.h5".format(step)

        reduced_path_exists = os.path.exists(elsaesser_path)
        t_not_in_dict = (not (t in elsasser_reduced_dict["elsasserEnergiesMinus"])) or (not (t in elsasser_reduced_dict["elsasserEnergiesPlus"]))

        if t_not_in_dict or overwrite_vol_avg_values:
            if reduced_path_exists:
                with h5py.File(elsaesser_path, "r") as hf:
                    group_plus = hf.get('elsaesserEnergyPlus')
                    group_minus = hf.get('elsaesserEnergyMinus')
                    ep_mean = group_plus.attrs.get("ep_volume_average")
                    em_mean = group_minus.attrs.get("em_volume_average")
                    print("Loaded " + ph.sim_name + " t={:d} ({:.2f}, {:.2f})".format(step, ep_mean, em_mean))
                    # pep_mean = group_plus.attrs.get("perp_volume_average")
                    # pem_mean = group_minus.attrs.get("perp_volume_average")
                elsasser_reduced_dict["elsasserEnergiesPlus"][t] = ep_mean
                elsasser_reduced_dict["elsasserEnergiesMinus"][t] = em_mean
                pickle.dump(elsasser_reduced_dict, open(elsasser_reduced_path, "wb"))
            else:
                print("Reduced Elsasser energies for " + ph.sim_name + " t={:d} do not exist.".format(step))
                # Either create reduced path or set to None
                fields_dir = ph.path_to_raw_data + 'fields/'
                if os.path.exists(fields_dir) and os.listdir(fields_dir):
                    if not os.path.exists(elsaesser_path_base):
                        os.makedirs(elsaesser_path_base)
                    vA_ind = (np.abs(times_values[:, 0] - step)).argmin()
                    vA = alfven_in_c[vA_ind]

                    os.chdir(ph.path_to_raw_data)

                    Bx = rdu.remove_ghost_zones(rdu.getField("Bx_%i" % step)[0])
                    By = rdu.remove_ghost_zones(rdu.getField("By_%i" % step)[0])
                    Bz = rdu.remove_ghost_zones(rdu.getField("Bz_%i" % step)[0]) - B0

                    # Loading vi directly results in loading rho_e and rho_i
                    # two more times than necessary! So instead, just do
                    # the same thing here.
                    rho_i = rdu.getField("rho_ions_%i" % step)[0]
                    rho_e = rdu.getField("rho_electrons_%i" % step)[0]
                    Jx_e = rdu.getField("Jx_electrons_%i" % step)[0]
                    Jy_e = rdu.getField("Jy_electrons_%i" % step)[0]
                    Jz_e = rdu.getField("Jz_electrons_%i" % step)[0]
                    Jx_i = rdu.getField("Jx_ions_%i" % step)[0]
                    Jy_i = rdu.getField("Jy_ions_%i" % step)[0]
                    Jz_i = rdu.getField("Jz_ions_%i" % step)[0]

                    # avoid divide by zero -- this isn't sure -- maybe rho = -1e121e-33,
                    #   but any problems should be very rare -- rarer than rho=0.
                    # Zeltron dumps only half of currents, so multiply by 2.
                    Vx = rdu.remove_ghost_zones(2.0*(Jx_i - Jx_e)/((rho_i - rho_e)*rdu.Consts.c + 1.1211111111e-30))
                    Vy = rdu.remove_ghost_zones(2.0*(Jy_i - Jy_e)/((rho_i - rho_e)*rdu.Consts.c + 1.1211111111e-30))
                    Vz = rdu.remove_ghost_zones(2.0*(Jz_i - Jz_e)/((rho_i - rho_e)*rdu.Consts.c + 1.1211111111e-30))

                    epx = Vx + Bx/B0*vA
                    epy = Vy + By/B0*vA
                    epz = Vz + Bz/B0*vA
                    emx = Vx - Bx/B0*vA
                    emy = Vy - By/B0*vA
                    emz = Vz - Bz/B0*vA

                    # Only calculate these variables for the means
                    ep_energy = epx**2 + epy**2 + epz**2
                    em_energy = emx**2 + emy**2 + emz**2

                    ep_mean = np.mean(ep_energy)
                    em_mean = np.mean(em_energy)
                    elsasser_reduced_dict["elsasserEnergiesPlus"][t] = ep_mean
                    elsasser_reduced_dict["elsasserEnergiesMinus"][t] = em_mean

                    pickle.dump(elsasser_reduced_dict, open(elsasser_reduced_path, "wb"))

                    with h5py.File(elsaesser_path, "w") as hf:
                        # Need to save the elsasser variables individually because
                        # of fourier transform
                        print("Saving to " + elsaesser_path)
                        group_plus = hf.create_group("elsaesserEnergyPlus")
                        group_plus.create_dataset("epx", data=epx)
                        group_plus.create_dataset("epy", data=epy)
                        group_plus.create_dataset("epz", data=epz)
                        group_plus.attrs.create('ep_volume_average', ep_mean)
                        group_minus = hf.create_group("elsaesserEnergyMinus")
                        group_minus.create_dataset("emx", data=emx)
                        group_minus.create_dataset("emy", data=emy)
                        group_minus.create_dataset("emz", data=emz)
                        group_minus.attrs.create('em_volume_average', em_mean)
                else:
                    print("With no data, could not calculate Elsasser fields for " + ph.sim_name + " at all. Returning None.")
                    return None

        else:
            string = "Found in dict for " + ph.sim_name + " tind={:d}".format(step)
            ep_mean = elsasser_reduced_dict["elsasserEnergiesPlus"][t]
            em_mean = elsasser_reduced_dict["elsasserEnergiesMinus"][t]
            if ep_mean is None:
                string += " is NONE"
                print(string)
            else:
                string += " is {:.2f}".format(ep_mean)
            # print(string)
        all_ep_means.append(ep_mean)
        all_em_means.append(em_mean)
        # all_pep_means.append(pep_mean)
        # all_pem_means.append(pem_mean)

    if to_plot:
        # ----------------------------------------------
        # set up figure info
        figdir = ph.path_to_figures + "elsasserVariables/means/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)

        # ----------------------------------------------
        # Plot the ratio of the +- energies
        figname = figdir + 'elsaesserEnergyRatio.png'
        figure_name = kwargs.get("figure_name", figname)

        # Body of the plot.
        new_ep_means = []
        new_em_means = []
        new_LC_times = []
        times_removed = []
        for i, mean in enumerate(all_ep_means):
            if mean is None or all_em_means[i] is None:
                times_removed.append(times_to_plot[i])
            else:
                new_ep_means.append(mean)
                new_em_means.append(all_em_means[i])
                new_LC_times.append(times_in_LC[i])
        print(ph.sim_name + ", removed times: " + ', '.join(map(str, times_removed)))
        if not new_ep_means:
            return elsasser_reduced_dict

        ratio = np.array(new_ep_means)/np.array(new_em_means)
        plt.plot(new_LC_times, ratio, markersize=3, marker="o", label=r"$\langle |Z^+|^2\rangle/\langle |Z^-|^2\rangle$", ls='None')
        plt.gca().axhline([np.mean(ratio)], ls='--', color='black', label="Mean: {:.2f}".format(np.mean(ratio)))

        plt.gca().set_xlabel("$tc/L$")
        plt.gca().tick_params(axis='y')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.gca().set_ylim([0, None])
        plt.title(sim_name)
        plt.legend()

        plt.tight_layout()
        if figure_name == "show":
            plt.show()
        else:
            plt.savefig(figure_name)
        plt.close()

        # ----------------------------------------------
        # Plot the ratio of the perpendicular +- energies
        # figname = figdir + 'perp_elsaesserEnergyRatio.png'
        # figure_name = kwargs.get("figure_name", figname)
    #
        # # Body of the plot.
        # ratio = np.array(all_pep_means)/np.array(all_pem_means)
        # plt.plot(times_in_LC, ratio, markersize=3, marker="o", label=r"$\langle |Z_\perp^+|^2\rangle/\langle |Z_\perp^-|^2\rangle$", ls='None')
        # plt.gca().axhline([np.mean(ratio)], ls='--', color='black', label="Mean: {:.2f}".format(np.mean(ratio)))
    #
        # plt.gca().set_xlabel("$tc/L$")
        # plt.gca().tick_params(axis='y')
        # plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        # plt.gca().grid(color='.9', ls='--')
        # plt.gca().set_ylim([0, None])
        # plt.title(sim_name)
        # plt.legend()
    #
        # plt.tight_layout()
        # if figure_name == "show":
            # plt.show()
        # else:
            # plt.savefig(figure_name)
        # plt.close()
    return elsasser_reduced_dict

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = True
    system_config = "lia_hp"
    overwrite_data = False
    overwrite_vol_avg_values = False

    if stampede2:
        system_config = "stampede2"

    # times_to_plot = [0]
    times_to_plot = None

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["times_to_plot"] = times_to_plot
    kwargs["overwrite_vol_avg_values"] = overwrite_vol_avg_values

    # --------------------
    # Tests
    # --------------------
    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    # sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    # ph = path_handler(sim_name, system_config)
    # save_elsaesser_quantities(ph, **kwargs)

    # --------------------
    sim_name = rdu.get_list_of_sim_names_with_phrases("/scratch/06165/ahankla/imbalanced_turbulence/", ["384cube", "dcorr029", "A075", "im025"])[0]
    # sim_name = rdu.get_list_of_sim_names_with_phrases("/mnt/d/imbalanced_turbulence/data_reduced/", ["768cube", "dcorr029", "A075", "im10"])[0]
    ph = path_handler(sim_name, system_config)
    save_elsaesser_quantities(ph, **kwargs)
