
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_xy(it,iz,spec,sym):

    if it=='':
       it='0'

    if iz=='':
       iz='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]

    #===============================================================================
    # The grid
    x=numpy.loadtxt(".././data/x.dat")
    y=numpy.loadtxt(".././data/y.dat")

    # The density
    map0=numpy.loadtxt(".././data/densities/mapxyz_"+spec+"_drift0.dat")
    map_temp=numpy.loadtxt(".././data/densities/mapxyz_"+spec+"_"+sym+it+".dat")
    
    mapxy=numpy.empty((len(y),len(x)))
    
    for ix in range(0,len(x)):
        for iy in range(0,len(y)):
            mapxy[iy,ix]=map_temp[iy+int(iz)*len(y),ix]

    mapxy=mapxy/numpy.max(map0)

    plt.pcolormesh(x/rho,y/rho,mapxy)
    plt.xlabel(r'$x/\rho$',fontsize=18)
    plt.ylabel(r'$y/\rho$',fontsize=18)
    plt.xlim([numpy.min(x/rho),numpy.max(x/rho)])
    plt.ylim([numpy.min(y/rho),numpy.max(y/rho)])
    
    plt.colorbar().ax.set_ylabel(r'$n/n_0$',fontsize=18)
    
    plt.title("time step="+it+"/ z="+iz+"/ Species="+spec+"/ Sym="+sym,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_xy(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
