import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from scipy import optimize
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from compare_handler import *
import itertools

small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)


def plot_particle_energy_spectrum(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    species = kwargs.get("species", None)
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    annotations = kwargs.get("annotations", True)
    times_to_plot = kwargs.get("times_to_plot", retrieve_time_values(ph)[:, 0])

    # times_to_plot = kwargs.get("times_to_plot", None)
    N = int(np.size(times_to_plot)/10) + 1
    times_to_plot = [int(t) for t in times_to_plot[::N]]
    times_string = "t" + "t".join(map(str, map(int, times_to_plot)))
    # times_to_plot = "[" + ", ".join(map(str, times_to_plot)) + "]"
    colors = itertools.cycle(pl.cm.cividis(np.linspace(0,1,len(times_to_plot))))

    if species not in ['electrons', 'ions']:
        print("Error: species must be one of electrons or ions")
        return
    figdir = ph.path_to_figures + "particle_energy_spectrum/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + species + "_" + times_string
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    if annotations:
        figname += "_annotated"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    consts = rdu.Consts
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    Lz = params["zmax"] - params["zmin"]
    os.chdir(ph.path_to_reduced_data + "particle_spectra/")
    time_values = retrieve_time_values(ph)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel(r'$\gamma$')
    plt.ylabel(r'$\gamma~f(\gamma)$')
    if ylims is not None:
        plt.ylim(ylims)
    else:
        plt.ylim([1e-5, 1e-0])
    if xlims is not None:
        plt.xlim(xlims)
    else:
        plt.xlim([5e1, 2e5])

    times_to_plot_in_LC = []
    for timestep in times_to_plot:
        time_index = (np.abs(timestep - time_values[:, 0])).argmin()
        time_in_LC = time_values[time_index, 2]
        times_to_plot_in_LC.append(time_in_LC)

        # dNdg is f(gamma)
        (dNdg, uBinEdges, step
        ) = rdu.getPtclEnergySpectrum(timestep, species, "bg", "gamma")
        gEdges = np.sqrt(uBinEdges**2+1.)
        gamma_values = 0.5*(gEdges[1:] + gEdges[:-1])
        plt.loglog(gamma_values, gamma_values*dNdg, color=next(colors), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], label=r"$t=${:.2f} $L/c$".format(time_in_LC))

    # Plot f(gamma) \propto \gamma^{-3}
    cut_gamma_values = gamma_values[48:-38]
    plt.loglog(cut_gamma_values, 2.4e5*cut_gamma_values**(-1.7), 'k-.')
    # plt.loglog(cut_gamma_values, 1.3e5*cut_gamma_values**(-1.7), 'k-.')

    if annotations:
        # Fit Maxwell-Juettner to last time step
        max_params, max_e = optimize.curve_fit(maxwellJuettner, gamma_values, dNdg)
        max_fine_gamma = np.linspace(gamma_values[0], gamma_values[-1], 1000)
        print(max_params)
        # fit_maxwellian = maxwellJuettner(max_fine_gamma, *max_params)
        # plt.loglog(max_fine_gamma, max_fine_gamma*fit_maxwellian, 'k--')
        plt.gca().axvline([3.0*max_params[1]], color='green', linestyle='-.')
        gamma_max = Lz*consts.e*params["B0"]/(2.0*consts.m_e*consts.c**2.0)
        plt.gca().axvline([gamma_max], color='green', linestyle=':')


    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    # plt.title(ph.sim_name + "\n")
    tick_offset = 1.0/len(times_to_plot)
    tick_locs = np.arange(0.0, 1.0, tick_offset)
    tick_locs = tick_locs + tick_offset/2.0
    cmap = pl.cm.get_cmap('cividis', np.size(tick_locs))
    colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=cmap), ticks=tick_locs)
    colorbar1.set_label(r"$tc/L$")
    colorbar1.ax.set_yticklabels(["{:.2f}".format(t) for t in times_to_plot_in_LC])
    # plt.legend(frameon=False)
    # plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figname)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()


def maxwellJuettner(gamma_values, A, Theta):
    # NOTE: need gamma^2 because getPtclAngularSpectrumIntegratedOverAngle does not
    # include the radius^2 when integrating over 3D
    return A*gamma_values**2.0/Theta**3.0*np.exp(-gamma_values/Theta)


if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"
    species = 'electrons'

    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    category = "768Imbalance-comparison"
    sim_name = comparison(category).configs[0]

    if stampede2:
        system_config = "stampede2"
    ph = path_handler(sim_name, system_config)
    times_to_plot = retrieve_time_values(ph)[:-15, 0]
    xlims = None
    ylims = None
    annotations = True

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["species"] = species
    kwargs["times_to_plot"] = times_to_plot
    kwargs["xlims"] = xlims
    kwargs["ylims"] = ylims
    kwargs["annotations"] = annotations

    plot_particle_energy_spectrum(ph, **kwargs)
