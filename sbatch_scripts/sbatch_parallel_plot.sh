#!/bin/bash
#SBATCH --time=10:00:00
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --job-name=768evavg
#SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_parallel.%j
#SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_parallel.%j
#SBATCH -A TG-PHY160032
#SBATCH --mail-type=ALL
#SBATCH --mail-user=lia.hankla@gmail.com
# #SBATCH --begin=now+3hours

# Order of the module load is important!
# module purge is also required!
module purge
module load intel/18.0.2
module load hdf5/1.10.4
module load python3/3.7.0

script='parallel_plot_elsasser_variables_768'

scriptpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/log_${script}.txt"
fname="${script}.py"

cd ${scriptpath}
pwd
date
module list

python3 -u ${fname} > ${logpath}

date
