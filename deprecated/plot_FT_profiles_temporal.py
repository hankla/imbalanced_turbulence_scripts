import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from scipy.fftpack import fft, fftfreq, fftshift
from matplotlib.ticker import FixedLocator

"""
This script will plot the fourier transform of temporal profiles at the location (0, 0, 0)
of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - extend to other locations
"""

def plot_FT_profiles_temporal(ph, **kwargs):
    # ------------------------------------------
    fields_to_plot = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    sim_name = ph.sim_name
    overwrite = kwargs.get("overwrite", False)
    log_yscale = kwargs.get("log_yscale", False)
    to_show = kwargs.get("to_show", False)
    only_after_decay = kwargs.get("only_after_decay", False)
    only_before_decay = kwargs.get("only_before_decay", False)
    ind_times_to_FT = kwargs.get("ind_times_to_FT", None) # a tuple with starting/ending indices.
    average_2d = kwargs.get("average_2d", False)
    # ------------------------------------------
    if to_show:
        matplotlib.use('TkAgg')
    figdir = ph.path_to_figures + "profiles-1d_temporal/"
    if average_2d:
        figdir += "2d_averaged/"
    figdir += "FT/"
    if log_yscale:
        figdir += "log_yscale/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    os.chdir(ph.path_to_reduced_data)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)

    sigmaDict = rdu.getSimSigmas(params)
    vA = sigmaDict["vAoverC"]
    # Get times to transform.
    tit_add = ""
    time_steps = retrieve_time_values(ph)[:, 0]
    if ind_times_to_FT is None:
        if "NDECAY" not in params:
            start_ind = 0
            end_ind = -1
            start_time_step = 0
            end_time_step = int(time_steps[-1])
            tit_add = " (entire duration)"
        elif only_after_decay:
            NDECAY = params["NDECAY"]
            start_ind = (np.abs(time_steps - NDECAY)).argmin()
            end_ind = -1
            start_time_step = int(NDECAY)
            end_time_step = int(time_steps[-1])
            tit_add = " (only after decay)"
        elif only_before_decay:
            start_ind = 0
            NDECAY = params["NDECAY"]
            end_ind = (np.abs(time_steps - NDECAY)).argmin()
            start_time_step = 0
            end_time_step = int(NDECAY)
            tit_add = " (only before decay)"
        else:
            start_ind = 0
            start_time_step = 0
            end_time_step = int(time_steps[-1])
            end_ind = -1
            tit_add = " (entire duration)"
            # tit_add = " between $t=${:.2f} and {:.2f} $L/c$".format(times_in_LC[start_ind], times_in_LC[end_ind])
    else:
        start_ind = ind_times_to_FT[0]
        end_ind = ind_times_to_FT[1]
    times_in_LC = times_in_LC[start_ind:end_ind]

    # vAnr = B0/(np.sqrt(4*np.pi*params['Drift. dens.']*consts.m_e*consts.c^2))

    # Note: don't divide by Lz because in units of Lz=1
    omegaA = 2*np.pi*vA
    if "omegaD" in params:
        omegaD = params["omegaD"]
    else:
        omegaD = omegaA*kwargs.get("omegaD", 0.6/np.sqrt(3.0))
    minorlocator = [omegaA, omegaD]
    minorloclab = [r"$\omega_A={:.2f}~c/L$".format(omegaA), r"$\omega_D={:.2f}~c/L$".format(omegaD)]

    print("Plotting " + sim_name + " Fourier transform of temporal profiles")
    # test_data = 0.1/B0*np.sin(omegaA*times_in_LC)

    profile_data = retrieve_profiles_temporal(ph, **kwargs)
    time_interval = times_in_LC[1] - times_in_LC[0]

    for field in fields_to_plot:
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        figname = figdir + field + "_FT-in-time{:d}-{:d}_x0y0z0.png".format(start_time_step, end_time_step)
        if average_2d:
            profile = np.array(profile_data[field]["xAyAz0"])/norm_value[0]
        else:
            profile = np.array(profile_data[field]["x0y0z0"])/norm_value[0]
        profile = profile[start_ind:end_ind]

        N = profile.size
        frequencies = fftfreq(N, time_interval)
        profile_FT = fft(profile)
        # profile_FT = fft(test_data)
        frequencies = 2*np.pi*fftshift(frequencies)
        profile_FT_abs_norm = 1.0/N * np.abs(fftshift(profile_FT))**2.0

        if omegaA > frequencies[-1]:
            print("Warning: FDUMP was too large to capture the Alfven frequency.")

        plt.figure()
        plt.ylabel("Power [arb. units]")
        plt.xlabel(r"Angular Frequency $\omega~[c/L]$")
        plt.plot(frequencies[N//2:], profile_FT_abs_norm[N//2:], marker="o", lw=2, markersize=2)
        if log_yscale:
            plt.yscale('log')
        plt.xscale('log')
        titstr = sim_name + "\n FT of temporal profile taken at "
        if average_2d:
            titstr += "$z=0$, averaged over $x$ and $y$"
        else:
            titstr += "$(x,y,z)=(0,0,0)$"
        titstr += "\n" + field_abbrev + norm_value[1]
        titstr += " between times {:.2f} and {:.2f} $L/c$".format(times_in_LC[0], times_in_LC[-1])
        titstr += tit_add
        plt.title(titstr)

        # Add labels
        ax = plt.gca()
        ax.axvline([omegaA], color="black", ls=":", label=r"$\omega_A=${:.2f} $c/L$".format(omegaA))
        ax.axvline([omegaD], color="black", ls="-.", label=r"$\omega_D=${:.2f} $c/L$".format(omegaD))
        # ax.tick_params('x', which='minor', labeltop=True, labelbottom=False, top=True)
        # ax.xaxis.set_minor_locator(FixedLocator(minorlocator))
        # ax.set_xticklabels(minorloclab, minor=True)
        plt.gca().grid(color='.9', ls='--')
        ax.tick_params(top=True, right=True, direction='in', which='both')
        plt.legend()

        plt.tight_layout()

        if to_show:
            plt.show()
        else:
            print("Saving figure: " + figname)
            plt.savefig(figname, bbox_inches="tight")

        plt.close()




if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False

    # sim_name = "zeltron_96cube-L1_mode-2zco-Jx_sigma05dcorr0omega075-035A025phi0_PPC32-FD50"
    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im00sigma05dcorr0omega035A075phi0_PPC32-FD30-decay2500"
    # sim_name = "test_data"
    sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"

    omegaD = 0.6/np.sqrt(3.0)
    # omegaD = None

    # kwargs
    overwrite = False
    to_show = False
    average_2d = False

    zVars = rdu.Zvariables.options
    # fields_to_plot = zVars[:]
    fields_to_plot = ["Bx", "By", "velx_total", "vely_total"]

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    kwargs = {}
    kwargs["fields_to_profile"] = fields_to_plot
    kwargs["to_show"] = to_show
    kwargs["overwrite"] = overwrite
    kwargs["average_2d"] = average_2d
    # kwargs["only_after_decay"] = False
    # kwargs["ind_times_to_FT"] = (10, 100)
    if omegaD is not None: kwargs["omegaD"] = omegaD
    plot_FT_profiles_temporal(ph, **kwargs, log_yscale=True)
    # plot_FT_profiles_temporal(ph, **kwargs, log_yscale=True, only_after_decay=True)
    # plot_FT_profiles_temporal(ph, **kwargs, log_yscale=True, only_before_decay=True)
    plot_FT_profiles_temporal(ph, **kwargs, log_yscale=False)
