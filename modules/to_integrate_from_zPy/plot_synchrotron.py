
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_synchrotron(it,spec,sym):

    if it=='':
       it='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # Frequency-array
    nu=numpy.loadtxt(".././data/nu.dat")
    eps1=nu*4.1356701e-15
    
    # Synchrotron spectrum
    nuFnu=numpy.loadtxt(".././data/synchrotron_iso_"+spec+"_"+sym+it+".dat")
    
    plt.plot(eps1,nuFnu,color='blue',lw=2)
    plt.xscale('log')
    plt.yscale('log')

    plt.xlabel(r'$\epsilon_1$ [eV]',fontsize=20)
    plt.ylabel(r'$\nu F_{\nu}$ [A.U.]',fontsize=20)
    plt.xlim([numpy.min(eps1),numpy.max(eps1)])
    plt.ylim([1e-4*numpy.max(nuFnu),3.0*numpy.max(nuFnu)])
    
    plt.title("time step="+it+"/ Species="+spec+"/ Sym="+sym , fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_synchrotron(sys.argv[1],sys.argv[2],sys.argv[3])
