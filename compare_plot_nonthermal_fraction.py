import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import comparison, get_comparison_Avar_as_Bvar,get_comparison_stats
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe
from fractions import Fraction

small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)

"""
This script will fit particle distributions for each simulation to a Maxwell-Juettner
and subtract from the full distribution to find the amount of energy and number of particles
that are nonthermal. It will plot the fraction of nonthermal energy/total energy and the fraction
of particles with nonthermal energy/total number of particles.
"""


def compare_plot_nonthermal_fraction(category, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    species = kwargs.get("species", "electrons")
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    ylog = kwargs.get("ylog", True)
    plot_final_Einj = kwargs.get("plot_final_Einj", False)
    if plot_final_Einj:
        final_Einj = kwargs.get("final_Einj")

    if species not in ['electrons', 'ions']:
        print("Error: species must be one of electrons or ions")
        return

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/particle_energy_partition/" + species + "/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "nonthermal_energy_fraction_time-evolution"
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    if ylog:
        figname += "_yLog"
    if plot_final_Einj:
        figname += "_Einj"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    labels = comparison(category).labels
    colors = comparison(category).colors
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    fig, ax = plt.subplots(2, 1, sharex=True)
    # plt.figure()
    plt.xlabel(r'$tc/L$')
    title = species + " spectrum" + "\n"
    title += category + "\n"
    plt.suptitle(title)
    ax[0].set_ylabel(r'$N_{\rm nonthermal}/N_{\rm total}$')
    ax[1].set_ylabel(r'$\langle\mathcal{E}_{\rm nonthermal}\rangle/\langle\mathcal{E}_{\rm pl}\rangle$')
    if ylog:
        ax[0].set_yscale('log')
        ax[1].set_yscale('log')
    if ylims is not None:
        ax[0].set_ylim(ylims)
        ax[1].set_ylim(ylims)
    if xlims is not None:
        ax[0].set_xlim(xlims)
        ax[1].set_xlim(xlims)

    for i, sim_name in enumerate(configs):
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, system_config)

        timesteps = retrieve_time_values(ph)[:, 0]
        times_in_LC = retrieve_time_values(ph)[:, 2]

        os.chdir(ph.path_to_reduced_data + "particle_spectra/")

        energy_fraction = []
        particle_fraction = []
        particle_number = []

        for timestep in timesteps:
            timestep = int(timestep)

            # load spectrum
            (dNdu, u_edges, step
            ) = rdu.getPtclEnergySpectrum(timestep, species, "bg", False)

            # (dNdg, uBinEdges, step
            # ) = rdu.getPtclEnergySpectrum(timestep, species, "bg", "gamma")
            # gamma-array
            u_centers = 0.5*(u_edges[1:] + u_edges[:-1])
            du = np.diff(u_edges)
            gamma_edges = np.sqrt(u_edges**2+1.)
            gamma_centers = 0.5*(gamma_edges[:-1] + gamma_edges[1:])
            dgamma = np.diff(gamma_edges)

            # Normalize dNdu for a better fit
            dNdu_norm = dNdu/(dNdu*du).sum()

            # cut off zero values: EXTREMELY important for goodness of fit
            zero_particles_indices = np.where(dNdu == 0)[0]
            max_ind = zero_particles_indices.max()
            min_ind = zero_particles_indices.min()
            while max_ind - min_ind > np.size(zero_particles_indices) - 1:
                zero_particles_indices = zero_particles_indices[1:]
                min_ind = zero_particles_indices.min()
            end_ind = zero_particles_indices[0]
            dNdu_norm = dNdu_norm[:end_ind]
            gamma_centers = gamma_centers[:end_ind]
            du = du[:end_ind]
            dgamma = dgamma[:end_ind]
            u_centers = u_centers[:end_ind]

            # Fit Maxwell-Juettner, only low- to medium-energies
            max_fit_ind = dNdu_norm.argmax()
            max_params, max_e = optimize.curve_fit(maxwellJuettner, gamma_centers[:max_fit_ind+10], dNdu_norm[:max_fit_ind+10])
            fit_maxwellian = maxwellJuettner(gamma_centers, *max_params)

            # # Plot to check the fit
            # plt.loglog(gamma_centers, gamma_centers*fit_maxwellian)
            # plt.loglog(gamma_centers, gamma_centers*dNdu_norm)
            # plt.show()

            # Location of the Maxwellian peak
            peak_index = fit_maxwellian.argmax()
            # Get nonthermal particles - only above peak
            nonthermal_particles = dNdu_norm - fit_maxwellian
            nonthermal_particles[:peak_index] = 0
            nonthermal_particles = np.clip(nonthermal_particles, a_min=0, a_max=None)

            # Get number fraction of nonthermal particles/total particles
            # NOTE: does not include volume element! Should be fine since
            # doing same for both
            number_total_particles = (du*dNdu_norm).sum()
            number_nonthermal_particles = (du*nonthermal_particles).sum()
            fraction_nonthermal_particles = number_nonthermal_particles/number_total_particles

            # Get energy fraction
            energy_in_maxwellian = (du * fit_maxwellian * u_centers).sum()
            energy_in_nonthermal = (du * nonthermal_particles * u_centers).sum()
            total_energy = energy_in_maxwellian + energy_in_nonthermal
            fraction_nonthermal_energy = energy_in_nonthermal/total_energy

            energy_fraction.append(fraction_nonthermal_energy)
            particle_fraction.append(fraction_nonthermal_particles)
            particle_number.append(number_total_particles)

        particle_number = np.array(particle_number)
        # print((particle_number == particle_number[0]).all())
        # print(np.allclose(particle_number, particle_number[0], rtol=1e-6))

        ax[0].plot(times_in_LC, particle_fraction, lw=2, label = labels[i], color=colors[i], path_effects=[mpe.Stroke(linewidth=3, foreground='k'), mpe.Normal()])
        ax[1].plot(times_in_LC, energy_fraction, lw=2, label = labels[i], color=colors[i], path_effects=[mpe.Stroke(linewidth=3, foreground='k'), mpe.Normal()])
        if plot_final_Einj:
            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
            einj_cum = np.cumsum(einj)
            dt_in_s = params["dt"]
            Lx = params["xmax"] - params["xmin"]
            Ly = params["ymax"] - params["ymin"]
            Lz = params["zmax"] - params["zmin"]
            volume = Lx*Ly*Lz
            mag_energy_density = (params["B0"])**2.0/(8.0*np.pi)
            einj_norm = einj_cum/volume/mag_energy_density
            dt_in_LC = dt_in_s*rdu.Consts.c/Lz
            einj_times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
            einj_times_in_LC = np.insert(einj_times_in_LC, 0, 0)
            einj_times_in_LC = einj_times_in_LC[:-1]
            equivalent_time_ind = (np.abs(-1.0*einj_norm - final_Einj)).argmin()
            einj_equiv_time = einj_times_in_LC[equivalent_time_ind]
            equivalent_time_ind = (np.abs(times_in_LC - einj_equiv_time)).argmin()
            equivalent_time = times_in_LC[equivalent_time_ind]
            n_value_at_teq = particle_fraction[equivalent_time_ind]
            e_value_at_teq = energy_fraction[equivalent_time_ind]
            print(ph.sim_name + " particle fraction: {:.3f}, energy fraction: {:.3f}".format(n_value_at_teq, e_value_at_teq))
            ax[0].plot(equivalent_time, n_value_at_teq, label=None, color='red', markersize=5, marker='o')
            ax[1].plot(equivalent_time, e_value_at_teq, label=None, color='red', markersize=5, marker='o')

    ax[0].tick_params(top=True, right=True, direction='in', which='both')
    ax[0].grid(color='.9', ls='--')
    ax[1].tick_params(top=True, right=True, direction='in', which='both')
    ax[1].grid(color='.9', ls='--')

    sim_set = comparison(category)
    if sim_set.cbar_label is not None:
        # fig.subplots_adjust(right=0.8)
        # cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks, ax=ax.ravel().tolist())
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False, ncol=sim_set.ncol)
    else:
        plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)

    # plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.suptitle('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        ax[0].grid(False, which='both')
        ax[1].grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()


def maxwellJuettner(gamma_values, A, Theta):
    # NOTE: need gamma^2 because getPtclAngularSpectrumIntegratedOverAngle does not
    # include the radius^2 when integrating over 3D
    return A*gamma_values**2.0/Theta**3.0*np.exp(-gamma_values/Theta)


if __name__ == '__main__':
    # ------- inputs ---------
    category = "xiAllboxSizeAll-comparison"
    category = "xi10boxSize-comparison"
    category = "768Imbalance-comparison"
    # category = "modeNum-comparison"
    stampede2 = False
    system_config = "lia_hp"
    species = 'electrons'
    plot_final_Einj = False

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["species"] = species
    kwargs["stampede2"] = stampede2
    kwargs["system_config"] = system_config
    # kwargs["ylims"] = [0, 0.7]
    kwargs["ylog"] = False

    # get minimum final Einj
    if plot_final_Einj:
        stats = get_comparison_stats(category, **kwargs)
        final_einj_densities = stats.loc["einj_density_at_20_Lc"]
        final_Einj = final_einj_densities.min()
        kwargs["final_Einj"] = final_Einj
        kwargs["plot_final_Einj"] = plot_final_Einj

    compare_plot_nonthermal_fraction(category, **kwargs)
