"""
This script will plot slices of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - from create_slices-2d_cross-helicity: need to implement subtracting vel/B means
"""
import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import shutil
import glob
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from plot_slices import plot_slice
from plot_profiles_spatial import plot_profiles_spatial
from plot_profiles_temporal import plot_profiles_temporal
from plot_volume_averages import plot_volume_average
from plot_length_scales_over_time import plot_length_scales
from plot_energy_conservation import plot_energy_conservation
from plot_flux_over_time import plot_flux_over_time
from plot_magnetic_energy_spectrum import plot_magnetic_energy_spectrum
from plot_injected_energy import plot_injected_energy
from plot_FT_vol_avg_over_time import plot_FT_volume_averages_over_time
from plot_FT_profiles_temporal import plot_FT_profiles_temporal
from plot_FT_profiles_spatial import plot_FT_profiles_spatial
from plot_particle_energy_spectrum import plot_particle_energy_spectrum
from plot_elsaesser_quantities import plot_elsaesser_quantities
from plot_angular_energy_distribution import plot_angular_energy_distribution
from plot_positive_vz_fraction import plot_positive_vz_fraction
from plot_energy_efficiencies import plot_energy_efficiencies

# ----------------------------------
#    INPUTS
# ----------------------------------
stampede2 = True
system_config = "lia_hp"

zVars = rdu.Zvariables.options + rdu.Zvariables.restFrame_options
fields_to_vavg = zVars
all_fields = fields_to_vavg + rdu.Zvariables.restFrame_options_NOvavg

# fields_to_profile = ["elsaesserMinusy", "elsaesserPlusy", "velz_total", "Ex", "By", "Bx", "Bz", "vely_total", "velx_total"]
fields_to_profile = ["velz_total", "Ex", "By", "Bx", "Bz", "vely_total", "velx_total"]
fields_to_FT = fields_to_profile
# fields_to_slice = ["Jmag", "Jtot_x", "Jtot_y", "Jtot_z"] + fields_to_profile
# fields_to_slice = ["pelsaesserDifference", "VpBp_total", "Jmag", "Jtot_z", "pelsaesserMinusEnergy", "pelsaesserPlusEnergy"]
fields_to_slice = ["VpBp_total", "Jmag"]
# fields_to_vavg = zVars
# fields_to_vavg = ["EdotJf", "pelsaesserEnergy", "pelsaesserDifference", "pelsaesserPlusEnergy", "pelsaesserMinusEnergy", "velz_total", "VpBp_total", "deltaBrms", "deltaBrms2", "vely_total", "velx_total", "internalEnergyDensity"]
# fields_to_vavg = ["electromagneticEnergyDensity", "deltaBrms2vA", "Uprms", "netEnergyDensity", "turbulentEnergyDensity", "internalEnergyDensity", "fluidEnergyDensity", "velz_total", "VpBp_total", "ExB_z", "deltaBrms", "deltaBrms2"]
# fields_to_vavg = ["electromagneticEnergyDensity", "netEnergyDensity", "turbulentEnergyDensity", "internalEnergyDensity", "fluidEnergyDensity", "Upz_total", "ExB_z", "velz_total", "magneticEnergyDensity", "electricEnergyDensity"]
fields_to_vavg = ["density3_electrons"]

t_start_ind = 0
t_end_ind = -1

ix = 0; iy = 0; iz = 0
overwrite_fig = True
overwrite_data = False
overwrite_Eheat = False
logScale = False

plot_vavgs = True
plot_energy = False
plot_angular = False
plot_pvz_fraction = False
plot_spectra = False
plot_profiles = False
plot_FT = False
plot_slices = False
only_stds = True
only_means = False

if stampede2:
    system_config = "stampede2"
    scratch_dir = "/scratch/06165/ahankla/imbalanced_turbulence/"

    list_of_sims = []
    list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "dcorr029", "A075", "seedstudy"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "384cube", "seedstudy5", "A075", "im10"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "dcorr029", "A075"])
    # list_of_sims = list_of_sims + rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "dcorr029", "A075", "im10"])
    # list_of_sims = list_of_sims[::-1]
    omegaD = 0.6/np.sqrt(3.0)*np.ones(len(list_of_sims))

else:
    scratch_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
    list_of_sims = rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["Jxyz_", "2zopp"])
    omegaD = 0.6/np.sqrt(3.0)*np.ones(len(list_of_sims))

# ----------------------------------
#    PLOTTING (NO EDITS REQUIRED)
# ----------------------------------
kwargs = {}
kwargs["overwrite_data"] = overwrite_data
kwargs["overwrite_Eheat"] = overwrite_Eheat
kwargs["logScale"] = logScale
kwargs["overwrite_fig"] = overwrite_fig
kwargs["fields_to_profile"] = fields_to_profile
kwargs["fields_to_vavg"] = fields_to_vavg
kwargs["only_stds"] = only_stds
kwargs["only_means"] = only_means
kwargs["ix"] = ix
kwargs["iy"] = iy
kwargs["iz"] = iz

for i, sim_name in enumerate(list_of_sims):
    print("******************************************")
    print("******************************************")
    print("Plotting simulation " + sim_name)

    # -------------------------------------------
    # Set paths
    ph = path_handler(sim_name, system_config)

    time_steps = retrieve_time_values(ph, overwrite_data)[:, 0]
    times_to_plot = time_steps[t_start_ind:t_end_ind]
    # times_to_plot = [0, 7200]
    # times_to_plot = [0, 21600]
    # times_to_plot.append(21600)
    # -------------------------------------------
    # Next step is to make sure the simulation parameters
    # are accessible from the reduced data path
    files_to_copy = ["phys_params.dat", "input_params.dat", "Eem.dat", "Eifluid.dat", "Eefluid.dat", "Einj.dat", "Ekin_ions_bg.dat", "Ekin_electrons_bg.dat", "divE.dat"]
    if stampede2:
        for file1 in files_to_copy:
            shutil.copyfile(ph.path_to_raw_data + file1, ph.path_to_reduced_data + file1)
        print("Files copied to reduced data folder.")
    # -------------------------------------------
    # Now, transfer the spectrum data to the reduced data path
    if stampede2:
        spectra_path = ph.path_to_raw_data
        destination_dir = ph.path_to_reduced_data + "particle_spectra/"
        if not os.path.exists(destination_dir):
            os.makedirs(destination_dir)
        for spectrum in glob.glob(spectra_path + "spectrum_electrons_bg_*.h5"):
            shutil.copy(spectrum, destination_dir)
        for spectrum in glob.glob(spectra_path + "spectrum_ions_bg_*.h5"):
            shutil.copy(spectrum, destination_dir)
        print("Spectra copied to reduced data folder.")


    # ---------------------------------------------
    if plot_vavgs:
        plot_volume_average(ph, **kwargs)
        plot_energy_efficiencies(ph, **kwargs)
        # plot_elsaesser_quantities(ph, **kwargs)

    if plot_energy:
        plot_length_scales(ph, **kwargs)
        plot_energy_conservation(ph)
        # injected energy over time
        plot_injected_energy(ph, **kwargs)

    if plot_angular:
        for timestep in times_to_plot[::10]:
            timestep = int(timestep)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=30, gamma_end_ind=40)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=40, gamma_end_ind=50)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=50, gamma_end_ind=60)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=60, gamma_end_ind=70)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=70, gamma_end_ind=80)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=80, gamma_end_ind=90)
            plot_angular_energy_distribution(ph, **kwargs, timestep=timestep, gamma_start_ind=90, gamma_end_ind=-1)

    if plot_pvz_fraction:
        plot_positive_vz_fraction(ph, **kwargs, gamma_start_ind=30, gamma_end_ind=40)
        plot_positive_vz_fraction(ph, **kwargs, gamma_start_ind=40, gamma_end_ind=50)
        plot_positive_vz_fraction(ph, **kwargs, gamma_start_ind=50, gamma_end_ind=60)

    if plot_spectra:
        # kinetic energy spectrum
        # need spectrum_electrons_bg_0.h5 etc for this
        # plot_particle_energy_spectrum(ph, times_to_plot=times_to_plot, species='both')
        # plot_particle_energy_spectrum(ph, times_to_plot=times_to_plot, species='both', xlims=[10, 1e5], ylims=[1.0e-3, 1])
        # plot_particle_energy_spectrum(ph, times_to_plot=time_steps[0:10], species='both')
        # plot_particle_energy_spectrum(ph, times_to_plot=time_steps[0:10], species='both', xlims=[10, 1e5], ylims=[1.0e-3, 1])

        # magnetic energy spectrum
        # will output to reduced data so doesn't have to be on stampede2
        plot_magnetic_energy_spectrum(ph, times_to_plot=times_to_plot)
        # plot_magnetic_energy_spectrum(ph, times_to_plot=time_steps[0:10])

    if plot_profiles:
        plot_profiles_spatial(ph, times_to_plot, **kwargs)
        plot_profiles_spatial(ph, times_to_plot, **kwargs, average_2d=True)
        plot_profiles_temporal(ph, **kwargs)
        plot_profiles_temporal(ph, **kwargs, average_2d=True)

    if plot_FT:
        # Plot volume averages first. Remember <Ex> etc doesn't mean anything! Only vz.
        # plot_FT_volume_averages_over_time(ph, omegaD=omegaD[i], **kwargs)
        plot_FT_volume_averages_over_time(ph, omegaD=omegaD[i], log_yscale=True, **kwargs)
        # plot_FT_volume_averages_over_time(ph, omegaD=omegaD[i], log_yscale=True, only_after_decay=True, **kwargs)
        # plot_FT_volume_averages_over_time(ph, omegaD=omegaD[i], log_yscale=True, only_before_decay=True, **kwargs)
        # plot_FT_volume_averages_over_time(ph, omegaD=omegaD[i], log_yscale=True, detrend=True, **kwargs)
        # Plot FT of profiles.
        # plot_FT_profiles_temporal(ph, omegaD=omegaD[i], **kwargs)
        # plot_FT_profiles_temporal(ph, omegaD=omegaD[i], log_yscale=True, **kwargs)
        plot_FT_profiles_temporal(ph, omegaD=omegaD[i], log_yscale=True, **kwargs, average_2d=True)
        # plot_FT_profiles_temporal(ph, omegaD=omegaD[i], log_yscale=True, only_before_decay=True, **kwargs)
        # plot_FT_profiles_temporal(ph, omegaD=omegaD[i], log_yscale=True, only_after_decay=True, **kwargs)

        # plot_FT_profiles_spatial(ph, **kwargs, log_yscale=True)
        plot_FT_profiles_spatial(ph, **kwargs, log_yscale=True, average_2d=True)
        # plot_FT_profiles_spatial(ph, **kwargs, log_yscale=False)

    if plot_slices:
        for time in times_to_plot[::10]:
            plot_slice(ph, time, fields_to_plot=fields_to_slice,
                       ix=ix, iy=iy, iz=iz, overwrite=overwrite_fig, logScale=logScale)
    # Plot fluxes
    # for quantity in rdu.Zvariables.flux_options:
        # plot_flux_over_time(ph, quantity=quantity, ix=0, iy=None, iz=None)
        # plot_flux_over_time(ph, quantity=quantity, ix=None, iy=0, iz=None)
        # plot_flux_over_time(ph, quantity=quantity, ix=None, iy=None, iz=0)
