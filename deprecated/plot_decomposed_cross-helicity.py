import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *


def plot_decomposed_cross_helicity(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    linestyle = kwargs.get("linestyle", "-")
    restFrame = kwargs.get("restFrame", False)
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    B0 = params["B0"]

    figpath = ph.path_to_figures + "cross-helicity/"
    figdir = figpath
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "decomposed_cross-helicity"
    if restFrame:
        figname += "_restFrame"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    times_in_LC = retrieve_time_values(ph)[:, 2]
    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel("$tc/L$")
    # plt.gca().set_yscale('log')

    norm_value = rdu.getNormValue("VdotB", simDir=ph.path_to_reduced_data)
    (vol_avgs, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
    if restFrame:
        vzbz = vol_avgs["VzBz_total_restFrame"]
        vzdbz = vol_avgs["VzdBz_total_restFrame"]
        vzB0 = np.zeros(vzbz.shape)
        vpbp = vol_avgs["VpBp_total_restFrame"]
        vdotB = vol_avgs["VdotB_total_restFrame"]
    else:
        vzbz = vol_avgs["VzBz_total"]
        vzdbz = vol_avgs["VzdBz_total"]
        vzB0 = vol_avgs["velz_total"]*B0
        vpbp = vol_avgs["VpBp_total"]
        vdotB = vol_avgs["VdotB_total"]

    # plt.plot(times_in_LC, vzbz/norm_value[0], lw=2, color="C0", label=r"$v_{tot,z}B_z}$"+norm_value[1], marker="P", markersize=6, ls=linestyle)
    if not restFrame:
        plt.plot(times_in_LC, vzB0/norm_value[0], lw=2, color="C4", label=r"$v_{tot,z}B_0}$"+norm_value[1], marker="P", markersize=6, ls=linestyle)
    plt.plot(times_in_LC, vzdbz/norm_value[0], lw=2, color="C0", label=r"$v_{tot,z}\delta B_z}$"+norm_value[1], marker="D", markersize=6, ls=linestyle, fillstyle="none", markevery=int(times_in_LC.size/10))
    plt.plot(times_in_LC, vpbp/norm_value[0], lw=2, color="C2", label=r"$v_{tot,\perp}B_\perp}$"+norm_value[1], marker="s", markersize=6, ls=linestyle, fillstyle="none")
    # plt.plot(times_in_LC, (vpbp+vzbz)/norm_value[0], lw=2, color="C3", label=r"$(v_{tot,\perp}B_\perp+v_{tot,z}B_z)$"+norm_value[1], marker="o", fillstyle="none", markersize=6, ls=linestyle, markevery=int(times_in_LC.size/10))
    plt.plot(times_in_LC, vdotB/norm_value[0], lw=2, color="C1", label=r"$\vec v_{tot}\cdot \vec B}$" + norm_value[1], marker="o", markersize=6, ls=linestyle, markevery=int(times_in_LC.size/20), fillstyle="none")

    # print(np.allclose(vdotB, vzbz+vpbp, atol=1e-6))

    titstr = ph.sim_name
    if restFrame:
        titstr += "\n Rest Frame"
    plt.title(titstr, fontsize=20)
    # plt.ylim([0.0001, 30])
    plt.gca().grid(color='.9', ls='--')
    plt.legend()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figname)
        plt.savefig(figure_name)
    plt.close()


if __name__ == '__main__':
    stampede2 = False
    system_config = "lia_hp"
    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["fields_to_vavg"] = ["VdotB_total", "VzBz_total", "VpBp_total"]
    kwargs["overwrite_data"] = False
    # kwargs["restFrame"] = True

    sim_name = "zeltron_96cube-L1_mode-1z-Jx_sigma05dcorr0omega075A025phi0_PPC32-FD50-short"

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, system_config)

    times = retrieve_time_values(ph)
    plot_decomposed_cross_helicity(ph, **kwargs)
