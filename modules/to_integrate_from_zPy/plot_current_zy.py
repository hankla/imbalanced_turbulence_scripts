
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_current_zy(it,ix,current,spec):

    if it=='':
       it='0'

    if ix=='':
       ix='0'

    if current=='':
       current='Jx'

    if spec=='':
       spec='electrons'

    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]

    #===============================================================================
    # The grid
    z=numpy.loadtxt(".././data/zfield.dat")
    y=numpy.loadtxt(".././data/yfield.dat")

    # The current
    map_temp=numpy.loadtxt(".././data/currents/"+current+"_"+spec+"_"+it+".dat")
    mapzy=numpy.empty((len(y),len(z)))

    for iz in range(0,len(z)):
        for iy in range(0,len(y)):
            mapzy[iy,iz]=map_temp[iy+iz*len(y),int(ix)]

    plt.pcolormesh(z/rho,y/rho,mapzy)
    plt.xlabel(r'$z/\rho$',fontsize=18)
    plt.ylabel(r'$y/\rho$',fontsize=18)
    plt.xlim([numpy.min(z/rho),numpy.max(z/rho)])
    plt.ylim([numpy.min(y/rho),numpy.max(y/rho)])
    
    plt.colorbar().ax.set_ylabel(current,fontsize=18)
    
    plt.title("time="+it+"/ x="+ix+"/ "+current+"/ "+spec,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_current_zy(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
