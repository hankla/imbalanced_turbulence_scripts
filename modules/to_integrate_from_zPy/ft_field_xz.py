
import numpy
import numpy.fft
import math
import matplotlib
import matplotlib.pyplot as plt
import sys
import zfileUtil

c=2.99792458e10

def ft_field_xz(it,iy,field):

    if it=='':
       it='0'

    if iy=='':
       iy='0'

    if field=='':
       field='Bx'

    #===============================================================================
    # Parameters of the simulation

    (params, units) = zfileUtil.getSimParams()

    # Nominal cyclotron frequency
    rho=c/params["omegac"]
    B0=params["B0"]

    NY = int(params["NY"])-1

    #===============================================================================
    # The grid
    fieldFile = field + "_" + it
    (mapxz, time, step, coords) = zfileUtil.getFieldSliceXZ(fieldFile, 
      int(iy))

    # get rid of upper (periodic) boundary
    mapxz = mapxz[:-1,:-1]
    (Nx,Nz) = mapxz.shape

    ftmap = numpy.fft.rfft2(mapxz)
    # ftmap.shape = (Nx, Nz//2 + 1)
    # Convert ft to power spectrum
    powSpec = abs(ftmap)**2
    if Nz % 2 == 0: 
      powSpec[:,1:-1] *= 2.
    else:
      powSpec[:,1:] *= 2.
    if Nx % 2 == 0:
      powSpec[1:Nx/2] += powSpec[-1:Nx/2:-1]
      powSpec = powSpec[:Nx/2+1]
    else:
      powSpec[1:Nx/2+1] += powSpec[-1:Nx/2:-1]
      powSpec = powSpec[:Nx/2+1]


    x1d=coords[0]
    z1d=coords[2]
    dkx = 2.*numpy.pi / (x1d[-1] - x1d[0])
    dkz = 2.*numpy.pi / (z1d[-1] - z1d[0])
    kx1d = numpy.arange(Nx/2+1) * dkx
    kz1d = numpy.arange(Nz/2+1) * dkz

    kx, kz = numpy.meshgrid(kx1d, kz1d, indexing='ij')

    # The field
    plt.pcolormesh(rho*kx,rho*kz,powSpec, 
      norm=matplotlib.colors.LogNorm(
        vmin=max(powSpec.min(),powSpec.max()/1e8), vmax=powSpec.max()))
    plt.xlabel(r'$k_x \rho$',fontsize=18)
    plt.ylabel(r'$k_z \rho$',fontsize=18)
    plt.xlim([numpy.min(kx1d*rho),numpy.max(kx1d*rho)])
    plt.ylim([numpy.min(kz1d*rho),numpy.max(kz1d*rho)])
    
    plt.colorbar().ax.set_ylabel(r"$|\tilde{\rm %s}(k_x,k_z)|^2$" % field,
      fontsize=18)
    
    plt.title("time step="+it+"/ iy="+iy+"/ Field="+field,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

if __name__ == "__main__": #{
  if len(sys.argv) != 4: #{
    print "usage: python ft_field_xz.py timestep y-index field"
    print " e.g., python ft_field_xz.py 300 480 Bx"
  else: #}{
    ft_field_xz(sys.argv[1],sys.argv[2],sys.argv[3])
  #}
#}
