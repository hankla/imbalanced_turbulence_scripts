"""
Template file for quickly constructing plots from heatmaps.
Use this for making plots that have a one-to-one correspondence with heatmaps.
"""

import numpy as np
import pylab

import matplotlib.colors as colors

from zpostprocess import zradcalc as zrc
from zpostprocess import zeltio as zio
from zpostprocess import zscriptutil as zsu
from zpostprocess import zutil as zu
from zpostprocess import plotting

TITLE_BASE = "Heatmap"
TITLE_FONT = 24.
AXES_LABEL_SIZE = 15.
AXES_LABEL_COLOR = "k"
AXES_SHADOW_COLOR = "w"

# Note not all PHI and LAM values result in correct heatmaps.
# If it doesn't look right, tune these until it does...
PHI = 0.0 * np.pi / 180
LAM = 0.0 * np.pi / 180
AXIS = zrc.nhat(PHI, LAM)

# Number of angular bins if the angular correlation function is plotted.
NCORRBIN = 30

# Number of digits to report for solid angles
omdig = 1
# Number of digits to report for other floating point quantities.
dig = 2


# "%.3g" == formatg(3)
# "3.14" == formatg(3.14159)
def formatg(numdig, val = None):
    output = "%." + str(numdig) + "g"
    if not (val is None):
        output = output % val
    return output


# Get logarithmic minor tick locations (10 ticks per decade) for plots spanning
# minval to maxval.
def getLogMinorTicks(minval, maxval):
    mintickpow = int(np.floor(np.log10(minval)))
    maxtickpow = int(np.floor(np.log10(maxval)))
    mintickprefix = int(np.ceil(minval / 10**mintickpow))
    maxtickprefix = int(np.floor(maxval / 10**maxtickpow))
    minorticks = np.array([])
    for power in range(mintickpow, maxtickpow + 1):
        if power == mintickpow and power == maxtickpow:
            newticks = np.arange(mintickprefix, maxtickprefix + 1) * 10**power
        elif power == mintickpow and power != maxtickpow:
            newticks = np.arange(mintickprefix, 10) * 10**power
        elif power != mintickpow and power == maxtickpow:
            newticks = np.arange(2, maxtickprefix + 1) * 10**power
        else:
            newticks = np.arange(2, 10) * 10**power
        minorticks = np.hstack( (minorticks, newticks) )

    return minorticks


def setDirectionalAxesLabelsAndLongTicks(ax, shadows = False):
    shadfontbuff = 0
    if shadows:
        try:
            import matplotlib.patheffects as pe
            text = ax.text(0, -0.20, "+z", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            text.set_path_effects([pe.withStroke(linewidth = 3, foreground = "w")])
            text = ax.text(0, np.pi / 2 + 0.2, "+y", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            text.set_path_effects([pe.withStroke(linewidth = 3, foreground = "w")])
            text = ax.text(np.pi / 2, -0.20, "+x", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            text.set_path_effects([pe.withStroke(linewidth = 3, foreground = "w")])
            text = ax.text(-np.pi / 2, -0.20, "-x", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            text.set_path_effects([pe.withStroke(linewidth = 3, foreground = "w")])
            labs = ax.xaxis.get_ticklabels()
            for lab in labs:
                lab.set_path_effects([pe.withStroke(linewidth = 1, foreground = "w")])
        except Exception:
            print "Cannot import matplotlib.patheffects; botching shadows..."
            ax.text(0.03, -0.20 - 0.04, "+z",
                horizontalalignment = "center",
                fontsize = TITLE_FONT + shadfontbuff,
                color = AXES_SHADOW_COLOR)
            ax.text(np.pi / 2 + 0.04, -0.20 - 0.03, "+x",
                horizontalalignment = "center",
                fontsize = TITLE_FONT + shadfontbuff,
                color = AXES_SHADOW_COLOR)
            ax.text(-np.pi / 2 + 0.03, -0.20 - 0.03, "-x",
                horizontalalignment = "center",
                fontsize = TITLE_FONT + shadfontbuff,
                color = AXES_SHADOW_COLOR)

            ax.text(0, -0.20, "+z", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            ax.text(0, np.pi / 2 + 0.2, "+y", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            ax.text(np.pi / 2, -0.20, "+x", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)
            ax.text(-np.pi / 2, -0.20, "-x", horizontalalignment = "center",
                fontsize = TITLE_FONT, color = AXES_LABEL_COLOR)


if __name__ == "__main__":
    usage = "Time-average the distribution functions. Make heatmaps for ranges"
    usage += " of energy bins after the averaging."
    parser = zsu.prepDefaultParser(usage, use_every_dumpstep = True)
    zsu.addBasicPlottingOptions(parser)
    zsu.addBinningOptions(parser)
    plotting.setDefaultPlot(defaultFontSize = TITLE_FONT,
        axesLabelSize = AXES_LABEL_SIZE, mplstyle = "classic")
    parser.add_argument("-t", "--type",
        type = str,
        choices = ["invcompton", "synchrotron", "particle"],
        default = "particle",
        help = "Select the type of data to plot.")
    parser.add_argument("--species",
        type = str,
        default = "electrons",
        choices = ["electrons", "ions", "both"],
        help = "Select particle species.")
    parser.add_argument("--mean_multiple",
        type = float,
        default = None,
        help = "Generate a contour on the equivalent heatmap where the"
            + " intensity is at least 'mean_multiple' times the average.")
    parser.add_argument("--plot_omegax",
        type = float,
        default = None,
        help = "Generate the \Omega_x contour.")
    parser.add_argument("--title_text",
        type = str,
        default = TITLE_BASE,
        help = "Override the default plot title. Does not affect the file name "
            + "if -s specified. The special case --title_text 'time' prints "
            + "the time in light-crossings at the top of the plot.")
    parser.add_argument("--honest_labels",
        default = False,
        action = "store_true",
        help = "Energy bins are beta * gamma. If false, label as gamma.")
    parser.add_argument("--fix_ts_scale",
        default = False,
        action = "store_true",
        help = "Fix the scale for all plots generated for the same timestep.")
    parser.add_argument("--label_omega_nonzero",
        type = float,
        default = None,
        help = "Label what fraction of the nonzero solid angle is occupied by "
            + "the contour defined by --label_omega_nonzero. If not supplied, "
            + "no label is displayed on the plot.")
    parser.add_argument("--label_omega_contour",
        default = False,
        action = "store_true",
        help = "Generate an explicit label describing the value of "
            + "--mean_multiple.  If --mean_multiple not supplied, this has "
            + "no effect.")
    parser.add_argument("--plot_angular_corr",
        default = False,
        action = "store_true",
        help = "Also plot the angular (aka 'spherical') correlation function "
            + "for the heatmap.")
    parser.add_argument("--vminmax",
        nargs = 2,
        type = float,
        default = None,
        help = "Specify maximum and minimum colobar values across all plots.")
    parser.add_argument("--cmap",
        type = str,
        default = "viridis",
        help = "Specify a matplotlib colormap.")
    parser.add_argument("--contour_color",
        type = str,
        default = "w",
        help = "Specify a matplotlib color for the contour.")
    parser.add_argument("--synthsyn",
        action = "store_true",
        default = False,
        help = "Compute a synthetic synchrotron spectrum assuming the "
            + "magnetic field is uniformly distributed in direction across "
            + "the simulation domain and has everywhere the same (upstream) "
            + "magnitude. Only has an effect if '--type synchrotron' is "
            + "supplied.")
    """
    parser.add_argument("--filename",
        type = str,
        default = None,
        help = "Specify a file name for the -s option.")
    """

    args = parser.parse_args()
    start, end = zsu.getEndAndStart(args)
    sim = zio.Simulation(args.simdir)

    steps = sim.timesteps()
    stepdigits = len(str(max(steps)))
    titlebase = args.title_text

    logint = False
    conversion = 1.0
    sig = sim["sigma"]
    grad = sim["gamma_rad_IC"] * np.sqrt(0.1)
    norm = sig
    # hmsym = r"dN / d \Omega"
    hmsym = r"I"
    cbarlab = r"$%s / \langle %s \rangle_{\Omega}$" % (hmsym, hmsym)
    species = [args.species]
    if args.species == "both":
        species = ["electrons", "ions"]
    tstr = "ZPtcl"
    honesty_lab = r"\beta" if args.honest_labels else r""
    spectralsym = "%s \gamma / \sigma" % honesty_lab
    maptype = "angular"
    sym = "bg"
    if args.type == "particle":
        if titlebase == TITLE_BASE:
            titlebase += "_" + tstr
    elif args.type == "synchrotron":
        maptype = "synchrotron"
        tstr = "ZSyn"
        if titlebase == TITLE_BASE:
            titlebase += "_" + tstr
        logint = True
        conversion = zu.H
        b0 = sim["B0 [G]"]
        norm = 3.0 / 2. * zu.E * b0 / (zu.ME * zu.C) * sig**2
        # spectralsym = "\epsilon / \sigma^2 \epsilon_c"
        spectralsym = r"\epsilon / (3 \sigma^2 \omega_{\rm B} / 2)"
        if args.synthsyn:
            synsynther = zrc.SynchIntegrator(b0 = b0, retspec = True)
            tstr = "ZSynthsyn"
    elif args.type == "invcompton":
        maptype = "invcompton"
        tstr = "ZIc"
        if titlebase == TITLE_BASE:
            titlebase += "_" + tstr
        logint = True
        conversion = zu.H
        norm = 4 * sig * sig * sim["icSoftPhotonEnergy [eV]"] * zu.EV_TO_ERG
        # Convert to frequency units.
        norm /= zu.H
        spectralsym = r"\epsilon / 4 \sigma^2 \epsilon_{\mathrm{ph}}"

    sim.prepare(maptype, species[0], sym)

    bins = sim.get_axes(axis = 0)
    binedges = sim.get_axes_edges(axis = 0)
    imins, imaxs = zsu.getIminsAndImaxs(bins, args, norm = norm,
        data = maptype, species = species[0], sym = sym)
    bindigits = max(4, max(len(str(max(imins))), len(str(max(imaxs)))))

    for ts in steps[start:end:args.use_every_dumpstep]:
        sim.set_timestep(ts)
        spec = zrc.Spectrum.fromsim(sim)

        if args.type == "synchrotron" and args.synthsyn:
            # Grab the particle distribution at this timestep
            sim.prepare("angular", species[0], sym)
            sim.set_timestep(ts)
            pspec = zrc.Spectrum.fromsim(sim)
            # Reset the simulation object
            sim.prepare(maptype, species[0], sym)
            sim.set_timestep(ts)
            # Synthesize the synchrotron emission spectrum from the particle
            # distribution.
            spec = synsynther(pspec = pspec, template_spec = spec,
                timing = args.verbose)

        if len(species) == 2:
            sim.prepare(maptype, species[-1], sym)
            spec = spec + zrc.Spectrum.fromsim(sim)
            sim.prepare(maptype, species[0], sym)

        cbarmin = None
        cbarmax = None
        corrmin = None
        corrmax = None
        tscorrfuncs = []
        tscosthes = []
        if args.fix_ts_scale:
            cbarmin = np.inf
            cbarmax = 0.0
            corrmin = np.inf
            corrmax = 0.0
            for imin, imax in zip(imins, imaxs):
                filename = TITLE_BASE + "_" + tstr
                filename += "_ts" + str(ts).zfill(stepdigits)
                filename += "_bins" + str(imin).zfill(bindigits) + "-" + \
                    str(imax).zfill(bindigits)
                print "Calculating axes for figure", filename

                angspec = zrc.Spectrum.intspectral_partial(spec, logaxis0 = True,
                    logint = logint, conversion = conversion, imin = imin,
                    imax = imax)

                lat, lon = angspec.get_axes()
                lat, lon = zu.rad(lat, lon)
                specv = angspec.get_spec()
                specvavg = zrc.Spectrum.intall(angspec) / (4 * np.pi)
                if specvavg != 0:
                    specv = specv / specvavg
                    minval = np.min(specv[specv != 0])
                else:
                    minval = 0.0
                maxval = np.max(specv)

                if minval != 0:
                    cbarmin = min(cbarmin, minval)
                cbarmax = max(cbarmax, maxval)

                if args.plot_angular_corr:
                    corrfunc, costhe = zrc.sphericalCorr(angspec,
                        nbins = NCORRBIN, normed = True)
                    corrmin = min(corrmin, np.min(corrfunc))
                    corrmax = max(corrmax, np.max(corrfunc))
                    tscorrfuncs.append(corrfunc)
                    tscosthes.append(costhe)

        if not (args.vminmax is None):
            cbarmin, cbarmax = args.vminmax

        for imin, imax in zip(imins, imaxs):
            filename = TITLE_BASE + "_" + tstr
            filename += "_ts" + str(ts).zfill(stepdigits)
            filename += "_bins" + str(imin).zfill(bindigits) + "-" + \
                str(imax).zfill(bindigits)
            titletext = titlebase
            if titletext.lower().startswith("time"):
                titletext = r"$t / (L / c) = %s$" % formatg(dig, sim.get_tlc())
            if len(titletext) != 0:
                titletext += "\n"
            titletext += r"$%s \leq %s \leq %s$" \
                % (formatg(dig, binedges[imin] / norm),
                   spectralsym,
                   formatg(dig, binedges[imax] / norm))
            # titletext += "; "
            # titletext += r"$\gamma_{\mathrm{rad}} / \sigma = %.3g$" % (grad / sig)
            if args.verbose:
                print "Making figure", filename

            angspec = zrc.Spectrum.intspectral_partial(spec, logaxis0 = True,
                logint = logint, conversion = conversion, imin = imin,
                imax = imax)

            # TODO: Manipulate 'angspec' to generate the data for the plots.
            lat, lon = angspec.get_axes()
            lat, lon = zu.rad(lat, lon)
            specv = angspec.get_spec()
            # if (specv == 0).all():
            #     if args.verbose:
            #         print "No data. Skipping."
            #     continue
            # else:
            #     if args.verbose:
            #         print
            specvavg = zrc.Spectrum.intall(angspec) / (4 * np.pi)
            if specvavg != 0:
                specv = specv / specvavg
                minval = np.min(specv[specv != 0])
            else:
                minval = 0.0
            maxval = np.max(specv)

            if not (cbarmin is None) and (cbarmin != np.inf):
                minval = cbarmin
                maxval = cbarmax

            phde, lde = angspec.get_axes_edges()
            phre, lre = zu.rad(phde, lde)
            dsinph = np.diff(np.sin(phre))
            dl = np.diff(lre)
            Domega = np.outer(dsinph, dl)
            if not (args.mean_multiple is None):
                idx = np.where(specv >= args.mean_multiple)
                omx4p = np.sum(Domega[idx]) / (4 * np.pi)
                x = np.dot(Domega[idx], specv[idx]) / (4 * np.pi)

            # The total solid angle subtended by nonzero bins.
            om4p = np.sum(Domega[specv > 0.0]) / (4 * np.pi)

            # The total solid angle args.label_omega_nonzero above the
            # mean.
            if not (args.label_omega_nonzero is None):
                idx2 = np.where(specv >= args.label_omega_nonzero)
                omlab4p = np.sum(Domega[idx2]) / (4 * np.pi)

            if not args.plot_angular_corr:
                fig = pylab.figure()
                ax = fig.add_subplot(111, projection = "aitoff")
            else:
                defw, defh = pylab.rcParams["figure.figsize"]
                fig = pylab.figure(figsize = (defw, 2 * defh))
                ax = fig.add_subplot(211, projection = "aitoff")
            # TODO: Make the plots.
            Lat, Lon = np.meshgrid(lat, lon, indexing = "ij")
            cmap = pylab.get_cmap(args.cmap)
            cmap.set_bad('k')
            if specvavg != 0.0:
                pm = ax.pcolormesh(Lon, Lat, specv,
                    norm = colors.LogNorm(vmin = minval, vmax = maxval,
                        clip = True),
                    shading = "flat", cmap = cmap, edgecolors = "face")
            else:
                pm = ax.pcolormesh(Lon, Lat,
                    np.ma.masked_array(specv, mask = True),
                    shading = "flat", cmap = cmap, vmin = -0.1, vmax = 0.1,
                    edgecolors = "face")

            cbar = fig.colorbar(pm, ax = ax)
            # cbar.ax.grid(True, which = "both")

            ax.set_title(titletext, y = 1.18)
            ax.grid(color = "white", linestyle = "--", alpha = 0.6, lw = 1)
            setDirectionalAxesLabelsAndLongTicks(ax, shadows = True)
            cbar.ax.set_ylabel(cbarlab, fontsize = TITLE_FONT)
            # cbar.ax.minorticks_on()
            # Hack to force the colorbar to contain minor ticks.
            if maxval != minval:
                minorticks = getLogMinorTicks(minval, maxval)
                # Note we need to normalize the ticks to the range [0, 1]
                cbar.ax.yaxis.set_ticks(pm.norm(minorticks), minor = True)

            contour = None
            if not (args.mean_multiple is None):
                contour = ax.contour(lon, lat, specv,
                    np.array([args.mean_multiple]),
                    colors = args.contour_color, linewidths = 3)
                xint = int(np.round(x * 100))
                xlab = r"$\Omega_{\rm bf} / 4 \pi = %s$" % \
                    formatg(omdig, omx4p)
                xlab += r"; $%d \%%$ of 'power'" % xint
                if args.label_omega_contour:
                    xlab += "\n"
                    xlab += r"$\Omega$ where "
                    xlab += r"$%s \geq %s\langle %s \rangle_{\Omega}$" \
                        % (hmsym, formatg(dig, args.mean_multiple), hmsym)
                if not (args.label_omega_nonzero is None):
                    xlab += "\n"
                    omxDivOm = omlab4p / om4p
                    if args.label_omega_nonzero != 1.0:
                        xlab += r"$\Omega(%s > %s \langle %s \rangle) /" \
                            % (hmsym, formatg(dig, args.label_omega_nonzero),
                               hmsym)
                    else:
                        xlab += r"$\Omega(%s > \langle %s \rangle) /" \
                            % (hmsym, hmsym)
                    xlab += r"\Omega(%s > 0) = %s$" % (hmsym,
                            formatg(omdig, omxDivOm))
                ax.set_xlabel(xlab, labelpad = 15, fontsize = TITLE_FONT)
            elif not (args.plot_omegax is None):
                omx, cumInt, power = zu.omega50(angspec,
                    frac = args.plot_omegax, ret_integral = True)
                omx4p = omx / (4 * np.pi)
                contour = ax.contour(lon, lat, cumInt,
                    np.array([args.plot_omegax * power]),
                    colors = args.contour_color, linewidths = 3)
                xint = int(np.round(args.plot_omegax * 100))
                xlab = r"$\Omega_{%d} / 4 \pi = %s$" % \
                    (xint, formatg(omdig, omx4p))
                xlab += r"; $%d \%%$ of 'power'" % xint
                ax.set_xlabel(xlab, labelpad = 15, fontsize = TITLE_FONT)

            # Add some path effects to make the contour stand out.
            if not contour is None:
                try:
                    import matplotlib.patheffects as pe
                    pylab.setp(contour.collections, path_effects =
                        [pe.withStroke(linewidth = 4, foreground = "k")])
                except Exception:
                    print("Cannot import path_effects;  no contour shadow.")


            ax.tick_params(labelsize = AXES_LABEL_SIZE)
            cbar.ax.tick_params(labelsize = AXES_LABEL_SIZE)

            if args.plot_angular_corr:
                ax2 = fig.add_subplot(212)
                ax2.set_ylabel("Angular Correlation", fontsize = TITLE_FONT)
                ax2.set_xlabel(r"$\cos \theta$", fontsize = TITLE_FONT)
                if args.fix_ts_scale:
                    i = np.where(imins == imin)[0][0]
                    corrfunc = tscorrfuncs[i]
                    costhe = tscosthes[i]
                else:
                    corrfunc, costhe = zrc.sphericalCorr(angspec,
                        nbins = NCORRBIN, normed = True)
                barxvals = np.hstack( ( costhe[:1], np.repeat(costhe[1:-1], 2),
                    costhe[-1:] ) )
                baryvals = np.repeat(corrfunc, 2)
                xvals = 0.5 * (costhe[1:] + costhe[:-1])
                yvals = corrfunc
                ax2.plot(barxvals, baryvals, 'b-')
                ax2.plot(xvals, yvals, 'bo')
                ax2.set_xlim(costhe.min(), costhe.max())
                if not (corrmin is None) and (corrmin != np.inf):
                    ax2.set_ylim(corrmin, corrmax)

            pylab.tight_layout()

            if args.save:
                filename = zsu.correctFname(filename, args.image_format)
                # print filename
                # import pdb; pdb.set_trace()
                # fig.savefig(filename)
                pylab.savefig(filename)
                pylab.close(fig)

    if not args.save:
        pylab.show()
