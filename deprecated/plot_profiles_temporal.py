import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *

"""
This script will plot temporal profiles at the location (0, 0, 0)
of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - extend to other locations
"""

def plot_profiles_temporal(ph, **kwargs):
    # ------------------------------------------
    fields_to_plot = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    sim_name = ph.sim_name
    overwrite = kwargs.get("overwrite_data", False)
    to_show = kwargs.get("to_show", False)
    to_fit = kwargs.get("to_fit", False)
    average_2d = kwargs.get("average_2d", False)
    # ------------------------------------------
    if to_show:
        matplotlib.use('TkAgg')

    times_in_LC = retrieve_time_values(ph)[:, 2]
    figbase = ph.path_to_figures + "profiles-1d_temporal/"
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    consts = rdu.Consts
    rho = consts.c/params["omegac"]

    print("Plotting " + sim_name + " temporal profiles")

    profile_data = retrieve_profiles_temporal(ph, **kwargs)

    for field in fields_to_plot:
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        if average_2d:
            profile = np.array(profile_data[field]["xAyAz0"])
        else:
            profile = np.array(profile_data[field]["x0y0z0"])
        plt.figure()
        plt.plot(times_in_LC, profile/norm_value[0], markersize=2, marker='o')

        # Figure paths
        if to_fit:
            figdir = figbase + "fits/"
        if average_2d:
            figdir = figbase + "2d_averaged/"
        else:
            figdir = figbase
        if not os.path.isdir(figdir):
            os.makedirs(figdir)

        figname = figdir + field
        if average_2d:
            figname += "_xAyAz0" + ".png"
        else:
            figname += "_x0y0z0" + ".png"

        titstr = sim_name + "\n" + field + "\n Temporal profile "
        if average_2d:
            titstr += "averaged over x and y, at z=0"
        else:
            titstr += "at x=0, y=0, z=0"

        if to_fit:
            from scipy.optimize import curve_fit
            def sin_func(t, a, omega, phi, c):
                return a*np.sin(omega*t+phi)+c
            bounds = ([0, 0, 0, -np.inf], [np.inf, np.inf, 2.0*np.pi, np.inf])
            params, cov = curve_fit(f=sin_func, xdata=times_in_LC, ydata=profile/norm_value[0], bounds=bounds)
            titstr += "\n Fit parameters: amp={:.2f}, phase={:.2f}, offset={:.2f}".format(*params)
            plt.plot(times_in_LC, sin_func(times_in_LC, *params), 'k--', label="Fit")

        plt.title(titstr)
        plt.xlabel("$tL/c$")
        ylabel = field_abbrev + norm_value[1]
        plt.ylabel(ylabel)
        plt.tight_layout()
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')

        if not to_show:
            print("Saving figure: " + figname)
            plt.savefig(figname, bbox_inches="tight")
            plt.close()

    if to_show:
        plt.show()



if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    # sim_name = "zeltron_96cube-L1_mode-1z-Jx_sigma05dcorr0omega075A025phi0_PPC32-FD50"
    # sim_name = "test_data"
    sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"

    # kwargs
    overwrite = False
    to_show = False
    to_fit = False
    average_2d = False

    zVars = rdu.Zvariables.options
    # fields_to_plot = zVars[:]
    fields_to_plot = ["vely_total", "Bx", "By", "velx_total"]
    fields_to_plot = ["ExB_z", "Ey"]

    # ------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    kwargs = {}
    kwargs["fields_to_profile"] = fields_to_plot
    kwargs["to_show"] = to_show
    kwargs["overwrite"] = overwrite
    kwargs["to_fit"] = False
    kwargs["average_2d"] = average_2d
    plot_profiles_temporal(ph, **kwargs)
