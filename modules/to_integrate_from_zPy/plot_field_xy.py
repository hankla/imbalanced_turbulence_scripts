
import numpy
import math
import matplotlib
import zfileUtil
zfileUtil.detectDisplay()
import matplotlib.pyplot as plt
import sys

def plot_field_xy(it,iz,field, **kws):
    consts = zfileUtil.Consts

    reduceRes = 1
    if "reduceRes" in kws:
      reduceRes = kws["reduceRes"]
    if it=='':
       it='0'

    if iz=='':
       iz='0'

    if field=='':
       field='Bx'

    # whether to plot just the picture, with no colorbar, axes, labels...
    picOnly = False
    if "picOnly" in kws and kws["picOnly"]:
      picOnly = True
    #===============================================================================
    # Parameters of the simulation

    (params, units) = zfileUtil.getSimParams()

    iyLo = None
    iyHi = None
    if "iymin" in kws: iyLo = kws["iymin"]
    if "iymax" in kws: iyHi = kws["iymax"]

    #===============================================================================
    # The grid
    # add time step to field
    fieldFile = "+".join([f + "_" + it for f in field.split("+")])
    slices = None
    if reduceRes > 1:
      slices = (slice(None, None, reduceRes), slice(iyLo, iyHi, reduceRes))
    elif iyLo is not None or iyHi is not None:
      slices = (slice(None), slice(iyLo, iyHi))
    (mapxy, time, step, coords) = zfileUtil.getFieldSliceXY(
      fieldFile, int(iz), slices = slices)
    x1d=coords[0]
    y1d=coords[1]
    x, y = numpy.meshgrid(x1d, y1d, indexing='ij')
    
    rho=consts.c/params["omegac"]
    B0=params["B0"]
    nd = params["Drift. dens."]
    nb = params["density ratio"] * nd
    norm = (1., "")
    if '/' in field:
      field = field.split("/")[-1]
    if len(field) == 2 and field[0] in "BE":
      norm = (B0, r"$/B_0$")
    elif len(field) > 8 and field[:6]=="mapxyz":
      if field[-2:] == "bg":
        norm = (nb, r"$/n_{be}$")
      elif field[-5:] == "drift":
        norm = (nb, r"$/n_{be}$")
    elif field[0] == "m": #}{
      norm = (nb, "n_{be}")
    elif field[0] == "r": #}{
      norm = (nb*consts.e, r"$/en_{be}$")
    elif len(field)>3 and field[:3]=="JxB": #}{
      norm = (nb*consts.e*consts.c * B0,"(e n_{be} c B_0)")
    elif field[0] == "J" or field[:-1]=="dblRedJ": #}{
      norm = (nb*consts.e*consts.c, "$(e n_{be} c)$")
    #}

    # The field
    #vmin=-1
    #vmax=1
    vmin = None
    vmax = None
    if ("vmin" in kws): vmin = kws["vmin"]
    if ("vmax" in kws): vmax = kws["vmax"]
    actualMin = mapxy.min()/norm[0]
    actualMax = mapxy.max()/norm[0]
    if (vmin is None): 
      vmin = actualMin
    if (vmax is None): vmax = actualMax
    if (vmin < 0 and vmax > 0 and vmax+vmin < vmax/3.):
      if "vmax" not in kws: vmax = max(vmax, -vmin)
      if "vmin" not in kws: vmin = -vmax

    xmin = None
    xmax = None
    if "xmin" in kws: xmin = kws["xmin"]
    if "xmax" in kws: xmax = kws["xmax"]
    ymin = numpy.min(y)/rho
    ymax = numpy.max(y)/rho
    if "ymin" in kws: ymin = kws["ymin"]
    if "ymax" in kws: ymax = kws["ymax"]
    xRange = (x.max()-x.min())/rho
    yRange = ymax - ymin
    sizefig = False
    autoFigsize = True
    if "figsizeInc" in kws and kws["figsizeInc"] != 1:
      sizefig = True
    rc = matplotlib.rcParams
    figsize = list(rc["figure.figsize"])
    if "figsize" in kws and kws["figsize"] is not None:
      sizefig = True
      autoFigsize = False
      figsize = kws["figsize"]
    if autoFigsize and abs(yRange/xRange -1.) > 0.2: #{
      #figsize = list(rc["figure.figsize"])
      ratio = max(0.75, min(2, yRange/xRange))
      figsize[1] = figsize[1] * ratio
      sizefig = True
    #}
    if sizefig: #{
      if "figsizeInc" in kws and kws["figsizeInc"] != 1:
        mag = kws["figsizeInc"]
        figsize=list(figsize)
        figsize[0] *= mag
        figsize[1] *= mag
      dpi = kws["dpi"] if "dpi" in kws else None
      plt.figure(figsize=figsize, dpi=dpi)
    #}
    #vmin=-2.
    #vmax=2.
    cnorm = None
    logScale = 0
    if "logScale" in kws and kws["logScale"]: #{
      if (vmin <= 0 or kws["logScale"]=="sym"):
        logScale = 2
        # Need to think more about how to do this
        #vAbsMin = abs(x).min()/rho
        #vAbsMax = abs(x).max()/rho
        #linthresh = min(max(vAbsMax/1e4, numpy.sqrt(vAbsMin*vAbsMax)), 
        #                min(vAbsMax, vmax/10.)
        linthresh = 0.1
        linscale = 0.1
        if "linthresh" in kws:
          linthresh = kws["linthresh"]
        if "linscale" in kws:
          linscale = kws["linscale"]
        cnorm = matplotlib.colors.SymLogNorm(
          linthresh=linthresh, linscale = linscale,
          vmin=vmin, vmax=vmax) 
      else:
        logScale = 1
        cnorm = matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax) 
        if vmin >= actualMin: 
          mapxy[mapxy < norm[0]*vmin] = norm[0]*vmin
    #}
    cbExtend = "neither"
    if actualMin < vmin - 0.05*(vmax-vmin) or (logScale and
      actualMin < vmin*0.95):
      cbExtend = 'min'
    if actualMax > vmax + 0.05*(vmax-vmin):
      cbExtend = 'max' if cbExtend=='neither' else 'both'
    cmapStr = "seismic" if vmin < 0 else "hot"
    if "cmap" in kws:
      cmapStr = kws["cmap"]
    cmap = plt.get_cmap(cmapStr)
    p = plt.pcolormesh(x/rho,y/rho,mapxy/norm[0],vmin=vmin,vmax=vmax,
      norm = cnorm,
      cmap = cmap,
      )
    if autoFigsize and abs(yRange/ratio - 1.) < 0.25:
      plt.gca().set_aspect("equal")
    plt.xlabel(r'$x/\rho_c$')
    plt.ylabel(r'$y/\rho_c$')
    plt.xlim([numpy.min(x/rho),numpy.max(x/rho)])

    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    
    if not picOnly:
      plt.colorbar(p,extend=cbExtend).ax.set_ylabel(field+norm[1])

    overlayAz = ("withAz" in kws and kws["withAz"])
    if overlayAz: #{
      # need to find path of zPy/for2d containing  plot_Az.py
      # If this doesn't work, then need zPy/for2D in PYTHONPATH
      import os
      dir3d = os.path.dirname(__file__)
      if len(dir3d) > 5 and dir3d[-5:]=="for3D":
        dir2d = dir3d[:-2] + "2D"
        import sys
        sys.path.append(dir2d)
      try:
        import plot_Az
      except:
        msg = "The script needs plot_Az.py to plot magnetic field lines. "
        msg += "Please add the directory containing that to the "
        msg += "environment variable PYTHONPATH -- it's probably "
        msg += "something like .../zPy/for2D"
        print msg
        raise #ValueError, msg
      ccolor = 'b' if vmin >= 0 else 'g'
      if isinstance(kws["withAz"], str): #{
        ccolor = kws["withAz"]
      #}
      (Az, timeAz, stepAz, xAz, yAz, izAz) = plot_Az.get_Az(it, iz)
      if iyLo is not None or iyHi is not None:
        Az = Az[:, slice(iyLo, iyHi)]
        yAz = yAz[slice(iyLo, iyHi-1)]
      numLevels = 40
      if "AzLevels" in kws:
        numLevels = kws["AzLevels"]
      cp = plt.contour(xAz/rho, yAz/rho, Az[:-1,:-1].transpose()/B0, 
        numLevels, 
        lw=1.0,
        linestyles='-',
        alpha = 0.6,
        #levels = numpy.arange(0.,0.8e8,0.04e8)
        colors=ccolor)
    #}
    
    title = "time step="+it+"/ z="+iz+"/ Field="+field
    Lx = params["xmax"] - params["xmin"]
    dt = params["dt"]
    title = field 
    title += r", $tc/L_x=$%.3g" % (int(it)*dt * consts.c / Lx)
    is2D = True
    if (params["NZ"] > 2):
      is2D = False
      dz = params["dz"]
      title += r", $z/\rho_c=$%.3g" %  (int(iz)*dz/rho)
    plt.title(title)
    #plt.subplots_adjust(left=0.14, bottom=0.19, right=0.98)
    if picOnly: #{
      plt.axis("off")
      plt.subplots_adjust(left=0, bottom=0, top=1, right=1)
    else: #}{
      if "subplotsKwargs" not in kws:
        try:
          plt.tight_layout()
        except:
          print "Warning: plot_field_xy.py tight_layout() failed"
          #plt.subplots_adjust(left=0.14, bottom=0.19, right=0.98)
          # keep going
    #}
    if "subplotsKwargs" in kws:
      plt.subplots_adjust(**kws["subplotsKwargs"])


    
    if 1: # set window title
      import sys
      wt = zfileUtil.getShortCwd()
      wt += ' ' + ' '.join(sys.argv[1:])
      plt.gcf().canvas.set_window_title(wt)

    save = False
    if "save" in kws and kws["save"]: #{
      save = True
      defaultName = field 
      if not is2D:
        defaultName += "_iz%s" % iz
      defaultName += "_%s" % it
      #kws["save"] = name
      zfileUtil.saveFig(defaultName, **kws)
    #}
    
    #===============================================================================

    if (not save) or ("show" in kws and kws["show"]):
      plt.show()
    
    #===============================================================================

if __name__ == "__main__": #{
  args, kwargs = zfileUtil.getArgsAndKwargs(sys.argv[1:])
  if len(args) != 3:
    print "usage: python plot_field_xy.py step iz-coordinate field",
    print "  [vmin=, vmax=, withAz=False/True/'w', save=, overwrite=]"
    print "  [ymin=inRhoC, ymax=inRhoC]" #splitY=#rhocAroundLayer
    print "  [xmin=inRhoC, xmax=inRhoC]"
    print "  [logScale={0,1,'sym'}, linthresh=, linscale=]"
    print "  note: linthresh is the value at which (for logScale='sym')"
    print "        the transition between linear and log will occur;"
    print "        linscale is the where that transition will occur on"
    print "        the colorscale---higher means higher on the colorscale."
    print "  [figsize='(horizInches,vertinches)' dpi=160 figsizeInc=2]"
    print "  [picOnly=True to show no axes/labels/titles]"
    print "  [subplotsKwargs='{\"left\":0.12, \"right\":0.98}' set subplots_adjust]"
    print " e.g., python plot_field_xy.py 3000 128 Bz"
    print "  field can be the sum of fields, like Jz_electrons+Jz_ions"
  else:
    zfileUtil.setDefaultPlot()
    plot_field_xy(args[0],args[1],args[2], **kwargs)
#}
