import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from path_handler_class import *
from reduce_data_utils import *
import raw_data_utils as rdu
from compare_handler import *

"""
This script will plot the fraction of particles with vz > 0
"""

def compare_plot_pvz_fraction_over_energy(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    overwrite_data = kwargs.get("overwrite_data", False)
    xlims = kwargs.get("xlim", None)
    ylims = kwargs.get("ylim", None)

    gamma_start_ind = kwargs.get("gamma_start_ind", 0)
    gamma_end_ind = kwargs.get("gamma_end_ind", -1)

    # Times in L/c
    tinitial = kwargs.get("tinitial", 0.0)
    tfinal = kwargs.get("tfinal", 20.0)
    titstr = category + "\n"
    t_tit_str = ""
    if tinitial != tfinal:
        t_tit_str += "Averaged from t={:.2f} - {:.2f} L/c".format(tinitial, tfinal)
        t_save_str = "_t{:.2f}-{:.2f}".format(tinitial, tfinal)
    else:
        t_tit_str += "At t={:.2f} L/c".format(tinitial)
        t_save_str = "_t{:.2f}".format(tinitial)
    titstr += t_tit_str

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/positive_vz_fraction/"
    if tinitial != tfinal:
        figdir += "time-averages/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "positive_vz_fraction_over_Energy"
    figname += t_save_str
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)
    if figure_name == "show":
        matplotlib.use("TkAgg")

    labels = comparison(category).labels
    colors = comparison(category).colors
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)

    # Grab gamma bins
    gamma_centers = load_gamma_centers(ph0)
    if gamma_end_ind == -1:
        gamma_end_ind = np.size(gamma_centers)

    plt.figure()

    for i, sim_name in enumerate(configs):
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, system_config)
        # ------------------------------------------------
        time_values = retrieve_time_values(ph)
        times_in_LC = time_values[:, 2]
        tinitial_ind = (np.abs(tinitial - times_in_LC)).argmin()
        tfinal_ind = (np.abs(tfinal - times_in_LC)).argmin()

        # Extract relevant info from pvz dictionary
        pvz_dict = retrieve_pvz_fraction_over_time(ph, overwrite_data)
        total_particles = np.zeros(gamma_centers.size)
        total_pvz_particles = np.zeros(gamma_centers.size)
        total_pvz_fraction_over_energy = np.zeros(gamma_centers.size)
        if pvz_dict is not None:
            for tind in np.arange(tinitial_ind, tfinal_ind + 1):
                for gamma_ind in np.arange(0, gamma_centers.size - 1):
                    gamma_str = "gamma{:d}".format(gamma_ind)
                    total_particles[gamma_ind] = pvz_dict[gamma_str][tind, 1]
                    total_pvz_particles[gamma_ind] = pvz_dict[gamma_str][tind, 0]

                pvz_fraction_over_energy = total_pvz_particles/total_particles
                total_pvz_fraction_over_energy += pvz_fraction_over_energy
            if tinitial_ind != tfinal_ind:
                total_pvz_fraction_over_energy = total_pvz_fraction_over_energy/(tfinal_ind - tinitial_ind + 1)
            plt.plot(gamma_centers, total_pvz_fraction_over_energy, color=colors[i], label=labels[i], path_effects=[mpe.Stroke(linewidth=3, foreground='k'), mpe.Normal()])

    plt.gca().axhline([0.5], color='black', ls='--')
    plt.gca().grid(color='.9', ls='--')
    plt.xscale('log')
    if ylims is not None:
        plt.ylim(ylims)
    if xlims is not None:
        plt.xlim(xlims)

    plt.title(titstr)
    plt.xlabel(r"$\gamma$")
    ylabel = "Fraction of particles with"
    ylabel += r" $v_z > 0$"
    plt.ylabel(ylabel)
    sim_set = comparison(category)
    if sim_set.cbar_label is not None:
        # fig.subplots_adjust(right=0.8)
        # cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()


if __name__ == '__main__':

    # ------- inputs ---------
    system_config = "lia_hp"
    # system_config = "stampede2"
    overwrite_data = False

    # Times in L/c
    tinitial = 19.0
    tfinal = 20.0

    to_tavg = True
    xlims = None
    ylims = None
    # xlims = [1e2, 2e3]
    ylims = [0, 1]

    category = "768Imbalance-comparison"

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["tinitial"] = tinitial
    kwargs["tfinal"] = tfinal
    kwargs["xlim"] = xlims
    kwargs["ylim"] = ylims

    compare_plot_pvz_fraction_over_energy(category, **kwargs)

    configs = comparison(category).configs
    sim_name0 = configs[0]
    ph0 = path_handler(sim_name0, system_config)
    time_values = retrieve_time_values(ph0)
    times_in_LC = time_values[:, 2]

    # for time in times_in_LC:
        # kwargs["tinitial"] = time
        # kwargs["tfinal"] = time
        # compare_plot_pvz_fraction_over_energy(category, **kwargs)
