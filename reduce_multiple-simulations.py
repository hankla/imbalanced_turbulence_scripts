"""
This script will plot slices of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - from create_slices-2d_cross-helicity: need to implement subtracting vel/B means
"""
import sys
sys.path.append("./modules/")
import matplotlib
matplotlib.use('Agg')
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data import reduce_data

stampede2 = True
overwrite = False



if stampede2:
    list_of_sims = ["zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_s0", "zeltron_96cube_no-forcing_travelling"]
    # list_of_sims = rdu.get_list_of_simulation_names("/scratch/06165/ahankla/imbalanced_turbulence/")
else:
    list_of_sims = ["zeltron_96cube_single-mode_master", "zeltron_96cube_balanced_master"]

for sim_name in list_of_sims:
    print("Reducing simulation " + sim_name)

    # -------------------------------------------
    # Set paths
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")
    # -------------------------------------------

    reduce_data(sim_name, ph, overwrite=overwrite)

print("Reduced data for simulations: ")
print(', '.join(list_of_sims))
print("by " + ("NOT" if not overwrite else "") +  " overwriting previous data (if it existed).")
