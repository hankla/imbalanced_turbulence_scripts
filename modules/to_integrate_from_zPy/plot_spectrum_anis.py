
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_spectrum_anis(it,il,ip,spec,sym):

    if it=='':
       it='0'

    if il=='':
       il='0'
       
    if ip=='':
       ip='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # gamma, phi and lambda arrays
    gam=numpy.loadtxt(".././data/gamma.dat")
    phi=numpy.loadtxt(".././data/phi.dat")
    lbd=numpy.loadtxt(".././data/lambda.dat")
    
    # Spectrum
    map_temp=numpy.loadtxt(".././data/angular_"+spec+"_"+sym+it+".dat")
    dNdg=numpy.empty(len(gam))
    
    for ig in range(0,len(gam)):
        dNdg[ig]=map_temp[int(ip)+ig*len(phi),int(il)]*gam[ig]*gam[ig]
    
    plt.plot(gam,dNdg,color='blue',lw=2)
    plt.xscale('log')
    plt.yscale('log')

    plt.xlabel(r'$\gamma$',fontsize=20)
    plt.ylabel(r'$\gamma^2 \frac{dN}{d\gamma}$',fontsize=20)
    plt.ylim([numpy.min(dNdg),3.0*numpy.max(dNdg)])
    
    plt.title("time step="+it+"/ Species="+spec+"/ Sym="+sym , fontsize=18)
      
    print "Lambda=",lbd[int(il)],", Phi=",phi[int(ip)]
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_spectrum_anis(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
