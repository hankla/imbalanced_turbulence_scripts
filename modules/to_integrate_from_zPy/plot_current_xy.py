
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_current_xy(it,iz,current,spec):

    if it=='':
       it='0'

    if iz=='':
       iz='0'

    if current=='':
       current='Jx'

    if spec=='':
       spec='electrons'

    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]

    #===============================================================================
    # The grid
    x=numpy.loadtxt(".././data/xfield.dat")
    y=numpy.loadtxt(".././data/yfield.dat")

    # The current
    map_temp=numpy.loadtxt(".././data/currents/"+current+"_"+spec+"_"+it+".dat")
    mapxy=numpy.empty((len(y),len(x)))

    for ix in range(0,len(x)):
        for iy in range(0,len(y)):
            mapxy[iy,ix]=map_temp[iy+int(iz)*len(y),ix]

    plt.pcolormesh(x/rho,y/rho,mapxy)
    plt.xlabel(r'$x/\rho$',fontsize=18)
    plt.ylabel(r'$y/\rho$',fontsize=18)
    plt.xlim([numpy.min(x/rho),numpy.max(x/rho)])
    plt.ylim([numpy.min(y/rho),numpy.max(y/rho)])
    
    plt.colorbar().ax.set_ylabel(current,fontsize=18)
    
    plt.title("time="+it+"/ z="+iz+"/ "+current+"/ "+spec,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_current_xy(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
