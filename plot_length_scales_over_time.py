import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
This script will basically re-create Zhdankin + 2018 Fig. 4
by plotting the volume-averaged skin depth, larmor radius,
magnetization, and the fluctuating magnetic field.
"""

def plot_length_scales(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    sim_name = ph.sim_name
    # set up figure info
    figdir = ph.path_to_figures + "volAvg-over-time/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "length_scales_vvzFig4_" + sim_name
    figure_name = kwargs.get("figure_name", figname)

    # Get information from energy output at every time step
    # rather than occasional data dumps
    if os.path.exists(ph.path_to_reduced_data + "Eem.dat"):
        os.chdir(ph.path_to_reduced_data)
    elif os.path.exists(ph.path_to_raw_data):
        os.chdir(ph.path_to_raw_data)
        if not os.path.exists(ph.path_to_reduced_data):
            os.makedirs(ph.path_to_reduced_data)
    else:
        print("Eem.dat not available. Length scales NOT plotted!")
        return
    (params, units) = rdu.getSimParams()
    B0=params["B0"]
    Lx = params["xmax"] - params["xmin"]
    Ly = params["ymax"] - params["ymin"]
    Lz = params["zmax"] - params["zmin"]
    dx = params["dx"]
    total_volume = Lz*Ly*Lz
    density = params["Drift. dens."]*2.0 # corrected factor of 2
    number_of_particles = density*total_volume

    #===============================================================================
    # Magnetic and electric energies
    background_magDens = B0**2/(8*np.pi)
    background_Emag = background_magDens * total_volume

    EM_data = np.loadtxt("Eem.dat")
    turb_emag = EM_data[:,0] - background_Emag # *turbulent* magnetic energy
    eelec = EM_data[:,1]

    Brms = np.sqrt(EM_data[:, 0]/total_volume*8.0*np.pi)
    dBrms = np.sqrt(Brms**2 - B0**2)

    Npoints = turb_emag.size
    dt_in_s = params["dt"]
    dt_in_LC = dt_in_s*rdu.Consts.c/Lz
    times_in_LC = np.cumsum(dt_in_LC*np.ones(turb_emag.shape))
    times_in_LC = np.insert(times_in_LC, 0, 0)
    times_in_LC = times_in_LC[:-1]
    times = times_in_LC
    #===============================================================================
    # Particle kinetic energies
    # Electrons
    ekine=np.loadtxt("Ekin_electrons_bg.dat")

    # Ions
    ekini=np.loadtxt("Ekin_ions_bg.dat")

    # Total
    ekin = ekine + ekini
    # Average
    ekin_avg = ekin/number_of_particles

    # in case cut off during dump, adjust lengths of arrays
    if ekin_avg.size != Brms.size:
        smallest_size = np.min([ekin_avg.size, Brms.size, times.size])
        ekin_avg = ekin_avg[:smallest_size]
        Brms = Brms[:smallest_size]
        dBrms = dBrms[:smallest_size]
        times = times[:smallest_size]

    #===============================================================================
    # Calculated quantities
    # KE = (\gamma - 1)mc^2
    var_name = "avg_gamma"
    values = ekin_avg/(rdu.Consts.m_e*rdu.Consts.c**2.0) + 1.0
    if not os.path.exists(ph.path_to_reduced_data + var_name + ".dat"):
        values = ekin_avg/(rdu.Consts.m_e*rdu.Consts.c**2.0) + 1.0
        np.savetxt(ph.path_to_reduced_data + var_name + ".dat", np.transpose(np.vstack((times, values))), delimiter=",", header="time (L/c), " + var_name)
    avg_gamma = np.loadtxt(ph.path_to_reduced_data + var_name + ".dat", delimiter=",", skiprows=1)[:,1]

    # Larmor radius = gamma mc^2/(eBrms)
    var_name = "larmor_radius"
    if not os.path.exists(ph.path_to_reduced_data + var_name + ".dat"):
        values = avg_gamma*rdu.Consts.m_e*rdu.Consts.c**2.0/(rdu.Consts.e * Brms)
        np.savetxt(ph.path_to_reduced_data + var_name + ".dat", np.transpose(np.vstack((times, values))), delimiter=",", header="time (L/c), " + var_name)
    larmor_radius = np.loadtxt(ph.path_to_reduced_data + var_name + ".dat", delimiter=",", skiprows=1)[:,1]

    # Skin depth = sqrt(gamma mc^2/(4 pi n0 e))
    var_name = "skin_depth"
    if not os.path.exists(ph.path_to_reduced_data + var_name + ".dat"):
        values = np.sqrt(avg_gamma*rdu.Consts.m_e*rdu.Consts.c**2.0/(4*np.pi*density*rdu.Consts.e**2.0))
        np.savetxt(ph.path_to_reduced_data + var_name + ".dat", np.transpose(np.vstack((times, values))), delimiter=",", header="time (L/c), " + var_name)
    skin_depth = np.loadtxt(ph.path_to_reduced_data + var_name + ".dat", delimiter=",", skiprows=1)[:,1]

    # Magnetization (VVZ20 definition, not VVZ18 definition)
    var_name = "magnetization"
    if not os.path.exists(ph.path_to_reduced_data + var_name + ".dat"):
        values = 3.0*Brms**2.0/(16.0*np.pi*density*avg_gamma*rdu.Consts.m_e*rdu.Consts.c**2.0)
        np.savetxt(ph.path_to_reduced_data + var_name + ".dat", np.transpose(np.vstack((times, values))), delimiter=",", header="time (L/c), " + var_name)
    magnetization = np.loadtxt(ph.path_to_reduced_data + var_name + ".dat", delimiter=",", skiprows=1)[:,1]

    # Debye length = skindepth/sqrt(3)
    debye_length = skin_depth/np.sqrt(3)

    if figure_name == "show":
        matplotlib.use('TkAgg')

    print("Plotting volume-averaged length scales (VVZ Fig. 4) for " + ph.sim_name)
    plt.figure()
    plt.plot(times, magnetization, color='red',lw=2, label=r"$\sigma$", marker="o", markersize=6, markevery=int(Npoints/10))
    plt.plot(times, dBrms/B0,color='blue',lw=2, label=r"$\delta B_{\mathrm{rms}}/B_0$", marker="s", markersize=6, markevery=int(Npoints/10))
    plt.plot(times, larmor_radius/dx,color='green',lw=2, label=r"$\rho_e/\Delta x$", marker="X", markersize=6, markevery=int(Npoints/10))
    plt.plot(times, skin_depth/dx,color='black',lw=2, label=r"$d_e/\Delta x$", marker="D", markersize=6, markevery=int(Npoints/10))
    plt.plot(times, debye_length/dx,color='magenta',lw=2, label=r"$\lambda_D/\Delta x$", marker="P", markersize=6, markevery=int(Npoints/10))
    plt.xlabel("$tc/L$", fontsize=18)
    plt.title(sim_name)
    plt.legend(frameon=False)

    # plt.tick_params(top=True, right=True)
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().minorticks_on()
    # plt.gca().tick_params(which='major', direction='inout')
    # plt.gca().tick_params(which='minor', direction='in', top=True, right=True)
    plt.gca().grid(color='.9', ls='--')
    plt.tight_layout()


    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = True
    vladimir_data = True
    overwrite_data = False
    to_show = False
    system_config = "lia_hp"

    # sim_name = rdu.get_list_of_sim_names_with_phrases("/mnt/d/imbalanced_turbulence/data_reduced/", ["im025", "384cube"])[0]
    # "zeltron_384cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD180-s11"
    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"

    # --------------------
    kwargs = {}
    if to_show:
        kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data

    if stampede2:
        system_config = "stampede2"

    if stampede2 and vladimir_data:
        system_config = "stampede2"
        vvz_path = "/scratch/02831/zhdankin/data_from_mira/"
        sim_name = "data_1536cube_track_64ppc"
        ph = path_handler(sim_name, system_config, path_to_raw_data=vvz_path + sim_name + "/")
    else:
        ph = path_handler(sim_name, system_config)

    plot_length_scales(ph, **kwargs)
