import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
This script will plot the spatial profiles of Ex, By, and vy to compare their phases.
"""

overwrite = False
stampede2 = False
system_config = "lia_hp"
# system_config = "jila_laptop"

sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
dim = "z"
ix = 0; iy = 0; iz = 0

if stampede2:
    ph = path_handler(sim_name, "stampede2")
else:
    ph = path_handler(sim_name, system_config)

figdir = ph.path_to_figures + "profiles-1d_spatial/compare_Ex-By-vely/" + dim + "-profile/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
fields_to_plot = ["Ex", "By", "vely_total"]
time_steps = retrieve_time_values(ph)[:, 0]
times_to_plot = time_steps[:]

colors = ["tab:blue", "tab:orange", "tab:green"]
linestyles = ["-", ":", "--"]
markers = ["o", "x", "s"]

coords = retrieve_coords(ph)
x_axes = {}
x_axes["z"] = coords[2]
x_axes["y"] = coords[1]
x_axes["x"] = coords[0]
x_axis = np.array(x_axes[dim])
if dim == "x": profstr = "iy{}iz{}".format(iy,iz)
elif dim == "y": profstr = "ix{}iz{}".format(ix, iz)
elif dim == "z": profstr = "ix{}iy{}".format(ix, iy)
(params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
consts = rdu.Consts
rho = consts.c/params["omegac"]

profile_data = retrieve_profiles_spatial(ph, times_to_plot, fields_to_profile=fields_to_plot)
Ex_norm_value = rdu.getNormValue("Ex", simDir=ph.path_to_reduced_data)
Ex_field_abbrev = rdu.getFieldAbbrev("Ex", True)
By_norm_value = rdu.getNormValue("By", simDir=ph.path_to_reduced_data)
By_field_abbrev = rdu.getFieldAbbrev("By", True)
vely_norm_value = rdu.getNormValue("vely_total", simDir=ph.path_to_reduced_data)
vely_field_abbrev = rdu.getFieldAbbrev("vely_total", True)

for time in times_to_plot:
    plt.figure()
    plt.xlabel("$" + dim + r"/\rho$") # units

    Ex_profile = profile_data["Ex"]["{:d}".format(int(time))][profstr.replace("i", '')]
    By_profile = profile_data["By"]["{:d}".format(int(time))][profstr.replace("i", '')]
    vely_profile = profile_data["vely_total"]["{:d}".format(int(time))][profstr.replace("i", '')]
    if vely_profile.size != x_axis.size:
        if x_axis.size == params["N" + dim.capitalize()]:
            x_axis_vel = x_axis[1:]

    plt.plot(x_axis/rho, Ex_profile/Ex_norm_value[0], label=Ex_field_abbrev+Ex_norm_value[1], marker=markers[0], ls=linestyles[0], markersize=4, markevery=int(Ex_profile.size/10), color=colors[0] )
    plt.plot(x_axis/rho, By_profile/By_norm_value[0], label=By_field_abbrev+By_norm_value[1], marker=markers[1], ls=linestyles[1], markersize=4, markevery=int(By_profile.size/10), color=colors[1] )
    plt.plot(x_axis_vel/rho, vely_profile/vely_norm_value[0], label=vely_field_abbrev+vely_norm_value[1], marker=markers[2], ls=linestyles[2], markersize=4, markevery=int(vely_profile.size/10), color=colors[2] )

    plt.legend()
    plt.gca().grid(color='.9', ls='--')
    figure_name = figdir + "ExByvy_comparison_t{}".format(int(time)) + "_" + profstr + ".png"
    titstr = sim_name + "\n" + dim + "-profile at "
    if dim == "x": titstr += "y={}, z={}".format(iy, iz)
    elif dim == "y": titstr += "x={}, z={}".format(ix, iz)
    elif dim == "z": titstr += "x={}, y={}".format(ix, iy)
    titstr += ", t={}".format(time)
    plt.title(titstr)
    plt.tight_layout()
    print("Saving " + figure_name)
    plt.savefig(figure_name, bbox_inches="tight")
    root_name = figure_name.split("/")[-1]
    if not os.path.isdir(figdir + "pdfs/"):
        os.makedirs(figdir + "pdfs/")
    plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

    plt.gca().grid(False, which='both')
    if not os.path.isdir(figdir + "pdfs/nogrids/"):
        os.makedirs(figdir + "pdfs/nogrids/")
    plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
    if not os.path.isdir(figdir + "nogrids/"):
        os.makedirs(figdir + "nogrids/")
    plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()
