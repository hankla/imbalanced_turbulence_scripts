import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plot_length_scales_over_time import plot_length_scales
import matplotlib.patheffects as mpe
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""

def plot_volume_average(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    fields_to_plot = kwargs.get("fields_to_vavg", rdu.Zvariables.options)
    overwrite_data = kwargs.get("overwrite_data", False)
    only_means = kwargs.get("only_means", True)
    only_stds = kwargs.get("only_stds", False)
    plot_decay_time = kwargs.get("plot_decay_time", True)
    (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    sim_name = ph.sim_name

    consts = rdu.Consts
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)

    sigmaDict = rdu.getSimSigmas(params)
    B0 = params["B0"]
    sigma = sigmaDict["sigma"]
    Lx = params["xmax"] - params["xmin"]
    Ly = params["ymax"] - params["ymin"]
    Lz = params["zmax"] - params["zmin"]
    Volume = Lx*Ly*Lz

    nonzero_vars = ["Bz_flucs", "Brms", "density", "bulk", "Energy", "larmor", "skinDepth"]
    norm_to_vA_vars = ["vel", "elsaesserMinus", "elsaesserPlus"]
    # ----------------------------------------------
    # Set functions for secondary axis (velocity only)
    def vaToC(velocity):
        return velocity*np.sqrt(sigma/(sigma+1.))
    def cToVa(velocity):
        return velocity/np.sqrt(sigma/(sigma+1.))

    # ----------------------------------------------
    # Now plot
    print("Plotting volume means and standard deviations for " + ph.sim_name)
    for field in fields_to_plot:
        # special case for Bz fluctuations.
        if field == "Bz_flucs":
            field_mean = vol_means["Bz"]
            field_mean -= B0
            norm_value = (B0, "$/B_0$")
            field_abbrev = "$(B_z-B_0)$"
        elif field == "VpBp_total":
            # Get vA(t)
            field_mean = vol_means[field]
            field_std = vol_stds[field]

            if not os.path.exists(ph.path_to_reduced_data + "magnetization.dat"):
                print("Plotting length scales")
                plot_length_scales(ph)
            mag_values = np.loadtxt(ph.path_to_reduced_data + "magnetization.dat", delimiter=",", skiprows=1)
            magnetization = mag_values[:,1]
            times_to_cut = mag_values[:, 0]

            # should be able to jump every FDUMP outputs
            jump = int(params["FDUMP"])
            magnetization = magnetization[::jump]
            # in case of going longer than needed
            magnetization = magnetization[:times_in_LC.size]
            alfven_in_c = np.sqrt(magnetization/(magnetization + 1.0))
            norm_value = (B0*alfven_in_c, "$/B_0v_A(t)$")
            field_abbrev = rdu.getFieldAbbrev(field, True)
        else:
            field_mean = vol_means[field]
            field_std = vol_stds[field]
            norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
            field_abbrev = rdu.getFieldAbbrev(field, True)

        # mean over time
        mval = np.mean(field_mean/norm_value[0])

        # set up figure info
        figdir = ph.path_to_figures + "volAvg-over-time/"
        if only_means:
            figdir += "means/"
        elif only_stds:
            figdir += "stds/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)
        figname = figdir + field.strip("_")
        figure_name = kwargs.get("figure_name", figname)

        # Body of the plot.
        if figure_name == "show":
            matplotlib.use("TkAgg")
        fig, ax1 = plt.subplots()
        ax1color = 'tab:blue'
        ax2color = 'tab:red'

        if only_stds:
            print(field)
            stdmval = np.nanmean(field_std/norm_value[0])
            axcolor = ax2color
            ax1.plot(times_in_LC, field_std/norm_value[0], markersize=3, marker="o", color=ax2color, path_effects=[mpe.Stroke(linewidth=2), mpe.Normal()])
            ylabel = r"Standard deviation of $\langle$" + field_abbrev + norm_value[1] + r"$\rangle$"
            ax1.axhline([stdmval], ls=":", color=ax2color)
        elif only_means or not (only_means and only_stds):
            axcolor = ax1color
            ax1.plot(times_in_LC, field_mean/norm_value[0], markersize=3, marker="o", color=ax1color, path_effects=[mpe.Stroke(linewidth=2,foreground='k'), mpe.Normal()])
            if field != "Bz" and "Brms" not in field and "density" not in field and "bulk" not in field and "Energy" not in field and "larmor" not in field:
                ax1.axhline([0], ls="--", color='black')
            ax1.axhline([mval], ls=":", color=ax1color)
            ylabel = r"$\langle$" + field_abbrev + norm_value[1] + r"$\rangle$"

        if (not only_stds) and (not only_means):
            stdmval = np.mean(field_std/norm_value[0])
            ax2 = plt.gca().twinx()
            ax2.plot(times_in_LC, field_std/norm_value[0], markersize=3, marker="o", linestyle="-.", color=ax2color)
            ax2.axhline([stdmval], ls=":", color=ax2color)
            ylabel2 = "standard deviation of " + field_abbrev + norm_value[1]
            ax2.set_ylabel(ylabel2, color=ax2color)
            ax2.tick_params(axis='y', labelcolor=ax2color)

        if "NDECAY" in params and plot_decay_time:
            NDECAY = params["NDECAY"]
            time_inds = retrieve_time_values(ph)[:, 0]
            tind = (np.abs(time_inds - NDECAY)).argmin()
            end_time = retrieve_time_values(ph)[tind, 2]
            ax1.axvline([end_time], color='black', ls='-.')
        ax1.set_xlabel("$tc/L$")
        ax1.set_ylabel(ylabel, color=axcolor)
        ax1.tick_params(axis='y', labelcolor=axcolor)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(sim_name)

        # create secondary axis for velocity
        # if "vel" in field or "VdotB" in field:
            # secax = plt.gca().secondary_yaxis('right', functions=(cToVa, vaToC))
            # # replace last occurence of c
            # ylabel = 'v_A'.join(ylabel.rsplit('c', 1))
            # secax.set_ylabel(ylabel)

        plt.tight_layout()
        if figure_name == "show":
            plt.show()
        else:
            plt.savefig(figure_name)
        plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    vladimir_data = False
    system_config = "lia_hp"
    overwrite_data = False
    only_means = True
    only_stds = False
    plot_decay_time = False

    # sim_name = "zeltron_768cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD360-s310"
    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    red_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
    sim_name = rdu.get_list_of_sim_names_with_phrases(red_dir, ["768cube", "dcorr029", "A075", "im075"])[0]
    # if not stampede2 and vladimir_data:
        # red_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
        # sim_name = rdu.get_list_of_sim_names_with_phrases(red_dir, ["1024cube", "data"])[0]

    zVars = rdu.Zvariables.options

    fields_to_plot = zVars
    # fields_to_plot = ["velz_total", "VpBp_total", "deltaBrms", "internalEnergyDensity", "velx_total", "vely_total"]
    # fields_to_plot = fields_to_plot[1:2]
    fields_to_plot = ["heatEnergyDensity"]
    fields_to_plot = ["turbulentEnergyDensity", "fluidEnergyDensity", "netEnergyDensity", "electromagneticEnergyDensity", "electricEnergyDensity", "magneticEnergyDensity", "Brms", "Brms2", "Upx_total", "Upy_total", "Upz_total"]
    fields_to_plot = ["ExB_z", "Upz_total", "internalEnergyDensity", "electromagneticEnergyDensity", "turbulentEnergyDensity", "netEnergyDensity"]
    fields_to_plot = ["relativisticVz2"]

    if only_means:
        only_stds = False

    kwargs = {}
    kwargs["figure_name"] = "show"
    kwargs["fields_to_vavg"] = fields_to_plot
    kwargs["overwrite_data"] = overwrite_data
    kwargs["only_means"] = only_means
    kwargs["only_stds"] = only_stds
    kwargs["plot_decay_time"] = plot_decay_time

    # --------------------
    if stampede2:
        system_config = "stampede2"

    if stampede2 and vladimir_data:
        vvz_path = "/scratch/02831/zhdankin/data_from_mira/"
        sim_name = "data_768cube_track_64ppc"
        ph = path_handler(sim_name, system_config, path_to_raw_data=vvz_path + sim_name + "/")
    else:
        ph = path_handler(sim_name, system_config)

    plot_volume_average(ph, **kwargs)
