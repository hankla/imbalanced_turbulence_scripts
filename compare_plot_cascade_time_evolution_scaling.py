import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import scipy.fftpack as fftpack
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
import itertools
from compare_handler import comparison
import matplotlib.patheffects as mpe

"""
This script will plot the estimated cascade time for different simulations

KWARGS:
- tinitial and tfinal are in Alfven-crossing times. Default is all times.
"""

def compare_plot_cascade_time_evolution_scaling(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    window_len = kwargs.get("window_len", 500)
    norm = kwargs.get("norm", "initial") # 'initial' for vA0, 'evolving' for vA(t)
    scaling_name = kwargs.get("scaling_name", "imbalance")
    t_limited = False
    if "tinitial_LC" in kwargs and "tfinal_LC" in kwargs:
        tinitial_LC = kwargs.get("tinitial_LC")
        tfinal_LC = kwargs.get("tfinal_LC")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial_LC), int(tfinal_LC))
        t_limited = True
    tinitial_tAvg_LC = kwargs.get("tinitial_tAvg_LC", 5.0)
    tfinal_tAvg_LC = kwargs.get("tfinal_tAvg_LC", 20.0)
    ylims_evolution = kwargs.get("ylims_evolution", None)
    ylims_scaling = kwargs.get("ylims_scaling", None)

    sim_set = comparison(category)
    consts = rdu.Consts

    labels = comparison(category).labels
    colors = comparison(category).colors
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)
    fillstyles = itertools.cycle(comparison(category).marker_fillstyles)

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    figdir = ph0.path_to_figures + "../compare/" + category + "/cascade-time/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)

    figname = figdir + "evolution_cascade_time_w{:d}_norm-".format(window_len) + norm
    if t_limited:
        figname += t_save_str
    if ylims_evolution:
        figname += "_yL"
    figname2 = figname + "_sparse.png"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    fig1 = plt.figure()
    fig2 = plt.figure()

    time_averages = []
    time_averages_sparse = []
    scaling_variables = []

    for i, sim in enumerate(configs):
        ph = path_handler(sim, system_config)
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        B0 = params["B0"]
        mag_energy_density = B0**2/(8*np.pi)
        Lx = params["xmax"] - params["xmin"]
        Ly = params["ymax"] - params["ymin"]
        Lz = params["zmax"] - params["zmin"]
        volume = Lx*Ly*Lz
        background_Emag = mag_energy_density * volume
        sigmaDict = rdu.getSimSigmas(params)
        sigma = sigmaDict["sigma"]
        alfven_speed_in_c = np.sqrt(sigma/(sigma + 1.0))
        vA0 = alfven_speed_in_c * consts.c
        tinitial_tAvg_AC = tinitial_tAvg_LC * alfven_speed_in_c
        tfinal_tAvg_AC = tfinal_tAvg_LC * alfven_speed_in_c

        if os.path.exists(ph.path_to_reduced_data + "Eem.dat"):
            os.chdir(ph.path_to_reduced_data)
        else:
            print("Eem.dat not available. Injected energy NOT plotted!")
            return

        # Electric and magnetic energies (NOT energy densities)
        em_energy = np.loadtxt("Eem.dat")
        magnetic_energy = em_energy[:,0] - background_Emag  # *turbulent* magnetic energy density
        electric_energy = em_energy[:,1]
        (vol_avgs, vol_stds) = retrieve_variables_vol_means_stds(ph, fields_to_vavg=["magneticEnergyDensity", "electricEnergyDensity", "electromagneticEnergyDensity", "netEnergyDensity", "internalEnergyDensity"])
        # print(ph.sim_name)
        # print(vol_avgs.keys())
        # mag_energy_sparse = np.array(vol_avgs["magneticEnergyDensity"])
        # mag_energy_sparse = mag_energy_sparse - mag_energy_sparse[0]
        # electric_energy_sparse = np.array(vol_avgs["electricEnergyDensity"])
        # em_energy_sparse = mag_energy_sparse + electric_energy_sparse
        # Checked that these match (or roughly match, for turb) the .dat values
        em_energy_sparse = np.array(vol_avgs["electromagneticEnergyDensity"])
        em_energy_sparse = (em_energy_sparse - em_energy_sparse[0])*volume
        turb_energy_sparse = np.array(vol_avgs["turbulentEnergyDensity"])*volume
        internal_energy_sparse = np.array(vol_avgs["internalEnergyDensity"])*volume

        # Fluid bulk energy and internal energy
        # NOT accounting for net motion!
        if os.path.exists("Efluid.dat"):
            internal_bulk = np.loadtxt("Efluid.dat")
            internal_energy = internal_bulk[:, 0]
            bulk_energy = internal_bulk[:, 1]
        else:
            ion_fluid_energy = np.loadtxt("Eifluid.dat")
            electron_fluid_energy = np.loadtxt("Eefluid.dat")

            internal_energy = ion_fluid_energy[:, 0] + electron_fluid_energy[:, 0]
            bulk_energy = ion_fluid_energy[:, 1] + electron_fluid_energy[:, 1]

        # Smoothing
        window = 'hanning'
        bulk_energy = smooth(bulk_energy, window_len, window)
        internal_energy = smooth(internal_energy, window_len, window)
        magnetic_energy = smooth(magnetic_energy, window_len, window)
        electric_energy = smooth(electric_energy, window_len, window)

        # Time parameters
        # Converting to Alfven times before derivative is important
        dt_in_s = params["dt"]
        # dt_in_LC = dt_in_s*consts.c/Lz
        dt_in_AC = dt_in_s*vA0/Lz
        times_in_AC = np.cumsum(dt_in_AC*np.ones(magnetic_energy.shape))
        times = times_in_AC
        times_in_LC_sparse = retrieve_time_values(ph)[:, 2]
        times_in_AC_sparse = times_in_LC_sparse*vA0/rdu.Consts.c
        dt_in_AC_sparse = times_in_AC_sparse[1] - times_in_AC_sparse[0]

        # Calculating cascade time
        internal_energy_derivative = np.gradient(internal_energy, dt_in_AC, edge_order=2)
        cascade_time = (magnetic_energy + electric_energy + bulk_energy)/(internal_energy_derivative)
        internal_energy_derivative_sparse = np.gradient(internal_energy_sparse, dt_in_AC_sparse, edge_order=2)
        cascade_time_sparse = (em_energy_sparse + turb_energy_sparse)/(internal_energy_derivative_sparse)
        if norm == "initial":
            norm_value = 1.0
            norm_value_sparse = 1.0
            ylabel_evolution = r'$\tau_{\rm casc}v_{A0}/L$'
            ylabel_scaling = r'$\overline{\tau_{\rm casc}}v_{A0}/L$'
        elif norm == "evolving":
            norm_value_sparse = []
            ylabel_evolution = r'$\tau_{\rm casc}v_{A}(t)/L$'
            ylabel_scaling = r'$\overline{\tau_{\rm casc}v_{A}(t)}/L$'
            if not os.path.exists(ph.path_to_reduced_data + "magnetization.dat"):
                plot_length_scales(ph)
            mag_values = np.loadtxt(ph.path_to_reduced_data + "magnetization.dat", delimiter=",", skiprows=1)
            magnetization = mag_values[:, 1]
            vA_over_time = np.sqrt(magnetization/(magnetization + 1.0))

            for t in times_in_AC_sparse:
                t_ind = (np.abs(times_in_AC - t)).argmin()
                norm_value_sparse.append(alfven_speed_in_c/vA_over_time[t_ind])

            vA_over_time = smooth(vA_over_time, window_len, window)
            norm_value = alfven_speed_in_c/vA_over_time
            norm_value = norm_value[:-window_len]


        # Truncate to avoid weird window effects
        times = times[:-window_len]
        cascade_time = cascade_time[:-window_len]
        internal_energy_derivative = internal_energy_derivative[:-window_len]

        plt.figure(fig1.number)
        plt.plot(times, cascade_time/norm_value, ls=next(linestyles), marker=next(markers), label=labels[i], color=colors[i], markevery=int(cascade_time.size/10), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], fillstyle=next(fillstyles))
        plt.figure(fig2.number)
        plt.plot(times_in_AC_sparse, cascade_time_sparse/norm_value_sparse, ls=next(linestyles), marker=next(markers), label=labels[i], color=colors[i], markevery=int(cascade_time.size/10), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], fillstyle=next(fillstyles))

        # Now prepare for time-average
        tinitial_ind = (np.abs(times_in_AC - tinitial_tAvg_AC)).argmin()
        tfinal_ind = (np.abs(times_in_AC - tfinal_tAvg_AC)).argmin()

        tinitial_ind_sparse = (np.abs(times_in_AC_sparse - tinitial_tAvg_AC)).argmin()
        tfinal_ind_sparse = (np.abs(times_in_AC_sparse - tfinal_tAvg_AC)).argmin()

        cascade_time_tavg = np.mean((cascade_time/norm_value)[tinitial_ind:tfinal_ind])
        cascade_time_tavg_sparse = np.mean((cascade_time_sparse/norm_value_sparse)[tinitial_ind_sparse:tfinal_ind_sparse])
        scaling_variable = params[scaling_name]
        time_averages.append(cascade_time_tavg)
        scaling_variables.append(scaling_variable)
        time_averages_sparse.append(cascade_time_tavg_sparse)

    plt.figure(fig1.number)
    if t_limited:
        # Convert from LC to Alfven-crossing
        tinitial_AC = tinitial_LC*alfven_speed_in_c
        tfinal_AC = tfinal_LC*alfven_speed_in_c
        plt.xlim([tinitial_AC, tfinal_AC])
    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False)
    else:
        plt.gca().legend(handles=legend_elements, ncol=3, frameon=False)
    titstr = category
    plt.xlabel('$tv_{A0}/L$')
    plt.ylabel(ylabel_evolution)
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    if ylims_evolution is not None:
        plt.ylim(ylims_evolution)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
        plt.title('')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    plt.figure(fig2.number)
    if t_limited:
        # Convert from LC to Alfven-crossing
        tinitial_AC = tinitial_LC*alfven_speed_in_c
        tfinal_AC = tfinal_LC*alfven_speed_in_c
        plt.xlim([tinitial_AC, tfinal_AC])
    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False)
    else:
        plt.gca().legend(handles=legend_elements, ncol=3, frameon=False)
    titstr = category
    plt.xlabel('$tv_{A0}/L$')
    plt.ylabel(ylabel_evolution)
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    if ylims_evolution is not None:
        plt.ylim(ylims_evolution)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        plt.savefig(figname2)
        # plt.title('')
        # root_name = figure_name.split("/")[-1]
        # if not os.path.isdir(figdir + "pdfs/"):
            # os.makedirs(figdir + "pdfs/")
        # plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))
#
        # plt.gca().grid(False, which='both')
        # if not os.path.isdir(figdir + "pdfs/nogrids/"):
            # os.makedirs(figdir + "pdfs/nogrids/")
        # plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        # if not os.path.isdir(figdir + "nogrids/"):
            # os.makedirs(figdir + "nogrids/")
        # plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # =============================================
    # Plot scaling now.
    # =============================================
    t_save_str = "_tAvg{:d}-{:d}".format(int(tinitial_tAvg_LC), int(tfinal_tAvg_LC))
    figname = figdir + "scaling_cascade_time_w{:d}_norm-".format(window_len) + norm
    if t_limited:
        figname += t_save_str
    if ylims_scaling:
        figname += "_yL"
    figname2 = figname + "_sparse.png"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    colors = itertools.cycle(comparison(category).colors)
    markers = itertools.cycle(comparison(category).markers)
    fillstyles = itertools.cycle(comparison(category).marker_fillstyles)

    fig3 = plt.figure()
    if scaling_name == "imbalance":
        xlabel = r"Balance parameter $\xi$"
    elif scaling_name == "NX":
        xlabel = "$N$"
    else:
        xlabel = scaling_name
    plt.xlabel(xlabel)
    plt.ylabel(ylabel_scaling)
    print(scaling_variables)
    print(time_averages)
    for x,y in zip(scaling_variables, time_averages):
        plt.plot(x, y, marker=next(markers), ls='None', color=next(colors), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], fillstyle=next(fillstyles))
    if ylims_scaling is not None:
        plt.ylim(ylims_scaling)

    if sim_set.cbar_label is not None and (scaling_name != "imbalance"):
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    # if "NX" not in scaling_name:
    plt.gca().legend(handles=comparison(category).legend_elements, ncol=sim_set.ncol, frameon=False)
    if scaling_name == "imbalance":
        plt.xticks(np.arange(0, 1.25, 0.25))
    if scaling_name == "NX":
        plt.gca().set_xscale('log')
        plt.xticks(np.unique(scaling_variables))
        plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in np.unique(scaling_variables)])
        plt.minorticks_off()
    plt.title(titstr)
    plt.tight_layout()
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
        plt.title('')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    fig4 = plt.figure()
    if scaling_name == "imbalance":
        xlabel = r"Balance parameter $\xi$"
    elif scaling_name == "NX":
        xlabel = "$N$"
    else:
        xlabel = scaling_name
    plt.xlabel(xlabel)
    plt.ylabel(ylabel_scaling)
    print(scaling_variables)
    print(time_averages)
    for x,y in zip(scaling_variables, time_averages_sparse):
        plt.plot(x, y, marker=next(markers), ls='None', color=next(colors), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], fillstyle=next(fillstyles))
    if ylims_scaling is not None:
        plt.ylim(ylims_scaling)

    if sim_set.cbar_label is not None and (scaling_name != "imbalance"):
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    # if "NX" not in scaling_name:
    plt.gca().legend(handles=comparison(category).legend_elements, ncol=sim_set.ncol, frameon=False)
    if scaling_name == "imbalance":
        plt.xticks(np.arange(0, 1.25, 0.25))
    if scaling_name == "NX":
        plt.gca().set_xscale('log')
        plt.xticks(np.unique(scaling_variables))
        plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in np.unique(scaling_variables)])
        plt.minorticks_off()
    plt.title(titstr)
    plt.tight_layout()
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figname2)
        plt.savefig(figname2)
        plt.title('')
        root_name = figname2.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

def smooth(x, window_len=5, window='hanning'):
    s = np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    if window == 'flat': # moving average
        w=np.ones(window_len, 'd')
    else:
        w=eval('np.'+window+'(window_len)')

    y = np.convolve(w/w.sum(), s, mode='valid')
    return y


if __name__ == '__main__':
    # category = "768Imbalance-comparison"
    category = "xiAllboxSizeExtremes-comparison"
    # category = "xi10boxSize-comparison"

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    # kwargs["figure_name"] = "show"
    kwargs["ylims_evolution"] = [0, 7]
    kwargs["ylims_scaling"] = [0, 2.6]
    kwargs["window_len"] = 3000

    # times in LC will be converted to Alfven-crossing
    kwargs["tinitial_LC"] = 0.0
    kwargs["tfinal_LC"] = 20.0
    kwargs["tinitial_tAvg_LC"] = 10.0
    kwargs["tfinal_tAvg_LC"] = 20.0
    kwargs["norm"] = "evolving"
    # kwargs["norm"] = "initial"

    compare_plot_cascade_time_evolution_scaling(category, **kwargs)
