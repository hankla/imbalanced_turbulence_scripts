"""
This script will plot slices of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - from create_slices-2d_cross-helicity: need to implement subtracting vel/B means
"""
import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from raw_data_utils import *
from plotting_utils import *

def plot_flux_over_time(ph, **kws):
    """
    ph: path_handler
    """
    # ------------------------------------------
    quantity = kws.get("quantity", "ExB_")
    overwrite = kws.get("overwrite", False)
    ix = kws.get("ix", None)
    iy = kws.get("iy", None)
    iz = kws.get("iz", 0)
    # ------------------------------------------

    times_in_LC = retrieve_time_values(ph)[:, 2]
    flux_over_time = retrieve_flux_variable(ph, quantity, ix, iy, iz, overwrite=overwrite)
    params, units = getSimParams(simDir=ph.path_to_reduced_data)

    Lx = params["xmax"] - params["xmin"]
    Ly = params["ymax"] - params["ymin"]
    Lz = params["zmax"] - params["zmin"]

    if ix is not None:
        quantity_str = quantity + "x"
        area = Ly*Lz
        dxstr = "$\mathrm{dydz}$"
        normstr = "$L_yL_z$"
        titstr = "x = {}".format(ix)
    elif iy is not None:
        quantity_str = quantity + "y"
        area = Lx*Lz
        dxstr = "$\mathrm{dxdz}$"
        normstr = "$L_xL_z$"
        titstr = "y = {}".format(iy)
    elif iz is not None:
        quantity_str = quantity + "z"
        area = Lx*Ly
        dxstr = "$\mathrm{dxdy}$"
        normstr = "$L_xL_y$"
        titstr = "z = {}".format(iz)

    ylabel = r"$\int$" + getFieldAbbrev(quantity_str) + dxstr
    norm_value = getNormValue(quantity_str, params)
    figdir = ph.path_to_figures + "flux-over-time/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + quantity_str.replace("_", "") + "_" + ph.sim_name

    plt.figure()
    plt.plot(times_in_LC, flux_over_time/norm_value[0]/area, markersize=3, marker="o" )
    plt.gca().axhline([0.0], color="k", ls="--")
    plt.ylabel(ylabel + norm_value[1] + normstr)
    plt.xlabel("$tc/L$")
    plt.title(ph.sim_name + "\n" + titstr)
    plt.tight_layout()

    plt.savefig(figname)


if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False

    # sim_name = "zeltron_96cube_single-mode_s0"
    # sim_name = "zeltron_96cube_balanced_s0"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_travelling"
    sim_name = "zeltron_96cube_balanced_master"
    # sim_name = "zeltron_96cube_single-mode_master"

    # kwargs
    overwrite = False
    quantity = "ExB_"
    iz = None; ix = 0; iy = None

    # --------------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    plot_flux_over_time(ph, quantity=quantity, ix=ix, iy=iy, iz=iz, overwrite=overwrite)
