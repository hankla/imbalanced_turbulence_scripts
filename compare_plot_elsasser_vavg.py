import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from save_elsaesser_quantities import save_elsaesser_quantities
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import *
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - cross helicity fluctuations
"""

def compare_plot_elsasser_vavg(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    match_times = kwargs.get("match_times", False)
    system_config = kwargs.get("system_config", "lia_hp")
    ylims = kwargs.get("ylims", None)
    log_yscale = kwargs.get("log_yscale", False)
    tequivalent = False
    t_limited = False
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True

    xi_means = {}

    sim_set = comparison(category)
    configs = sim_set.configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/elsasserVariables/"
    if match_times:
        figdir += "matched_times/"
    if log_yscale:
        figdir += "log_yscale/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figbase = figdir
    figbase += "mean_"

    labels = sim_set.labels
    colors = sim_set.colors

    smallest_end_time = None

    markers = itertools.cycle(sim_set.markers)
    linestyles = itertools.cycle(sim_set.linestyles)
    markersizes = itertools.cycle(sim_set.markersizes)
    markerfills = itertools.cycle(sim_set.marker_fillstyles)
    # field_abbrev = rdu.getFieldAbbrev(field, True)

    f1 = plt.figure()
    f2 = plt.figure()
    # plt.xlabel("$tc/L$")
    # ylabel = r"$\langle$" + field_abbrev + r"$\rangle$"
    # plt.ylabel(ylabel)
    # plt.gca().axhline([0], color="black", linestyle="--")
    # figure_name = figbase + field.strip("_")
    figure_name = figbase + "elsasserEnergies"
    if t_limited:
        figure_name += t_save_str
    if "final_Einj" in kwargs and not tequivalent:
        figure_name += "_plotEinj"
    figure2_name = figure_name + "_timeAveraged{:d}-{:d}.png".format(int(tinitial), int(tfinal))
    figure_name += ".png"

    for i, sim_name in enumerate(configs):
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, system_config)
#
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        times_in_LC = retrieve_time_values(ph)[:, 2]
        time_indices = retrieve_time_values(ph)[:, 0]
        if smallest_end_time is None or times_in_LC[-1] < smallest_end_time:
            smallest_end_time = times_in_LC[-1]

        reduced_dict = save_elsaesser_quantities(ph, **kwargs, to_plot=False)
        if reduced_dict is None:
            # Skip.
            next(markers)
            next(linestyles)
            next(markersizes)
            next(markerfills)
            continue
        elsasser_plus_vavg_times = np.array(list(reduced_dict["elsasserEnergiesPlus"].keys()))
        elsasser_plus_vavg_values = np.array(list(reduced_dict["elsasserEnergiesPlus"].values()))
        elsasser_plus_vavg_values = elsasser_plus_vavg_values[elsasser_plus_vavg_times.argsort()]
        elsasser_minus_vavg_times = np.array(list(reduced_dict["elsasserEnergiesMinus"].keys()))
        elsasser_minus_vavg_values = np.array(list(reduced_dict["elsasserEnergiesMinus"].values()))
        elsasser_minus_vavg_values = elsasser_minus_vavg_values[elsasser_minus_vavg_times.argsort()]

        if t_limited:
            tinitial_ind = (np.abs(tinitial - times_in_LC)).argmin()
            if tequivalent:
                tfinal_ind = equivalent_time_ind
            else:
                tfinal_ind = (np.abs(tfinal - times_in_LC)).argmin()
            times_in_LC = times_in_LC[tinitial_ind:tfinal_ind]
            time_indices = time_indices[tinitial_ind:tfinal_ind]
            elsasser_plus_vavg_values = elsasser_plus_vavg_values[tinitial_ind:tfinal_ind]
            elsasser_minus_vavg_values = elsasser_minus_vavg_values[tinitial_ind:tfinal_ind]

            new_ep_means = []
            new_em_means = []
            new_LC_times = []
            times_removed = []
            for j, mean in enumerate(elsasser_plus_vavg_values):
                if mean is None or elsasser_minus_vavg_values[j] is None:
                    times_removed.append(time_indices[j])
                else:
                    new_ep_means.append(mean)
                    new_em_means.append(elsasser_minus_vavg_values[j])
                    new_LC_times.append(times_in_LC[j])
            print(ph.sim_name + ", removed times: " + ', '.join(map(str, times_removed)))
            if not new_ep_means:
                # Skip.
                next(markers)
                next(linestyles)
                next(markersizes)
                next(markerfills)
                continue
            elsasser_plus_vavg_values = np.array(new_ep_means)
            elsasser_minus_vavg_values = np.array(new_em_means)
            times_in_LC = np.array(new_LC_times)

        ratio = elsasser_plus_vavg_values/elsasser_minus_vavg_values
        ls = next(linestyles)
        marker = next(markers)
        markersize = next(markersizes)
        markerfill = next(markerfills)

        plt.figure(f1.number)
        plt.plot(times_in_LC, ratio, label=labels[i], ls=ls, marker=marker, markersize=markersize, fillstyle=markerfill, markevery=int(elsasser_plus_vavg_values.size/10)+1, color=colors[i], path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
        plt.figure(f2.number)
        xi_value = params["imbalance"]
        time_avg = np.mean(ratio)
        if str(xi_value) + "_768" not in xi_means:
            xi_means[str(xi_value) + "_768"] = []
        if str(xi_value) + "_384" not in xi_means:
            xi_means[str(xi_value) + "_384"] = []
        if "384" in sim_name:
            xi_means[str(xi_value) + "_384"].append(time_avg)
        if "768" in sim_name:
            xi_means[str(xi_value) + "_768"].append(time_avg)
        plt.plot(xi_value, time_avg, ls=ls, marker=marker, markersize=markersize, fillstyle=markerfill, color=colors[i], path_effects=[mpe.Stroke(linewidth=1.8, foreground='k'), mpe.Normal()])

    plt.figure(f1.number)
    plt.xlabel("$tc/L$")
    plt.ylabel(r"$\langle Z_+^2\rangle/\langle Z_-^2\rangle$")
    if ylims is not None:
        plt.ylim(ylims)
    if log_yscale:
        plt.yscale('log')

    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    titstr = ""
    if match_times:
        titstr += " xlim adjusted"
    titstr += "\n" + category
    plt.title(titstr + "\n")

    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False, ncol=sim_set.ncol)
    else:
        plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)

    if match_times:
        plt.xlim([0.0, smallest_end_time])
    if t_limited and not tequivalent:
        plt.xlim([tinitial, tfinal])
    plt.tight_layout()
    metadata={'Author':'Lia Hankla', 'Subject':category, 'Keywords':'Git commit: ' + ph.get_current_commit()}
    print("Saving figure " + figure_name)
    plt.savefig(figure_name, bbox_inches='tight')
    root_name = figure_name.split("/")[-1]
    if not os.path.isdir(figdir + "pdfs/"):
        os.makedirs(figdir + "pdfs/")
    plt.title('')
    plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight', metadata=metadata)

    plt.gca().grid(False, which='both')
    if not os.path.isdir(figdir + "pdfs/nogrids/"):
        os.makedirs(figdir + "pdfs/nogrids/")
    plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
    if not os.path.isdir(figdir + "nogrids/"):
        os.makedirs(figdir + "nogrids/")
    plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()


    for xi in xi_means.keys():
        print("for xi=" + xi + ", the time average has a mean {:.2f} and std {:.2f} (nsims = {:d})".format(np.mean(xi_means[xi]), np.std(xi_means[xi]), len(xi_means[xi])))
        print("               with values " + ', '.join(map(str, xi_means[xi])))
    plt.figure(f2.number)
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel(r"$\overline{\langle Z_+^2\rangle/\langle Z_-^2\rangle}$")
    if ylims is not None:
        plt.ylim(ylims)
    if log_yscale:
        plt.yscale('log')

    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    titstr = ""
    titstr += "\n" + category
    plt.title(titstr + "\n")

    plt.xticks(np.arange(0, 1.25, 0.25))
    plt.tight_layout()
    metadata={'Author':'Lia Hankla', 'Subject':category, 'Keywords':'Git commit: ' + ph.get_current_commit()}
    figure_name = figure2_name
    print("Saving figure " + figure_name)
    plt.savefig(figure_name, bbox_inches='tight')
    root_name = figure_name.split("/")[-1]
    if not os.path.isdir(figdir + "pdfs/"):
        os.makedirs(figdir + "pdfs/")
    plt.title('')
    plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight', metadata=metadata)

    plt.gca().grid(False, which='both')
    if not os.path.isdir(figdir + "pdfs/nogrids/"):
        os.makedirs(figdir + "pdfs/nogrids/")
    plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
    if not os.path.isdir(figdir + "nogrids/"):
        os.makedirs(figdir + "nogrids/")
    plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()

if __name__ == '__main__':
    category = "xiAllboxSizeExtremes-comparison"
    # category = "768Imbalance-comparison"
    # category = "8xyzIm00Mf-comparison"

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    kwargs["tinitial"] = 00.0
    kwargs["tfinal"] = 05.0
    # kwargs["ylims"] = [-0.55, 0.55]
    # kwargs["ylims"] = [-0.7, 0.7]
    # kwargs["ylims"] = [0, 0.2]

    compare_plot_elsasser_vavg(category, **kwargs)
