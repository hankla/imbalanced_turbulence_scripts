import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *


"""
This script will plot the volume-averaged length scales over time:
the Debye length, skin depth, and Larmor radius.

TO DO:
- create main function so can run independently
- adapt for other length scales
"""

def compare_plot_length_scales_over_time(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    cut_initial = kwargs.get("cut_initial", False)
    match_times = kwargs.get("match_times", False)
    max_scaling = kwargs.get("max_scaling", False)
    system_config = kwargs.get("system_config", "lia_hp")

    figure_base = None
    # figure_base = "show"

    sim_name_low = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0"
    sim_name_high = "zeltron_192cube_z-mode-dcorr0sigma05A025_s0"
    labels = [r"Fiducial ($\rho_e=\Delta x$)", r"Doubled ($\rho_e=2\Delta x$)"]

    fields_to_plot = ["Debye", "skinDepth", "larmorRadius"]

    colors = ["tab:blue", "tab:orange", "tab:green", "tab:red"]
    linestyles = ["-", ":", "-.", "--"]
    markers = ["o", "x", "s", "d"]

    # Set up path handler
    if stampede2:
        ph_low = path_handler(sim_name_low, "stampede2")
        ph_high = path_handler(sim_name_high, "stampede2")
    else:
        ph_low = path_handler(sim_name_low, system_config)
        ph_high = path_handler(sim_name_high, system_config)
    times_in_LC_low = retrieve_time_values(ph_low)[:, 2]
    times_in_LC_high = retrieve_time_values(ph_high)[:, 2]
    smallest_end_time = np.min([times_in_LC_high[-1], times_in_LC_low[-1]])
    vol_avgs_low = retrieve_variables_vol_means_stds(ph_low)[0]
    vol_avgs_high = retrieve_variables_vol_means_stds(ph_high)[0]

    if figure_base is None:
        # Construct figure paths
        figpath = ph_low.path_to_figures + "../compare/" + fig_category + "/volAvg-over-time/"
        if match_times:
            figpath += "matched_times/"
        if cut_initial:
            figpath += "noT0/"
        figdir = figdir + "length-scales/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)

    for field in fields_to_plot:
        # norm value is dx, not 2dx
        norm_value = rdu.getNormValue(field, simDir=ph_low.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        field_data_low = vol_avgs_low[field]
        field_data_high = vol_avgs_high[field]

        if figure_base == "show":
            matplotlib.use("TkAgg")
        elif figure_base is None:
            figure_name = figdir + field.strip("_")
        plt.figure()
        plt.xlabel("$tc/L$")
        ylabel = r"$\langle$" + field_abbrev + norm_value[1] + r"$_F\rangle$"
        plt.ylabel(ylabel)

        plt.plot(times_in_LC_low, field_data_low/norm_value[0], label=labels[0], ls=linestyles[0], marker=markers[0], markersize=4, markevery=int(field_data_low.size/10), color=colors[0])
        plt.plot(times_in_LC_high, field_data_high/norm_value[0], label=labels[1], ls=linestyles[1], marker=markers[1], markersize=4, markevery=int(field_data_high.size/10), color=colors[1])

        if match_times:
            plt.xlim([0.0, smallest_end_time])
        plt.legend()
        plt.gca().grid(color='.9', ls='--')
        titstr = ""
        if cut_initial:
            titstr += "$t=0$ removed"
        if match_times:
            titstr += " xlim adjusted"
        plt.title(titstr)
        plt.tight_layout()
        if figure_base == "show":
            plt.show()
        else:
            print("Saving figure " + figure_name)
            plt.savefig(figure_name)
            root_name = figure_name.split("/")[-1]
            if not os.path.isdir(figdir + "pdfs/"):
                os.makedirs(figdir + "pdfs/")
            plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

            plt.gca().grid(False, which='both')
            if not os.path.isdir(figdir + "pdfs/nogrids/"):
                os.makedirs(figdir + "pdfs/nogrids/")
            plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
            if not os.path.isdir(figdir + "nogrids/"):
                os.makedirs(figdir + "nogrids/")
            plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
        plt.close()
