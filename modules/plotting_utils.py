import numpy
import math
import sys
import os
import matplotlib
matplotlib.use('Agg')
import raw_data_utils as rdu
import reduce_data_utils as redu
# rdu.detectDisplay()
import matplotlib.pyplot as plt

def plot_field2d(field,
  time = 0,
  ix = None,
  iy = None,
  iz = None,
  **kws): #{
  """
  """

  consts = rdu.Consts
  (params, units) = rdu.getSimParams()
  # For quasi-2D simulations that are 3D with one cell in z, set iz=0
  # automatically
  is2D = False
  if (params["NZ"] <= 2):
    is2D = True
    iz = 0

  rho=consts.c/params["omegac"]
  dxs = (params["dx"], params["dy"], 1 if is2D else params["dz"])

  reduceRes = 1
  if "reduceRes" in kws:
    reduceRes = kws["reduceRes"]
  smooth = 0
  if "smooth" in kws:
    smooth = kws["smooth"]
  def smoothRa(ra): #{
    # assume ra is periodic with an extra point
    for n in range(smooth): #{
      ra2 = 0.5*ra
      ra2[:-1] += 0.25*ra[1:]
      ra2[1:-1] += 0.25*ra[:-2]
      ra2[0] += 0.25*ra[-2]
      ra2[-1] = ra2[0]
      ra = ra2

      ra2 = 0.5*ra
      ra2[:,:-1] += 0.25*ra[:,1:]
      ra2[:,1:-1] += 0.25*ra[:,:-2]
      ra2[:,0] += 0.25*ra[:,-2]
      ra2[:,-1] = ra2[:,0]
      ra = ra2
    #}
    return ra
  #}

  # whether to plot just the picture, with no colorbar, axes, labels...
  picOnly = False
  if "picOnly" in kws and kws["picOnly"]:
    picOnly = True

  if "x" in kws: #{
    if ix is not None:
      raise ValueError("cannot specify both 'x' and 'ix'")
    ix = int(round(kws["x"]*rho/dxs[0]))
  #}
  if "y" in kws: #{
    if iy is not None:
      raise ValueError("cannot specify both 'y' and 'iy'")
    iy = int(round(kws["y"]*rho/dxs[1]))
  #}
  if "z" in kws: #{
    if iz is not None:
      raise ValueError("cannot specify both 'z' and 'iz'")
    iz = int(round(kws["z"]*rho/dxs[2]))
  #}

  numNone = 0
  if ix is None:
    numNone += 1
  else:
    sliceDir = 0
    iSlice = ix
  if iy is None:
    numNone += 1
  else:
    sliceDir = 1
    iSlice = iy
  if iz is None:
    numNone += 1
  else:
    sliceDir = 2
    iSlice = iz
  if numNone != 2:
    msg = "Exactly one of ix, iy, and iz must be specified (i.e., not None)"
    msg += ": (ix, iy, iz) = "
    msg += repr((ix, iy, iz))
    raise ValueError(msg)

  iMinusPlus = 0
  if "minusPlus" in kws: #{
    if "iMinusPlus" in kws:
      raise ValueError("cannot specify both 'minusPlus' and 'iMinusPlus'")
    mp = kws["minusPlus"]
    if isinstance(mp, int) or isinstance(mp, float):
      iMinusPlus = int(round(mp*rho/dxs[sliceDir]))
    else:
      iMinusPlus = [int(round(delta*rho/dxs[sliceDir])) for delta in mp]
  #}
  if "iMinusPlus" in kws: #{
    iMinusPlus = kws["iMinusPlus"]
  #}
  if isinstance(iMinusPlus, int):
    iMinusPlus = (iMinusPlus,)*2

  sliceDirLabel = "xyz"[sliceDir]

  axesDict = {"x":0, "y":1, "z":2}
  if "hAxis" in kws:
    hAxis = axesDict[kws["hAxis"]]
  elif ix is None:
    hAxis = 0
  else:
    hAxis = 2
  if hAxis == sliceDir:
    msg = "hAxis cannot be the fixed direction specified by "
    msg += "keyword i" + "xyz"[hAxis]
    raise ValueError(msg)
  if "vAxis" in kws:
    vAxis = axesDict[kws["vAxis"]]
  else:
    vAxis = (hAxis+1) % 3
    if vAxis == hAxis or vAxis == sliceDir:
      vAxis = (vAxis + 1) % 3
  if vAxis == sliceDir:
    msg = "vAxis cannot be the fixed direction specified by "
    msg += "keyword i" + "xyz"[hAxis]
    raise ValueError(msg)
  if vAxis == hAxis:
    msg = "vAxis cannot be the same as hAxis (%i)" % vAxis
    raise ValueError(msg)
  try:
    it = rdu.getUserSpecifiedDumpStep(time, params=params)
  except:
    raise
  tDict = rdu.getTime(step=it)

  plotKwargs = dict()
  if "plotKwargs" in kws:
    plotKwargs = kws["plotKwargs"]
  if "lw" not in plotKwargs:
    plotKwargs["lw"]=2

  ihLo = None
  ihHi = None
  if "ihmin" in kws: ihLo = kws["ihmin"]
  if "ihmax" in kws: ihHi = kws["ihmax"]

  ivLo = None
  ivHi = None
  if "ivmin" in kws: ivLo = kws["ivmin"]
  if "ivmax" in kws: ivHi = kws["ivmax"]

  if "layerFieldKws" in kws:
    layerFieldKws = kws["layerFieldKws"]
    if sliceDir != 1:
      msg = "A layerField plot must be in the x-y plane (iy=0)"
      raise ValueError(msg)
    if iSlice != 0:
      msg = "A layerField plot must specify iy=0"
      raise ValueError(msg)
  else:
    layerFieldKws = None

  Ls, Ns = rdu.getSimLengths(params=params)

  #===============================================================================
  # The grid
  # add time step to field
  fieldFile = "+".join([f + ("_%i" % it) for f in field.split("+")])
  hSlice = slice(None)
  vSlice = slice(None)
  hSlice = slice(ihLo, ihHi, reduceRes)
  vSlice = slice(ivLo, ivHi, reduceRes)
  slices = (hSlice, vSlice)
  (data, fieldTime, step, coords) = rdu.getFieldSlice(
    fieldFile, axis0 = hAxis, axis1 = vAxis, i2 = iSlice,
    i2minusPlus = iMinusPlus, slices = slices, layerFieldKws=layerFieldKws)
  fftP = ("fftP" in kws and kws["fftP"])
  if fftP: #{
    data = rdu.rfft2PowSpec(data)
    # b_k := Sum_x B_x e^{-ikx} is what rfft2 calculates
    #  (here written only in 1D for simplicity).
    # (rfft2PowSpec returns |b_k|^2, combining neg and pos frequencies).
    # However, we really want
    # B_k := Sum_x B_x e^{-ikx} dx so that
    # (ideally, for low k), B_k is independent of dx (for fixed L_x = N dx).
    # So B_k := b_k dx.
    #
    # We have Sum_k b_k^2 = N Sum_x B_x^2 so
    # Energy = Sum_x B_x^2 dx = dx/N Sum_k b_k^2 = 1/(N dx) Sum_k B_k^2
    #                         = 1/(2pi) Sum_k B_k^2 dk
    #   where dk = 2pi/L_x = 2pi/(N dx).
    # Note for (convenient graph) normalization of B_k:
    #   assume that B_k -> 0 for k > k_c, so
    # Energy ~ <B_x^2> L_x ~ 1/(2pi) <B_k^2> k_c.
    # And what is k_c?  Well, we might as well take k_c ~ 2pi/L, the
    # longest wavelength.  So
    # Energy ~ (1/L_x) <B_k^2>
    # Now, rough normalization is
    # <B_k^2> ~ <B_x^2> L_x^2 = B_0^2 L_x^2, or for 2D
    # <B_k^2> ~ B_0^2 (L_x L_y)^2
    data *= (params["d" + "xyz"[hAxis]] * params["d" + "xyz"[vAxis]])**2

    h1d = (2.*numpy.pi / Ls[hAxis]) * numpy.arange(data.shape[0])
    v1d = (2.*numpy.pi / Ls[vAxis]) * numpy.arange(data.shape[1])
    # rescale coords immediately
    h1d *= rho/(2.*numpy.pi)
    v1d *= rho/(2.*numpy.pi)
    fftP = True
  #}
  else: #{
    h1d=coords[hAxis] #[hSlice] # getFieldSlice already returned sliced coords
    v1d=coords[vAxis] #[vSlice]
    # rescale coords immediately
    h1d /= rho
    v1d /= rho
  #}
  data = smoothRa(data)
  #h, v = numpy.meshgrid(h1d, v1d, indexing='ij')

  (normValue, normValueStr, quantityStr) = rdu.getNormValue(
    field, params = params, errIfUnknown = True, addDivSign = True)
  norm = (normValue, normValueStr)
  if fftP: #{
    if len(normValueStr) > 0 and normValueStr[0] == "/":
      newStr = "/(%s $L_%s L_%s)^2$" % (
        normValueStr[1:], "xyz"[hAxis], "xyz"[vAxis] )
    else:
      newStr = "(%s $L_%s L_%s)^2$" % (
        normValueStr, "xyz"[hAxis], "xyz"[vAxis] )
    norm = ( (normValue * Ls[hAxis] * Ls[vAxis])**2, newStr )
  #}

  # The field
  hmin = None
  hmax = None
  if ("hmin" in kws): hmin = kws["hmin"]
  if ("hmax" in kws): hmax = kws["hmax"]
  if hmin is None:
    if fftP:
      hmin = h1d[h1d>0].min()
    else:
      hmin = h1d.min()
  if hmax is None: hmax = h1d.max()
  vmin = None
  vmax = None
  if ("vmin" in kws): vmin = kws["vmin"]
  if ("vmax" in kws): vmax = kws["vmax"]
  if vmin is None:
    if fftP:
      vmin = v1d[v1d>0].min()
    else:
      vmin = v1d.min()
  if vmax is None: vmax = v1d.max()
  cmin = None
  cmax = None
  if ("cmin" in kws): cmin = kws["cmin"]
  if ("cmax" in kws): cmax = kws["cmax"]
  data = numpy.ma.masked_invalid(data)
  actualMin = data.min()/norm[0]
  actualMax = data.max()/norm[0]
  if (cmin is None): cmin = actualMin
  if (cmax is None): cmax = actualMax
  if (cmin < 0 and cmax > 0): # and cmax+cmin < cmax/3.):
    if "cmax" not in kws: cmax = max(cmax, -cmin)
    if "cmin" not in kws: cmin = -cmax

  hRange = hmax - hmin
  vRange = vmax - vmin
  sizefig = False
  autoFigsize = True
  if "figsizeInc" in kws and kws["figsizeInc"] != 1:
    sizefig = True
  rc = matplotlib.rcParams
  figsize = list(rc["figure.figsize"])
  if "figsize" in kws and kws["figsize"] is not None:
    sizefig = True
    autoFigsize = False
    figsize = kws["figsize"]
  ratio = 1.
  if autoFigsize and abs(vRange/hRange -1.) > 0.2: #{
    #figsize = list(rc["figure.figsize"])
    ratio = max(0.75, min(2, vRange/hRange))
    figsize[1] = figsize[1] * ratio
    sizefig = True
  #}
  if sizefig: #{
    if "figsizeInc" in kws and kws["figsizeInc"] != 1:
      mag = kws["figsizeInc"]
      figsize=list(figsize)
      figsize[0] *= mag
      figsize[1] *= mag
    dpi = kws["dpi"] if "dpi" in kws else None
    print("Setting figsize =", figsize, ", dpi =", dpi)
    plt.figure(figsize=figsize, dpi=dpi)
  #}
  #cmin=-2.
  #cmax=2.
  cnorm = None
  logScale = 0
  if "logScale" in kws and kws["logScale"]: #{
    if (cmin <= 0 or kws["logScale"]=="sym"):
      logScale = 2
      # Need to think more about how to do this
      #vAbsMin = abs(x).min()/rho
      #vAbsMax = abs(x).max()/rho
      #linthresh = min(max(vAbsMax/1e4, numpy.sqrt(vAbsMin*vAbsMax)),
      #                min(vAbsMax, vmax/10.)
      linthresh = 0.1
      linscale = 0.1
      if "linthresh" in kws:
        linthresh = kws["linthresh"]
      if "linscale" in kws:
        linscale = kws["linscale"]
      cnorm = matplotlib.colors.SymLogNorm(
        linthresh=linthresh, linscale = linscale,
        vmin=cmin, vmax=vmax)
    else:
      logScale = 1
      cnorm = matplotlib.colors.LogNorm(vmin=cmin, vmax=cmax)
      if cmin >= actualMin:
        data[data < norm[0]*cmin] = norm[0]*cmin
  #}
  cbExtend = "neither"
  if actualMin < cmin - 0.05*(cmax-cmin) or (logScale and
    actualMin < cmin*0.95):
    cbExtend = 'min'
  if actualMax > cmax + 0.05*(cmax-cmin):
    cbExtend = 'max' if cbExtend=='neither' else 'both'
  if cmin < 0 and cmax > 0:
    cmapStr = "seismic"
  elif cmin >= 0:
    cmapStr = "inferno"
  else:
    cmapStr = "inferno_r"
  if "cmap" in kws:
    cmapStr = kws["cmap"]
  cmap = plt.get_cmap(cmapStr)
  p = plt.pcolormesh(h1d,v1d,data.transpose()/norm[0],
    vmin=cmin,vmax=cmax,
    norm = cnorm,
    cmap = cmap, rasterized=True
    )
  if autoFigsize: #and abs(vRange/ratio - 1.) < 0.25:
    plt.gca().set_aspect("equal")

  if fftP:
    plt.xscale("log")
    plt.yscale("log")
    xlabel = r'$k_{%s}\rho_c/2\pi$' % "xyz"[hAxis]
    ylabel = r'$k_{%s}\rho_c/2\pi$' % "xyz"[vAxis]
  else:
    xlabel = r'$%s/\rho_{e0}$' % "xyz"[hAxis]
    ylabel = r'$%s/\rho_{e0}$' % "xyz"[vAxis]

  if "hlabel" in kws:
    xlabel = kws["hlabel"]
  plt.xlabel(xlabel)

  if "vlabel" in kws:
    ylabel = kws["vlabel"]
  plt.ylabel(ylabel)

  plt.xlim(hmin, hmax)
  plt.ylim(vmin, vmax)

  clabel = rdu.getFieldAbbrev(field)
  if fftP:
    clabel = r"$|$" + clabel + r"$(k)|^2$"
  clabel += norm[1]
  if "clabel" in kws:
    clabel = kws["clabel"]
  if not picOnly:
    # plt.colorbar(p,extend=cbExtend).ax.set_ylabel(clabel)
    plt.colorbar(p,extend="neither",orientation="horizontal", shrink=0.8).ax.set_xlabel(clabel)

  saveData = ("saveDataFile" in kws) and kws["saveDataFile"]
  if saveData: #{
    import sys
    import os
    #h1d,v1d,data.transpose()/norm[0],
    h5FileName = kws["saveDataFile"]
    if len(h5FileName) < 3 or h5FileName[-3:] != ".h5":
      h5FileName = h5FileName + ".h5"
    if h5FileName[0] == ".":
      msg = "saveDataFile must not be empty and must not begin with '.'"
      raise ValueError(msg)
  #fieldFile = "+".join([f + ("_%i" % it) for f in field.split("+")])
  #(data, fieldTime, step, coords) = rdu.getFieldSlice(
  #  fieldFile, axis0 = hAxis, axis1 = vAxis, i2 = iSlice,
  #  i2minusPlus = iMinusPlus, slices = slices, layerFieldKws=layerFieldKws)
  #  writeArrayToHdf5(data, h5FileName, writeMode, datasetName): #{
    print("Saving " + h5FileName)
    rdu.writeArrayToHdf5(data, h5FileName, 'w', "field",
      datasetAttribs = {"fieldFile":fieldFile,
                        "axis0":hAxis,
                        "axis0label":"xyz"[hAxis],
                        "axis1":vAxis,
                        "axis1label":"xyz"[vAxis],
                        "iSlice":iSlice,
                        "i2minusPlus":numpy.array(iMinusPlus),
                        "hSlice":str(hSlice),
                        "vSlice":str(vSlice),
                        "layerFieldKws":str(layerFieldKws),
                        "commandKws":str(kws),
                        "command":" ".join(sys.argv),
                        "directory":os.getcwd(),
                        "norm":norm[0],
                        "normStr":norm[1],
                        "normQtyStr":quantityStr,
                        "time":fieldTime,
                        "timeDict":str(tDict),
                        "step":step,
                        "params":str(params),
                        "units":str(units),
                        })
    rdu.writeArrayToHdf5(h1d*rho, h5FileName, 'a', "hAxisCoords",
      datasetAttribs = {"axis":hAxis, "rho":rho})
    rdu.writeArrayToHdf5(v1d*rho, h5FileName, 'a', "vAxisCoords",
      datasetAttribs = {"axis":vAxis, "rho":rho})
  #}

  overlayAz = ("withAz" in kws and kws["withAz"])
  if overlayAz: #{
    if (hAxis != 0 or vAxis != 1): #{
      msg = "to use overlayAz, please set hAxis=0 and vAxis=1 (defaults)"
      raise ValueError(msg)
    #}
    # need to find path of zPy/for2d containing  plot_Az.py
    # If this doesn't work, then need zPy/for2D in PYTHONPATH
    import os
    dir3d = os.path.dirname(__file__)
    if len(dir3d) > 5 and dir3d[-5:]=="for3D":
      dir2d = dir3d[:-2] + "2D"
      import sys
      sys.path.append(dir2d)
    try:
      import plot_Az
    except:
      msg = "The script needs plot_Az.py to plot magnetic field lines. "
      msg += "Please add the directory containing that to the "
      msg += "environment variable PYTHONPATH -- it's probably "
      msg += "something like .../zPy/for2D"
      print(msg)
      raise #ValueError, msg
    ccolor = 'b' if cmin >= 0 else 'g'
    if isinstance(kws["withAz"], str): #{
      ccolor = kws["withAz"]
    #}
    (Az, timeAz, stepAz, xAz, yAz, izAz) = plot_Az.get_Az(it, iSlice)
    if ihLo is not None or ihHi is not None:
      Az = Az[:, slice(ihLo, ihHi)]
      yAz = yAz[slice(ihLo, ihHi-1)]
    numLevels = 40
    if "AzLevels" in kws:
      numLevels = kws["AzLevels"]
    cp = plt.contour(xAz/rho, yAz/rho, Az[:-1,:-1].transpose()/params["B0"],
      numLevels,
      lw=1.0,
      linestyles='-',
      alpha = 0.6,
      #levels = numpy.arange(0.,0.8e8,0.04e8)
      colors=ccolor)
    if saveData: #{
      rdu.writeArrayToHdf5(Az[:-1,:-1], h5FileName, 'a', "field",
        datasetAttribs = {"B0":params["B0"],
          "rho":rho,
          "time":timeAz,
          "step":stepAz,
          "iz":izAz,
          })
      rdu.writeArrayToHdf5(xAz, h5FileName, 'a', "xAz",
        datasetAttribs = {"rho":rho})
      rdu.writeArrayToHdf5(yAz, h5FileName, 'a', "yAz",
        datasetAttribs = {"rho":rho})
    #}
  #}

  def overlayYvsXZ(kind, ccolor): #{
    if (hAxis != 1 and vAxis != 1): #{
      msg = "to use '" + kind + "', either hAxis or vAxis must be 1"
      raise ValueError(msg)
    #}
    if isinstance(kws[kind], str):
      ccolor = kws[kind]
    bdSlices = [slice(None), slice(1), slice(None)]
    bdSlices[sliceDir] = slice(iSlice, iSlice+1)

    if kind == "unreconLayer":
      dsetNames = ("yLowerLayerBottomZX",
        "yLowerLayerTopZX", "yUpperLayerBottomZX", "yUpperLayerTopZX")
    elif kind == "withHalfB":
      dsetNames = ("yLowerLayerDnHalfBZX",
        "yLowerLayerUpHalfBZX", "yUpperLayerDnHalfBZX", "yUpperLayerUpHalfBZX")
    else: # kind == "layerCenter"
      dsetNames = ("yLowerLayerCenterZX", "yUpperLayerCenterZX")

    ns = 0
    if "layerSmooths" in kws:
      ns = kws["layerSmooths"]
    (yLo0XZ, dgnTime, dgnStep, dgnCoords) = rdu.getBstreamDgnField(
      it, dsetNames[0],
      numSmooths=ns, slices=bdSlices, params=params)
    yLo1XZ = rdu.getBstreamDgnField(it, dsetNames[1],
      numSmooths=ns, slices=bdSlices, params=params)[0]
    if (len(dsetNames) > 2):
      yHi0XZ = rdu.getBstreamDgnField(it, dsetNames[2],
        numSmooths=ns, slices=bdSlices, params=params)[0]
      yHi1XZ = rdu.getBstreamDgnField(it, dsetNames[3],
        numSmooths=ns, slices=bdSlices, params=params)[0]
    dgnh1d=dgnCoords[hAxis].copy() #[hSlice] # getFieldSlice already returned sliced coords
    dgnv1d=dgnCoords[vAxis].copy() #[vSlice]
    # rescale coords immediately
    dgnh1d /= rho
    dgnv1d /= rho

    if hAxis==0:
      urSlices = (hSlice, 0, 0)
    elif vAxis==0:
      urSlices = (vSlice, 0, 0)
    elif hAxis==2:
      urSlices = (0, 0, hSlice)
    else: #vAxis==2
      urSlices = (0, 0, vSlice)
    def plot(ys, name): #{
      ySub = ys[urSlices]/rho
      # We might plot a reduced field, with full-resolution dgn
      # so get coords from dgn

      if (vAxis==1):
        (a1,a2) = (dgnh1d, ySub)
      # densities (e.g., mapxyz_electrons_bg) don't have the periodic
      #   upper bound, so sometimes have to cut them off
        if len(ySub) == len(dgnh1d)+1:
          a2 = a2[:-1]
      else:
        (a1,a2) = (ySub, dgnv1d)
        if len(ySub) == len(dgnv1d)+1:
          a1 = a1[:-1]

      plt.plot(a1, a2, color=ccolor)
      if saveData: #{
        rdu.writeArrayToHdf5(ys, h5FileName, 'a', name,
          datasetAttribs = {
            "rho":rho,
            "time":dgnTime,
            "step":dgnStep,
            "hAxis":hAxis,
            "vAxis":vAxis,
            "axis0Coords":dgnCoords[0], #[hSlice] # getFieldSlice already returned sliced coords
            "axis1Coords":dgnCoords[1], #[vSlice]
            "axis2Coords":dgnCoords[2],
            "hSlice":str(hSlice),
            "vSlice":str(vSlice),
            "urSlices":str(urSlices),
            "bdSlices":str(bdSlices),
            "numSmooths":ns,
            "plotSubsetScaled":ySub,
            "plotCoordsScaled":dgnh1d if (vAxis==1) else dgnv1d,
            })
      #}
    #}

    plot(yLo0XZ, dsetNames[0])
    plot(yLo1XZ, dsetNames[1])
    if (len(dsetNames) > 2):
      plot(yHi0XZ, dsetNames[2])
      plot(yHi1XZ, dsetNames[3])
    #}
  #}
  if "unreconLayer" in kws and kws["unreconLayer"]: #{
    overlayYvsXZ("unreconLayer", 'y')
  #}
  if "withHalfB" in kws and kws["withHalfB"]: #{
    overlayYvsXZ("withHalfB", 'c')
  #}
  if "layerCenter" in kws and kws["layerCenter"]: #{
    overlayYvsXZ("layerCenter", 'k')
  #}

  #title = "time "+timeSpec+"/ " + sliceDirLabel + "="+iSlice+"/ Field="+field
  title = ""
  if not isinstance(time, int):
    title += time + ", "
  if layerFieldKws is None:
    title += "i%s=%i" % (sliceDirLabel, iSlice)
  else:
    layer = {0:0,1:1,"lower":0,"upper":1}[layerFieldKws["layer"]]
    posRelToLayer = {"below":-1, -1:-1, "center":0, 0:0, "above":1, 1:1}[
      layerFieldKws["posRelToLayer"]]
    bnds = "unrecon"
    if "layerBoundedByHalfB" in layerFieldKws:
      if layerFieldKws["layerBoundedByHalfB"]:
        bnds = "halfB"
    title += ("Below","","Above")[posRelToLayer+1]
    title += ("Lo","Hi")[layer] + "Layer"
    title += "-" + bnds
  if (iMinusPlus[0]!=0 or iMinusPlus[1]!=0): #{
    if (iMinusPlus[0]==iMinusPlus[1]):
      title += r"$\pm $%i" % iMinusPlus[0]
    else:
      title += r"$-$%i$+$%i" % tuple(iMinusPlus)
  #}
  if fftP:
    title += r", %s$(k)$" % (rdu.getFieldAbbrev(field))
  else:
    title += ", %s" % (rdu.getFieldAbbrev(field))
  title += "\nn=%i" % it
  #title = field
  if (params["NX"] > 2):
    title += r", $tc/L_z=$%.3g" % tDict["tcOverLx"]
  else:
    title += r", $tc/L_z=$%.3g" % tDict["tcOverLz"]
  if sliceDir<2 or not is2D: #{
    if layerFieldKws is None:
      dc = dxs[sliceDir]
      title += r", $%s/\rho_c=$%.3g" %  (sliceDirLabel, iSlice*dc/rho)
    else:
      pass
  #}

  if "title" in kws:
    title = kws["title"]
  if "titleplus" in kws:
    title += kws["titleplus"]
  title = r"$tc/L_z=$%.3g" % tDict["tcOverLz"]
  plt.title(title)
  # plt.title("")
  #plt.subplots_adjust(left=0.14, bottom=0.19, right=0.98)
  if picOnly: #{
    plt.axis("off")
    plt.subplots_adjust(left=0, bottom=0, top=1, right=1)
  else: #}{
    if "subplotsKwargs" not in kws:
      try:
        plt.tight_layout()
      except:
        print("Warning: plot_field3d.py tight_layout() failed")
        #plt.subplots_adjust(left=0.14, bottom=0.19, right=0.98)
        # keep going
  #}
  if "subplotsKwargs" in kws:
    plt.subplots_adjust(**kws["subplotsKwargs"])



  if 1: # set window title
    import sys
    wt = rdu.getShortCwd()
    wt += ' ' + ' '.join(sys.argv[1:])
    plt.gcf().canvas.set_window_title(wt)

  save = False
  if "save" in kws and kws["save"]: #{
    save = True
    defaultName = field
    if not is2D:
      defaultName += "_i%s%s" % (sliceDirLabel, iSlice)
    defaultName += "_%s" % it
    #kws["save"] = name
    rdu.saveFig(defaultName, **kws)
  #}

  if fftP: #{ # Make 1D line-outs from the 2D plot
    plt.figure()

    plt.loglog(h1d,data[:,0]/norm[0], label="vs. $k_%s$" % "xyz"[hAxis])
    plt.loglog(v1d,data[0,:]/norm[0], label="vs. $k_%s$" % "xyz"[vAxis])
    if 1: #{ # plot azimuthally-averaged spectrum
      import scipy
      import scipy.interpolate

      dataInterp = scipy.interpolate.RectBivariateSpline(
        h1d,v1d,data,kx=1,ky=1,s=0)
      ths = numpy.arange(2*len(h1d)) * 0.5*numpy.pi/(2*len(h1d)-1)
      cosThs = numpy.cos(ths)
      sinThs = numpy.sin(ths)
      aone = 1. - 1e-12
      krs = []
      avgData = []
      for kr in h1d: #{
        if (kr >= h1d[1] and kr <= h1d[-1] and kr <= v1d[-1] and kr >=v1d[1]):
          krs.append(kr)
          avgData.append( dataInterp(aone*kr*cosThs, aone*kr*sinThs,
            grid=False).mean() )
      #}

      plt.loglog(krs,avgData/norm[0], label="avg. vs. $k_r$")
    #}
    plt.xlabel(r"$k\rho_c/2\pi$")
    plt.ylabel(clabel)
    leg = plt.legend(loc='best')
    leg.set_zorder(0)
    plt.tight_layout()
  #}

  #===============================================================================

  if ((not save) and (not saveData)) or ("show" in kws and kws["show"]):
    plt.show()

  #===============================================================================
#}


if __name__ == "__main__": #{
  args, kwargs = rdu.getArgsAndKwargs(sys.argv[1:])
  if len(args) != 1:
    print("usage: python plot_field2d.py field [keywords]", end=' ')
    print("  fftP=True [default: False] to display FT power spectrum.")
    print("  time={step, '8*Lx/c', 'Bt=0.88'}")
    print("  ix=, iy=, or iz=: specify the slice index (only one)")
    print("  or, alternatively, x=, y=, z= in units of rho")
    print("  iMinusPlus = 10 or (5,10) to average over ix +/- 10, or (ix-5,ix+10+1)")
    print("  minusPlus = like iMinusPlus, but in units of rho")
    print("  hAxis = x, y, z (default: not the slice index) ")
    print("  vAxis = x, y, z (default: not the silce index or hAxis)")
    print("  hmin=inRhoC, hmax=inRhoC") #splitY=#rhocAroundLayer
    print("  vmin=inRhoC, vmax=inRhoC")
    print("  cmin=, cmax=")
    print("  hlabel=, vlabel=, clabel=, title=")
    print("  fontSize=, axesLabelSize=")
    print("  reduceRes = 1 (default--no reduction), 2, 3 (not rec.), 4, ...")
    print("  smooth = 0 (default--no smoothing), 1, 2, 3, ...")
    print("  layerFieldKws =")
    print("    dict(layer=0/1, posRelToLayer=-1/0/1,")
    print("         yAvgMethod=None/'layer'/'cells'/'rhoc',yAvgDist=,")
    print("         layerSmooths=)")
    print("    - where dict has valid keyword arguments to rdu.getFieldOnLayer ")
    print("    - to plot field value in/on/over layer")
    print("    - works only for x-z plots")
    print("  withAz=False/True/'w'  (AzLevels=#contours)")
    print("  save=, overwrite=")
    print("  saveData=fileName.h5")
    print("  unreconLayer=False/True/'y' -- plot the bounds of unreconnected flux")
    print("  withHalfB=False/True/'g' -- plot where B is half its upstream value")
    print("  layerCenter=False/True/'k' -- plot where Bx=0 in layer center")
    print("     (need BstreamDgn for above three)")
    print("  layerSmooths=(default 0): number of smooths for layer diagnostics")
    print("  [logScale={0,1,'sym'}, linthresh=, linscale=]")
    print("  note: linthresh is the value at which (for logScale='sym')")
    print("        the transition between linear and log will occur;")
    print("        linscale is the where that transition will occur on")
    print("        the colorscale---higher means higher on the colorscale.")
    print("  [figsize='(horizInches,vertinches)' dpi=160 figsizeInc=2]")
    print("  [picOnly=True to show no axes/labels/titles]")
    print("  [subplotsKwargs='{\"left\":0.12, \"right\":0.98}' set subplots_adjust]")
    print("  [saveDataFile = an .h5 file to which all plot data will be saved]")
    print(" e.g., python plot_field2d.py 3000 128 Bz")
    print("  field can be the sum of fields, like Jz_electrons+Jz_ions")
  else:
    sdpDict = dict()
    if "fontSize" in kwargs:
      sdpDict["defaultFontSize"] = kwargs["fontSize"]
    if "axesLabelsize" in kwargs:
      sdpDict["axesLabelSize"] = kwargs["axesLabelSize"]
    rdu.setDefaultPlot(**sdpDict)

    plot_field2d(args[0], **kwargs)
#}



# ------------------------------------------------
def plot_spectrum(it,spec,sym, ph, xlims=None, ylims=None):
    from reduce_data_utils import retrieve_time_values
    import numpy as np
    import matplotlib.pylab as pl
    time_values = retrieve_time_values(ph)

    if it=='':
       it='0'
    # allow a range of timesteps,
    #  e.g., it='[50,100,150]' or it='range(50,151,50)'
    if it[0] == "[" or it[0] == "r":
       it = eval(it)
    else:
       it = [int(it)]

    if spec=='':
       spec='electrons'

    if sym=='':
       sym='bg'

    #===============================================================================
    # power
    p = 1.
    colors = pl.cm.viridis(np.linspace(0, 1, len(it)))
    cmap = plt.cm.get_cmap('viridis')
    times_in_LC = []

    for i, n in enumerate(it): #{
      # load spectrum
      (dNdbg, uBinEdges, step
        ) = rdu.getPtclEnergySpectrum(n, spec, sym)
      # gamma-array
      gEdges = numpy.sqrt(uBinEdges**2+1.)
      gam = 0.5*(gEdges[:-1] + gEdges[1:])
      t_ind = np.where(time_values[:, 0] == n)[0][0]
      time_in_LC = time_values[t_ind, 2]
      times_in_LC.append(time_in_LC)

      if 0: #{
        import os
        prefix = ""
        print((prefix+"dir =", os.getcwd()))
        print((prefix+"n =", n))
        print((prefix+"spec =", spec + "_" + sym))
        print((prefix+"gammas =", repr(list(gam))))
        print((prefix+"dNdg =", repr(list(dNdbg))))
      #}

      ys = gam**p * dNdbg
      plt.loglog(gam, ys, lw=2, label = "$t=${:.2f} $L/c$".format(time_in_LC), color=colors[i])
    #}

    plt.xlabel(r'$\gamma$',fontsize=20)
    plt.ylabel(r'$\gamma\frac{dN}{d\gamma}$',fontsize=20)
    if ylims is not None:
      plt.ylim(ylims)
    if xlims is not None:
      plt.xlim(xlims)
    #plt.ylim([numpy.min(ys),3.0*numpy.max(ys)])
    title = "\n" + spec + " spectrum"
    if len(it) == 1:
      title = ("time step=%i" % it[0]) + title
    else:
      # plt.legend(loc='best')
      times_in_LC = np.sort(np.unique(np.array(times_in_LC)))
      ticks = (times_in_LC - times_in_LC[0])/(times_in_LC[-1]-times_in_LC[0])
      colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=cmap),ticks=ticks, shrink=0.94)
      colorbar1.set_label(r"$tc/L$")
      colorbar1.ax.set_yticklabels(["{:.2f}".format(x) for x in times_in_LC])
    title += "\n" + ph.sim_name
    plt.title(title , fontsize=12)
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.tight_layout()


# --------------------------------------------------
def energy_conservation(**kwargs):
  #===============================================================================
  sizes = []

  plt.figure()
  plt.gca().set_yscale('log')
  consts = rdu.Consts
  ph = kwargs.get("ph", None)
  if ph is not None:
    if os.path.exists(ph.path_to_raw_data):
        os.chdir(ph.path_to_raw_data)
    elif os.path.exists(ph.path_to_reduced_data + "Eem.dat"):
        os.chdir(ph.path_to_reduced_data)
    else:
        print("Eem.dat not available. Energy conservation NOT plotted!")
        return
    (params, units) = rdu.getSimParams()
    B0=params["B0"]
    Lx = params["xmax"] - params["xmin"]
    Ly = params["ymax"] - params["ymin"]
    Lz = params["zmax"] - params["zmin"]
    volume = Lz*Ly*Lz
    dt_in_s = params["dt"]
  else:
    B0 = kwargs.get("B0")
    volume = kwargs.get("volume")
    dt_in_s = kwargs.get("dt")

  background_magDens = B0**2/(8*numpy.pi)
  background_Emag = background_magDens * volume

  # Magnetic and electric energies
  data=numpy.loadtxt("Eem.dat")
  emag=data[:,0] - background_Emag # *turbulent* magnetic energy density
  eelc=data[:,1]

  sizes.append(emag.size)
  sizes.append(eelc.size)
  Npoints = emag.size

  dt_in_LC = dt_in_s*consts.c/Lz
  times_in_LC = numpy.cumsum(dt_in_LC*numpy.ones(emag.shape))
  times_in_LC = numpy.insert(times_in_LC, 0, 0)
  times_in_LC = times_in_LC[:-1]
  times = times_in_LC
  plt.xlabel('$tc/L$',fontsize=18)

  sizes.append(times.size)
  #===============================================================================
  # Fluid bulk energy and internal energy
  data_i = numpy.loadtxt("Eifluid.dat")
  data_e = numpy.loadtxt("Eefluid.dat")

  eint = data_i[:, 0] + data_e[:, 0]
  ebulk = data_i[:, 1] + data_e[:, 1]

  sizes.append(eint.size)
  sizes.append(ebulk.size)
  #===============================================================================
  # Fluid kinetic energy
  ekin_i = numpy.loadtxt("Ekin_ions_bg.dat") # + numpy.loadtxt("Ekin_ions_drift.dat")
  ekin_e = numpy.loadtxt("Ekin_electrons_bg.dat") # + numpy.loadtxt("Ekin_electrons_drift.dat")

  sizes.append(ekin_i.size)
  sizes.append(ekin_e.size)

  #===============================================================================
  # Injected energy LMH
  einj = numpy.loadtxt("Einj.dat")
  einj_integrated = numpy.cumsum(einj)
  # einj_integrated = numpy.zeros(einj.size)
  # for i in range(0, len(einj)):
    # einj_integrated[i:] += einj[i]

  sizes.append(einj_integrated.size)
  # print(einj)
  # print(einj_integrated)

  #===============================================================================
  # in case cut off during dump, adjust lengths of arrays
  if not all(x == sizes[0] for x in sizes):
    smallest_size = numpy.min(sizes)
    emag = emag[:smallest_size]
    eelc = eelc[:smallest_size]
    times = times[:smallest_size]
    eint = eint[:smallest_size]
    ebulk = ebulk[:smallest_size]
    ekin_i = ekin_i[:smallest_size]
    ekin_e = ekin_e[:smallest_size]
    einj_integrated = einj_integrated[:smallest_size]

  #===============================================================================
  # Total energy

  etot=emag+eelc+ekin_e+ekin_i+einj_integrated
  # etot=emag+eelc+ekineb+ekined+ekinpb+ekinpd+esyne+esynp+eicse+eicsp+einj_integrated
  # plt.plot(etot,ls='-',lw=3,color='black', label="Total")

  error=(etot[len(etot)-1]-etot[0])/etot[0]*100.0

  # Relative error
  # print("")
  # print("********************************")
  # print("Total energy relative error:")
  # print(error,"%")
  # print("********************************")
  #===============================================================================
  # Plot!
  plt.plot(times, emag/background_Emag,color='red',lw=2, label=r"$E_{\mathrm{mag}}/E_{\mathrm{mean}}$", marker="o", markersize=6, markevery=int(Npoints/10))
  plt.plot(times, eelc/background_Emag,color='green',lw=2, label=r"$E_{\mathrm{elec}}/E_{\mathrm{mean}}$", marker="s", markersize=6, markevery=int(Npoints/10))

  plt.gca().axhline([1.0], color="black", lw=2, label="$1$")
  plt.plot(times, eint/background_Emag, lw=2, color="magenta", label=r"$E_{\mathrm{int}}/E_{\mathrm{mean}}$", marker="X", markersize=6, markevery=int(Npoints/10))
  plt.plot(times, ebulk/background_Emag, lw=2, color="blue", label=r"$E_{\mathrm{bulk}}/E_{\mathrm{mean}}$", marker="D", markersize=6, markevery=int(Npoints/10))
  plt.plot(times, -1.0*einj_integrated/background_Emag, lw=2, label=r"$-E_{\mathrm{inj}}/E_{\mathrm{mean}}$", marker="^", markersize=6, markevery=int(Npoints/10))

  title_string = ph.sim_name
  title_string += "\n Error= %+2.3f " % error + "%"
  plt.title(title_string, fontsize=12)
  plt.ylim([0.0001, 30])
  plt.gca().grid(color='.9', ls='--')
  # plt.title(ph.sim_name + "\n Error= %+2.3f " % error + "%", fontsize=20)
  plt.legend()
