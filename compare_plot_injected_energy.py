import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import scipy.fftpack as fftpack
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
import itertools
from compare_handler import comparison

"""
This script will plot the injected energy (J dot E) at each time step for a given simulation.

KWARGS:
- tinitial and tfinal are in light-crossing times. Default is all times.
"""

def compare_plot_injected_energy(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True

    sim_set = comparison(category)
    consts = rdu.Consts

    labels = comparison(category).labels
    colors = comparison(category).colors
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    figdir = ph0.path_to_figures + "../compare/" + category + "/energy-conservation/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)

    figname = figdir + "cumulative_injected_energy"
    if t_limited:
        figname += t_save_str
    figname += ".png"
    print(figname)
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel('$tc/L$')
    plt.ylabel(r'$\langle\mathcal{E}_{inj}/(B_0^2/8\pi)\rangle$')

    for i, sim in enumerate(configs):
        ph = path_handler(sim, system_config)
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        B0 = params["B0"]
        mag_energy_density = B0**2/(8*np.pi)
        Lx = params["xmax"] - params["xmin"]
        Ly = params["ymax"] - params["ymin"]
        Lz = params["zmax"] - params["zmin"]
        volume = Lz*Ly*Lz

        if os.path.exists(ph.path_to_reduced_data + "Eem.dat"):
            os.chdir(ph.path_to_reduced_data)
        else:
            print("Eem.dat not available. Injected energy NOT plotted!")
            return
        einj = np.loadtxt("Einj.dat")
        einj = np.cumsum(einj)
        einj_density = einj/volume
        einj_norm = einj_density/mag_energy_density

        dt_in_s = params["dt"]
        dt_in_LC = dt_in_s*consts.c/Lz
        times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
        times = times_in_LC

        plt.plot(times, einj_norm, ls=next(linestyles), marker=next(markers), label=labels[i], color=colors[i], markevery=int(einj.size/10))

    if t_limited:
        plt.xlim([tinitial, tfinal])
    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend()
    else:
        plt.gca().legend(handles=legend_elements, ncol=3)
    titstr = category
    plt.title(titstr)
    plt.gca().axhline([0.0], color="black", ls="--")
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

if __name__ == '__main__':
    category = "xiAllboxSizeAll-comparison"

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    # kwargs["figure_name"] = "show"
    kwargs["tinitial"] = 0.0
    kwargs["tfinal"] = 20.0

    compare_plot_injected_energy(category, **kwargs)
