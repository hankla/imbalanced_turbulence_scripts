import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import scipy.fftpack as fftpack
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
import itertools
from compare_handler import *
import matplotlib.patheffects as mpe

small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)

"""
NOTE: this script has eta \propto dB^{-2}. Use compare_fit_injection_efficiency_test for
eta \propto dB^{-3} (version used in paper).

This script will fit the internal energy to a straight line
between tinitial and tfinal for each simulation to obtain the injection efficiency \eta_{\rm inj}
for each simulation.

Injection efficiency from VVZ+ 2018. Given in Hankla+ 2021 as Eq. 16.

KWARGS:
- tinitial and tfinal: times between which the slope will be fit, in light-crossing times.
- compensate: compensate for the different mode factors. Not recommended.

"""

def compare_fit_injection_efficiency(category, ind_variable="imbalance", **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    ylog = kwargs.get("ylog", False)
    ylims = kwargs.get("ylims", None)
    compensate = kwargs.get("compensate", False)
    linear_fit = kwargs.get("linear_fit", False)
    quadratic_fit = kwargs.get("quadratic_fit", False)
    predicted_fit = kwargs.get("predicted_fit", False)
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True

    field = "internalEnergyDensity"
    sim_set = comparison(category)
    consts = rdu.Consts

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    (params, units) = rdu.getSimParams(simDir=ph0.path_to_reduced_data)
    Lz = params["zmax"] - params["zmin"]
    sigmaDict = rdu.getSimSigmas(params)
    sigma = sigmaDict["sigma"]
    vA0 = consts.c*np.sqrt(sigma/(sigma + 1.0))
    # B0 = params["B0"]
    # gamma0 = params["thde"]*3.
    light_crossing_time = Lz/consts.c
    # injection_constant = gamma0*sigma*(2.0*params["Drift. dens."])*consts.m_e*consts.c**2.0*vA0/Lz
    # injection_constant = B0**2.0*vA0/(8.0*np.pi*Lz)
    injection_constant = vA0/(Lz)
    (x_values, internal_slopes) = get_comparison_Avar_as_Bvar(category, ind_variable, field + "_slope", **kwargs)
    magneticEnergy_means = get_comparison_Avar_as_Bvar(category, ind_variable, "magneticEnergyDensity_mean", **kwargs)[1]
    deltaME = np.array(magneticEnergy_means) - 1.0 # magneticEnergy already normed to background
    x_values = np.array(x_values)
    if ind_variable == "NX":
        x_values = x_values - 1
        # TODO: generalize conversion.
        # conversion = 1.0/(2.0*np.pi*initial_larmor_radius)
        conversion = 1.0/9.42
        x_values = x_values * conversion
    # account for normalization to initial field and L/c times.
    # internal_slopes = np.array(internal_slopes)*B0**2.0/(8.*np.pi)/light_crossing_time
    internal_slopes = np.array(internal_slopes)/np.array(deltaME)/light_crossing_time
    injection_efficiency = internal_slopes/injection_constant
    if ind_variable == "imbalance" and compensate:
        mode_factors = 2.0 - x_values
        injection_efficiency = injection_efficiency * mode_factors


    # Fit to quadratic or linear
    x_values_fine = np.linspace(x_values.min(), x_values.max(), 1000)
    if quadratic_fit:
        fit_params, fit_e = optimize.curve_fit(quadratic, x_values, injection_efficiency)
        fitted_quadratic = quadratic(x_values_fine, *fit_params)
    if linear_fit:
        fit_params, fit_e = optimize.curve_fit(linear, x_values, injection_efficiency)
        fitted_linear = linear(x_values_fine, *fit_params)
    if predicted_fit:
        fit_params, fit_e = optimize.curve_fit(predicted, x_values, injection_efficiency)
        fitted_predicted = predicted(x_values_fine, *fit_params)

    figdir = ph0.path_to_figures + "../compare/" + category + "/energy-partition/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)

    markers = itertools.cycle(comparison(category).markers)
    markersizes = itertools.cycle(comparison(category).markersizes)
    fillstyles = itertools.cycle(comparison(category).marker_fillstyles)
    colors = itertools.cycle(comparison(category).colors)

    figname = figdir + field + "_injection_efficiency"
    if t_limited:
        figname += t_save_str
    if compensate:
        figname += "_mFcompensated"
    if linear_fit:
        figname += "_fitLinear"
        if quadratic_fit:
            figname += "Quadratic"
        if predicted_fit:
            figname += "Predicted"
    elif quadratic_fit:
        figname += "_fitQuadratic"
    elif predicted_fit:
        figname += "_fitPredicted"
    if ylog:
        figname += "_log"
    if ylims:
        figname += "_yL"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()

    # print(injection_efficiency)
    print(comparison(category).configs)
    print(deltaME)
    print(injection_efficiency)
    for i in np.arange(x_values.size):
        plt.plot(x_values[i], injection_efficiency[i], linestyle='None', marker=next(markers), color=next(colors), fillstyle=next(fillstyles), path_effects=[mpe.Stroke(linewidth=1.5, foreground='k'), mpe.Normal()], ms=next(markersizes))
    # for x, efficiency, marker, color,fillstyle in zip(x_values, injection_efficiency, markers,colors,fillstyles):
        # plt.plot(x, efficiency, linestyle='None', marker=marker, color=color, path_effects=[mpe.Stroke(linewidth=1.5, foreground='k'), mpe.Normal()], fillstyle=fillstyle)

    if quadratic_fit:
        plt.plot(x_values_fine, fitted_quadratic, 'k:')
    if linear_fit:
        plt.plot(x_values_fine, fitted_linear, 'k--')
    if predicted_fit:
        plt.plot(x_values_fine, fitted_predicted, 'k:')

    # plt.gca().legend(handles=sim_set.legend_elements, ncol=sim_set.ncol, frameon=False, bbox_to_anchor=(1.01, 1))
    plt.gca().legend(handles=sim_set.legend_elements, ncol=sim_set.ncol, frameon=True)
    plt.ylabel(r'Injection efficiency $\eta_{\rm inj}$')
    if ind_variable == "imbalance":
        plt.xlabel(r"Balance parameter $\xi$")
        plt.xticks(np.unique(x_values))
    elif ind_variable == "NX":
        xlabel = r"$L/2\pi\rho_{e0}$"
        plt.xlabel(xlabel)
        plt.gca().set_xscale('log')
        plt.xticks(np.unique(x_values))
        plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in np.unique(x_values)])
        plt.minorticks_off()
    else:
        plt.xlabel(ind_variable)
    titstr = category
    # plt.gca().set_xticklabels(["{:.2f}".format(xi) for xi in xi_values])
    plt.title(titstr + "\n")
    plt.gca().grid(color='.9', ls='--')
    if ylims:
        plt.ylim(ylims)
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()


def quadratic(xi_values, A, B, C):
    return A*xi_values**2.0 + B*xi_values + C


def linear(xi_values, A, B):
    return A*xi_values + B


def predicted(xi_values, A, B):
    return A*(xi_values**2.0 + 1) + B


if __name__ == '__main__':
    category = "xiExtremesboxSizeAll-comparison"
    # category = "xiAllboxSizeExtremes-comparison"
    # category = "xiExtremesboxSizeAll-comparison"
    ind_variable = "NX"
    # category = "384Imbalance-comparison"
    # category = "xiAllboxSizeExtremes-comparison"
    # ind_variable = "imbalance"
    category = "8xyzIm00Mf-comparison"
    ind_variable = "NM"

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    # kwargs["figure_name"] = "show"
    kwargs["tinitial"] = 05.0
    kwargs["tfinal"] = 14.0
    kwargs["reload_all_sims"] = True
    kwargs["ylims"] = [0, 1.8]

    compare_fit_injection_efficiency(category, ind_variable, **kwargs, compensate=False)
    # compare_fit_injection_efficiency(category, ind_variable, **kwargs, quadratic_fit=True)
    # compare_fit_injection_efficiency(category, ind_variable, **kwargs, linear_fit=True, quadratic_fit=True)
    # compare_fit_injection_efficiency(category, ind_variable, **kwargs, linear_fit=True, predicted_fit=True)
