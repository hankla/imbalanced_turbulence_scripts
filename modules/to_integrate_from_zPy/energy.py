
import numpy
import math
import matplotlib.pyplot as plt

def energy():

    #===============================================================================
    # Magnetic and electric energies

    data=numpy.loadtxt("Eem.dat")
    emag=data[:,0]
    eelc=data[:,1]

    plt.plot(emag,color='blue',lw=2, label="Magnetic energy")
    plt.plot(eelc*10.0,color='red',lw=2, label=r"Electric energy$\times10$")

    plt.xlabel('Time step',fontsize=18)
    plt.ylabel('Energy',fontsize=18)

    #===============================================================================
    # Particle kinetic energies

    # Electrons
    ekineb=numpy.loadtxt("Ekin_electrons_bg.dat")
    ekined=numpy.loadtxt("Ekin_electrons_drift.dat")

    # Ions
    ekinpb=numpy.loadtxt("Ekin_ions_bg.dat")
    ekinpd=numpy.loadtxt("Ekin_ions_drift.dat")

    plt.plot(ekineb+ekined,ls='--',lw=2,color='green', label="Kinetic energy")
    plt.plot(ekinpb+ekinpd,ls=':',lw=2,color='green')

    #===============================================================================
    # Radiative energies

    # Synchrotron
    # Electrons
    esyne=numpy.loadtxt("Esyn_electrons.dat")
    # Ions
    esynp=numpy.loadtxt("Esyn_electrons.dat")

    plt.plot(esyne,ls='--',lw=2,color='magenta', label="Synchrotron energy")
    plt.plot(esynp,ls=':',lw=2,color='magenta')

    # Inverse Compton
    # Electrons
    eicse=numpy.loadtxt("Eics_electrons.dat")
    # Ions
    eicsp=numpy.loadtxt("Eics_electrons.dat")

    plt.plot(eicse,ls='--',lw=2,color='cyan', label="IC energy")
    plt.plot(eicsp,ls=':',lw=2,color='cyan')

    #===============================================================================
    # Injected energy LMH
    einj = numpy.loadtxt("Einj.dat")
    einj_integrated = numpy.zeros(einj.size)
    for i in range(0, len(einj)):
        einj_integrated[i:] += einj[i]
    print(einj)
    print(einj_integrated)
    plt.plot(einj_integrated, lw=2, label="Injected energy")

    #===============================================================================
    # Total energy

    etot=emag+eelc+ekineb+ekined+ekinpb+ekinpd+esyne+esynp+eicse+eicsp+einj_integrated
    plt.plot(etot,ls='-',lw=3,color='black', label="Total")
    plt.legend()

    error=(etot[len(etot)-1]-etot[0])/etot[0]*100.0

    # Relative error
    print("")
    print("********************************")
    print("Total energy relative error:")
    print(error,"%")
    print("********************************")

    plt.title("Error= %+2.3f " % error + "%", fontsize=20)

    #===============================================================================

    # plt.show()
    
    #===============================================================================
    
# energy()
