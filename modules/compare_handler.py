# Class to handle comparisons
import numpy as np
import os
import pandas as pd
import fnmatch
import matplotlib
import matplotlib.pylab as pl
import matplotlib.pyplot as plt
from path_handler_class import path_handler
import raw_data_utils as rdu
from reduce_data_utils import *
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe

small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)

class comparison:
    def __init__(self, category, reduced_data_path="/mnt/d/imbalanced_turbulence/data_reduced/", setup="lia_hp"):
    # def __init__(self, category, reduced_data_path="/work2/06165/ahankla/stampede2/imbalanced_turbulence/data_reduced/", setup="stampede2"):
        self.setup = setup
        self.category = category
        self.linestyles = []
        self.colors = []
        self.variable = "imbalance"
        self.markers = ["o", "v", "s", "D", "^"]
        self.marker_fillstyles = ['full']
        self.markersizes = [matplotlib.rcParams['lines.markersize']]
        self.legend_elements = []
        self.ncol = 3
        self.cbar_label = None
        self.labels = []
        self.omegaD = []
        self.tinitial = 2.0 # L/c
        self.tfinal = 20.0 # L/c


        if category == "256Imbalance-comparison":
            self.configs = ["zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s2", "zeltron_256cube_8xyz-Jxyz_im025sigma05dcorr029omega035A075phisame_PPC32-FD120-s25", "zeltron_256cube_8xyz-Jxyz_im05sigma05dcorr029omega035A075phisame_PPC32-FD120-s5", "zeltron_256cube_8xyz-Jxyz_im075sigma05dcorr029omega035A075phisame_PPC32-FD120-s75", "zeltron_256cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD120-s10"]
            # self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["256cube"], True)
            self.variable = "imbalance"
            self.ncol = 3

            xi_values = []
            # Get balance parameter and driving frequency from phys_params
            for sim in self.configs:
                (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                xi_values.append(params["imbalance"])
                self.omegaD.append(params["omegaD"])

            xi_values = np.array(xi_values)
            # Sort with xi values from 1.0 to 0.0
            sorted_inds = xi_values.argsort()[::-1]
            xi_values = xi_values[sorted_inds]
            self.configs = np.array(self.configs)[sorted_inds]
            self.omegaD = np.array(self.omegaD)[sorted_inds]

            # self.labels = [r"$\xi={:.2f}$".format(xi) for xi in xi_values]
            num_balance_params = xi_values.size

            # self.labels = [r"$\xi={:.2f}$".format(xi) for xi in xi_values]
            self.colors = pl.cm.viridis(np.linspace(0, 1, num_balance_params))
            self.linestyles = ['-']
            self.markers = []
            initial_markers = ["v", "d", "s", "^", "<"]

            # need to adjust ticks to be in center
            tick_offset = 1.0/num_balance_params
            tick_locs = np.arange(0.0, 1.0, tick_offset)
            tick_locs = tick_locs + tick_offset/2.0
            self.cmap = plt.cm.get_cmap('viridis_r', np.size(tick_locs))
            self.cbar_label = r"Balance Parameter $\xi$"
            self.cbar_tick_labs = [str(xi) for xi in np.sort(xi_values)]
            self.cbar_ticks = tick_locs

        elif category == "384Imbalance-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "8xyz", "dcorr029"], True)
            self.configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "4xyz"], True)
            self.variable = "imbalance"
            self.ncol = 3
            self.linestyles = ['-']
            num_balance_params = 5
            initial_colors = pl.cm.viridis(np.linspace(0, 1, num_balance_params))
            self.colors = []
            self.markers = []
            initial_markers = ["o", "^", "s"]

            xi_values = []
            # Get balance parameter and driving frequency from phys_params
            for sim in self.configs:
                (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                xi_values.append(params["imbalance"])
                self.omegaD.append(params["omegaD"])
                if params["imbalance"] == 0.0:
                    self.colors.append(initial_colors[0])
                elif params["imbalance"] == 0.25:
                    self.colors.append(initial_colors[1])
                elif params["imbalance"] == 0.5:
                    self.colors.append(initial_colors[2])
                elif params["imbalance"] == 0.75:
                    self.colors.append(initial_colors[3])
                elif params["imbalance"] == 1.0:
                    self.colors.append(initial_colors[4])

                if "4xyz" in sim:
                    if "NM4" in sim:
                        self.markers.append(initial_markers[2])
                    else:
                        self.markers.append(initial_markers[1])
                else:
                    self.markers.append(initial_markers[0])


            xi_values = np.array(xi_values)
            # Sort with xi values from 1.0 to 0.0
            sorted_inds = xi_values.argsort()[::-1]
            xi_values = xi_values[sorted_inds]
            # self.configs = np.array(self.configs)[sorted_inds]
            # self.omegaD = np.array(self.omegaD)[sorted_inds]

            # self.labels = [r"$\xi={:.2f}$".format(xi) for xi in xi_values]

            # need to adjust ticks to be in center
            tick_offset = 1.0/num_balance_params
            tick_locs = np.arange(0.0, 1.0, tick_offset)
            tick_locs = tick_locs + tick_offset/2.0
            self.cmap = plt.cm.get_cmap('viridis_r', np.size(tick_locs))
            self.cbar_label = r"Balance Parameter $\xi$"
            self.cbar_tick_labs = [str(xi) for xi in np.sort(xi_values)]
            self.cbar_ticks = tick_locs

        elif category == "768Imbalance-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "A075"], True)
            self.variable = "imbalance"
            self.ncol = 3
            self.linestyles = ['-']

            xi_values = []
            # Get balance parameter and driving frequency from phys_params
            for sim in self.configs:
                (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                xi_values.append(params["imbalance"])
                self.omegaD.append(params["omegaD"])

            xi_values = np.array(xi_values)
            # Sort with xi values from 1.0 to 0.0
            sorted_inds = xi_values.argsort()[::-1]
            xi_values = xi_values[sorted_inds]
            self.configs = np.array(self.configs)[sorted_inds]
            self.omegaD = np.array(self.omegaD)[sorted_inds]
            num_balance_params = xi_values.size

            # self.labels = [r"$\xi={:.2f}$".format(xi) for xi in xi_values]
            self.colors = pl.cm.viridis(np.linspace(0, 1, num_balance_params))

            # need to adjust ticks to be in center
            tick_offset = 1.0/num_balance_params
            tick_locs = np.arange(0.0, 1.0, tick_offset)
            tick_locs = tick_locs + tick_offset/2.0
            self.cmap = plt.cm.get_cmap('viridis_r', np.size(tick_locs))
            self.cbar_label = r"Balance Parameter $\xi$"
            self.cbar_tick_labs = [str(xi) for xi in np.sort(xi_values)]
            self.cbar_ticks = tick_locs

        elif category == "1024Imbalance-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["1024cube", "data"], True)
            self.variable = "imbalance"
            self.ncol = 1
            self.linestyles = ['-']
        elif category == "1536Imbalance-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["1536cube", "data"], True)
            self.variable = "imbalance"
            self.ncol = 1
            self.linestyles = ['-']

        elif category == "modeNum-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im10", "4xyz", "s110"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im10", "8xyz", "s110"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im00", "8xyz", "s110"], True)
            self.configs = initial_configs
            self.variable = "NM"
            self.ncol = 2
            self.linestyles = ['-', '--', ':']

            self.labels = [r"$N=4$, $A0$ same", r"$N=4$, $A0$ adjusted", r"$N=8$"]
            self.colors = ['blue', 'red', 'green']

        elif category == "modeFactor-comparison":
            # DON'T DO THIS ONE!! IM10 has different random seed. Use 8xyzIm00Mf instead.
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im00", "8xyz", "s11"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im10", "8xyz", "s110"], True)
            self.configs = initial_configs
            self.variable = "imbalance"
            self.ncol = 2
            self.linestyles = ['-', '--', ':']

            self.labels = [r"Original $\xi=0$", r"$\xi=0$, $A0$ adjusted", r"$\xi=1.0$"]
            self.colors = ['blue', 'red', 'green']

        elif category == "8xyzIm00Mf-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im00", "8xyz", "s11"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "A075", "im10", "8xyz", "s11", "mf"], True)
            self.configs = initial_configs
            self.variable = "imbalance"
            self.ncol = 1
            self.linestyles = ['-', '--', ':']

            self.labels = [r"Original $\xi=0$", r"$\xi=0$, $A0$ adjusted", r"$\xi=1.0$"]
            self.colors = ['blue', 'red', 'green']
            self.legend_elements = []
            for i in np.arange(len(self.colors)):
                self.legend_elements.append(matplotlib.lines.Line2D([0], [0], label=self.labels[i], marker=self.markers[i], color=self.colors[i]))

        elif category == "768vvz-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "A075", "im10"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "64ppc", "data"], True)
            self.configs = initial_configs
            self.variable = "PPC"
            self.ncol = 2
            self.linestyles = ['-', '--']

            self.labels = ["AMH", "VVZ"]
            self.colors = ['blue', 'red']

        elif category == "singleMode-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["z-mode"], True)
            self.configs = self.configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["256cube", "im00", "dcorr0o", "A075"], True)
            self.configs = self.configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["256cube", "im00", "dcorr029o", "s2", "A075"], True)
            self.colors = pl.cm.viridis(np.linspace(0, 1, len(self.configs)))
            for i, sim in enumerate(self.configs):
                if "256cube" in sim:
                    if "dcorr029" in sim:
                        label = r"8 $+z$ modes, $\gamma_D=0.29\omega_D$"
                    else:
                        label = r"8 $+z$ modes, $\gamma_D=0.0$"
                elif "mz" in sim:
                    label = "Single $-z$ mode"
                else:
                    label = "Single $+z$ mode"
                self.labels.append(label)
                self.legend_elements.append(matplotlib.lines.Line2D([0], [0], label=label, marker=self.markers[i], color=self.colors[i]))
                self.ncol = 2

        elif category == "xiExtremesboxSizeAll-comparison":
            self.variable = "NX"
            num_balance_params = 5
            xi_values = []
            self.ncol = 1
            self.configs = []
            self.markers = []
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "A075", "dcorr029", "im00"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "A075", "dcorr029", "im10"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["data", "track_64ppc"], True)
            initial_markers = ["o", "^"]
            # initial_markers = ["v", "d", "s", "^", "<"]
            imbalance_colors = pl.cm.viridis(np.linspace(0, 1, num_balance_params)) # 5 values of imbalance
            for sim in initial_configs:
                to_include = True
                if "96cube" in sim or "mf" in sim:
                    to_include = False
                else:
                    if "im00" in sim and "256cube" in sim and not ("FD120-s2" in sim):
                        to_include = False
                    if to_include:
                        self.configs.append(sim)
                        (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                        xi_values.append(params["imbalance"])
                        self.omegaD.append(params["omegaD"])

                        self.linestyles = ['-']

                        # Make markers and colors depend on imbalance.
                        if xi_values[-1] == 1.0:
                            self.markers.append(initial_markers[0])
                            self.colors.append(imbalance_colors[0])
                        elif xi_values[-1] == 0.0:
                            self.markers.append(initial_markers[-1])
                            self.colors.append(imbalance_colors[-1])
                self.legend_elements = [matplotlib.lines.Line2D([0], [0], color=imbalance_colors[0], ls='None', label=r"$\xi=1.0$",marker=initial_markers[0], path_effects=[mpe.Stroke(linewidth=1.5, foreground='k'), mpe.Normal()]),
                                   matplotlib.lines.Line2D([0], [0], color=imbalance_colors[-1], ls='None', label=r"$\xi=0.0$", marker=initial_markers[-1], path_effects=[mpe.Stroke(linewidth=1.5, foreground='k'), mpe.Normal()])]
                # need to adjust ticks to be in center
                tick_offset = 1.0/num_balance_params
                tick_locs = np.arange(0.0, 1.0, tick_offset)
                tick_locs = tick_locs + tick_offset/2.0
                # self.cmap = plt.cm.get_cmap('viridis_r', np.size(tick_locs))
                # self.cbar_label = r"Balance Parameter $\xi$"
                # self.cbar_tick_labs = ["0.0", "0.25", "0.5", "0.75", "1.0"]
                # self.cbar_ticks = tick_locs

        elif category == "xiAllboxSizeExtremes-comparison":
            self.variable = "imbalance"
            num_balance_params = 5
            xi_values = []
            self.ncol = 1
            self.configs = []
            self.markers = []
            self.markersizes = []
            self.marker_fillstyles = []
            self.outlier_indices = []
            initial_fillstyles = ["none", "full"]
            initial_markersizes = [matplotlib.rcParams['lines.markersize'], matplotlib.rcParams['lines.markersize']*1.5]
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "A075", "dcorr029", "384cube", "8xyz"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "A075", "dcorr029", "768cube"], True)
            initial_markers = ["o", "v", "s", "D", "^"]
            # initial_markers = ["o", "o", "o", "o", "o"]
            colors = pl.cm.viridis(np.linspace(0, 1, num_balance_params)) # 5 values of imbalance
            self.colors = []
            for sim in initial_configs:
                if "mf" not in sim:
                    self.configs.append(sim)
                    (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                    xi_values.append(params["imbalance"])
                    self.omegaD.append(params["omegaD"])

                    self.linestyles = ['-']
                    # Make markers depend on box size
                    if "384cube" in sim:
                        self.linestyles.append("--")
                        self.marker_fillstyles.append(initial_fillstyles[0])
                        self.markersizes.append(initial_markersizes[0])

                    elif "768cube" in sim:
                        self.linestyles.append(":")
                        self.marker_fillstyles.append(initial_fillstyles[1])
                        self.markersizes.append(initial_markersizes[1])
                        # self.colors.append(colors[-1])

                    if "im10" in sim:
                        self.colors.append(colors[0])
                        self.markers.append(initial_markers[0])
                    elif "im075" in sim:
                        self.colors.append(colors[1])
                        self.markers.append(initial_markers[1])
                    elif "im05" in sim:
                        self.colors.append(colors[2])
                        self.markers.append(initial_markers[2])
                    elif "im025" in sim:
                        self.colors.append(colors[3])
                        self.markers.append(initial_markers[3])
                    elif "im00" in sim:
                        self.colors.append(colors[4])
                        self.markers.append(initial_markers[4])
                    if "seedstudy5" in sim:
                        self.outlier_indices.append(len(self.markers) - 1)

                    self.legend_elements = [matplotlib.lines.Line2D([0], [0], ls='None', label="$N=384$", marker="P", fillstyle=initial_fillstyles[0], color="black", path_effects=[mpe.Stroke(linewidth=0, foreground='k'), mpe.Normal()], markersize=10),
                                            matplotlib.lines.Line2D([0], [0], ls='None', label="$N=768$", marker="P", fillstyle=initial_fillstyles[1], color="black", path_effects=[mpe.Stroke(linewidth=0, foreground='k'), mpe.Normal()], markersize=10)]
                    # need to adjust ticks to be in center
                    tick_offset = 1.0/num_balance_params
                    tick_locs = np.arange(0.0, 1.0, tick_offset)
                    tick_locs = tick_locs + tick_offset/2.0
                    # print(self.configs)
                    # print(self.markers)
                    # print(self.marker_fillstyles)
                    self.cmap = plt.cm.get_cmap('viridis_r', np.size(tick_locs))
                    self.cbar_label = r"Balance Parameter $\xi$"
                    self.cbar_tick_labs = ["0.0", "0.25", "0.5", "0.75", "1.0"]
                    self.cbar_ticks = tick_locs

        elif category == "xiAllboxSizeAll-comparison":
            self.variable = "imbalance"
            num_balance_params = 5
            xi_values = []
            self.ncol = 2
            self.configs = []
            self.markers = []
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "A075", "dcorr029"], True)
            initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["data_", "track"], True)
            # initial_markers = ["o", "x", "s", "d", "^", "*"]
            initial_markers = ["^", "s", "d", "o", "P", "D"]
            # initial_markers = ["v", "d", "s", "^", "<"]
            imbalance_colors = pl.cm.viridis(np.linspace(0, 1, num_balance_params)) # 5 values of imbalance
            for sim in initial_configs:
                to_include = True
                if "96cube" in sim:
                    to_include = False
                if "im00" in sim and "256cube" in sim and not ("FD120-s2" in sim):
                    to_include = False
                # if "im00" in sim and "384cube" in sim and not ("FD180-s11" in sim):
                    # to_include = False
                # if "im10" in sim and "384cube" in sim and not ("FD180-s110" in sim):
                    # to_include = False
                if to_include:
                    self.configs.append(sim)
                    (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                    xi_values.append(params["imbalance"])
                    self.omegaD.append(params["omegaD"])

                    self.linestyles = ['-']
                    # Make markers depend on box size
                    if "256cube" in sim:
                        self.linestyles.append("-")
                        self.markers.append(initial_markers[0])
                    elif "384cube" in sim:
                        self.linestyles.append("--")
                        self.markers.append(initial_markers[1])
                    elif "512cube" in sim:
                        self.linestyles.append("-.")
                        self.markers.append(initial_markers[2])
                    elif "768cube" in sim:
                        self.linestyles.append(":")
                        self.markers.append(initial_markers[3])
                    elif "1024cube" in sim:
                        self.markers.append(initial_markers[4])
                    elif "1536cube" in sim:
                        self.markers.append(initial_markers[5])

                    # Make markers and colors depend on imbalance.
                    if "im10" in sim or "data_" in sim:
                        # self.markers.append(initial_markers[0])
                        self.colors.append(imbalance_colors[0])
                    elif "im075" in sim:
                        # self.markers.append(initial_markers[1])
                        self.colors.append(imbalance_colors[1])
                    elif "im05" in sim:
                        # self.markers.append(initial_markers[2])
                        self.colors.append(imbalance_colors[2])
                    elif "im025" in sim:
                        # self.markers.append(initial_markers[3])
                        self.colors.append(imbalance_colors[3])
                    elif "im00" in sim:
                        # self.markers.append(initial_markers[4])
                        self.colors.append(imbalance_colors[4])
                self.legend_elements = [matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$N=256$",marker=initial_markers[0]),
                                        matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$N=384$", marker=initial_markers[1]),
                                        matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$N=512$", marker=initial_markers[2]),
                                        matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$N=768$", marker=initial_markers[3]),
                                        matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$N=1024$", marker=initial_markers[4]),
                                        matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$N=1536$", marker=initial_markers[5])]
                # need to adjust ticks to be in center
                tick_offset = 1.0/num_balance_params
                tick_locs = np.arange(0.0, 1.0, tick_offset)
                tick_locs = tick_locs + tick_offset/2.0
                self.cmap = plt.cm.get_cmap('viridis_r', np.size(tick_locs))
                self.cbar_label = r"Balance Parameter $\xi$"
                self.cbar_tick_labs = ["0.0", "0.25", "0.5", "0.75", "1.0"]
                self.cbar_ticks = tick_locs

        elif category == "xiAllboxSizeAllwithDcorr0-comparison":
            self.variable = "imbalance"
            xi_values = []
            self.configs = []
            self.markers = []
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "A075"], True)
            initial_markers = ["v", "d", "s", "^", "<"]
            imbalance_colors = pl.cm.viridis(np.linspace(0, 1, 5)) # 5 values of imbalance
            for sim in initial_configs:
                to_include = True
                if "96cube" in sim:
                    to_include = False
                if "im00" in sim and (("FD120-s1" in sim or "FD120-s3" in sim) or "FD120-s4" in sim):
                    to_include = False
                if to_include:
                    self.configs.append(sim)
                    (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                    xi_values.append(params["imbalance"])
                    self.omegaD.append(params["omegaD"])

                    # Make linestyles depend on box size
                    if "256cube" in sim:
                        self.linestyles.append("-")
                        self.markers.append(initial_markers[0])
                    elif "384cube" in sim:
                        self.linestyles.append("--")
                        self.markers.append(initial_markers[1])
                    elif "512cube" in sim:
                        self.linestyles.append("-.")
                        self.markers.append(initial_markers[2])
                    elif "768cube" in sim:
                        self.linestyles.append(":")
                        self.markers.append(initial_markers[3])

                    # Make markers and colors depend on imbalance.
                    if "dcorr0omega" in sim:
                        self.colors.append("black")
                    elif "im10" in sim:
                        # self.markers.append(initial_markers[0])
                        self.colors.append(imbalance_colors[0])
                    elif "im075" in sim:
                        # self.markers.append(initial_markers[1])
                        self.colors.append(imbalance_colors[1])
                    elif "im05" in sim:
                        # self.markers.append(initial_markers[2])
                        self.colors.append(imbalance_colors[2])
                    elif "im025" in sim:
                        # self.markers.append(initial_markers[3])
                        self.colors.append(imbalance_colors[3])
                    elif "im00" in sim:
                        # self.markers.append(initial_markers[4])
                        self.colors.append(imbalance_colors[4])
                self.legend_elements = [matplotlib.lines.Line2D([0], [0], color='black', ls='-', label="$N=256$",marker=initial_markers[0]),
                                   matplotlib.lines.Line2D([0], [0], color='black', ls='--', label="$N=384$", marker=initial_markers[1]),
                                   matplotlib.lines.Line2D([0], [0], color='black', ls='-.', label="$N=512$", marker=initial_markers[2])]
                self.cmap = plt.cm.get_cmap('viridis_r')
                self.cbar_label = r"Balance Parameter $\xi$"
                self.cbar_tick_labs = ["0.0", "0.25", "0.5", "0.75", "1.0"]
                self.cbar_ticks = [0.0, 0.25, 0.5, 0.75, 1.0]

        elif category == "512Imbalance-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["zeltron", "512cube"], True)
            self.variable = "imbalance"
            xi_values = []
            # Get balance parameter and driving frequency from phys_params
            for sim in self.configs:
                (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                xi_values.append(params["imbalance"])
                self.omegaD.append(params["omegaD"])

            xi_values = np.array(xi_values)
            # Sort with xi values from 1.0 to 0.0
            sorted_inds = xi_values.argsort()[::-1]
            xi_values = xi_values[sorted_inds]
            self.configs = np.array(self.configs)[sorted_inds]

            self.colors = pl.cm.viridis(np.linspace(0, 1, len(self.configs)))
            self.linestyles = ['-']
            self.cmap = plt.cm.get_cmap('viridis_r')
            self.cbar_label = r"Balance Parameter $\xi$"
            self.cbar_tick_labs = ["0.0", "0.25", "0.5", "0.75", "1.0"]
            self.cbar_ticks = [0.0, 0.25, 0.5, 0.75, 1.0]
            # self.labels = [r"$\xi={:.2f}$".format(xi) for xi in xi_values]


        elif category == "256cube-seed-comparison":
            self.configs = ["zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s4", "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s3", "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s2", "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s1"]
            self.variable = "imbalance"
            self.labels = ["Seed 4", "Seed 3", "Seed 2", "Seed 1"]
            self.linestyles = ["-"]
            self.markers = ["o", "s", "x", "^"]


        elif category == "384im00seed-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "im00", "dcorr029", "A075"], True)
            self.configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "im00", "dcorr029", "A075"], True)
            self.variable = "NM"
            self.labels = ["Seed {:d}".format(i+1) for i in np.arange(len(self.configs))]
            self.linestyles = ["-"]
            self.markers = None
            # self.markers = ["o", "s", "x", "^"]
            self.colors = pl.cm.plasma(np.linspace(0, 1, len(self.configs)))
            self.cmap = plt.cm.get_cmap('cividis')

        elif category == "384im10seed-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "im10", "dcorr029", "A075", "8xyz"], True)
            # initial_configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "im10", "dcorr029", "A075", "4xyz", "NM4"], True)
            self.configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "im10", "dcorr029", "A075"], True)
            self.variable = "NM"
            self.labels = ["Seed {:d}".format(i+1) for i in np.arange(len(self.configs))]
            self.linestyles = ["-"]
            self.markers = None
            # self.markers = ["o", "s", "x", "^"]
            self.cmap = plt.cm.get_cmap('cividis')

        elif category == "384im05seed-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "im05", "dcorr029", "A075"], True)
            self.configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "im05", "dcorr029", "A075"], True)
            self.variable = "imbalance"
            self.variable = "NM"
            self.labels = ["Seed {:d}".format(i+1) for i in np.arange(len(self.configs))]
            self.linestyles = ["-"]
            self.markers = ["o", "s", "x", "^"]
            self.markers = None

        elif category == "384im075seed-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "im075", "dcorr029", "A075"], True)
            self.configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "im075", "dcorr029", "A075"], True)
            # self.variable = "imbalance"
            self.variable = "NM"
            self.labels = ["Seed {:d}".format(i+1) for i in np.arange(len(self.configs))]
            self.linestyles = ["-"]
            self.markers = ["o", "s", "x", "^"]
            self.markers = None

        elif category == "384im025seed-comparison":
            initial_configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "im025", "dcorr029", "A075"], True)
            self.configs = initial_configs + rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["768cube", "im025", "dcorr029", "A075"], True)
            # self.variable = "imbalance"
            self.variable = "NM"
            self.labels = ["Seed {:d}".format(i+1) for i in np.arange(len(self.configs))]
            self.linestyles = ["-"]
            self.markers = ["o", "s", "x", "^"]
            self.markers = None
        elif category == "384seedstudy5-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "dcorr029", "A075", "seedstudy5"], True)
            self.variable = "imbalance"
            self.labels = ["Seed {:d}".format(i+1) for i in np.arange(len(self.configs))]
            self.linestyles = ["-"]
            self.markers = ["o", "s", "x", "^"]

        elif category == "384cube-seed-comparison":
            self.configs = rdu.get_list_of_sim_names_with_phrases(reduced_data_path, ["384cube", "dcorr029", "A075"], True)
            self.variable = "imbalance"
            self.colors = []
            self.markers = ["o"]
            self.linestyles = ['-']
            num_seeds = 8
            colors = pl.cm.inferno(np.linspace(0, 1, num_seeds))
            for sim in self.configs:
                color_set = False
                for j in np.arange(3, num_seeds+1):
                    if "seedstudy{:d}".format(int(j)) in sim:
                        self.colors.append(colors[j-1, :])
                        color_set = True
                if not color_set:
                    if "im00" in sim:
                        if "s2" in sim: self.colors.append(colors[1, :])
                        else: self.colors.append(colors[0, :])
                    else: self.colors.append(colors[0, :])

        elif category == "xi10boxSize-comparison":
            self.configs = ["zeltron_256cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD120-s10", "zeltron_384cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD180-s110", "zeltron_512cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD240-s210", "zeltron_768cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD360-s310", "data_768cube_track_64ppc", "data_1024cube_track_64ppc", "data_1536cube_track_64ppc"]
            self.variable = "NX"
            self.labels = ["$256^3$", "$384^3$", "$512^3$", "$768^3$", "$768^3$ VVZ", "$1024^3$", "$1536^3$"]
            self.linestyles = ["-", "--", ":", "-."]
            self.markers = ["o", "s", "x", "d"]
            self.ncol=2

        elif category == "xi05boxSize-comparison":
            self.configs = ["zeltron_256cube_8xyz-Jxyz_im05sigma05dcorr029omega035A075phisame_PPC32-FD120-s5", "zeltron_384cube_8xyz-Jxyz_im05sigma05dcorr029omega035A075phisame_PPC32-FD180-s15"] #, "zeltron_512cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD240-s21"]
            self.variable = "NX"
            self.labels = ["$256^3$", "$384^3$", "$512^3$"]
            self.linestyles = ["-", "--", ":"]
            self.markers = ["o", "s", "x"]

        elif category == "xi0boxSize-comparison":
            self.configs = ["zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s2", "zeltron_384cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD180-s11", "zeltron_512cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD240-s21", "zeltron_768cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD360-s30"]
            self.variable = "NX"
            self.labels = ["$256^3$", "$384^3$", "$512^3$", "$768^3$"]
            self.linestyles = ["-", "--", ":", "-."]
            self.markers = ["o", "s", "x", "d"]
            self.ncol=2

        elif category == "256-96cube-comparison":
            self.configs = ["zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s3", "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s2", "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s1", "zeltron_96cube-L1_mode-8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD30-Jext"]
            self.variable = "NX"
            xi_values = []
            # Get balance parameter and driving frequency from phys_params
            for sim in self.configs:
                (params, units) = rdu.getSimParams(simDir=path_handler(sim, self.setup).path_to_reduced_data)
                xi_values.append(params["imbalance"])
                self.omegaD.append(params["omegaD"])

            self.labels = ["$256^3$ seed 3", "$256^3$ seed 2", "$256^3$ seed 1", "$96^3$"]
            self.colors = pl.cm.viridis(np.linspace(0, 1, len(self.configs)))
            self.linestyles = ["-", "-", "-", "--"]
            self.markers = ["o", "o", "o", "x"]
        # ---------------------------------------
        else:
            print(category + " is unknown. Please add to the compare_handler.py file.")
        self.colors = np.array(self.colors)


        # ---------------------------------------
        # Set default colors, linestyles, and markers
        # ---------------------------------------
        num_sims = len(self.configs)
        if np.size(self.colors) == 0:
            if num_sims > 2:
                self.colors = pl.cm.viridis(np.linspace(0, 1, num_sims))
            else:
                self.colors = ["tab:blue", "tab:orange"]

        if not self.markers:
            self.markers = ["o", "x", "s", "d", "^", "*", "D", ">", "<", "P", "p"]
            if num_sims < len(self.markers):
                self.markers = self.markers[:num_sims]

        if not self.linestyles:
            self.linestyles = ["-", ":", "-.", "--"]
            if num_sims < len(self.linestyles):
                self.linestyles = self.linestyles[:num_sims]

        if not self.labels:
            self.labels = [""]*len(self.configs)

# ----------------------------------------------------------------------
def get_fields_to_linear_fit():
    return ["velx_total", "vely_total", "velz_total", "internalEnergyDensity", "ExB_z", "Upz_total"]

def get_all_composite_statistics(setup="stampede2", **kwargs):
    reload_all_sims = kwargs.get("reload_all_sims", False)
    reload_sims = kwargs.get("reload_sims", False)
    tinitial = kwargs.get("tinitial", 2.0) # L/c
    tfinal = kwargs.get("tfinal", 20.0) # L/c
    subtract_initial_value = kwargs.get("subtract_initial_value", False)
    t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
    tequivalent = False
    fields_to_linear_fit = kwargs.get("fields_to_linear_fit", get_fields_to_linear_fit())

    if "final_Einj" in kwargs:
        final_Einj = kwargs.get("final_Einj")
        if "tequivalent" in kwargs and kwargs["tequivalent"]:
            t_save_str = "_t{:d}-teq".format(int(tinitial))
            tequivalent = True
    if reload_all_sims:
        reload_sims = False
    configs = kwargs.get("configs", None)

    if setup == "lia_hp":
        path_to_figures = "/mnt/d/imbalanced_turbulence/figures/"
        path_to_reduced_data = "/mnt/d/imbalanced_turbulence/data_reduced/"
        path_to_raw_data = "/mnt/d/imbalanced_turbulence/data_raw/"
        path_to_scripts = "/mnt/d/imbalanced_turbulence/scripts/"

    elif setup == "stampede2":
        path_to_figures = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/figures/"
        path_to_reduced_data = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/data_reduced/"
        path_to_raw_data = "/scratch/06165/ahankla/imbalanced_turbulence/"
        path_to_scripts = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"

    stats_path = path_to_reduced_data + "composite_simulations_stats.csv"

    if os.path.exists(stats_path):
        composite_stats = pd.read_csv(stats_path, index_col=0)

    # Reload all simulations or just the ones we need
    if not os.path.exists(stats_path) or reload_all_sims or reload_sims:
        if reload_sims and os.path.exists(stats_path):
            print("Loading config simulations in reduced data...")
            reduced_sims = configs
        else:
            print("Loading all simulations in reduced data...")
            consts = rdu.Consts
            composite_stats = {}

            # Get a list of datasets to go through
            reduced_sims = fnmatch.filter(os.listdir(path_to_reduced_data), 'zeltron_*')
            reduced_sims = reduced_sims + fnmatch.filter(os.listdir(path_to_reduced_data), 'data_*')

        #  load data from sim, or delete from list if not much data
        for sim in reduced_sims:
            # print(sim)

            ph = path_handler(sim, setup)
            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            times_in_LC = retrieve_time_values(ph)[:, 2]

            tinitial_index = (np.abs(times_in_LC - tinitial)).argmin()
            tfinal_index = (np.abs(times_in_LC - tfinal)).argmin()

            # Load in total injected energy after 20 L/c
            einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
            fdump = int(params["FDUMP"])
            dt_in_s = params["dt"]
            Lx = params["xmax"] - params["xmin"]
            Ly = params["ymax"] - params["ymin"]
            Lz = params["zmax"] - params["zmin"]
            volume = Lx*Ly*Lz
            mag_energy_density = (params["B0"])**2.0/(8.0*np.pi)
            einj_norm = np.cumsum(einj)/volume/mag_energy_density
            dt_in_LC = dt_in_s*consts.c/Lz
            einj_times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
            einj_times_in_LC = np.insert(einj_times_in_LC, 0, 0)
            einj_times_in_LC = einj_times_in_LC[:-1]
            einj_time_20_ind = (np.abs(einj_times_in_LC - 20.0)).argmin()
            params["einj_density_at_20_Lc"] = -1.0*einj_norm[einj_time_20_ind]

            num_time_steps = times_in_LC.size - 1
            einj.resize((num_time_steps, fdump))
            einj = np.sum(einj, axis=1)
            einj = np.insert(einj, 0, 0)
            einj_density = einj/volume
            einj_cum = np.cumsum(einj_density)

            # Find t_equivalent
            if tequivalent:
                equivalent_time_ind = (np.abs(-1.0*einj_norm - final_Einj)).argmin()
                einj_equiv_time = einj_times_in_LC[equivalent_time_ind]
                equivalent_time_ind = (np.abs(times_in_LC - einj_equiv_time)).argmin()
                equivalent_time = times_in_LC[equivalent_time_ind]
                params["equivalent_time"] = equivalent_time
                tfinal_index = equivalent_time_ind

            if os.path.exists(ph.path_to_raw_data) and "fields_to_vavg" not in kwargs:
                fields_to_vavg = ["fluidEnergyDensity", "internalEnergyDensity", "velx_total", "vely_total", "velz_total", "deltaBrms", "VpBp_total"]
                kwargs["fields_to_vavg"] = fields_to_vavg

            (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)

            for field in kwargs["fields_to_vavg"]:
                # # Catch the efficiencies
                # if "_efficiency" in field:
                    # field_name = field.replace("_efficiency", "")
                    # field_mean = vol_means[field_name]
                    # # XX

                if vol_means[field] is None:
                    params[field + "_mean"] = None
                    params[field + "_std"] = None
                    params[field + "_mean" + t_save_str] = None
                    params[field + "_std" + t_save_str] = None
                    if field in fields_to_linear_fit:
                        params[field + "_slope"] = None
                        params[field + "_intercept"] = None
                        params[field + "_slope" + t_save_str] = None
                        params[field + "_intercept" + t_save_str] = None
                else:
                    # For all fields, find the mean and std in time
                    norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
                    normed_field = np.array(vol_means[field])/norm_value[0]

                    if subtract_initial_value:
                        normed_field = normed_field - normed_field[0]

                    params[field + "_mean"] = np.mean(normed_field)
                    params[field + "_std"] = np.std(normed_field)

                    # Now do only for specified times
                    trimmed_normed_field = normed_field[tinitial_index:tfinal_index+1]
                    params[field + "_mean" + t_save_str] = np.mean(trimmed_normed_field)
                    params[field + "_std" + t_save_str] = np.std(trimmed_normed_field)

                    # Now do linear fits if need be
                    if field in fields_to_linear_fit:
                        linear_model = np.polyfit(times_in_LC, normed_field, 1)
                        params[field + "_slope"] = linear_model[0]
                        params[field + "_intercept"] = linear_model[1]

                        # Now do only for specified times
                        linear_model = np.polyfit(times_in_LC[tinitial_index:tfinal_index+1], trimmed_normed_field, 1)
                        params[field + "_slope" + t_save_str] = linear_model[0]
                        params[field + "_intercept" + t_save_str] = linear_model[1]

                    if field == "VpBp_total":
                        # normalize to vA(t), then take the mean
                        if not os.path.exists(ph.path_to_reduced_data + "magnetization.dat"):
                            plot_length_scales(ph)
                        mag_values = np.loadtxt(ph.path_to_reduced_data + "magnetization.dat", delimiter=",", skiprows=1)
                        magnetization = mag_values[:, 1]

                        # should be able to jump every FDUMP outputs
                        jump = int(params["FDUMP"])
                        magnetization = magnetization[::jump]

                        # in case of going longer than needed
                        magnetization = magnetization[:vol_means[field].size]
                        alfven_in_c = np.sqrt(magnetization/(magnetization + 1.0))
                        norm_value = (params["B0"]*alfven_in_c, "$/B_0v_A(t)$")

                        # # Normalize to initial alfven speed
                        # sigmaDict = rdu.getSimSigmas(params)
                        # sigma0 = sigmaDict["sigma"]
                        # alfven0 = np.sqrt(sigma0/(sigma0+1.0))
                        # norm_value = (params["B0"]*alfven0, "$B_0v_{A0}$")

                        # get average value over time of perp cross-helicity
                        vpbp = np.array(vol_means[field])/norm_value[0]
                        vpbp_mean = np.mean(vpbp)
                        params[field + "_mean"] = vpbp_mean

                        # Now do only for specified times
                        field_normed = vpbp[tinitial_index:tfinal_index+1]
                        params[field + "_mean" + t_save_str] = np.mean(field_normed)
                        params[field + "_std" + t_save_str] = np.std(field_normed)

            if "pelsaesserMinusEnergy_mean" in kwargs and "pelsaesserPlusEnergy_mean" in kwargs:
                if params["pelsaesserMinusEnergy_mean"] is not None and params["pelsaesserPlusEnergy_mean"] is not None:
                    params["pelsaesserEnergyRatio"] = params["pelsaesserPlusEnergy_mean"]/params["pelsaesserMinusEnergy_mean"]
                else:
                    params["pelsaesserEnergyRatio"] = None


            # parse sim name if need be
            if "NM" not in params:
                nmodes = int((sim.split('_')[2]).split('-')[1][0])
                params["NM"] = nmodes
            if "NDECAY" not in params:
                if "decay" not in sim:
                    params["NDECAY"] = -1
                else:
                    ndecay = int((sim.split('-')[-1])[5:])
                    params["NDECAY"] = ndecay
            if "imbalance" not in params:
                if 'im' not in sim:
                    params["imbalance"] = 1.0
                else:
                    imstr = (sim.split('im')[1]).split("sigma")[0]
                    if len(imstr) == 2:
                        params["imbalance"] = np.float(imstr)/10.0
                    elif len(imstr) == 3:
                        params["imbalance"] = np.float(imstr)/100.0
            if "AD" not in params:
                A0str = (sim.split('A0')[1]).split('phi')[0]
                if len(A0str) == 2:
                    params["AD"] = np.float(A0str)/100.0
            if "gammaD" not in params:
                g0str = (sim.split('dcorr')[1]).split('omega')[0]
                if len(g0str) <= 2:
                    params["gammaD"] = np.float(g0str)/10.0
                elif len(g0str) == 3:
                    params["gammaD"] = np.float(g0str)/100.0
            if "omegaD" not in params:
                o0str = (sim.split('omega')[1]).split('A0')[0]
                if len(o0str) <= 2:
                    params["omegaD"] = np.float(o0str)/10.0
                elif len(o0str) == 3:
                    params["omegaD"] = np.float(o0str)/100.0

            composite_stats[sim] = params

        # composite_stats = pd.DataFrame.from_dict(composite_stats, orient='index')
        # composite_stats.rename(columns = {'Unnamed: 0':'sim_name'}, inplace=True)
        # print(composite_stats.columns)
        composite_stats = pd.DataFrame.from_dict(composite_stats)
        # write to file
        composite_stats.to_csv(stats_path)

    composite_stats = pd.read_csv(stats_path, index_col=0)
    # composite_stats = pd.read_csv(stats_path)
    return composite_stats

# ---------------------------------------------------------------------------

def get_comparison_stats(category, **kwargs):
    sims_to_compare = comparison(category, **kwargs).configs
    all_compare_stats = get_all_composite_statistics(**kwargs)
    compare_stats = all_compare_stats[sims_to_compare]
    return compare_stats


def get_comparison_Avar_as_Bvar(category, avar_name, bvar_name, **kwargs):
    fields_to_vavg_bak = kwargs.get("fields_to_vavg", None)
    bvar_split = bvar_name.split("_")
    if bvar_split[-1] == "slope" or bvar_split[-1] == "mean":
        bvar_split.pop()
    kwargs["fields_to_vavg"] = ["_".join(bvar_split)]

    compare_stats = get_comparison_stats(category, **kwargs)
    if fields_to_vavg_bak is not None:
        kwargs["fields_to_vavg"] = fields_to_vavg_bak

    # print(compare_stats.index)
    sim_names = compare_stats.columns.to_list()
    avar_values = []
    bvar_values = []

    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        avar_name2 = avar_name + t_save_str
        bvar_name2 = bvar_name + t_save_str

    if "tinitial" in kwargs and "final_Einj" in kwargs:
        tinitial = kwargs.get("tinitial")
        final_Einj = kwargs.get("final_Einj")
        t_save_str = "_t{:d}-teq".format(int(tinitial))
        avar_name2 = avar_name + t_save_str
        bvar_name2 = bvar_name + t_save_str


    for sim in sim_names:
        if avar_name2 in compare_stats[sim]:
            avar_name = avar_name2
        if bvar_name2 in compare_stats[sim]:
            bvar_name = bvar_name2
        avar_values.append(compare_stats[sim][avar_name])
        bvar_values.append(compare_stats[sim][bvar_name])

        if np.isnan(compare_stats[sim][bvar_name]):
            print(sim + " has no " + bvar_name)
    # Sort according to names
    category_names = comparison(category).configs

    return((avar_values, bvar_values))


def plot_comparison_Avar_as_Bvar(category, avar_name, bvar_name, **kwargs):
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    system_config = kwargs.get("system_config", "lia_hp")
    stampede2 = kwargs.get("stampede2", False)
    constant_fit = kwargs.get("constant_fit", False)
    linear_fit = kwargs.get("linear_fit", False)
    quadratic_fit = kwargs.get("quadratic_fit", False)
    ylims = kwargs.get("ylims", None)
    do_error_bars = kwargs.get("error_bars", True)
    error_bar_type = kwargs.get("error_bar_type", None)
    subtract_initial_value = kwargs.get("subtract_initial_value", False)
    tequivalent = False
    t_limited = False
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True
    if "final_Einj" in kwargs:
        tinitial = kwargs.get("tinitial", 2.0)
        final_Einj = kwargs.get("final_Einj")
        tequivalent = True
        t_limited = True
        t_save_str = "_t{:d}-teq".format(int(tinitial))


    if bvar_name == "deltaBrms_mean" or "internalEnergyDensity_slope":
        predicted_fit = kwargs.get("predicted_fit", False)
    else:
        predicted_fit = False

    if stampede2:
        system_config = "stampede2"
    if avar_name == "imbalance":
        xlabel = r"Balance parameter $\xi$"
    elif avar_name == "NX":
        # xlabel = "$N$"
        xlabel = r"$L/2\pi\rho_{e0}$"
    else:
        xlabel = avar_name
    ph0 = path_handler(comparison(category).configs[0], system_config)

    if "magneticTurbulentEnergyRatio" in bvar_name:
        (Avar, mag_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "magneticEnergyDensity_mean", **kwargs)
        (Avar, turb_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "turbulentEnergyDensity_mean", **kwargs)
        if mag_energy is not None and turb_energy is not None:
            Bvar = np.array(mag_energy)/np.array(turb_energy)
        else:
            Bvar = None
    elif "EMTurbulentEnergyRatio" in bvar_name:
        (Avar, mag_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "electromagneticEnergyDensity_mean", **kwargs)
        (Avar, turb_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "turbulentEnergyDensity_mean", **kwargs)
        if mag_energy is not None and turb_energy is not None:
            Bvar = np.array(mag_energy)/np.array(turb_energy)
            # print(Avar)
            # print(Bvar)
        else:
            Bvar = None
    elif "electricTotalRatio" in bvar_name:
        (Avar, EM_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "electromagneticEnergyDensity_mean", **kwargs)
        (Avar, electric_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "magneticEnergyDensity_mean", **kwargs)
        if EM_energy is not None and electric_energy is not None:
            Bvar = np.array(electric_energy)/np.array(EM_energy)
            # print(Avar)
        else:
            Bvar = None
    elif "intInjSlopeRatio" in bvar_name:
        (Avar, int_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "internalEnergyDensity_slope", **kwargs)
        (Avar, inj_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "cumInjectedEnergyDensity_slope", **kwargs)
        if int_energy is not None and inj_energy is not None:
            Bvar = np.array(int_energy)/np.array(inj_energy)
            # print(Avar)
            # print(Bvar)
        else:
            Bvar = None
    elif "electricEnergyDensity" in bvar_name:
        (Avar, em_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "electromagneticEnergyDensity_mean", **kwargs)
        (Avar, magnetic_energy) = get_comparison_Avar_as_Bvar(category, avar_name, "magneticEnergyDensity_mean", **kwargs)
        if em_energy is not None and magnetic_energy is not None:
            Bvar = np.array(em_energy)-np.array(magnetic_energy)
            # print(Avar)
            # print(Bvar)
        else:
            Bvar = None
    else:
        (Avar, Bvar) = get_comparison_Avar_as_Bvar(category, avar_name, bvar_name, **kwargs)
        if avar_name == "NX":
            Avar = np.array(Avar) - 1
            # TODO: generalize conversion.
            # conversion = 1.0/(2.0*np.pi*initial_larmor_radius)
            conversion = 1.0/9.42
            Avar = Avar * conversion
        # print(Bvar)
    # print(Avar)
    print(Bvar)
    print(np.mean(Bvar))
    print(np.std(Bvar))
    if do_error_bars:
        if error_bar_type == "temporal":
            error_bars = get_time_error_bars(category, bvar_name, **kwargs)
        elif error_bar_type == "both":
            (mean_dict, error_dict) = get_ensemble_error_bars([bvar_name], **kwargs)
            error_spreads = error_dict[bvar_name]
            error_medians = mean_dict[bvar_name]
            temporal_error_bars = get_time_error_bars(category, bvar_name, **kwargs)
        elif error_bar_type == "statistical":
            (mean_dict, error_dict) = get_ensemble_error_bars([bvar_name], **kwargs)
            # print("get statistical")
            # print(error_dict)
            # print(error_dict.columns)
            # print(error_dict[bvar_name])
            # print(error_dict[bvar_name].shape)
            # print(error_dict.loc[bvar_name, :])
            error_spreads = error_dict[bvar_name]
            error_medians = mean_dict[bvar_name]
            # error_value = error_dict.loc[bvar_name, :]
            # mean_value = mean_dict.loc[bvar_name, :]
            # error_value = error_dict._get_value(0, bvar_name)
            # mean_value = mean_dict._get_value(0, bvar_name)
            # print(error_dict)
            # print(error_value)
            # percent_error = error_value/mean_value
            # error_bars = np.array(Bvar)*percent_error
            # XX change to only xi=0 and 1 have error bars for 768
        elif error_bar_type is None:
            if "mean" in bvar_name and "TurbulentEnergyRatio" not in bvar_name:
                error_bars = get_time_error_bars(category, bvar_name, **kwargs)
                # now we adjust to the mean value

            elif "slope" in bvar_name or "einj" in bvar_name and "TurbulentEnergyRatio" not in bvar_name:
                (mean_dict, error_dict) = get_ensemble_error_bars([bvar_name], **kwargs)
                error_medians = mean_dict[bvar_name]
                error_spreads = error_dict[bvar_name]
                # error_value = error_dict._get_value(0, bvar_name)
                # mean_value = mean_dict._get_value(0, bvar_name)
                # percent_error = error_value/mean_value
                # error_bars = np.array(Bvar)*percent_error
                # error_bars = np.ones(len(Bvar))
                # error_bars = error_value*error_bars
            else:
                error_bars = np.zeros(np.array(Bvar).shape)

    if avar_name == "NX":
        plt.gca().set_xscale('log')
        plt.xticks(np.unique(Avar))
        plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in np.unique(Avar)])
        plt.minorticks_off()


    if "einj" not in bvar_name:
        field = ("_").join(bvar_name.split("_")[:-1])
    else:
        field = bvar_name
    norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)[1]
    if "VpBp" in field:
        norm_value = "$/B_0v_A(t)$"
        # norm_value = "$/B_0v_{A0}$"
    field_abbrev = rdu.getFieldAbbrev(field, True)
    if field == "pelsaesserEnergyRatio":
        ylabel = r"$\langle |z_\perp^+|^2\rangle/\langle |z_\perp^-|^2\rangle$"
    else:
        ylabel = ""
        if "einj" not in bvar_name:
            if bvar_name.split("_")[-1] == "mean":
                if "efficiency" in bvar_name:
                    if subtract_initial_value or "Delta" in field_abbrev:
                        field_abbrev_init = field_abbrev
                        field_abbrev = field_abbrev.replace("\Delta", "")
                    field1 = field_abbrev.split("/")[0].replace("$","")
                    field2 = field_abbrev.split("/")[1].replace("$","")
                    ylabel += r"$\overline{"
                    if subtract_initial_value or "Delta" in field_abbrev_init:
                        ylabel += "\Delta "
                    ylabel += r"\langle" + field1 + r"\rangle/\langle" + field2 + r"}\rangle}$"
                else:
                    ylabel += r"$\langle$"
                    ylabel += r"$\overline{"
                    if subtract_initial_value or "Delta" in field_abbrev:
                        field_abbrev = field_abbrev.replace("\Delta", "")
                        ylabel += "\Delta "
                    ylabel += field_abbrev.strip("$") + r"}\rangle$" + norm_value
            else:
                ylabel += field_abbrev + r"$\rangle$ " + norm_value + bvar_name.split("_")[-1]
        else:
            ylabel += r"$\langle$"
            ylabel += r"$\overline{"
            if subtract_initial_value:
                ylabel += r"$\Delta $"
            ylabel += field_abbrev.strip("$") + r"}\rangle$" + norm_value

    path_to_figures = ph0.path_to_figures + "../compare/" + category + "/scalings/"
    if not os.path.exists(path_to_figures):
        os.makedirs(path_to_figures)
    figname = path_to_figures + bvar_name + "_scaling-with_" + avar_name
    if do_error_bars:
        figname += "_errorBars"
        if "error_bar_type" in kwargs:
            if kwargs["error_bar_type"] == "statistical":
                figname += error_bar_type.capitalize()
            elif kwargs["error_bar_type"] == "both":
                figname += "temporalStatistical"
    if predicted_fit:
        figname += "_fitPredicted"
        if linear_fit:
            figname += "Linear"
        if quadratic_fit:
            figname += "Quadratic"
    elif constant_fit:
        figname += "_fitConstant"
        if linear_fit:
            figname += "Linear"
        if quadratic_fit:
            figname += "Quadratic"
    elif linear_fit:
        figname += "_fitLinear"
        if quadratic_fit:
            figname += "Quadratic"
    elif quadratic_fit:
        figname += "_fitQuadratic"
    if t_limited:
        figname += t_save_str
    if subtract_initial_value:
        figname += "_change"
    if ylims is not None:
        figname += "_yL"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    plt.figure()
    markers = itertools.cycle(comparison(category).markers)
    markersizes = itertools.cycle(comparison(category).markersizes)
    fillstyles = itertools.cycle(comparison(category).marker_fillstyles)
    colors = itertools.cycle(comparison(category).colors)
    linestyles = itertools.cycle(comparison(category).linestyles)
    if do_error_bars:
        if kwargs["error_bar_type"] == "statistical" or kwargs["error_bar_type"] == "both":
            print(error_spreads)
            error_mins = error_spreads[0]
            error_maxs = error_spreads[1]
            for j in np.arange(len(Avar)):
                x_offset = 0.05
                x = Avar[j]
                error_median = [error_medians[j]]
                error_spread = [[np.abs(error_mins[j]-error_medians[j])], [np.abs(error_maxs[j]-error_medians[j])]]
                # for x,error_median,error_spread in zip(Avar, error_medians, error_spreads):
                print(error_spread)
                print(error_median)
                eplot = plt.errorbar(x + x_offset, error_median, yerr=error_spread, marker='o', ls="None", color="black", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], fillstyle='none')
                eplot[-1][0].set_linestyle(next(linestyles))
                # eplot[-1][0].set_linestyle('-')
                eplot[-1][0].set_path_effects([mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
            if kwargs["error_bar_type"] == "both":
                for x,y,error_bar in zip(Avar, Bvar, temporal_error_bars):
                    eplot = plt.errorbar(x, y, yerr=error_bar, marker=next(markers), ls="None", color=next(colors), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
                    eplot[-1][0].set_linestyle(next(linestyles))
                    # eplot[-1][0].set_linestyle('-')
                    eplot[-1][0].set_path_effects([mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
            else:
                for x,y in zip(Avar, Bvar):
                    plt.plot(x, y, marker=next(markers), ls='None', color=next(colors), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
        else:
            for x,y,error_bar in zip(Avar, Bvar, error_bars):
                eplot = plt.errorbar(x, y, yerr=error_bar, marker=next(markers), ls="None", color=next(colors), path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
                eplot[-1][0].set_linestyle(next(linestyles))
                # eplot[-1][0].set_linestyle('-')
                eplot[-1][0].set_path_effects([mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
    else:
        for x,y in zip(Avar, Bvar):
            plt.plot(x, y, marker=next(markers), ls='None', color=next(colors), path_effects=[mpe.Stroke(linewidth=1.5, foreground='k'), mpe.Normal()], fillstyle=next(fillstyles), ms=next(markersizes))

    Avar_extended = np.linspace(Avar[0], Avar[-1], 100)
    Bvar = np.array(Bvar)
    Avar = np.array(Avar)
    nan_indices = np.where(np.isnan(Bvar))[0]
    Avar = Avar[~(np.isnan(Bvar))]
    Bvar = Bvar[~(np.isnan(Bvar))]

    if predicted_fit:
        if bvar_name == "deltaBrms_mean":
            var_to_fit = np.array(Bvar)**2.0
        else:
            var_to_fit = np.array(Bvar)
        from scipy.optimize import curve_fit
        (params, units) = rdu.getSimParams(simDir=ph0.path_to_reduced_data)
        def predicted_func(xi, a, b):
            # return a+ b*xi**2.0
            return a*(1+xi**2.0)+b
        fit_params, pcov = curve_fit(predicted_func, Avar, var_to_fit)
        fit_data = predicted_func(Avar_extended, *fit_params)
        plt.plot(Avar_extended, fit_data, label="Predicted Fit", ls='-.', color='black')
        if (bvar_name == "Upz_total_mean" or bvar_name == "ExB_z_mean") and not linear_fit:
            # get outlier indices
            non_outlier_indices = np.arange(Avar.size)
            outlier_indices = np.array(comparison(category).outlier_indices)

            # Adjust for nan indices being deleted
            for nan_index in nan_indices:
                for ind in np.arange(outlier_indices.size):
                    if nan_index < outlier_indices[ind]:
                        outlier_indices[ind] = outlier_indices[ind] - 1
            non_outlier_indices = np.delete(non_outlier_indices, outlier_indices)
            Avar_no_outliers = Avar[non_outlier_indices]
            Bvar_no_outliers = Bvar[non_outlier_indices]
            print(Bvar[outlier_indices])
            fit_params = curve_fit(predicted_func, Avar_no_outliers, Bvar_no_outliers)[0]
            linear_exclude_outliers_vals = predicted_func(Avar_extended, *fit_params)
            plt.plot(Avar_extended, linear_exclude_outliers_vals, label="Predicted fit excluding outliers", ls=':', color='black')


    if constant_fit:
        constant_model = np.polyfit(Avar, Bvar, 0)
        constant_model_vals = np.poly1d(constant_model)(Avar_extended)
        plt.plot(Avar_extended, constant_model_vals, label="Constant fit", ls='-.', color='black')
    if linear_fit:
        linear_model = np.polyfit(Avar, Bvar, 1)
        linear_model_vals = np.poly1d(linear_model)(Avar_extended)
        plt.plot(Avar_extended, linear_model_vals, label="Linear fit", ls='--', color='black')
        if (bvar_name == "Upz_total_mean" or bvar_name == "ExB_z_mean") and not predicted_fit:
            # get outlier indices
            non_outlier_indices = np.arange(len(Avar))
            outlier_indices = np.array(comparison(category).outlier_indices)
            for outlier in outlier_indices:
                non_outlier_indices = np.delete(non_outlier_indices, np.where(non_outlier_indices == outlier))
            Avar_no_outliers = np.array(Avar)[non_outlier_indices]
            Bvar_no_outliers = np.array(Bvar)[non_outlier_indices]
            linear_exclude_outliers = np.polyfit(Avar_no_outliers, Bvar_no_outliers, 1)
            linear_exclude_outliers_vals = np.poly1d(linear_exclude_outliers)(Avar_extended)
            plt.plot(Avar_extended, linear_exclude_outliers_vals, label="Linear fit excluding outliers", ls=':', color='black')
    if quadratic_fit:
        quadratic_model = np.polyfit(Avar, Bvar, 2)
        quadratic_model_vals = np.poly1d(quadratic_model)(Avar_extended)
        plt.plot(Avar_extended, quadratic_model_vals, label="Quadratic fit", ls=':', color='black')

    plt.gca().axhline([0.0], color='black', linestyle='--')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if ylims:
        plt.ylim(ylims)
    title = category
    # if "ExB_z" in bvar_name:
        # plt.gca().axhline([0.0], color='black', linestyle='-.')
    # if constant_fit:
        # title += "\n Constant fit={:.2}".format(*constant_model)
    # if linear_fit:
        # title += "\n Linear fit slope={:.2}, intercept={:.2}".format(*linear_model)
    # if quadratic_fit:
        # title += "\n Quadratic fit: {:.2}x^2+{:.2}x+{:.2}".format(*quadratic_model)
    if predicted_fit:
        title += "\n Predicted fit: {:.2f}(1+x^2)+{:.2f}".format(*fit_params)
    if t_limited and "einj" not in bvar_name:
        if tequivalent:
            title += "\n time-averaged from t={:.2f} to ".format(tinitial) + r"$t_{eq}$"
        else:
            title += "\n time-averaged from t={:.2f} to {:.2f} $L/c$".format(tinitial, tfinal)

    if "electromagneticEnergyDensity" in bvar_name or "turbulentEnergyDensity" in bvar_name:
        if not ylims:
            plt.ylim([0, 2.0])
    if "efficiency" in bvar_name:
        plt.ylim([0, 1])
        if "netEnergyDensity" in bvar_name:
            plt.ylim([0, 0.10])
    plt.title(title + "\n")
    # --------------------------------------------
    #       Special legends time!
    # --------------------------------------------
    if category == "8mImbalanceAll-comparison":
        # Next, have a legend for dcorr 0 vs. nonzero
        legend_elements = [matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$\gamma_D=0.0$",marker=comparison(category).markers[0]),
                            matplotlib.lines.Line2D([0], [0], color='black', ls='None', label="$\gamma_D=0.29\omega_A$", marker=comparison(category).markers[1])]
    sim_set = comparison(category)
    if sim_set.cbar_label is not None and (avar_name != "imbalance"):
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    # if "NX" not in avar_name:
    plt.gca().legend(handles=comparison(category).legend_elements, ncol=sim_set.ncol, frameon=True)
    if avar_name == "imbalance":
        plt.xticks(np.arange(0, 1.25, 0.25))
    if avar_name == "NX":
        plt.gca().set_xscale('log')
        plt.xticks(np.unique(Avar))
        plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in np.unique(Avar)])
        plt.minorticks_off()
    plt.tight_layout()
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches="tight")
        plt.title('')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(path_to_figures + "pdfs/"):
            os.makedirs(path_to_figures + "pdfs/")
        plt.savefig(path_to_figures + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches="tight")

        plt.gca().grid(False, which='both')
        if not os.path.isdir(path_to_figures + "pdfs/nogrids/"):
            os.makedirs(path_to_figures + "pdfs/nogrids/")
        plt.savefig(path_to_figures + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches="tight")
        if not os.path.isdir(path_to_figures + "nogrids/"):
            os.makedirs(path_to_figures + "nogrids/")
        plt.savefig(path_to_figures + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches="tight")
        plt.close()



def get_ensemble_error_bars(bvar_names, reload_seed_sims=True, **kwargs):
    """
    Use the standard deviation of the 256 cube runs with different seeds
    to estimate the error
    """

    ensemble_category = kwargs.get("ensemble_category", "xiDependent")
    if ensemble_category == "xiDependent":
        error_bar_path = "/mnt/d/imbalanced_turbulence/data_reduced/error_estimates_xiDependent.csv"
        mean_value_path = "/mnt/d/imbalanced_turbulence/data_reduced/mean_value_estimates_xiDependent.csv"
    # if ensemble_category == "xi0":
        # error_bar_path = "/mnt/d/imbalanced_turbulence/data_reduced/error_estimates_xi0.csv"
        # mean_value_path = "/mnt/d/imbalanced_turbulence/data_reduced/mean_value_estimates_xi0.csv"
        # category = "384im00seed-comparison"
    # elif ensemble_category == "xi1":
        # error_bar_path = "/mnt/d/imbalanced_turbulence/data_reduced/error_estimates_xi1.csv"
        # mean_value_path = "/mnt/d/imbalanced_turbulence/data_reduced/mean_value_estimates_xi1.csv"
        # category = "384im10seed-comparison"
    # elif ensemble_category == "xi10-00":
        # kwargs["ensemble_category"] = "xi1"
        # xi1_errors = get_ensemble_error_bars(bvar_names, **kwargs)
        # kwargs["ensemble_category"] = "xi0"
        # xi0_errors = get_ensemble_error_bars(bvar_names, **kwargs)
        # # XX
        # return (mean_value, error_bars)

    if reload_seed_sims or not os.path.exists(error_bar_path):
        print("Reloading error bars")
        error_bars = {}
        mean_value = {}
        for bvar_name in bvar_names:
            error_bars[bvar_name] = None
            mean_value[bvar_name] = []
            ebar_min = []
            ebar_max = []
            for xi in ["10", "075", "05", "025", "00"]:
                variable_values = np.array(get_comparison_Avar_as_Bvar("384im" + xi + "seed-comparison", "imbalance", bvar_name, **kwargs)[1])
                ebar_min.append(variable_values.min())
                ebar_max.append(variable_values.max())
                # spread = np.array([variable_values.min(), variable_values.max()]).reshape((2,1))
                # if error_bars[bvar_name] is None:
                    # error_bars[bvar_name] = spread
                # else:
                    # error_bars[bvar_name] = np.hstack((error_bars[bvar_name], spread))
                # print(error_bars[bvar_name].shape)
                mean_value[bvar_name].append(np.median(variable_values))
                # error_bars[bvar_name].append(np.std(variable_values))
                # mean_value[bvar_name].append(np.mean(variable_values))
            error_bars[bvar_name] = [ebar_min, ebar_max]
            # variable_values = get_comparison_Avar_as_Bvar(category, "imbalance", bvar_name, **kwargs)[1]
            # error_bars[bvar_name] = np.std(variable_values)
            # mean_value[bvar_name] = np.mean(variable_values)
        # print("error 1")
        # print(error_bars)
        # print(mean_value)
        # error_bars = pd.DataFrame(error_bars, index=[0])
        # mean_value = pd.DataFrame(mean_value, index=[0])
        xi_values = ['1.0', '0.75', '0.5', '0.25', '0.0']
        error_bars["xi"] = xi_values
        # error_bars = pd.DataFrame.from_dict(error_bars, orient='index', columns=xi_values)
        # mean_value = pd.DataFrame.from_dict(mean_value, orient='index', columns=xi_values)
        # error_bars = pd.DataFrame.from_dict(error_bars)
        # mean_value = pd.DataFrame.from_dict(mean_value)
        # error_bars.to_csv(error_bar_path, index=False)
        # mean_value.to_csv(mean_value_path, index=False)

    # error_bars = pd.read_csv(error_bar_path, index_col=0)
    # mean_value = pd.read_csv(mean_value_path, index_col=0)
    # error_bars = pd.read_csv(error_bar_path)
    # mean_value = pd.read_csv(mean_value_path)

    # print(error_bars)
    # print("columns")
    # print(error_bars.columns)
    for bvar_name in bvar_names:
        if bvar_name not in error_bars:
            new_error_bars = []
            new_mean_value = []
            for xi in ["10", "075", "05", "025", "00"]:
                variable_values = get_comparison_Avar_as_Bvar("384im" + xi + "seed-comparison", "imbalance", bvar_name, **kwargs)[1]
                new_error_bars.append(np.std(variable_values))
                new_mean_value.append(np.mean(variable_values))
            # error_bars.loc[len(error_bars)] = new_error_bars
            # mean_value.loc[len(mean_value)] = new_mean_value
            error_bars[bvar_name] = new_error_bars
            mean_value[bvar_name] = new_mean_value
            # # variable_values = get_comparison_Avar_as_Bvar(category, "imbalance", bvar_name, **kwargs)[1]
            # # error_bars[bvar_name] = np.std(variable_values)
            # # mean_value[bvar_name] = np.mean(variable_values)
            # print(variable_values)
            # print(error_bars[bvar_name])
            # print(mean_value[bvar_name])

    # print("error 2")
    # print(error_bars)
    # error_bars = pd.DataFrame(error_bars, index=[0])
    # mean_value = pd.DataFrame(mean_value, index=[0])
    # error_bars = pd.DataFrame(error_bars)
    # mean_value = pd.DataFrame(mean_value)
    # print("error 3")
    # print(error_bars)
    # error_bars.to_csv(error_bar_path, index=False)
    # mean_value.to_csv(mean_value_path, index=False)
    return (mean_value, error_bars)



def get_time_error_bars(category, bvar_name, reload_sims=False, **kwargs):
    """
    Use the standard deviation in time
    to estimate the error
    """
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
    if "tinitial" in kwargs and "final_Einj" in kwargs:
        tinitial = kwargs.get("tinitial")
        final_Einj = kwargs.get("final_Einj")
        t_save_str = "_t{:d}-teq".format(int(tinitial))

    vol_avgs_bak = kwargs.get("fields_to_vavg", None)
    kwargs["fields_to_vavg"] = [bvar_name.replace("_mean", "").replace("_slope", "")]
    compare_stats = get_comparison_stats(category, **kwargs)
    if vol_avgs_bak is not None:
        kwargs["fields_to_vavg"] = vol_avgs_bak

    error_bars = []
    bvar_std = bvar_name.replace('mean', 'std') + t_save_str
    for sim in comparison(category).configs:
        if bvar_std != bvar_name + t_save_str:
            error_bars.append(compare_stats[sim][bvar_std])
        else:
            error_bars.append(0.0)

    return error_bars
