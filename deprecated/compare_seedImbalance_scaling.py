import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from compare_plot_profiles_spatial import compare_plot_profiles_spatial
from compare_plot_profiles_temporal import compare_plot_profiles_temporal
from compare_plot_vol_avg_over_time import compare_plot_vol_avg_over_time
from compare_plot_FT_vol_avg_over_time import compare_plot_FT_vol_avg_over_time
from compare_plot_FT_profiles_temporal import compare_plot_FT_profiles_temporal
from compare_plot_FT_profiles_spatial import compare_plot_FT_profiles_spatial
from compare_plot_initial_energy_partition import compare_plot_initial_energy_partition
from compare_plot_particle_energy_spectrum_same_time import compare_plot_particle_energy_spectrum_same_time
import sys
sys.path.append("./modules/")
from compare_handler import *
from path_handler_class import path_handler
import raw_data_utils as rdu
from reduce_data_utils import *

matplotlib.use("TkAgg")

category = "boxSizeAllxi00-10"
system_config = "lia_hp"
stampede2 = False
to_show = False
reload_sims = True

do_error_bars = True
plot_final_Einj = False
cut_at_tequivalent = False
tinitial = 10.0 # L/c
tfinal = 20.0 # L/c

# error_bar_type = "statistical"
error_bar_type = None

field1 = "internalEnergyDensity_efficiency_mean"
field2 = "netEnergyDensity_efficiency_mean"
# field3 = "turbulentEnergyDensity_mean"
# field4 = "electromagneticEnergyDensity_mean"
field3 = "ExB_z_mean"
field4 = "netEnergyDensity_mean"

# -------------------------------------------------
kwargs = {}
kwargs["system_config"] = system_config
kwargs["reload_sims"] = reload_sims
kwargs["tinitial"] = tinitial
kwargs["tfinal"] = tfinal
kwargs["error_bars"] = do_error_bars
kwargs["error_bar_type"] = error_bar_type
if to_show:
    kwargs["figure_name"] = "show"

# The nuclear option
kwargs["reload_all_sims"] = True

t_save_str = ""
if "tinitial" in kwargs and "tfinal" in kwargs:
    tinitial = kwargs.get("tinitial")
    tfinal = kwargs.get("tfinal")
    t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
    t_title_str = "Averaged from $t=${:.2f} to {:.2f} $L/c$".format(tinitial, tfinal)
sim_name0 = comparison("768Imbalance-comparison").configs[0]
ph0 = path_handler(sim_name0, system_config)
path_to_figures = ph0.path_to_figures + "../compare/" + category + "/scalings/"
if not os.path.exists(path_to_figures):
    os.makedirs(path_to_figures)
field_abbrev = rdu.getFieldAbbrev(field1, True)
norm_value = rdu.getNormValue(field1, simDir=ph0.path_to_reduced_data)[1]
if "einj" not in field1:
    ylabel1 = r"$\langle$"
    ylabel1 += field_abbrev + norm_value + r"$\rangle$ " + field1.split("_")[-1]
else:
    ylabel1 = r"$\langle$"
    ylabel1 += field_abbrev + norm_value + r"$\rangle$ "
field_abbrev = rdu.getFieldAbbrev(field2, True)
norm_value = rdu.getNormValue(field2, simDir=ph0.path_to_reduced_data)[1]
if "einj" not in field2:
    ylabel2 = r"$\langle$"
    ylabel2 += field_abbrev + norm_value + r"$\rangle$ " + field2.split("_")[-1]
else:
    ylabel2 = r"$\langle$"
    ylabel2 += field_abbrev + norm_value + r"$\rangle$ "
field_abbrev = rdu.getFieldAbbrev(field3, True)
norm_value = rdu.getNormValue(field3, simDir=ph0.path_to_reduced_data)[1]
if "einj" not in field3:
    ylabel3 = r"$\langle\Delta$"
    ylabel3 += field_abbrev + norm_value + r"$\rangle$ " + field3.split("_")[-1]
else:
    ylabel3 = r"$\langle\Delta$"
    ylabel3 += field_abbrev + norm_value + r"$\rangle$ "
field_abbrev = rdu.getFieldAbbrev(field4, True)
norm_value = rdu.getNormValue(field4, simDir=ph0.path_to_reduced_data)[1]
if "einj" not in field4:
    ylabel4 = r"$\langle\Delta$"
    ylabel4 += field_abbrev + norm_value + r"$\rangle$ " + field4.split("_")[-1]
else:
    ylabel4 = r"$\langle$"
    ylabel4 += field_abbrev + norm_value + r"$\rangle$ "

seed00field1 = get_comparison_Avar_as_Bvar("384im00seed-comparison", "imbalance", field1, **kwargs)[1]
seed00field2 = get_comparison_Avar_as_Bvar("384im00seed-comparison", "imbalance", field2, **kwargs)[1]
seed00field3 = get_comparison_Avar_as_Bvar("384im00seed-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
seed00field4 = get_comparison_Avar_as_Bvar("384im00seed-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
seed025field1 = get_comparison_Avar_as_Bvar("384im025seed-comparison", "imbalance", field1, **kwargs)[1]
seed025field2 = get_comparison_Avar_as_Bvar("384im025seed-comparison", "imbalance", field2, **kwargs)[1]
seed025field3 = get_comparison_Avar_as_Bvar("384im025seed-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
seed025field4 = get_comparison_Avar_as_Bvar("384im025seed-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
seed05field1 = get_comparison_Avar_as_Bvar("384im05seed-comparison", "imbalance", field1, **kwargs)[1]
seed05field2 = get_comparison_Avar_as_Bvar("384im05seed-comparison", "imbalance", field2, **kwargs)[1]
seed05field3 = get_comparison_Avar_as_Bvar("384im05seed-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
seed05field4 = get_comparison_Avar_as_Bvar("384im05seed-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
seed075field1 = get_comparison_Avar_as_Bvar("384im075seed-comparison", "imbalance", field1, **kwargs)[1]
seed075field2 = get_comparison_Avar_as_Bvar("384im075seed-comparison", "imbalance", field2, **kwargs)[1]
seed075field3 = get_comparison_Avar_as_Bvar("384im075seed-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
seed075field4 = get_comparison_Avar_as_Bvar("384im075seed-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
seed10field1 = get_comparison_Avar_as_Bvar("384im10seed-comparison", "imbalance", field1, **kwargs)[1]
seed10field2 = get_comparison_Avar_as_Bvar("384im10seed-comparison", "imbalance", field2, **kwargs)[1]
seed10field3 = get_comparison_Avar_as_Bvar("384im10seed-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
seed10field4 = get_comparison_Avar_as_Bvar("384im10seed-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
(xi_values, imbalance768field1) = get_comparison_Avar_as_Bvar("768Imbalance-comparison", "imbalance", field1, **kwargs)
imbalance768field2 = get_comparison_Avar_as_Bvar("768Imbalance-comparison", "imbalance", field2, **kwargs)[1]
imbalance768field3 = get_comparison_Avar_as_Bvar("768Imbalance-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
imbalance768field4 = get_comparison_Avar_as_Bvar("768Imbalance-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
imbalance512field1 = get_comparison_Avar_as_Bvar("512Imbalance-comparison", "imbalance", field1, **kwargs)[1]
imbalance512field2 = get_comparison_Avar_as_Bvar("512Imbalance-comparison", "imbalance", field2, **kwargs)[1]
imbalance512field3 = get_comparison_Avar_as_Bvar("512Imbalance-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
imbalance512field4 = get_comparison_Avar_as_Bvar("512Imbalance-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
imbalance256field1 = get_comparison_Avar_as_Bvar("256Imbalance-comparison", "imbalance", field1, **kwargs)[1]
imbalance256field2 = get_comparison_Avar_as_Bvar("256Imbalance-comparison", "imbalance", field2, **kwargs)[1]
imbalance256field3 = get_comparison_Avar_as_Bvar("256Imbalance-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
imbalance256field4 = get_comparison_Avar_as_Bvar("256Imbalance-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
imbalance1024field1 = get_comparison_Avar_as_Bvar("1024Imbalance-comparison", "imbalance", field1, **kwargs)[1]
imbalance1024field2 = get_comparison_Avar_as_Bvar("1024Imbalance-comparison", "imbalance", field2, **kwargs)[1]
imbalance1024field3 = get_comparison_Avar_as_Bvar("1024Imbalance-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
imbalance1024field4 = get_comparison_Avar_as_Bvar("1024Imbalance-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]
imbalance1536field1 = get_comparison_Avar_as_Bvar("1536Imbalance-comparison", "imbalance", field1, **kwargs)[1]
imbalance1536field2 = get_comparison_Avar_as_Bvar("1536Imbalance-comparison", "imbalance", field2, **kwargs)[1]
imbalance1536field3 = get_comparison_Avar_as_Bvar("1536Imbalance-comparison", "imbalance", field3, **kwargs, subtract_initial_value=True)[1]
imbalance1536field4 = get_comparison_Avar_as_Bvar("1536Imbalance-comparison", "imbalance", field4, **kwargs, subtract_initial_value=True)[1]

plt.figure()
figure_name = path_to_figures + field1 + "_All" + t_save_str + ".png"
plt.plot(np.ones(len(seed00field1))*0.0, seed00field1, 'bs', ls='None')
plt.plot(np.ones(len(seed025field1))*0.25, seed025field1, 'bs', ls='None')
plt.plot(np.ones(len(seed05field1))*0.5, seed05field1, 'bs', ls='None')
plt.plot(np.ones(len(seed075field1))*0.75, seed075field1, 'bs', ls='None')
plt.plot(np.ones(len(seed10field1))*1.0, seed10field1, 'bs', ls='None', label=r"$N=384$")
plt.plot(xi_values, imbalance256field1, 'm^', ls='None', label=r"$N=256$")
plt.plot(xi_values, imbalance512field1, 'gd', ls='None', label=r"$N=512$")
plt.plot(xi_values, imbalance768field1, 'ro', ls='None', label=r"$N=768$")
plt.plot(1.0, imbalance1024field1, 'yP', ls='None', label=r"$N=1024$")
plt.plot(1.0, imbalance1536field1, 'cD', ls='None', label=r"$N=1536$")
# plt.ylim([0, 1])
# plt.gca().set_yscale('log')
handles, labels = plt.gca().get_legend_handles_labels()
labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t))
# labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
plt.legend(handles, labels, frameon=False)
plt.xlabel(r"Balance parameter $\xi$")
plt.xticks(np.arange(0, 1.25, 0.25))
plt.ylabel(ylabel1)
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")

plt.figure()
figure_name = path_to_figures + field2 + "_All" + t_save_str + ".png"
plt.plot(np.ones(len(seed00field2))*0.0, seed00field2, 'bs', ls='None')
plt.plot(np.ones(len(seed025field2))*0.25, seed025field2, 'bs', ls='None')
plt.plot(np.ones(len(seed05field2))*0.5, seed05field2, 'bs', ls='None')
plt.plot(np.ones(len(seed075field2))*0.75, seed075field2, 'bs', ls='None')
plt.plot(np.ones(len(seed10field2))*1.0, seed10field2, 'bs', ls='None', label=r"$N=384$")
plt.plot(xi_values, imbalance256field2, 'm^', ls='None', label=r"$N=256$")
plt.plot(xi_values, imbalance512field2, 'gd', ls='None', label=r"$N=512$")
plt.plot(xi_values, imbalance768field2, 'ro', ls='None', label=r"$N=768$")
plt.plot(1.0, imbalance1024field2, 'yP', ls='None', label=r"$N=1024$")
plt.plot(1.0, imbalance1536field2, 'cD', ls='None', label=r"$N=1536$")
plt.ylim([0, 0.1])
# plt.gca().set_yscale('log')
handles, labels = plt.gca().get_legend_handles_labels()
# labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t))
plt.legend(handles, labels, frameon=False)
plt.xlabel(r"Balance parameter $\xi$")
plt.xticks(np.arange(0, 1.25, 0.25))
plt.ylabel(ylabel2)
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")
plt.close()

plt.figure()
figure_name = path_to_figures + field3 + "_All" + t_save_str + ".png"
plt.plot(np.ones(len(seed00field3))*0.0, seed00field3, 'bs', ls='None')
plt.plot(np.ones(len(seed025field3))*0.25, seed025field3, 'bs', ls='None')
plt.plot(np.ones(len(seed05field3))*0.5, seed05field3, 'bs', ls='None')
plt.plot(np.ones(len(seed075field3))*0.75, seed075field3, 'bs', ls='None')
plt.plot(np.ones(len(seed10field3))*1.0, seed10field3, 'bs', ls='None', label=r"$N=384$")
plt.plot(xi_values, imbalance256field3, 'm^', ls='None', label=r"$N=256$")
plt.plot(xi_values, imbalance512field3, 'gd', ls='None', label=r"$N=512$")
plt.plot(xi_values, imbalance768field3, 'ro', ls='None', label=r"$N=768$")
plt.plot(1.0, imbalance1024field3, 'yP', ls='None', label=r"$N=1024$")
plt.plot(1.0, imbalance1536field3, 'cD', ls='None', label=r"$N=1536$")
# plt.ylim([0, 2.0])
# plt.gca().set_yscale('log')
handles, labels = plt.gca().get_legend_handles_labels()
# labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0][3:]))
plt.legend(handles, labels, frameon=False)
plt.xlabel(r"Balance parameter $\xi$")
plt.xticks(np.arange(0, 1.25, 0.25))
plt.ylabel(ylabel3)
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")
plt.close()

plt.figure()
figure_name = path_to_figures + field4 + "_All" + t_save_str + ".png"
plt.plot(np.ones(len(seed00field4))*0.0, seed00field4, 'bs', ls='None')
plt.plot(np.ones(len(seed025field4))*0.25, seed025field4, 'bs', ls='None')
plt.plot(np.ones(len(seed05field4))*0.5, seed05field4, 'bs', ls='None')
plt.plot(np.ones(len(seed075field4))*0.75, seed075field4, 'bs', ls='None')
plt.plot(np.ones(len(seed10field4))*1.0, seed10field4, 'bs', ls='None', label=r"$N=384$")
plt.plot(xi_values, imbalance256field4, 'm^', ls='None', label=r"$N=256$")
plt.plot(xi_values, imbalance512field4, 'gd', ls='None', label=r"$N=512$")
plt.plot(xi_values, imbalance768field4, 'ro', ls='None', label=r"$N=768$")
plt.plot(1.0, imbalance1024field4, 'yP', ls='None', label=r"$N=1024$")
plt.plot(1.0, imbalance1536field4, 'cD', ls='None', label=r"$N=1536$")
plt.ylim([0, 1.0])
# plt.ylim([0, 2.0])
# plt.gca().set_yscale('log')
handles, labels = plt.gca().get_legend_handles_labels()
# labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t))
plt.legend(handles, labels, frameon=False)
plt.xlabel(r"Balance parameter $\xi$")
plt.xticks(np.arange(0, 1.25, 0.25))
plt.ylabel(ylabel4)
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")
plt.close()

plt.figure()
figure_name = path_to_figures + "sumInternalNet_All" + t_save_str + ".png"
plt.plot(np.ones(len(seed00field1))*0.0, np.array(seed00field1) + np.array(seed00field2), 'bs', ls='None')
plt.plot(np.ones(len(seed025field1))*0.25, np.array(seed025field1) + np.array(seed025field2), 'bs', ls='None')
plt.plot(np.ones(len(seed05field1))*0.5, np.array(seed05field1) + np.array(seed05field2), 'bs', ls='None')
plt.plot(np.ones(len(seed075field1))*0.75, np.array(seed075field1) + np.array(seed075field2), 'bs', ls='None')
plt.plot(np.ones(len(seed10field1))*1.0, np.array(seed10field1) + np.array(seed10field2), 'bs', ls='None', label=r"$N=384$")
plt.plot(xi_values, np.array(imbalance256field1) + np.array(imbalance256field2), 'm^', ls='None', label=r"$N=256$")
plt.plot(xi_values, np.array(imbalance512field1) + np.array(imbalance512field2), 'gd', ls='None', label=r"$N=512$")
plt.plot(xi_values, np.array(imbalance768field1) + np.array(imbalance768field2), 'ro', ls='None', label=r"$N=768$")
plt.plot(1.0, np.array(imbalance1024field1) + np.array(imbalance1024field2), 'yP', ls='None', label=r"$N=1024$")
plt.plot(1.0, np.array(imbalance1536field1) + np.array(imbalance1536field2), 'cD', ls='None', label=r"$N=1536$")
plt.ylim([0, 1.0])
# plt.gca().set_yscale('log')
handles, labels = plt.gca().get_legend_handles_labels()
# labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t))
plt.legend(handles, labels, frameon=False)
plt.xlabel(r"Balance parameter $\xi$")
plt.xticks(np.arange(0, 1.25, 0.25))
plt.ylabel(r'$(\Delta\mathcal{E}_{\rm int}+\Delta\mathcal{E}_{\rm net})/\mathcal{E}_{\rm inj}$')
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")
plt.close()

(box_sizes_xi0, field1_values_boxSize_xi0) = get_comparison_Avar_as_Bvar("xi0boxSize-comparison", "NX", field1, **kwargs)
field2_values_boxSize_xi0 = get_comparison_Avar_as_Bvar("xi0boxSize-comparison", "NX", field2, **kwargs)[1]
field3_values_boxSize_xi0 = get_comparison_Avar_as_Bvar("xi0boxSize-comparison", "NX", field3, **kwargs, subtract_initial_value=True)[1]
field4_values_boxSize_xi0 = get_comparison_Avar_as_Bvar("xi0boxSize-comparison", "NX", field4, **kwargs, subtract_initial_value=True)[1]
box_sizes_xi0 = np.array(box_sizes_xi0) - 1
(box_sizes_xi10, field1_values_boxSize_xi10) = get_comparison_Avar_as_Bvar("xi10boxSize-comparison", "NX", field1, **kwargs)
field2_values_boxSize_xi10 = get_comparison_Avar_as_Bvar("xi10boxSize-comparison", "NX", field2, **kwargs)[1]
field3_values_boxSize_xi10 = get_comparison_Avar_as_Bvar("xi10boxSize-comparison", "NX", field3, **kwargs, subtract_initial_value=True)[1]
field4_values_boxSize_xi10 = get_comparison_Avar_as_Bvar("xi10boxSize-comparison", "NX", field4, **kwargs, subtract_initial_value=True)[1]
box_sizes_xi10 = np.array(box_sizes_xi10) - 1

# # Make xi=0 box size plot
# plt.figure()
# figure_name = path_to_figures + field1 + "_xi00" + t_save_str + ".png"
# plt.plot(box_sizes_xi0, field1_values_boxSize_xi0, 'bo', ls='None')
# plt.plot(box_sizes_xi0, field2_values_boxSize_xi0, 'gs', ls='None')
# plt.plot(np.ones(len(seed00field1))*384, seed00field1, 'bo', ls='None', label=r"$N=384$")
# plt.plot(np.ones(len(seed00field2))*384, seed00field2, 'gs', ls='None', label=r"$N=384$")
# plt.xlabel(r"$N$")
# plt.ylim([5e-3, 1])
# plt.gca().set_yscale('log')
# plt.gca().set_xscale('log')
# plt.title(r"$\xi=0.0$")
# plt.xticks(box_sizes_xi0)
# plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in box_sizes_xi0])
# plt.minorticks_off()
# plt.tight_layout()
# plt.gca().grid(color='.9', ls='--')
# plt.gca().tick_params(top=True, right=True, direction='in', which='both')
# print("Saving figure " + figure_name)
# plt.savefig(figure_name, bbox_inches="tight")
# plt.close()
# 
# # Make xi=1 box size plot
# plt.figure()
# figure_name = path_to_figures + field1 + "_xi10" + t_save_str + ".png"
# plt.plot(box_sizes_xi10, field1_values_boxSize_xi10, 'bo', ls='None')
# plt.plot(box_sizes_xi10, field2_values_boxSize_xi10, 'gs', ls='None')
# plt.plot(np.ones(len(seed10field1))*384, seed10field1, 'bo', ls='None', label=r"$N=384$")
# plt.plot(np.ones(len(seed10field2))*384, seed10field2, 'gs', ls='None', label=r"$N=384$")
# plt.ylim([5e-3, 1])
# plt.gca().set_yscale('log')
# plt.gca().set_xscale('log')
# plt.xlabel(r"$N$")
# plt.title(r"$\xi=1.0$")
# plt.gca().set_xticks(box_sizes_xi10)
# plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in box_sizes_xi10])
# plt.gca().get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
# plt.minorticks_off()
# plt.tight_layout()
# plt.gca().grid(color='.9', ls='--')
# plt.gca().tick_params(top=True, right=True, direction='in', which='both')
# print("Saving figure " + figure_name)
# plt.savefig(figure_name, bbox_inches="tight")
# plt.close()

colors = pl.cm.viridis(np.linspace(0, 1, np.size(xi_values)))
# Make field1 box size plot
plt.figure()
figure_name = path_to_figures + field1 + t_save_str + ".png"
plt.plot(box_sizes_xi0, field1_values_boxSize_xi0, color=colors[-1], marker='^', ls='None', label=r"$\xi=0.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(box_sizes_xi10, field1_values_boxSize_xi10, color=colors[0], marker='o', ls='None', label=r"$\xi=1.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed00field1))*384, seed00field1, color=colors[-1], marker='^', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed10field1))*384, seed10field1, color=colors[0], marker='o', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.legend(frameon=False)
plt.xlabel(r"$N$")
plt.ylim([0, 1])
# plt.gca().set_yscale('log')
plt.gca().set_xscale('log')
plt.ylabel(ylabel1)
plt.xticks(box_sizes_xi10)
plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in box_sizes_xi10])
plt.minorticks_off()
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")

# Make field2 box size plot
plt.figure()
figure_name = path_to_figures + field2 + t_save_str + ".png"
plt.plot(box_sizes_xi0, field2_values_boxSize_xi0, color=colors[-1], marker='^', ls='None', label=r"$\xi=0.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(box_sizes_xi10, field2_values_boxSize_xi10, color=colors[0], marker='o', ls='None', label=r"$\xi=1.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed00field2))*384, seed00field2, color=colors[-1], marker='^', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed10field2))*384, seed10field2, color=colors[0], marker='o', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.legend(frameon=False)
plt.xlabel(r"$N$")
plt.ylim([0, 0.1])
# plt.gca().set_yscale('log')
plt.gca().set_xscale('log')
plt.ylabel(ylabel2)
plt.xticks(box_sizes_xi10)
plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in box_sizes_xi10])
plt.minorticks_off()
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")

# Make field3 box size plot
plt.figure()
figure_name = path_to_figures + field3 + t_save_str + ".png"
plt.plot(box_sizes_xi0, field3_values_boxSize_xi0, color=colors[-1], marker='^', ls='None', label=r"$\xi=0.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(box_sizes_xi10, field3_values_boxSize_xi10, color=colors[0], marker='o', ls='None', label=r"$\xi=1.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed00field3))*384, seed00field3, color=colors[-1], marker='^', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed10field3))*384, seed10field3, color=colors[0], marker='o', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.legend(frameon=False)
plt.xlabel(r"$N$")
# plt.ylim([0, 2.0])
# plt.gca().set_yscale('log')
plt.gca().set_xscale('log')
plt.ylabel(ylabel3)
plt.xticks(box_sizes_xi10)
plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in box_sizes_xi10])
plt.minorticks_off()
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")

# Make field4 box size plot
plt.figure()
figure_name = path_to_figures + field4 + t_save_str + ".png"
plt.plot(box_sizes_xi0, field4_values_boxSize_xi0, color=colors[-1], marker='^', ls='None', label=r"$\xi=0.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(box_sizes_xi10, field4_values_boxSize_xi10, color=colors[0], marker='o', ls='None', label=r"$\xi=1.0$", path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed00field4))*384, seed00field4, color=colors[-1], marker='^', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.plot(np.ones(len(seed10field4))*384, seed10field4, color=colors[0], marker='o', ls='None', path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
plt.legend(frameon=False)
plt.xlabel(r"$N$")
plt.ylim([0, 1.0])
# plt.ylim([0, 2.0])
plt.gca().set_xscale('log')
plt.ylabel(ylabel4)
plt.xticks(box_sizes_xi10)
plt.gca().set_xticklabels(["{:d}".format(int(n)) for n in box_sizes_xi10])
plt.minorticks_off()
plt.tight_layout()
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.title(t_title_str)
print("Saving figure " + figure_name)
plt.savefig(figure_name, bbox_inches="tight")
# plt.show()
