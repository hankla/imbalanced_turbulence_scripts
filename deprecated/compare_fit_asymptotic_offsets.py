import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
import matplotlib.patheffects as mpe
from compare_handler import *
from matplotlib.lines import Line2D
from scipy import optimize
"""
This script will fit the energy efficiencies to A + B/t to find
the asymptotic values of each quantity. The idea is to see whether
a quantity is predominately decaying (mostly B), or increases with
the amount of injected energy (mostly A).

TO DO:
- Make tinitial, tfinal flexible.
- Handle outliers better
"""

def compare_fit_asymptotic_offsets(category, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    tinitial = 5.0
    tfinal = 20.0
    t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
    t_tit_str = "\n Fit from t={:.2f} - {:.2f} L/c".format(tinitial, tfinal)
    x_offset = 0.03

    sim_set = comparison(category)
    fields = ["internalEnergyDensity_efficiency", "turbulentEnergyDensity_efficiency", "netEnergyDensity_efficiency", "electromagneticEnergyDensity_efficiency"]
    kwargs["fields_to_vavg"] = fields
    asymptotic_values = {}
    final_values = {}
    xi_values = []
    for field in fields:
        asymptotic_values[field] = []
        final_values[field] = []

    outlier_indices = []
    for i, sim_name in enumerate(sim_set.configs):
        if "seedstudy5" in sim_name:
            outlier_indices.append(i)
        ph = path_handler(sim_name, "lia_hp")
        vol_means = retrieve_variables_vol_means_stds(ph, **kwargs)[0]
        times_in_LC = retrieve_time_values(ph)[:, 2]
        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        Lz = params["zmax"] - params["zmin"]
        Ly = params["ymax"] - params["ymin"]
        Lx = params["xmax"] - params["xmin"]
        volume = Lx*Ly*Lz
        # Cut values to start at fit_start_time
        cut_index = (np.abs(tinitial - times_in_LC)).argmin()
        cut_times = times_in_LC[cut_index:]
        fine_cut_times = np.linspace(cut_times[0], cut_times[-1], 1000)
        xi_values.append(params["imbalance"])

        # Load in injected energy
        if os.path.exists(ph.path_to_raw_data):
            einj = np.loadtxt(ph.path_to_raw_data + "Einj.dat")
        elif os.path.exists(ph.path_to_reduced_data + "Einj.dat"):
            einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
        else:
            print("Einj.dat not available. Injected energy NOT plotted!")
            return
        fdump = int(params["FDUMP"])
        num_time_steps = times_in_LC.size - 1
        einj.resize((num_time_steps, fdump))
        einj = np.sum(einj, axis=1)
        einj = np.insert(einj, 0, 0)
        einj = einj/volume
        einj = -einj
        einj_cum = np.cumsum(einj)

        for field in fields:
            field_mean = vol_means[field]
            # Delta_field_mean = field_mean - field_mean[0]
            # cut_values = Delta_field_mean[cut_index:]/einj_cum[cut_index:]
            cut_values = field_mean[cut_index:]
            fit_params, fit_e = optimize.curve_fit(inverse_time_decay, cut_times, cut_values, bounds=([-np.inf, 0], [np.inf, np.inf]))
            asymptotic_values[field].append(fit_params[1])
            final_values[field].append(field_mean[-1])



    xi_values = np.array(xi_values)
    int_offsets = np.array(asymptotic_values["internalEnergyDensity_efficiency"])
    net_offsets = np.array(asymptotic_values["netEnergyDensity_efficiency"])
    turb_offsets = np.array(asymptotic_values["turbulentEnergyDensity_efficiency"])
    EM_offsets = np.array(asymptotic_values["electromagneticEnergyDensity_efficiency"])
    final_int = np.array(final_values["internalEnergyDensity_efficiency"])
    final_turb = np.array(final_values["turbulentEnergyDensity_efficiency"])
    final_net = np.array(final_values["netEnergyDensity_efficiency"])
    # xi_values = np.array([0.0, 0.25, 0.5, 0.75, 1.0])
    # int_offsets = np.array([0.93, 0.987, 0.975, 0.889, 0.984])
    # net_offsets = np.array([0.08, 0.0091, 0.01, 0.03, 0])
    # EM_offsets = np.array([0, 0, 0, 0.0067, 0])
    # turb_offsets = np.array([0.047, 0.0524, 0.039, 0.070, 0.047])
    # final_int = np.array([0.739, 0.782, 0.872, 0.834, 0.882])
    # final_turb = np.array([0.106, 0.104, 0.078, 0.089, 0.074])
    # final_net = np.array([0.069, 0.018, 0.001, 0.022, 0.004])


    ph0 = path_handler(sim_set.configs[0], "lia_hp")
    # set up figure info
    figdir = ph0.path_to_figures + "../compare/" + category + "/energy-partition/asymptotic_offsets/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "internalEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.plot(xi_values, int_offsets, 'ro', ls="None")
    plt.plot(xi_values + x_offset, int_offsets, 'ro', ls="None", fillstyle='none')
    for xy in zip(xi_values, int_offsets):
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels(["{:.2f}".format(xi) for xi in xi_values])
    plt.ylim([0, 1])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Internal energy density")
    titstr = category + t_tit_str
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    plt.legend(handles=lines_to_add, labels=labels_to_add, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "EMEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    plt.figure()
    plt.plot(xi_values, EM_offsets, 'ro', ls="None")
    plt.plot(xi_values + x_offset, EM_offsets, 'ro', ls="None", fillstyle='none')
    for xy in zip(xi_values, EM_offsets):
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 1])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("EM energy density")
    titstr = category + t_tit_str
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    plt.legend(handles=lines_to_add, labels=labels_to_add, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "turbEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    plt.figure()
    plt.plot(xi_values, final_turb, 'ro', ls="None")
    plt.plot(xi_values + x_offset, turb_offsets, 'ro', ls="None", fillstyle='none')
    # for xy in zip(xi_values, turb_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 0.2])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Turbulent energy density")
    titstr = category + t_tit_str
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    plt.legend(handles=lines_to_add, labels=labels_to_add, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "netEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    plt.figure()
    # for xy in zip(xi_values, net_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    for xi, asymptotic, final, marker, color in zip(xi_values, net_offsets, final_net, sim_set.markers, sim_set.colors):
        plt.plot(xi, final, linestyle='None', marker=marker, color=color) # path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
        plt.plot(xi + x_offset, asymptotic, linestyle='None', marker=marker, color=color, fillstyle='none') # path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()])
    # plt.plot(xi_values, final_net, 'ro', ls="None", label="Internal")
    # plt.plot(xi_values + x_offset, net_offsets, 'ro', ls="None", fillstyle='none')
    # plt.plot(xi_values[outlier_indices], final_net[outlier_indices], 'bo', ls="None", label="Internal")
    # plt.plot(xi_values[outlier_indices] + x_offset, net_offsets[outlier_indices], 'bo', ls="None", fillstyle='none')
    # print(outlier_indices)
    # print(xi_values[outlier_indices])
    # print(net_offsets[outlier_indices])
    plt.gca().set_xticks(np.unique(xi_values))
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 0.2])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Net energy density")
    titstr = category + t_tit_str
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    plt.legend(handles=lines_to_add, labels=labels_to_add, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "sumIntNetEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    plt.figure()
    plt.plot(xi_values, int_offsets + net_offsets, 'ro', ls="None")
    for xy in zip(xi_values, int_offsets + net_offsets):
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 1.1])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Internal + Net energy density")
    titstr = category + t_tit_str
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    plt.legend(handles=lines_to_add, labels=labels_to_add, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "sumAllEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    plt.figure()
    plt.plot(xi_values, int_offsets + turb_offsets + EM_offsets + net_offsets, 'ro', ls="None")
    for xy in zip(xi_values, int_offsets + turb_offsets + EM_offsets + net_offsets):
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 1.1])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Internal + Net energy density")
    titstr = category + t_tit_str
    plt.title(titstr)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    plt.legend(handles=lines_to_add, labels=labels_to_add, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "NetTurbEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    plt.figure()
    # plt.plot(xi_values + x_offset, int_offsets, 'ro', ls="None", fillstyle='none')
    # for xy in zip(xi_values, int_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.plot(xi_values + x_offset, turb_offsets, 'g^', ls="None", fillstyle='none')
    # for xy in zip(xi_values, turb_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.plot(xi_values + x_offset, net_offsets, 'bs', ls="None", fillstyle='none')
    # for xy in zip(xi_values, net_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    # plt.plot(xi_values, final_int, 'ro', ls="None", label="Internal")
    plt.plot(xi_values, final_turb, 'g^', ls="None", label="Turbulent")
    plt.plot(xi_values, final_net, 'bs', ls="None", label="Net")
    # legend1 = plt.legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    legend1 = plt.legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles + lines_to_add
    labels = labels + labels_to_add
    plt.legend(handles=handles, labels=labels, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 0.20])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Fraction of injected energy")
    titstr = category + t_tit_str
    plt.title(titstr + "\n")
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    # set up figure info
    figname = figdir + "AllEnergyDensity"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)
    x_offset = 0.03

    # Body of the plot.
    plt.figure()
    plt.plot(xi_values + x_offset, int_offsets, 'ro', ls="None", fillstyle='none')
    # for xy in zip(xi_values, int_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.plot(xi_values + x_offset, turb_offsets, 'g^', ls="None", fillstyle='none')
    # for xy in zip(xi_values, turb_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.plot(xi_values + x_offset, net_offsets, 'bs', ls="None", fillstyle='none')
    # for xy in zip(xi_values, net_offsets):
        # # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy, textcoords='data')
        # plt.gca().annotate('{:.3f}'.format(xy[1]), xy=xy)
    plt.plot(xi_values, final_int, 'ro', ls="None", label="Internal")
    plt.plot(xi_values, final_turb, 'g^', ls="None", label="Turbulent")
    plt.plot(xi_values, final_net, 'bs', ls="None", label="Net")
    # legend1 = plt.legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    legend1 = plt.legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    lines_to_add = [Line2D([0],[0], color='black', linestyle='None', marker='o'), Line2D([0], [0], color='black', linestyle='None', marker='o', fillstyle='none')]
    labels_to_add = ['Measured', 'Fit']
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = handles + lines_to_add
    labels = labels + labels_to_add
    plt.legend(handles=handles, labels=labels, bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    plt.gca().set_xticks(xi_values)
    plt.gca().set_xticklabels([0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 1.00])
    plt.xlabel(r"Balance parameter $\xi$")
    plt.ylabel("Fraction of injected energy")
    titstr = category + t_tit_str
    plt.title(titstr + "\n")
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()

    print(sim_set.configs)

def inverse_time_decay(time_values, amplitude, offset):
    return amplitude*1.0/time_values + offset


if __name__ == '__main__':

    # ------- inputs ---------
    # category = "768Imbalance-comparison"
    # category = "xiAllboxSizeAll-comparison"
    category = "xiAllboxSizeExtremes-comparison"
    overwrite_data = False

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data

    # --------------------
    compare_fit_asymptotic_offsets(category, **kwargs)
