"""
File adapted from Greg Werner's zPy/for3D/zfileUtil.py (sent March 3, 2020)

TO DO:
    - implement cross helicity (calculate, norms, abbrev...)
"""
import os
import sys
import shutil
import glob
import numpy
import tables
import re

class Consts: # cgs
  pass
Consts.c = 299792458e2 # cm/s
Consts.e = 4.8032068e-10 # statcoulombs
Consts.k = 1.380658e-16 # erg/K
Consts.h = 6.6260755e-27 # erg s
Consts.m_e = 9.1093897e-28 # g
Consts.m_p = 1.6726231e-24 # g
Consts.evtoerg = 1.602177e-12 # erg/eV
Consts.pi = numpy.pi
Consts.r_e = Consts.e**2 / (Consts.m_e * Consts.c**2) # cm

class Zvariables: # all variables that can be calculated
  pass
Zvariables.options = ["density3_ions", "density3_electrons", "density3_total",
                      "Bx", "By", "Bz",
                      "Ex", "Ey", "Ez",
                      "Jx_external", "Jy_external", "Jz_external",
                      "larmorRadius", "skinDepth", "magnetization",
                      "alfvenSpeedRms", "chargeDensity",
                      "Jx_electrons", "Jy_electrons", "Jz_electrons",
                      "Jx_ions", "Jy_ions", "Jz_ions",
                      "velx_electrons", "velx_ions", "velx_total",
                      "vely_electrons", "vely_ions", "vely_total",
                      "velz_electrons", "velz_ions", "velz_total",
                      "Jmag", "ExB_x", "ExB_y", "ExB_z",
                      "EdotJ_electrons", "EdotJ_ions",
                      "EdotJf",
                      "vExB_x", "vExB_y", "vExB_z",
                      "VdotB_electrons", "VdotB_ions",
                      "VdotB_total", "VdotdB_total",
                      "Brms", "Brms2", "deltaBrms", "deltaBrms2", "deltaBrms2vA",
                      "vrms_electrons", "vrms_ions", "vrms_total",
                      "pelsaesserEnergy", "pelsaesserDifference",
                      "pelsaesserMinusEnergy", "pelsaesserPlusEnergy",
                      "elsaesserPlusx", "elsaesserPlusy", "elsaesserPlusz",
                      "elsaesserMinusx", "elsaesserMinusy", "elsaesserMinusz",
                      "elsaesserMinusRms", "elsaesserPlusRms", "elsaesserRatio",
                      "elsaesserEnergyMinus", "elsaesserEnergyPlus",
                      "elsaesserEnergyRatio", "avgPtclEnergy",
                      "Upx_electrons", "Upx_ions", "Upx_total",
                      "Upy_electrons", "Upy_ions", "Upy_total",
                      "Upz_electrons", "Upz_ions", "Upz_total",
                      "Uprms", "fluidEnergyDensity", "internalEnergyDensity",
                      "deltaUprms", "dUprmsOverEint", "dUprms2OverEint",
                      "bulkEnergyDensity", "bulkLorentzFactor",
                      "heatEnergyDensity", "netEnergyDensity", "turbulentEnergyDensity",
                      "magneticEnergyDensity", "electricEnergyDensity",
                      "electromagneticEnergyDensity", "electricTotalRatio",
                      "cumInjectedEnergyDensity", "intInjSlopeRatio",
                      # "vBalignment_ions", "vBalignment_electrons", "vBalignment_total",
                      # "vBalignmentAngle_ions", "vBalignmentAngle_electrons", "vBalignmentAngle_total",
                      "Jtotal_x", "Jtotal_y", "Jtotal_z",
                      "zEnergyDensity", "perpEnergyDensity",
                      "Debye",
                      "VzBz_electrons", "VzBz_ions", "VzBz_total",
                      "VzdBz_electrons", "VzdBz_ions", "VzdBz_total",
                      "VpBp_electrons", "VpBp_ions", "VpBp_total",
                      "relativisticVz1", "relativisticVz2"
]

Zvariables.restFrame_options = [
                      "fluidEnergyDensity_restFrame", "internalEnergyDensity_restFrame",
                      "bulkEnergyDensity_restFrame",
                      "VdotB_electrons_restFrame", "VdotB_ions_restFrame",
                      "VdotB_total_restFrame", "VdotdB_total_restFrame",
                      "VzBz_electrons_restFrame", "VzBz_ions_restFrame", "VzBz_total_restFrame",
                      "VzdBz_electrons_restFrame", "VzdBz_ions_restFrame", "VzdBz_total_restFrame",
                      "VpBp_electrons_restFrame", "VpBp_ions_restFrame", "VpBp_total_restFrame"
]
# these restFrame options should NOT be volume-averaged
Zvariables.restFrame_options_NOvavg = ["velx_electrons_restFrame", "velx_ions_restFrame", "velx_total_restFrame",
                      "vely_electrons_restFrame", "vely_ions_restFrame", "vely_total_restFrame",
                      "velz_electrons_restFrame", "velz_ions_restFrame", "velz_total_restFrame",
                      "Upx_electrons_restFrame", "Upx_ions_restFrame", "Upx_total_restFrame",
                      "Upy_electrons_restFrame", "Upy_ions_restFrame", "Upy_total_restFrame",
                      "Upz_electrons_restFrame", "Upz_ions_restFrame", "Upz_total_restFrame",
                      ]
Zvariables.flux_options = ["ExB_", "B", "E"] # fill this out more

def strBegin(s, begin): #{
  return len(s) >= len(begin) and s[:len(begin)] == begin
#}
def strEnd(s, end): #{
  return len(s) >= len(end) and s[-len(end):] == end
#}

def getZfileUtilDir(): #{
  dirname = __file__
  if not strEnd(dirname, "zfileUtil.py"):
    msg = "cannot find directory containing zfileUtil.py"
    raise RuntimeError(msg)
  dirname = '/'.join(dirname.split("/")[:-1])
  if strEnd(dirname, "for3D"):
    dirname = dirname[:-2] + "2D"
  return dirname
#}

def detectDisplay(): #{
  """
  import matplotlib # this is actually not necessary
  import zfileUtil
  zfileUtil.detectDisplay()
  (with detectDisplay called before any other matplotlib import)
  should be called to allow saving figures to files even when no
  display is present.
  """
  #interactiveShell = ("DISPLAY" in os.environ and
  #  (osoadColorMaps): #{
  #cmapData = numpy.loadtxt(getZfileUtilDir() + "/GMT_split.rgb",
  # From: http://www.ncl.ucar.edu/Document/Graphics/ColorTables/GMT_split.shtml
  # A colormap with from blue to red going throug black at center.
  #ncolors= 40
  # r g b
  import matplotlib
  cmapData = numpy.array([
    [0.476863, 0.476863, 0.975098], [0.426667, 0.426667, 0.925294],
    [0.376471, 0.376471, 0.875490], [0.326275, 0.326275, 0.825686],
    [0.276078, 0.276078, 0.775882], [0.225882, 0.225882, 0.726078],
    [0.175686, 0.175686, 0.676275], [0.125490, 0.125490, 0.626471],
    [0.075294, 0.075294, 0.576667], [0.025098, 0.025098, 0.526863],
    [0.000000, 0.000000, 0.476863], [0.000000, 0.000000, 0.426667],
    [0.000000, 0.000000, 0.376471], [0.000000, 0.000000, 0.326275],
    [0.000000, 0.000000, 0.276078], [0.000000, 0.000000, 0.225882],
    [0.000000, 0.000000, 0.175686], [0.000000, 0.000000, 0.125490],
    [0.000000, 0.000000, 0.075294], [0.000000, 0.000000, 0.025098],
    [0.025098, 0.000000, 0.000000], [0.075294, 0.000000, 0.000000],
    [0.125490, 0.000000, 0.000000], [0.175686, 0.000000, 0.000000],
    [0.225882, 0.000000, 0.000000], [0.276078, 0.000000, 0.000000],
    [0.326275, 0.000000, 0.000000], [0.376471, 0.000000, 0.000000],
    [0.426667, 0.000000, 0.000000], [0.476863, 0.000000, 0.000000],
    [0.526863, 0.025098, 0.025098], [0.576667, 0.075294, 0.075294],
    [0.626471, 0.125490, 0.125490], [0.676275, 0.175686, 0.175686],
    [0.726078, 0.225882, 0.225882], [0.775882, 0.276078, 0.276078],
    [0.825686, 0.326275, 0.326275], [0.875490, 0.376471, 0.376471],
    [0.925294, 0.426667, 0.426667], [0.975098, 0.476863, 0.476863],
    ])
    # Following produces a colormap with 40 discrete colors
    #cmap = matplotlib.colors.ListedColormap(cmapData, name='GMT_split')
    #matplotlib.cm.register_cmap(name='GMT_split', cmap=cmap)
    # Following produces a continuous colormap
  def seqToSegments(ra): #{
      n = len(ra)
      segs = numpy.empty((n,3))
      segs[:,0] = numpy.arange(n)/(n-1.)
      segs[:,1] = ra
      segs[:,2] = ra
      return segs
    #}
  cdict = {'red':seqToSegments(cmapData[:,0]),
           'green':seqToSegments(cmapData[:,1]),
           'blue':seqToSegments(cmapData[:,2])}
  matplotlib.cm.register_cmap(name='GMT_split', data=cdict)
  #cmap = matplotlib.pyplot.get_cmap("GMT_split")
  #}

#}

def getColorSeq(numc, cmap = "brg"): #{
  import matplotlib
  import matplotlib.pyplot as plt
  # sequential color maps without white or yellow: copper, cool, winter
  # viridis has a little orangeish-yellow that's not too bad
  # other colormaps without white/yellow: brg, rainbow (just a little yellow)
  # (Many other color maps can be used by excluding one end.)
  # Cooley doesn't have viridis, but does have brg and rainbow.
  cmapStr = cmap
  cmap = plt.get_cmap(cmap)
  #print [cmap(x/4.) for x in range(6)]
  avoidEnds = (cmap != "brg")
  if avoidEnds:
    #colors = [cmap(float(c+1)/(numc+1)) for c in range(numc)]
    colors = [cmap(float(c+0.5)/(numc)) for c in range(numc)]
  elif numc == 1:
    colors = [cmap(0)]
  elif numc == 2 and cmapStr == "brg":
    colors = ["b", "r"]
  else:
    colors = [cmap(float(c)/(numc-1)) for c in range(numc)]
  return colors
#}

def getDashSeq(num): #{
  dashes = [(20,0.01)]
  if (num < 7):
    for i in range(4,4+(num-1)*3+1,3): #{
      dashes.append( (i, i/2, i, i/2) )
      dashes.append( (i+3, i/2+3, i/2, i/2) )
    #}
  else:
    for i in range(4,4+((num+2)/3-1)*2+1,2): #{
      dashes.append( (i, i/2, i, i/2) )
      dashes.append( (i+2, i/2+2, i/2, i/2) )
      dashes.append( (i+2, i/2+2, i/2, i/2, i/2, i/2) )
    #}
  return dashes
#}

def getWidthSeq(numc): #{
  widths = [2,4] * (int(numc/2)+1)
  return widths
#}

def numListToStr(lyst, formatStr = "%.4g"): #{
  res = '[' + ', '.join([formatStr % x for x in lyst]) + ']'
  return res
#}

def getArgsAndKwargs(allArgs): #{
  """
  Take a list of strings (e.g., command line arguments)
  and split them into args and kwargs -- a kwarg has an equals sign in
  it, e.g., "name=val".  Returns the args as they are, in a list, and
  return a dictionary of kwargs, converting the value through evaluation.
  """
  args = []
  kwargs = dict()
  for a in allArgs: #{
    if '=' in a: #{
      parts = a.split("=")
      k = parts[0]
      vStr = '='.join(parts[1:])
      #k,vStr = a.split("=")
      try: #{
      # don't let local variables (like k and a and vStr) get evaluated
        #v = eval(vStr)
        v = eval(vStr, dict(), dict())
      except: #}{
        v = vStr
      #}
      kwargs[k] = v
    else: #}{
      args.append(a)
    #}
  #}
  return (args, kwargs)
#}

def saveFig(defaultName, **kwargs): #{
  """
  Given a default name and a dictionary kwargs:
    "save" -> True/False or a string name
    "saveExts" -> e.g., ["png", "pdf"]
    "overwrite" -> False, True
    "saveCmd" -> False, True, or string name
                 appends the command-line to the fig. file + "Cmd.txt"
                 (or to the name, if specified)
  If save = True, then the the file will be saved as ./plots/defaultName.
    (if defaultName is None, an exception will be raised)
  Returns whether a file was saved.
  """
  fileNames = []
  exts = ["png"]
  if "saveExts" in kwargs:
    exts = kwargs["saveExts"]
  save = kwargs["save"] if "save" in kwargs else False
  if save: #{
    if isinstance(kwargs["save"], str): #{
      fileNames = [kwargs["save"]+"."+ext for ext in exts]
      cmdFileName = kwargs["save"] + "Cmd.txt"
    else: #}{
      if not os.path.exists("./plots"): #{
        print("You must make 'mkdir plots' before saving", end=' ')
        print("(or specify save=filename)")
        cmdFileName = None
      else: #}{
        if defaultName is None:
          msg = 'No default name has been specified; save=filename must '
          msg += 'give the filename.'
          raise ValueError(msg)
        basename = "./plots/" + defaultName
        fileNames = [basename+"."+ext for ext in exts]
        cmdFileName = basename + "Cmd.txt"
      #}
    #}
  #}
  fn2 = []
  ow = False
  if "overwrite" in kwargs:
    ow = kwargs["overwrite"]
  for f in fileNames: #{
    if ow or not os.path.exists(f): #{
      fn2.append(f)
    else: #}{
      print("I refuse to overwrite " + f + " (use overwrite=True to override)")
      fn2 = []
      break
    #}
  #}
  if len(fn2) > 0: #{
    import matplotlib
    for f in fn2: #{
      print("Saving " + f)
      bbox_inches = kwargs["bbox_inches"
        ] if "bbox_inches" in kwargs else None
      matplotlib.pyplot.savefig(f, bbox_inches=bbox_inches)
    #}
  #}
  saveCmd = save
  if "saveCmd" in kwargs:
    saveCmd = kwargs["saveCmd"]
    if saveCmd is None:
      saveCmd = save
    if isinstance(saveCmd, str): #{
      cmdFileName = saveCmd
    #}
  #}
  if saveCmd: #{
    with open(cmdFileName,'a') as f: #{
      f.write('#')
      f.write(repr(sys.argv))
      f.write('\n')
      f.write('# executed in dir: ')
      f.write(repr(os.getcwd()))
      f.write('\n')
    #}
  #}
  return save
#}

def rfft2PowSpec(ra, norm = None): #{
  """
  Return the 2D power spectrum of the real 2D array ra.
  If ra has upper periodic bounds (e.g., ra[-1] == ra[0]), the upper row
  will be removed before the fft.

  If (after removing periodic upper bounds)
  ra.shape = (m,n), the result has shape (m//2+1,n//2+1),
  with negative frequency contributions added to positive frequencies; so
  result[0,0] is the 0-frequency component, and
  result[m//2, n//2] is the Nyquist component.

  Note on normalization (considering 1D for simplicity).
  If norm=None, then numpy.fft.rfft2 returns
    f_k := Sum_x f_x e^{-ikx} (though double check numpy.fft to be sure)
  For physical gridded quantities f_x = f(x), we expect f_x to be
  independent of grid cell size dx.  Therefore,
    F_k := f_k dx = Sum_x f_x e^{-ikx} dx
  is also nominally independent of dx (for small dx and small k).
  Therefore, the caller might want to modify the returned f_k to get F_k.
  (Or rather, since this returns |f_{kx,ky}|^2, multiply it by dx^2 dy^2.)
  Note that:
     Sum_x |f_x|^2 dx = (dx/N) Sum_k |f_k|^2 = 1/(N dx) Sum_k |F_k|^2
   = 1/(2pi) Sum_k |F_k|^2 dk
      where dk = 2pi/L = 2pi/(N dx)
  """
  (m,n) = ra.shape
  # check for periodic upper bound
  if (abs(ra[-1,:] - ra[0,:]) < 1e-6*abs(ra[0,:])).all():
    # upper row is copy of row 0, so delete.
    m -= 1
  if (abs(ra[:,-1] - ra[:,0]) < 1e-6*abs(ra[:,0])).all():
    # upper row is copy of row 0, so delete.
    n -= 1
  ra = ra[:m,:n]
  fra = numpy.fft.rfft2(ra, norm=norm)
  # Because we use rfft2, the negative frequency coefs (which are just
  # the conjugates of the positive frequency coefs) are missing for
  # axis = -1.  So we square and multiply by 2.
  sfra = 2* abs(fra)**2
  # But now we need to combine postive/negative-freq coefs for axis=0.
  if (m%2==0): #{
    # m is even, so FT has m//2 + 1 elements
    fm = m//2 + 1
    sfra[1:fm-1] += sfra[m-1:fm-1:-1]
  else: #}{
    # m is odd, so FT has m//2 + 1 elements
    fm = m//2 + 1
    sfra[1:fm] += sfra[m-1:fm-1:-1]
  #}
  # We don't have to do the above for the second dimension because
  #   rfft2 already did it.
  res = sfra[:fm]
  return res
#}

def getFieldAbbrev(field, TeX=True): #{
  """
  Takes a zeltron field name and return an abbreviated name;
  if TeX=True, the abbreviated name has $$ signs and TeX symbols.

  field can be multiple fields separated by "+"
  """
  if "+" in field:
    str1 = "+".join([getFieldAbbrev(f, TeX=True) for f in field.split("+")])
    return str1

  def search(pat, name, matchObjList): #{
    mo = re.match(pat, name)
    matchObjList[0] = mo
    return mo
  #}
  moList = [None]
  if search("mapxyz_(.*)_bg", field, moList): #{
    c = moList[0].group(1)[0]
    res = ("n_b%s" % c, r"$n_{b%s}$" % c)
  elif search("mapxyz_(.*)_drift", field, moList): #}{
    c = moList[0].group(1)[0]
    res = ("n_d%s" % c, r"$n_{d%s}$" % c)
  elif field in ["Bx", "By", "Bz", "Ex", "Ey", "Ez"]:
    res = (field[0] + "_" + field[1], "$" + field[0] + "_" + field[1] + "$")
  elif "Debye" in field:
    res = (field, "$\lambda_D$")
  elif "Jx" in field or "Jy" in field or "Jz" in field:
    if "electrons" in field.split("_"):
      res = (field[0] + "_e" + field[1], "$" + field[0] + "_{e" + field[1] + "}$")
    else:
      res = (field[0] + "_i" + field[1], "$" + field[0] + "_{i" + field[1] + "}$")
  elif "Jtot" in field:
    res = (field, "$J_{tot," + field[-1]+"}$")
  elif "einj_density" in field:
    res = (field, "$\mathcal{E}_{inj}(t=20.0)$")
  elif "chargeDensity" in field:
    res = (field, "$e(n_i(t) - n_e(t))$")
  elif "relativisticVz" in field:
    res = (field, "$v_{net}$")
  elif "alfvenSpeed" in field:
    # total only
    res = (field, "$v_A(t)$")
  elif "vel" in field:
    if "electrons" in field.split("_"):
      field_name = field[0] + "_e" + field[3]
      field_latex = "$" + field[0] + "_{e, " + field[3]
    elif "ions" in field.split("_"):
      field_name = field[0] + "_i" + field[3]
      field_latex = "$" + field[0] + "_{i, " + field[3]
    else:
      field_name = field[0] + "_{tot}" + field[3]
      field_latex = "$" + field[0] + "_{tot, " + field[3]
    if "restFrame" in field:
      field_latex += ", rF"
      field_name += "_rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "EdotJ" in field:
    if "electrons" in field.split("_"):
      res = ("EdotJ_e", "$E\cdot J_e$")
    elif "ions" in field.split("_"):
      res = ("EdotJ_i", "$E\cdot J_i$")
    else:
      res = ("EdotJf", "$E\cdot J_f$")
  elif field[0:3] == "ExB":
    res = ("ExB_" + field[4], r"$(E\times B)_" + field[4] + "$")
  elif field[0:4] == "vExB":
    res = ("vExB_" + field[5], r"$v(E\times B)_" + field[5] + "$")
  elif field == "Jmag":
    res = ("Jmag", r"$|J|$")
  elif "density3" in field:
    if "electrons" in field.split("_"):
      res = ("n_e", "$n_e(t)$")
    if "ions" in field.split("_"):
      res = ("n_i", "$n_i(t)$")
    if "total" in field.split("_"):
      res = ("n_tot", "$n_{tot}(t)$")
  elif "VdotB" in field:
    if "electrons" in field.split("_"):
      field_name = "BdotV_e"
      field_latex = "$B\cdot v_{e"
    elif "ions" in field.split("_"):
      field_name = "BdotV_i"
      field_latex = "$B\cdot v_{i"
    else:
      field_name = "BdotV_tot"
      field_latex = "$B\cdot v_{tot"
    if "restFrame" in field:
      field_name += "_restFrame"
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "VzBz" in field:
    field_name = field
    if "electrons" in field.split("_"):
      field_latex = "$B_z v_{z,e"
    elif "ions" in field.split("_"):
      field_latex = "$B_z v_{z,i"
    else:
      field_latex = "$B_z v_{z,tot"
    if "restFrame" in field:
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "VzdBz" in field:
    field_name = field
    if "electrons" in field.split("_"):
      field_latex = "$\delta B_z v_{z,e"
    elif "ions" in field.split("_"):
      field_latex = "$\delta B_z v_{z,i"
    else:
      field_latex = "$\delta B_z v_{z,tot"
    if "restFrame" in field:
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "VpBp" in field:
    field_name = field
    if "electrons" in field.split("_"):
      field_latex = "$B_\perp\cdot v_{\perp,e"
    elif "ions" in field.split("_"):
      field_latex = "$B_\perp\cdot v_{\perp,i"
    else:
      field_latex = "$B_\perp\cdot v_{\perp,tot"
    if "restFrame" in field:
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "VdotdB" in field:
    if "electrons" in field.split("_"):
      field_name = "dBdotV_e"
      field_latex = "$\delta B\cdot v_{e"
    elif "ions" in field.split("_"):
      field_name = "dBdotV_i"
      field_latex = "$\delta B\cdot v_{i"
    else:
      field_name = "dBdotV_tot"
      field_latex = "$\delta B\cdot v_{tot"
    if "restFrame" in field:
      field_name += "_restFrame"
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "electromagneticEnergyDensity" in field:
    field_latex = r"$\mathcal{E}_{\rm EM}$"
    if "efficiency" in field:
      field_latex = r"$\Delta$" + field_latex + r"$/\mathcal{E}_{\rm inj}$"
    res = (field, field_latex)
  elif field == "electricEnergyDensity":
    res = ("electricEnergyDensity", r"$\mathcal{E}_{\rm elec}$")
  elif field == "cumInjectedEnergyDensity":
    res = (field, r"$\mathcal{E}_{\rm inj}$")
  elif field == "Erms":
    res = ("Erms", r"$E_{rms}$")
  elif field == "Erms2":
    res = ("Erms2", r"$E_{rms}^2$")
  elif field == "magneticEnergyDensity":
    res = ("magneticEnergyDensity", r"$\mathcal{E}_{\rm mag}$")
  elif field == "Brms":
    res = ("Brms", r"$B_{rms}$")
  elif field == "Brms2":
    res = ("Brms2", r"$B_{rms}^2$")
  elif field == "deltaBrms":
    res = ("deltaBrms", r"$\delta B_{rms}$")
  elif field == "deltaBrms2":
    res = ("deltaBrms2", r"$\delta B_{rms}^2$")
  elif field == "deltaBrms2vA":
    res = ("deltaBrms2vA", r"$\delta B_{rms}^2v_A(t)$")
  elif field == "zEnergyDensity":
    res = ("zEnergyDensity", r"$\frac{1}{2}\rho v_z^2$")
  elif field == "perpEnergyDensity":
    res = ("perpEnergyDensity", r"$\frac{1}{2}\rho (v_x^2+v_y^2)$")
  elif "vrms" in field:
    if "electrons" in field.split("_"):
      field_name = "v_electrons_rms"
      field_latex = "$v_{e,rms"
    elif "ions" in field.split("_"):
      field_name = "v_ions_rms"
      field_latex = "$v_{i,rms"
    else:
      field_name = "v_total_rms"
      field_latex = "$v_{tot,rms"
    if "restFrame" in field:
      field_name += "_restFrame"
      field_latex += ",rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "pelsaesserEnergy" in field:
    res = ("pelsaesserEnergy", r"$(|z^+_\perp|^2 + |z^-_\perp|^2)$")
  elif "pelsaesserDifference" in field:
    res = ("pelsaesserDifference", r"$(|z^+_\perp|^2 - |z^-_\perp|^2)$")
  elif "pelsaesserPlusEnergy" in field:
    res = ("pelsaesserPlusEnergy", r"$|z^+_\perp|^2$")
  elif "pelsaesserMinusEnergy" in field:
    res = ("pelsaesserMinusEnergy", r"$|z^-_\perp|^2$")
  elif "elsaesserPlus" in field:
    if "Rms" in field:
      res = ("elsaesserPlus_rms", r"$z^+_{rms}$")
    else:
      res = ("elsaesserPlus_" + field[-1], r"$z^+_" + field[-1] + "$")
  elif "elsaesserMinus" in field:
    if "Rms" in field:
      res = ("elsaesserMinus_rms", r"$z^-_{rms}$")
    else:
      res = ("elsaesserMinus_" + field[-1], r"$z^-_" + field[-1] + "$")
  elif "elsaesserRatio" in field:
    res = ("elsaesserRatio", r"$z^+/z^-$")
  elif "elsaesserEnergyRatio" in field:
    res = ("elsaesserEnergyRatio", r"$(z^+/z^-)^2$")
  elif "elsaesserEnergyMinus" in field:
    res = ("elsaesserEnergyMinus", r"$z_-^2$")
  elif "elsaesserEnergyPlus" in field:
    res = ("elsaesserEnergyPlus", r"$z_+^2$")
  elif "fluidEnergyDensity" in field:
    field_name = "fluidEnergyDensity"
    field_latex = r"$\mathcal{E}_{f"
    if "restFrame" in field:
      field_name += "_restFrame"
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "internalEnergyDensity" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{int"
    if "restFrame" in field:
      field_name += "_restFrame"
      field_latex += ", rF"
    field_latex += "}$"
    if "efficiency" in field:
      field_latex = r"$\Delta$" + field_latex + r"$/\mathcal{E}_{\rm inj}$"
    res = (field_name, field_latex)
  elif "heatEnergyDensity" in field:
    field_name = "heatEnergyDensity"
    field_latex = r"$\mathcal{E}_{heat}$"
    res = (field_name, field_latex)
  elif "netEnergyDensity" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{net}$"
    if "efficiency" in field:
      field_latex = r"$\Delta$" + field_latex + r"$/\mathcal{E}_{\rm inj}$"
    res = (field_name, field_latex)
  elif "turbulentEnergyDensity" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{turb}$"
    if "efficiency" in field:
      field_latex = r"$\Delta$" + field_latex + r"$/\mathcal{E}_{\rm inj}$"
    res = (field_name, field_latex)
  elif "magneticTurbulentEnergyRatio" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{\rm mag}/\mathcal{E}_{\rm turb}$"
    res = (field_name, field_latex)
  elif "EMTurbulentEnergyRatio" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{\rm EM}/\mathcal{E}_{\rm turb}$"
    res = (field_name, field_latex)
  elif "electricTotalRatio" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{\rm elec}/\mathcal{E}_{\rm EM}$"
    res = (field_name, field_latex)
  elif "intInjSlopeRatio" in field:
    field_name = field
    field_latex = r"$\dot\mathcal{E}_{\rm int}/\dot\mathcal{E}_{\rm inj}$"
    res = (field_name, field_latex)
  elif "bulkEnergyDensity" in field:
    field_name = field
    field_latex = r"$\mathcal{E}_{bulk"
    if "restFrame" in field:
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)
  elif "magnetization" in field:
    res = ("sigma", r"$\sigma(t)$")
  elif "avgPtclEnergy" in field:
    res = ("avgPtclEnergy", r"$E_f$")
  elif "larmorRadius" in field:
    res = ("larmor radius", r"$\rho_e(t)$")
  elif "skinDepth" in field:
    res = ("skin depth", r"$d_e(t)$")
  elif "bulkLorentzFactor" in field:
    res = ("bulkLorentzFactor", r"$\Gamma_{eff}$")
  elif "vBalignmentAngle" in field:
    if "electrons" in field.split("_"):
      res = ("veBalignmentAngle", r"$\arccos\left(\frac{v_e\cdot \delta B}{|v_e||\delta B|}\right)$")
    elif "ions" in field.split("_"):
      res = ("viBalignmentAngle", r"$\arccos\left(\frac{v_i\cdot \delta B}{|v_i||\delta B|}\right)$")
    else:
      res = ("vtotBalignmentAngle", r"$\arccos\left(\frac{v_{tot}\cdot \delta B}{|v_{tot}||\delta B|}\right)$")
  elif "vBalignment" in field:
    if "electrons" in field.split("_"):
      res = ("veBalignment", r"$\frac{v_e\cdot \delta B}{|v_e||\delta B|}$")
    elif "ions" in field.split("_"):
      res = ("viBalignment", r"$\frac{v_i\cdot \delta B}{|v_i||\delta B|}$")
    else:
      res = ("vtotBalignment", r"$\frac{v_{tot}\cdot \delta B}{|v_{tot}||\delta B|}$")
  elif "Up" in field:
    field_name = field
    if "rms" in field.split("_"):
      field_latex = "$\mathcal{P}_{rms"
    else:
      field_latex = "$\mathcal{P}_{" + field[2]

    if "electrons" in field.split("_"):
      field_latex += ",e"
    elif "ions" in field.split("_"):
      field_latex += ",i"
    else:
      field_latex += ",tot"
    if "restFrame" in field:
      field_name += "_restFrame"
      field_latex += ", rF"
    field_latex += "}$"
    res = (field_name, field_latex)

  else: #}{
    res = (field, field)
  #}
  if TeX:
    res = res[1]
  else:
    res = res[0]
  return res
#}

def getNormValue(quantity, params = None, errIfUnknown = True,
                 addDivSign = True, **kwargs): #{
  """
  params should be getSimParams()[0] if known; if None, getSimParams()
    will be called (which means this must be called from data/)

  quantity = a string like "Bx" or "Ex"

  returns (normValue, normValueStr, quantityStr)
    where normValue is an appropriate normative value for quantity,
      (i.e., the quantity should be divided by normValue to get a useful number)
    and normValueStr is a string describing the norm,
    and quantityStr is a string describing quantity
    If quantity is unknown, an exception is raised if errIfUnknown,
      or (1, "") is returned.
  """
  if params is None:
    simDir = kwargs.get("simDir", ".")
    params, units = getSimParams(simDir=simDir)
  sigmaDict = getSimSigmas(params)
  rho=Consts.c/params["omegac"]
  B0=params["B0"]
  dx = params["dx"]
  sigma = sigmaDict["sigma"] # note this is the convention in Zhdankin+2020, NOT 2018
  if "density" in params:
    nb = params["density"]
  else:
    nd = params["Drift. dens."]
    nb = params["density ratio"] * nd
  # default
  norm = (1., "")
  q = quantity
  qStr = quantity
  # reduction factor (1=no reduction)
  rf = 1
  while len(q) > 0 and q[-1]=="R":
    q = q[:-1]
    rf += 1
  def getDensityNorm(q): #{
    """
    return (densityNormalization, densNormStr, particle mass)
    Note: this is problematic for combining bg and drift, or worse,
      electrons and ions.
    """
    nNorm = (nb, r"$n_{e0}$")
    mass = Consts.m_e
    if "drift" in q:
      nNorm = (nb, r"$n_{e0}$")
    elif "bg" in q:
      nNorm = (nb, r"$n_{e0}$")
    if "ion" in q:
      mass *= params["mass ratio"]
    return nNorm + (mass,)
  #}
  if (len(q) > 3 and q[:3]=="yee"):
    q = q[3:]
  if '/' in q:
    q = q.split("/")[-1]
  if (len(q) == 2 or (len(q)==4 and q[1:]=="mag")) and q[0] in "BE":
    norm = (B0, r"$B_0$")
    qStr = quantity
  elif q == "Az":
    Ls, Ns = getSimLengths(params=params)
    norm = (B0*Ls[0], "$B_0 L_x$")
    qStr = r'$A_z$'

  elif len(q) > 8 and q[:5]=="mapxy":
    species = "electrons" if "lec" in q else "ions"
    if "bg" in q[-2:]:
      norm = (nb, r"$n_{e0}$")
      qStr = r"$n_{b" + species[0] + "}$"
    elif "drift" in q:
      norm = (nb, r"$n_{e0}$")
      qStr = r"$n_{d" + species[0] + "}$"
    elif errIfUnknown:
      msg = "the normalized value for quantity '" + q + "' is unknown."
      raise ValueError(msg)
  elif len(q)>3 and q[:3]=="JxB": #}{
    norm = (nb*Consts.e*Consts.c * B0, "(e n_{e0} c B_0)")
    qStr = quantity
  elif q[0] == "J" or q[:-1]=="dblRedJ": #}{
    norm = (nb*Consts.e*Consts.c, "$(e n_{e0} c)$")
    qStr = quantity
  elif q[0] == "Jmag" or q[:-1]=="dblRedJmag": #}{
    norm = (nb*Consts.e*Consts.c, "$(e n_{e0} c)$")
    qStr = quantity
  elif strBegin(q,"rho") or strBegin(q,"chargeDensity"):
    norm = (nb*Consts.e, "$(e n_{e0})$")
  elif strBegin(q, "einj_density"):
    norm = (B0**2.0/(8.0*numpy.pi), r"$(B_0^2/8\pi)$")
  elif strBegin(q,"density3"):
    if "total" in q:
      norm = (2.0*nb, "$n_{0,tot}$")
    else:
      norm = (nb, "$n_{e0}$")
  elif strBegin(q,"ExB"):
    norm = (B0**2, "$B_0^2$")
  elif strBegin(q,"vExB"):
    norm = (1, "$c$")
  elif strBegin(q,"EdotJ"):
    norm = (nb*Consts.e*Consts.c*B0, "$(e B_0 n_{e0} c)$")
  elif strBegin(q,"vel"):
    norm = (1., "$c$")
  elif strBegin(q, "alfvenSpeed"):
    norm = (1., "$c$")
  elif strBegin(q,"relativisticVz"):
    norm = (1., "$c$")
  elif strBegin(q,"veffrms"):
    norm = (1., "$c$")
  elif strBegin(q,"VdotB"):
    norm = (B0, "$B_0c$")
  elif strBegin(q,"VdotdB"):
    norm = (B0, "$B_0c$")
  elif strBegin(q,"VzBz"):
    norm = (B0, "$B_0c$")
  elif strBegin(q,"VzdBz"):
    norm = (B0, "$B_0c$")
  elif strBegin(q,"VpBp"):
    sigmaDict = getSimSigmas(params)
    sigma = sigmaDict["sigma"]
    vA = numpy.sqrt(sigma/(sigma+1.0))
    norm = (B0*vA, "$B_0v_{A0}$")
  elif strBegin(q,"Erms2"):
    norm = (B0**2.0, "$B_0^2$")
  elif strBegin(q,"Erms"):
    norm = (B0, "$B_0$")
  elif strBegin(q,"Brms2"):
    norm = (B0**2.0, "$B_0^2$")
  elif strBegin(q,"Brms"):
    norm = (B0, "$B_0$")
  elif strBegin(q,"deltaBrms2vA"):
    norm = (B0**2.0, "$B_0^2c$")
  elif strBegin(q,"deltaBrms2"):
    norm = (B0**2.0, "$B_0^2$")
  elif strBegin(q,"deltaBrms"):
    norm = (B0, "$B_0$")
  elif strBegin(q,"vrms"):
    norm = (1.0, "$c$")
  elif strBegin(q,"pelsaesser"):
    norm = (1.0, "$c^2$")
  elif strBegin(q,"elsaesserRatio"):
    norm = (1.0, "")
  elif "elsaesserEnergyRatio" in q:
    norm = (1.0, "")
  elif strBegin(q,"elsaesserEnergy"):
    norm = (1.0, "$c^2$")
  elif strBegin(q,"elsaesser"):
    norm = (1.0, "$c$")
  elif strBegin(q,"avgPtclEnergy"):
    norm = (Consts.m_e*Consts.c**2, "$mc^2$")
  elif strBegin(q,"magnetization"):
    norm = (sigma, "$\sigma_0$")
  elif strBegin(q,"larmorRadius"):
    norm = (dx, r"$\Delta x$")
  elif strBegin(q,"Debye"):
    norm = (dx, r"$\Delta x$")
  elif strBegin(q,"skinDepth"):
    norm = (dx, r"$\Delta x$")
  elif strBegin(q,"vBalignment"):
    norm = (1.0, "")
  elif strBegin(q,"intInjSlopeRatio"):
    norm = (1.0, "")
  elif strBegin(q,"electricTotalRatio"):
    norm = (1.0, "")
    addDivSign = False
  elif strBegin(q,"bulkLorentzFactor"):
    norm = (1.0, "")
    addDivSign = False
  elif strBegin(q, "zEnergyDensity"):
    norm = (B0**2/(8*numpy.pi), "$(B_0^2/8\pi)$")
  elif strBegin(q, "perpEnergyDensity"):
    norm = (B0**2/(8*numpy.pi), "$(B_0^2/8\pi)$")
  elif "EnergyDensity" in q:
    if "efficiency" in q:
      norm = (1.0, "")
      addDivSign = False
    else:
      norm = (B0**2/(8*numpy.pi), "$(B_0^2/8\pi)$")
  elif "TurbulentEnergyRatio" in q:
    norm = (1.0, "")
    addDivSign = False
  # elif strBegin(q,"Up"):
    # norm = (Consts.m_e*Consts.c, "$mc$")
  elif strBegin(q,"unreconnectedFluxSep") or strBegin(q,"BxFwhm"):
    rhoc = Consts.c/params["omegac"]
    norm = (rhoc, r"$\rho_c$")
  elif strBegin(q,"F"): #}{
    (nNorm, nNormStr, mass) = getDensityNorm(q)
    mc = Consts.m_e*Consts.c
    norm = (nNorm * Consts.c, nNormStr + r"$c$")
  elif strBegin(q,"Ue_"): #}{
    (nNorm, nNormStr, mass) = getDensityNorm(q)
    mc2 = mass*Consts.c**2
    if "elec" in q:
      norm = (nNorm * params["sigma_e"] * mc2,
        r"$\sigma_e$" + nNormStr + r"$m_e c^2$")
    else:
      norm = (nNorm * params["sigma_i"] * mc2,
        r"$\sigma_i$" + nNormStr + r"$m_i c^2$")
  elif strBegin(q,"Up"): #}{
    (nNorm, nNormStr, mass) = getDensityNorm(q)
    mc = mass*Consts.c
    params, units = getSimParams(simDir=simDir)
    gamma0 = 3.0*params["thbe"]
    if "elec" in q or "total" in q:
      norm = (nNorm * params["sigma_e"] * mc * gamma0,
        r"$\sigma_e$" + nNormStr + r"$\gamma_0m_e c$")
    else:
      norm = (nNorm * params["sigma_i"] * mc,
        r"$\sigma_i$" + nNormStr + r"$m_i c$")
  elif q[0] == "m": #}{
    norm = (nb, "n_{e0}")
    qStr = quantity
  elif strBegin(q,"P"): #}{
    (nNorm, nNormStr, mass) = getDensityNorm(q)
    mc2 = mass*Consts.c**2
    if "elec" in q:
      norm = (nNorm * params["sigma_e"] * mc2,
        r"$\sigma_e$" + nNormStr + r"$m_e c^2$")
    else:
      norm = (nNorm * params["sigma_i"] * mc2,
        r"$\sigma_i$" + nNormStr + r"$m_i c^2$")
  elif errIfUnknown: #}{
    msg = "the normalized value for quantity '" + q + "' is unknown."
    raise ValueError(msg)
  #}
  if q in ("Bx", "By", "Bz", "Ex", "Ey", "Ez", "Jx", "Jy", "J_z"): #{
    qStr = "$" + q[0] + "_" + q[1] + "$"
  elif q == "rho": #}{
    qStr = r"$\rho$"
  #}
  if addDivSign: #{
    val, valStr = norm
    if norm != "":
      if norm[0] == "$":
        valStr = "$/" + valStr[1:]
      else:
        valStr = "/" + valStr
    norm = (val, valStr)
  #}

  return norm + (qStr,)

#}

def avgMaxwellianGammaM1(theta, driftGammaM1 = 0.): #{
  #return the average gamma-1 for a Maxwellian distribution with
  # temperature (k_B) T = theta m c^2
  # <gamma - 1>
  # = gamma_d [ K_1(1/theta)/K_2(1/theta) + 3 theta + beta_d^2 theta] - 1
  # = (gamma_d-1)
  #   + gamma_d [K_1(1/theta)/K_2(1/theta) -1 + 3 theta + beta_d^2 theta]
  # First, evaluate gamma-1 for no drift
  import scipy
  import scipy.special
  thbc = 0.00144
  if theta > thbc:
    gm1 = scipy.special.k1(1./theta)/scipy.special.kn(2,1./theta)
    gm1 += 3*theta - 1.
  else:
    # can't evaluate k1 for very small theta
    #gm1 = min(1.5*theta,
    #     3*thbc-1.+scipy.special.k1(1./thbc)/scipy.special.kn(2,1./thbc))
    gm1 =  1.5*theta + 15./8.*theta**2 * (1.-theta)
  if driftGammaM1 != 0:
    driftBetaSqrGamma = driftGammaM1*(2.+driftGammaM1) / (
                          1. + driftGammaM1)
    gm1 *= (1. + driftGammaM1)
    gm1 += driftBetaSqrGamma * theta
    gm1 += driftGammaM1
  return gm1
#}

def getMaxwellianTemp(avgGammaM1, driftGammaM1 = 0.): #{
  """
  returns the temperature (normalized to mc^2) for a Maxwellian
  distribution with a given drift-Lorentz factor and average
  Lorentz factor minus 1 (or kinetic energy normalized to mc^2).
  """
  import scipy
  import scipy.optimize
  def f(th): #{
    res = avgMaxwellianGammaM1(th, driftGammaM1=driftGammaM1) - avgGammaM1
    return res
  #}
  if driftGammaM1 != 0:
    driftBetaSqrGamma = driftGammaM1*(2.+driftGammaM1) / (
                          1. + driftGammaM1)
    th0 = ((1.+avgGammaM1)/driftGammaM1 - 1.)/(3./2. + driftBetaSqrGamma)
    th1 = avgGammaM1 / (driftGammaM1*(3.+driftBetaSqrGamma))
  else:
    th0 = (1.+avgGammaM1) / 3.
    th1 = 2./3. * avgGammaM1
  while f(th0) > 0:
    th0 *= 0.5
  while f(th1) < 0:
    th1 *= 2.
  #print th0, f(th0)
  #print th1, f(th1)
  th = scipy.optimize.bisect(f, th0, th1)
  return th
#}

def smooth2dPeriodic(ra, numSmooths=1, hasUpperBounds=None): #{
  """
  ra is a 2d array that's periodic -- with or without the upper bound
  """
  sh = ra.shape

  def same(x,y): #{
    #res = ( abs(x-y).max() <= 1e-6 * (abs(x)+abs(y)).mean() )
    res = (x==y).all()
    return res
  #}
  # Guess whether ra has a periodic copy at the upper bound.
  # This is maybe dangerous.  What if ra[-1] coincidententally equals ra[0]?
  if hasUpperBounds is None: #{
    hasUb = [False, False]
    if same(ra[0], ra[-1]):
      hasUb[0] = True
    if same(ra[:,0], ra[:,-1]):
      hasUb[1] = True
    for d in range(2):
      if ra.shape[d] <= 1:
        hasUb[d] = False
  #}

  if (hasUb[0] and hasUb[1]): #{
    raPer = ra[:-1,:-1]
  elif hasUb[0]:
    raPer = ra[:-1]
  elif hasUb[1]:
    raPer = ra[:,:-1]
  else:
    raPer = ra
  for n in range(numSmooths): #{
    if (ra.shape[0] > 1):
      ra2 = 0.5*raPer
      ra2[1:-1] += 0.25*raPer[2:]
      ra2[1:-1] += 0.25*raPer[:-2]
      ra2[0] += 0.25*raPer[-1] + 0.25*raPer[1]
      ra2[-1] += 0.25*ra2[0] + 0.25*raPer[-2]
      raPer = ra2
    if (ra.shape[1] > 1):
      ra2 = 0.5*raPer
      ra2[:,1:-1] += 0.25*raPer[:,2:]
      ra2[:,1:-1] += 0.25*raPer[:,:-2]
      ra2[:,0] += 0.25*raPer[:,-1] + 0.25*raPer[:,1]
      ra2[:,-1] += 0.25*ra2[:,0] + 0.25*raPer[:,-2]
      raPer = ra2
  #}
  if (hasUb[0] and hasUb[1]): #{
    ra[:-1,:-1] = raPer
    ra[:-1,-1] = raPer[:,0]
    ra[-1,:-1] = raPer[0,:]
    ra[-1,-1] = ra[0,0]
    ra[0,-1] = ra[0,0]
    ra[-1,0] = ra[0,0]
  elif hasUb[0]:
    ra[:-1] = raPer
    ra[-1] = raPer[0]
  elif hasUb[1]:
    ra[:,:-1] = raPer
    ra[:,-1] = raPer[:,0]
  else:
    ra = raPer
  return ra
#}

def arrayShapeFromHdf5(h5FileName, datasetName): #{
  """
  Return the shape of an hdf5 dataset
  """
  try:
    file = tables.open_file(h5FileName,'r')
    getNode = file.get_node
  except:
    try:
      file = tables.openFile(h5FileName,'r')
      getNode = file.getNode
    except:
      msg = "\n\nError: Failed to open hdf5 file: '" + h5FileName + "'"
      msg += " (is it an hdf5 file?)\n"
      raise ValueError(msg)

  groupName = '/' + datasetName

  try:
    res = getNode(groupName).shape
  except:
    file.close()
    msg = "\n\nError: Failed to access dataset '" + groupName + "'"
    msg += " in hdf5 file: '" + h5FileName + "'\n"
    raise ValueError(msg)
  file.close()

  return res
#}

def readArrayFromHdf5(h5FileName, datasetName, slices = None,
  shapeOnly = False, alternateDatasetName = None): #{
  """
  Returns a (numpy float64) array of the field values
  from an hdf5 dataset

  If slices is set, this will retrieve the requested slice of a field;
  e.g., if the field is 4x5x3, then
  slices = (slice(None), slice(2,5), slice(None)) will return a
  4x3x3 subset.

  If shapeOnly, returns the field shape rather than the field, and
    slices is ignored.
  """
  import h5py
  try:
    file = tables.open_file(h5FileName,'r')
    getNode = file.get_node
  except:
    try:
      file = tables.openFile(h5FileName,'r')
      getNode = file.getNode
    except:
      msg = "\n\nError: Failed to open hdf5 file: '" + h5FileName + "'"
      msg += " in " + os.getcwd() + " (is it an hdf5 file?)\n"
      raise ValueError(msg)

  groupName = '/' + datasetName

  if alternateDatasetName is not None and datasetName not in getNode("/"):
    if alternateDatasetName not in getNode("/"):
      msg = "Neither " + datasetName + " nor " + alternateDatasetName
      msg += " names a datset in " + h5FileName
      raise ValueError(msg)
    groupName = '/' + alternateDatasetName

  try:
    if shapeOnly: #{
      res = getNode(groupName).shape
    else: #}{
      if (slices is None):
        res = numpy.array( getNode(groupName) )
      else:
        # print("Getting slices =", slices, "for", h5FileName)
        res = numpy.array( getNode(groupName)[tuple(slices)] )
        #print "Got slices =", slices, "->", res.shape
    #}
  except:
    file.close()
    raise
    msg = "\n\nError: Failed to open dataset '" + groupName + "'"
    msg += " in hdf5 file: '" + h5FileName + "'\n"
    raise ValueError(msg)
  file.close()

  return res
#}

def writeArrayToHdf5(array, h5FileName, writeMode, datasetName,
  datasetAttribs = None): #{
  """
  Write a (numpy float64) array to an hdf5 dataset.

  writeMode is 'w' to delete and create new file, 'a' to append dataset
    to existing file (or create if not existing).
  """
  try:
    file = tables.open_file(h5FileName, mode = writeMode)
    createArray = file.create_array
  except:
    try:
      file = tables.openFile(h5FileName,'r')
      createArray = file.createArray
    except:
      msg = "\n\nError: Failed to open hdf5 file: '" + h5FileName + "'"
      msg += " for writing (is it an hdf5 file?)\n"
      raise ValueError(msg)

  groupName = '/' + datasetName
  outTable = createArray("/", datasetName, array, datasetName)
  #if (datasetDescription != None):
  #  outTable._v_attrs.description = datasetDescription
  if datasetAttribs is not None:
    for (key, val) in datasetAttribs.items():
      setattr(outTable._v_attrs, key, val)

  file.close()
#}

def isHdf5(fileName): #{
  return ( (" "*3 + fileName)[-3:] == ".h5")
#}

def getShortCwd(): #{
  """
  return an abbreviated name for the current working directory;
  this tries to discard unuseful information -- it's far from
  failsafe -- might be better to specify what to look for than
  what to include.
  """
  wd = os.getcwd()
  begRegExp = ["AstroAccel", "wernerg", "gwerner", "elecIon20[12][0-9]",
    "pair3D", "turbulence"]
  endRegExp = ["data", "trunk.*", "branch.*"]
  for regExp in begRegExp: #{
    mo = re.search("^.*" + regExp + "[^/]*/(.*)", wd)
    if mo:
      wd = mo.group(1)
  #}
  for regExp in endRegExp: #{
    mo = re.search("(.*)/" + regExp + ".*", wd)
    if mo:
      wd = mo.group(1)
  #}
  return wd
#}

def findDumpFiles(baseName,
  searchDirs = (".", "fields", "currents", "densities", "orbits",
    "particles", "extra")
  ): #{
  """
  baseName is the name of a Zeltron dump file, without dump step or
    extension.  E.g., "Bx" or "mapxyz_electrons_bg", for files
    fields/Bx_0.h5 and densities/mapxyz_electrons_bg_120.h5.
  searchDirs is a list of directories through which to search

  Returns (dir, ext, steps), where
    dir = the directory containing the files of the form
      baseName + "_" + str(n) + "." + ext for n in steps
    steps = the list of dumpsteps where the file was saved (in order)

  If no matching file is found, dir = ext = None, and steps = []
  """
  foundDir = None
  ext = None
  steps = []
  for dname in searchDirs: #{
    files = glob.glob(dname + "/" + baseName + "_*.*")
    if len(files) > 0: #{
      foundDir = dname
      ext = files[0].split(".")[-1]
      lenExt = len(ext)+1
      for f in files:
        if len(f) < lenExt or f[-lenExt:] != "." + ext:
          msg = "File " + files[0] + " and file " + f + " have different "
          msg += "extensions."
          raise RuntimeError(msg)
      steps = sorted([int(f[:-lenExt].split("_")[-1]) for f in files])
      break
    #}
  #}
  return (foundDir, ext, steps)
#}

def zFileName(baseName = None, h5name = None, txtName = None,
  errorIfNotFound = False): #{
  if (baseName is None) and (h5name is None or txtName is None):
    raise ValueError("either baseName must be specified, or both h5name and txtName")
  res = None
  fileType = None
  if h5name is not None and os.path.exists(h5name):
    res = h5name
    fileType = "h5"
  elif txtName is not None and os.path.exists(txtName):
    res = txtName
    fileType = "txt"
  elif baseName is not None:
    if os.path.exists(baseName + ".h5"):
      res = baseName + ".h5"
      fileType = "h5"
    elif os.path.exists(baseName + ".txt"):
      res = baseName + ".txt"
      fileType = "txt"
    elif os.path.exists(baseName + ".dat"):
      res = baseName + ".dat"
      fileType = "txt"
  if errorIfNotFound and res is None:
    testedNames = [s for s in [h5name, txtName] if s is not None]
    if baseName is not None:
      testedNames += [baseName + ".h5", baseName + ".txt", baseName + ".dat"]
    msg = "File not found after trying possible names: " + repr(testedNames)
    msg += " in " + os.getcwd() + "/"
    raise IOError(msg)
  return (res, fileType)
#}

def globWithRegEx(s): #{
  """
  Returns a list of files matching pattern s.
  Returns glob.glob(s)
    except: if s contains braced text, e.g., {[0-9]+}
            then the list of files will be matched against the text inside
            the braces using the python re (regEx) module.
            This allows more complicated regEx matching.
            N.B. If any text is braced, it may be best to put all wildcards
              in braces.
            N.B. the braced text must not contain braces or "/", so that
                 limits the regEx matching usually.
    N.B. python glob.glob uses fnmatch.fnmatch, hence
      * = everything
      ? = any 1 character
      [seq] = any character in seq
      [!seq] = any character not in seq! for not,
               as in a[!b]c rather than what bash uses: a[^b]c.
      [?] = matches ?
    N.B. for expressions in braces, see re documentation.
      However, some useful things:
      '.' = any character
      '*' = any number (inc. zero) of the preceding character/expresion
          '.*' = anything
      '+' = one or more of the preceding
      '?' = 0 or 1 of the preceding
      use:
      {(A|B)} to match against A or B.
  """
  #s = re.sub("#","[0-9]", s)
  if re.search("{[^}]*}", s): #{
    # replace all {...} with * for globbing
    sGlob = re.sub("{[^}]*}", "*", s)
    # replace all {...} with ...
    # and replace * with .* and ? with . and [! with [^ if not in braces
    sRegEx = ""
    pos = 0
    def globWildcardToRegEx(s2): #{
      s2 = re.sub(r"\*", ".*", s2)
      s2 = re.sub(r"\?", ".", s2)
      s2 = re.sub(r"\[!", "[^", s2)
      return s2
    #}
    for mo in re.finditer("{[^}]*}", s): #{
      if "{" in mo.group(0)[1:]:
        msg = "cannot have '{' in a braced regEx " + repr(mo.group(0))
        msg += " in expression " + repr(s)
        raise ValueError(msg)
      elif "/" in mo.group(0):
        msg = "cannot have '/' in a braced regEx " + repr(mo.group(0))
        msg += " in expression " + repr(s)
        raise ValueError(msg)
      sRegEx += globWildcardToRegEx(s[pos:mo.start()])
      sRegEx += mo.group(0)[1:-1] # copy stuff in braces without braces
      pos = mo.end()
    #}
    sRegEx += globWildcardToRegEx(s[pos:])
    #sRegEx = re.sub("{([^}]*)}", r"\1", s)
    sRegEx += "$" # force match of entire line
    sAll = glob.glob(sGlob)
    res = [ss for ss in sAll if re.match(sRegEx, ss)]
  else: #}{
    res = glob.glob(s)
  #}
  return res
#}

def globDirs(simDirs, excludeUnrun = True): #{
  """
  expand a list of directories (possibly) with wildcards;
  return (newSimDirs, simCount) where
    newSimDirs is a list of directories
    (wildcard-expanded: see globWithRegEx)
    and simCount is a list of the number of entries in newSimDirs
      that were added for each entry in simDirs.
  """
  newSimDirs = []
  simCount = []
  def sortFn(s): #{
    sOrig = s
    if "pert" not in s: #{
      # find where to put pert
      mo = re.search("(.*)(sig[^/]*)/", s)
      if mo: #{
        ss = mo.group(2)
        # if sim ends with, e.g., "-2" put pert before that, else at end
        mo2 = re.search("(.*)(-[0-9][0-9]*)", ss)
        if mo2: #{
          ss = mo2.group(1) + "-pert0p01" + mo2.group(2) + "/"
        else:
          ss += "-pert0p01/"
        #}
        s = mo.group(1) + ss
      #}
    else: #}{
      # normalize name = e.g., pertp01 -> pert0p01
      # find where to put pert
      mo = re.search("(.*)pert([0-9p][0-9p]*)([^0-9p].*)", s)
      if mo: #{
        ss = mo.group(2)
        if ss[0]=="p":
          ss = "pert0" + ss
          s = mo.group(1) + ss + mo.group(3)
        #}
      #}
    #}
    # sort so d1/blah/data comes before d1/blah-2/data
    s = s.replace("/", " /") # force split around "/"
    if 1: #{ # try to separate numbers, so they're sorted correctly
      sn = s.replace("-", " ")
      snList = sn.split()
      snList2 = []
      for sni in snList: #{
        r = []
    # sort so d1/blah/data comes before d1/blah-2/data
        while len(sni) > 0 and sni[0] == "/":
          r += [None] # None is less than numbers and strings
          sni = sni[1:]
        try: #{
          [float(sni)]
          r += [float(sni)]
        except ValueError: #}{
          gotNum = False
          # N.B. following matches, e.g., "/trunkDec9"
          mo = re.match("(.*?)([0-9]*p?[0-9][0-9]*)(.*)$", sni)
          if mo: #{
            try:
              rm = float(mo.group(2).replace("p", "."))
              if len(mo.group(1)) > 0: r.append(mo.group(1))
              r.append(rm)
              if len(mo.group(3)) > 0: r.append(mo.group(3))
              gotNum = True
            except:
              gotNum = False
          #}
          if not gotNum: #{
            r += [sni]
          #}
        #}
        snList2 += r
      #}
      s = snList2
    #}
    #print sOrig, "->", s
    return s
  #}
  for sim in simDirs: #{
    if '*' in sim or '?' in sim or '[' in sim or '{' in sim: #{
      newSims = globWithRegEx(sim)
      # sort so d1/blah/data comes before d1/blah-2/data
      #newSims.sort(key = lambda s: s.replace("/", " "))
      newSims.sort(key = sortFn)
      newSimDirs += newSims
      simCount.append(len(newSims))
    else:
      newSimDirs.append(sim)
      simCount.append(1)
    #}
  #}
  if excludeUnrun: #{
    dirsWithSims = []
    dropSims = []
    for di, d in enumerate(newSimDirs):
      if os.path.exists(d + "/input_params.dat"):
        dirsWithSims.append(d)
      else:
        dropSims.append(di)
        print("Excluding dir " + d + " because it doesn't contain ", end=' ')
        print("input_params.dat")
    count = 0
    countInd = -1
    tmpCount = 0
    for si in range(len(newSimDirs)): #{
      if (tmpCount == 0):
        tmpCount = simCount[countInd]
      if (si in dropSims): #{
        simCount[countInd] -= 1
      #}
      tmpCount -= 1
      if (tmpCount == 0):
        countInd += 1
    #}
    newSimDirs = dirsWithSims
  #}
  return newSimDirs, simCount
#}

def getSimParams(simDir = ".", oldSigmaCalc = False): #{
  """
  returns (paramDict, unitDict), where paramDict maps various parameter
    names to their values, and unitDict maps parameter names to strings
    indicating the units of the values.
  working dir should be data/

  Old versions of zeltron incorrectly calculate sigma when mu != 1.
  """
  if simDir != ".":
    cwd = os.getcwd()
    os.chdir(simDir)
  file1 = "./phys_params.dat"
  file2 = "./input_params.dat"
  paramDict = dict()
  unitDict = dict()
  for fileName in [file1, file2]: #{
    try: #{
      with open(fileName) as f: #{
        paramLine = f.readline()
        vals = [float(v) for v in f.readline().split()]
        paramLine = re.sub(r"   *", ",", paramLine.strip())
        paramsWithUnits = paramLine.split(",")
        params = [re.sub(r" *\[.*\].*", "", p) for p in paramsWithUnits]
        units = [re.sub(r"].*", "", re.sub(r"^.*\[", r"", p)) for p in paramsWithUnits]
        paramDict.update(dict(list(zip(params,vals))))
        unitDict.update(dict(list(zip(params,units))))
      #}
    except: #}{
      print("Exception in zfileUtil.getSimParams() in " + os.getcwd())
      if simDir != ".":
        os.chdir(cwd)
      raise
    #}
  #}
  if 1: #{
    mu = paramDict["mass ratio"]
    # 1/sigma = 1/sigma_i + 1/sigma_i
    # sigma_e = mu sigma_i

    if "sigma" in paramDict: #{
      # print("native sigma = ", paramDict["sigma"])
      if oldSigmaCalc: # zeltron used to calculate sigma = sigma_e/2 (incorrectly)
        paramDict["sigma_e"] = 2.*paramDict["sigma"]
        paramDict["sigma_i"] = paramDict["sigma_e"] / mu
        paramDict["sigma"] = 1./(
          1./paramDict["sigma_e"]+1./paramDict["sigma_i"])
        if (mu != 1):
          print("zfileUtil Warning: assuming that zeltron still ")
          print("  calculates sigma=sigma_e/2 even when mu != 1")
          print("  -> mu = ", mu)
          print("  -> using sigma_e ", paramDict["sigma_e"])
          print("  -> using sigma_i ", paramDict["sigma_i"])
      else:
        paramDict["sigma_e"] = (1.+mu)*paramDict["sigma"]
        paramDict["sigma_i"] = paramDict["sigma_e"] / mu
        if (mu != 1):
          print("zfileUtil Warning: assuming that zeltron calculates ")
          print("  sigma correctly (!= sigma_e/2 when mu != 1) -- ")
          print("  zeltron didn't use to do this correctly")
          print("  -> mu = ", mu)
          print("  -> using sigma_e ", paramDict["sigma_e"])
          print("  -> using sigma_i ", paramDict["sigma_i"])
      unitDict["sigma_e"] = unitDict["sigma"]
      unitDict["sigma_i"] = unitDict["sigma"]
    #}

    #if paramDict["mass ratio"] == 1.:
    #  paramDict["sigma_e"] = 2.*paramDict["sigma"]
    #  unitDict["sigma_e"] = unitDict["sigma"]
    #  paramDict["sigma_i"] = 2.*paramDict["sigma"]
    #  unitDict["sigma_i"] = unitDict["sigma"]
  #}
  correctOldOmegac = True
  if  correctOldOmegac and "thde" in paramDict: #{
    thde = paramDict["thde"]
    rhoe0 = Consts.m_e * Consts.c**2 / (Consts.e * paramDict["B0"])
    omegac = paramDict["omegac"]
    oldRhoc = thde * rhoe0
    oldOmegac = Consts.c/oldRhoc
    if (abs(oldOmegac - omegac) < 1e-6 * min(omegac, oldOmegac)): #{
      print("Warning: simulation used old omegac = c / (thde * rho0); ")
      print("     correcting to new omegac = c / (sigma_e * rho_e0).")
      omegac = Consts.c / (paramDict["sigma_e"] * rhoe0)
      paramDict["omegac"] = omegac
    #}
  #}
  if simDir != ".":
    os.chdir(cwd)
  return (paramDict, unitDict)
#}

def getTime(step=None, time=None,
  tcOverLx=None, tcOverLy=None, tcOverLz=None,
  tcOverL=None, Ldir=None,
  tOmegac=None, params = None): #{
  """
  returns {"step":n, "time":t "tcOverLx":t, "tcOverLy":t, "tcOverLz":t,
    "tcOverLs":(tc/Lx, tc/Ly, tc/Lz), "tOmegac":t},
    converting an input time to several different forms
    -- time is in seconds
  params = getSimParams()[0] -- if None, then getSimParams() will be
  called (must be in data/ directory)
  """
  if params is None:
    params, units = getSimParams()
  dt = params["dt"]
  Ls, Ns = getSimLengths(params=params)
  omegac = params["omegac"]
  if time is not None: #{
    t = time
  elif step is not None: #}{
    t = step*dt
  elif tOmegac is not None: #}{
    t = tOmegac / omegac
  else: #}{
    if tcOverLx is not None:
      tcOverL = tcOverLx
      Ldir = 0
    elif tcOverLy is not None:
      tcOverL = tcOverLy
      Ldir = 1
    elif tcOverLz is not None:
      tcOverL = tcOverLz
      Ldir = 2
    t = tcOverL * Ls[Ldir] / Consts.c
  #}
  LsNz = [(x if x>0 else max(Ls)) for x in Ls]
  step = int(round(t/dt))
  tcOverLs = [t * Consts.c / LsNz[d] for d in range(3)]
  rd = {"time":t, "step":step, "tOmegac":t*omegac,
        "tcOverLx":tcOverLs[0], "tcOverLy":tcOverLs[1],
        "tcOverLz":tcOverLs[2], "tcOverLs":tcOverLs}
  return rd
#}

def getSimLengths(params = None, zeroLzIn2d=True): #{
  """
  returns the sim length in cells, which is 1 less than the field shape,
  since most fields contain upper guard cells
  """
  if params is None:
    params, units = getSimParams()
  Lx = params["xmax"] - params["xmin"]
  Nx = params["NX"] - 1
  Ly = params["ymax"] - params["ymin"]
  Ny = params["NY"] - 1
  if "zmax" in params:
    Lz = params["zmax"] - params["zmin"]
    Nz = params["NZ"] - 1
    if zeroLzIn2d and (Nz == 1):
      Lz = 0.
  else:
    Lz = 0.
    Nz = 1.
  return (numpy.array((Lx, Ly, Lz)),
    numpy.array((Nx, Ny, Nz), dtype=numpy.int64))
#}

def getComputeParams(params = None): #{
  """
  returns a dictionary of numrical/computational parameters
  """
  if params is None:
    params, units = getSimParams()
  Ls, Ns = getSimLengths(params=params, zeroLzIn2d=False)
  d = dict()
  d["Ns"] = Ns
  is3d = (Ns[2] > 1)
  Np = numpy.int64([params["NPROC along X"], params["NPROC along Y"], 1])
  if is3d:
    Np[2] = params["NPROC along Z"]
  d["ndim"] = 3 if is3d else 2
  d["numBgPtcls"] = 2*params["NP"]
  d["numProcs"] = Np
  d["numRanks"] = numpy.prod(Np)
  d["numCells"] = numpy.prod(Ns)
  d["cellsPerDomain"] = numpy.prod(Ns)/numpy.prod(Np)
  return d
#}

def getUserSpecifiedDumpStep(timeSpec, params = None,
  dumpType = None,
  energiesVsTime = None): #{
  """
  returns the nearest dump (time) step to timeSpec
  - if timeSpec is an int, finds the nearest dump step to timeSpec
  - if timeSpec is a string or a float, assumes it's an actual time (in s.)
    -- if it's a string, evaluate it, allowing use of
        Lx, Ly, Lz, rhoc, c, dt
       --except if it's "last" then try to find the last dump step
         (only works if in data/ directory)
    -- unless it's a string beginning with an "=" in it, as in
       "Bt=0.8" or "ekineb=1.2" where the left side is the name of an
       energy, and the right is a fraction of its initial value;
       the time will be set to the first time when that value is reached;
       i.e., when Bt falls to 80% of its initial value, or ekineb rises
       20% above its initial value.

  dumpType = None (general), "spectra" (for energy spectra),
    "angular" (for angular dists)
  """
  if params is None:
    params, units = getSimParams()

  filepat = {None:"fields/Bx_*.h5",
    "spectra":"spectrum_electrons_bg_*.h5",
    "angular":"angular_electrons_bg_*.h5"}
  dumpPer = {None:params["FDUMP"],
    "spectra":"errorNotImplemented",
    "angular":"errorNotImplemented"}
  getNearestDump = True
  if isinstance(timeSpec, int): #{
    it = timeSpec
    getNearestDump = False
  elif isinstance(timeSpec, float): #}{
    it = timeSpec / params["dt"]
  elif timeSpec == "last": #}{
    afiles = glob.glob(filepat)
    print("afiles=", afiles)
    steps = sorted([int(f[:-3].split("_")[-1]) for f in afiles])
    it = steps[-1]
  elif isinstance(timeSpec, str): #}{
    if "=" in timeSpec: #{
      ekind, val = timeSpec.split("=")
      val = float(val)
      needUnrB = False
      if len(ekind) > 3 and ekind[:3] == "unr":
        needUnrB = True
      if energiesVsTime is not None and needUnrB and "unrB" not in energiesVsTime:
        energiesVsTime = None
        print("Warning: zfileUtil.getUserSpecifiedDumpStep is getting ")
        print("         new energies because it needs 'unrB'")
      if energiesVsTime is None:
        ed = getEnergiesVsTime(getUnrB = needUnrB, params = params)
      else:
        ed = energiesVsTime
      if ekind not in ed: #{
        msg = "Energy " + ekind + "is not valid.  Valid energies are: "
        msg += ", ".join(list(ed.keys())) + "."
        msg += "Error in " + os.getcwd() + "."
        raise ValueError(msg)
      #}
      evst = ed[ekind]
      threshold = val*evst[0]
      if val >= 1.:
        ns = numpy.where(evst > threshold)[0]
      else:
        ns = numpy.where(evst < threshold)[0]
      if len(ns) == 0: #{
        msg = "There is never a time when energy " + ekind
        msg += " reaches %g, i.e., %g times its initial energy %g" % (
          threshold, val, evst[0])
        raise ValueError(msg)
      #}
      if 1: #{
        print("evst =", evst)
        print("threshold =", threshold)
        print("ns =", ns)
      #}
      it = ns[0]
      if 0:
        print(os.getcwd().split("/")[-3], timeSpec, "->", it, end=' ')
        Ls, Ns = getSimLengths(params=params)
        print(it * params["dt"] * Consts.c/Ls[0], "Lx/c")
    else: #}{
      Ls, Ns = getSimLengths(params=params)
      rhoc=Consts.c/params["omegac"]
      lengthDict = {"Lx":Ls[0], "Ly":Ls[1], "Lz":Ls[2], "rhoc":rhoc,
      "c":Consts.c, "dt":params["dt"]}
      it = eval(timeSpec, lengthDict) / params["dt"]
    #}
  #}
  FDUMP = int(params["FDUMP"])
  if getNearestDump:
    it = int(round(float(it)/FDUMP)*FDUMP)
  return it
#}

def getStepTiming(): #{
  """
  assumes you're in data/ directory
  """
  stepTimes = numpy.loadtxt("timestep.dat")
  return stepTimes
#}

def getSimSigmas(params = None): #{
  if params is None:
    params, units = getSimParams()
  sigma_e = params["sigma_e"]
  sigma_i = params["sigma_i"]
  mu = sigma_i/sigma_e

  thbe = params["thbe"]
  thbi = params["thbi"]
  gm1e = avgMaxwellianGammaM1(thbe)
  gm1i = avgMaxwellianGammaM1(thbi)
  sigma_he = sigma_e / (gm1e + thbe)
  sigma_hi = sigma_i / (gm1i + thbi)
  sigma = 1./(1./sigma_e + 1./sigma_i)
  sigma_h = 1./(1./sigma_he + 1./sigma_hi)
  # LMH: changed vAoverC to be sigma instead
  # of sigma_h (what is sigma_h anyway?)
  d = {"sigma_e":sigma_e, "sigma_i":sigma_i,
       "sigma_he":sigma_he, "sigma_hi":sigma_hi,
       "sigma":sigma, "sigma_h":sigma_h,
       "vAoverC":numpy.sqrt(sigma/(1.+sigma))}
  return d
#}

def getSimLabelInfo(simDirs, useDirName = False,
  skipSimFn = None, combineSame = True, splitCommonLabel = True,
  excludeFromLabel = [], diffTolerance = 1e-5,
  splitSingleConfiguration = True): #{
  """
  Finds the following quantities for each simulation:
    labelOrder = ["Lx", "Ly", "Lz", "Bz", "eta", "betad", "res", "ppc", "pert",
                  "dm1", "extra"]
  Returns (labelDicts, labels, configurationNumbers,
           commonLabelDict, commonLabel, simKinds)
    where
    labelDicts is a dictionary (for each element of simDirs)
      that maps the quantities to values;
    labels is list of strings labeling each simulation
    configurationNumbers is a list of (i,m,n) for each simulation, where
      i in the configuration index (i is the same for all simulations with
      the same configuration, and increases by 1 with each new
      configuration),
      n is the number of simulations (in a row, in order of simDirs) with
      the same configuration, and m is the index within those n.
      This can help with labeling--e.g., one might label only configuration
      (4,0,3) and use the same color/style/etc. for (4,1,3) and (4,2,3).
      But then, configurationNumbers has one more element, the number of
      unique configurations.
    commonLabelDict is a dictionary that maps the quantities to values
      (for quantities that have the same value for every simulation)
    commonLabel is a string that labels the things all (unskipped)
      simulations have in common
    simKinds is a list of strings labeling the (guessed) kind of simulation, e.g.,
      "elecIon", "pairLowSigmah", "rdki2d", "unknown"
      or, if all simDirs are the same kind, it's just one string
  simDirs should be a list of simulation data/ directories
  useDirName = whether to try to extract the info from the simDir name,
    rather than calling getSimParams()
  skipSimFn is a function of a labelDict that returns True if the simulation
    should not be included
  combineSame: if False, then configurationNumbers will be (0,1) for
    each simulation.
  splitCommonLabel: whether to split the common label into multiple lines
    if a number, it will be split around that number of characters
  excludeFromLabel: a list of strings that will be excluded from
    labels, presumably because they're irrelevant
  splitSingleConfiguration: if true, then if there is only one
    configuration, then configurationNumbers will be altered so that
    each simulation is considered to be a separated
    configuration (helpful for plotting when sims of the same configuration
    are plotted in the same style--there's usually no sense in having all lines
    on the plot in the same style)
  """
  count = 0
  lastLabel = None
  labelQs = []
  labelsWithDiffVals = set()
  labelsWithSameVals = set()
  firstUnskippedSim = -1
  simKinds = []

  def diff(x,y): #{
    if isinstance(x, str):
      res = (x != y)
    else:
      res = abs(x-y) > diffTolerance * max(abs(x),abs(y))
    return res
  #}

  if len(simDirs) == 0: #{
    simLabels = []
    configurationNumbers = []
    commonLabelDict = dict()
    commonLabel = ""
    return (labelQs, simLabels, configurationNumbers,
          commonLabelDict, commonLabel, simKinds)
  #}

  def skipSim(labelDict): #{
    if skipSimFn is not None:
      return skipSimFn(labelDict)
    else:
      return False
  #}
  # first extract info from sim names
  for si, sim in enumerate(simDirs): #{
    try: #{
      cwd = os.getcwd()
      os.chdir(sim)
      sim = os.getcwd()
      os.chdir(cwd)
      if not useDirName:
        params, units = getSimParams(simDir = sim)

      Bz = 0.
      if ("Bz" in sim): #{
        Bz = re.sub("p",".", re.search("Bz([0-9p]*)-", sim).group(1))
        Bz = float(Bz)
      #}
      dm1 = ("dm1" in sim) # drift particles are positrons
      # for elec-ion 3D
      mo = re.search(r".*m[0-9]*-([0-9p]*)-[0-9p]*-([0-9pD]*)-sigi*([0-9p]*)-res([0-9p]*)-ppc([0-9p]*)([^/]*).*",
        sim)
      # for 2D low sigma_h pair
      mo2 = re.search(r".*m1-([0-9p]*)-[0-9p]*-sigh([0-9p]*)-res([0-9p]*)-ppc([0-9p]*)([^/]*).*",
        sim)
      if not mo2: #{
        mo3 = re.search(r".*m1-([0-9p]*)-[0-9p]*-sigh([0-9p]*)-eta([0-9p]*)-res([0-9p]*)-ppc([0-9p]*)([^/]*).*",
          sim)
      #}
      if useDirName: #{
        dirNameParseFail = False
        if mo: #{
          simKind = "elecIon"
          # For e-i, we use Lx/sigma_i rho_i0
          Lx = 2*float(re.sub("p",".",mo.group(1)))
          Lz = re.sub("p",".",mo.group(2))
          if (Lz == "2D"):
            Lz = 0.
          else:
            Lz = 2*float(Lz)
            #Lz *= Lx/20.
          sigma_i = float(re.sub("p",".",mo.group(3)))
          sigma_h = None
          eta = 5.
          res = float(re.sub("p",".",mo.group(4)))
          ppc = float(re.sub("p",".",mo.group(5)))
          extra = mo.group(6)
        elif mo2: #}{
          simKind = "pairLowSigmah"
          Lx = 2*float(re.sub("p",".",mo2.group(1)))
          Lz = 0.
          sigma_i = None
          sigma_h = float(re.sub("p",".",mo2.group(2)))
          eta = 5.
          res = float(re.sub("p",".",mo2.group(3)))
          ppc = float(re.sub("p",".",mo2.group(4)))
          extra = mo2.group(5)
        elif mo3: #}{
          simKind = "pairLowSigmah"
          Lx = 2*float(re.sub("p",".",mo3.group(1)))
          Lz = 0.
          sigma_i = None
          sigma_h = float(re.sub("p",".",mo3.group(2)))
          eta = float(re.sub("p",".",mo3.group(3)))
          res = float(re.sub("p",".",mo3.group(4)))
          ppc = float(re.sub("p",".",mo3.group(5)))
          extra = mo3.group(6)
        else: #}{
          dirNameParseFail = True
          simKind = "unknown"
          print("Failed to parse sim dir name:", sim)
          Lx = 0
          Lz = 0
          sigma_i = None
          sigma_h = None
          eta = 0
          res = 0
          ppc = 0
          extra = ""
        #}
      #}
      if (not useDirName) or dirNameParseFail: #{
        Ls, Ns = getSimLengths(params = params)
        sigma_e = params["sigma_e"]
        sigma_i = params["sigma_i"]
        thbe = params["thbe"]
        thbi = params["thbi"]
        gme = avgMaxwellianGammaM1(thbe)
        gmi = avgMaxwellianGammaM1(thbi)
        sigma_he = sigma_e / (gme + thbe)
        sigma_hi = sigma_i / (gmi + thbi)
        sigma_h = 1./(1./sigma_he + 1./sigma_hi)
        B0 = params["B0"]
        rhoc = sigma_e * Consts.m_e * Consts.c**2 / (Consts.e * B0)
        Lx = int(round(Ls[0]/rhoc))
        Ly = int(round(Ls[1]/rhoc))
        Lz = Ls[2]/rhoc #int(round(Ls[2]/rhoc))
        if Ns[0]>1:
          dx = Ls[0]/Ns[0]
        elif Ns[1]>1:
          dx = Ls[1]/Ns[1]
        else:
          dx = Ls[2]/Ns[2]
        eta = 1./params["density ratio"]
        betad = params["betad"]
        res = int(round(rhoc/dx))
        ppc = int(round(params["NP"] / numpy.prod(Ns)))
        mu = params["mass ratio"]
        if Ns[0]==1:
          simKind = "rdki2d"
        elif mu > 1:
          simKind = "elecIon"
        elif mu==1 and sigma_e > 20: #{
          sigma_h = 0.5*sigma_e/(4.*params["thbe"])
          simKind = "pair"
          if (sigma_h < 5):
            simKind = "pairLowSigmah"
        else: #}{
          simKind = "unknown"
        #}
        mo1 = re.search(r".*(-[0-9][0-9]*)/.*", sim)
        extra = ""
        if mo1: #{
          extra = mo1.group(1)
        #}
      #}
      simKinds.append(simKind)
      if "pert" in sim: #{
        mo = re.search(r"pert([0-9p]*)", sim)
        pert = float(re.sub("p",".", mo.group(1)))
        if mo.group(1) in extra:
          extra = re.sub("pert" + mo.group(1), "", extra)
          if ("--" in extra and extra[0]=="-") or extra == "-":
            extra = extra[1:]
      else:
        pert = 0.01
      #}
      labelQs.append({"Bz":Bz, "dm1":"dm1" if dm1 else "",
        "Lx":Lx, "Ly":Ly, "Lz":Lz, "sigma_i":sigma_i, "sigma_h":sigma_h,
        "eta":eta, "betad":betad, "res":res, "ppc":ppc, "extra":extra,
        "pert":pert
        })
      if not skipSim(labelQs[-1]): #{
        if firstUnskippedSim==-1: #{
          firstUnskippedSim = si
        else: #}{
          for key in labelQs[-1]: #{
            #if labelQs[-1][key] != labelQs[firstUnskippedSim][key]: #{
            #  labelsWithDiffVals.add(key)
            ##}
            if diff(labelQs[-1][key], labelQs[firstUnskippedSim][key]): #{
              labelsWithDiffVals.add(key)
            #}
          #}
        #}
      #}
    except: #}{
      print("Working dir =", os.getcwd())
      print("ERROR for sim", sim)
      raise
    #}
  #}
  if len(set(simKinds)) == 1:
    simKinds = simKinds[0]
  if firstUnskippedSim >= 0: #{
    for key in labelQs[firstUnskippedSim]: #{
      if key not in labelsWithDiffVals: labelsWithSameVals.add(key)
    #}
  #}
  formatStrs ={ "Lx":r"$L_x=$%.4g",
    "eta":r"$\eta$=%.3g",
    "betad":r"$\beta_d$=%.3g",
    "res":r"$\sigma_i \rho_{i0}/\Delta x=$%.3g",
    "ppc":r"ppc=%.4g",
    "Ly":r"$L_y=$%.4g",
    "Lz":r"$L_z=$%.4g",
    "Bz":r"$B_z=$%.3g",
    "sigma_i":r"$\sigma_i=$%.3g" if simKind != "pairLowSigmah" else "",
    "sigma_h":r"$\sigma_h=$%.3g" if simKind != "elecIon" else "",
    "pert":r"pert=%.4g",
    "extra":"%s",
    "dm1":"%s",
  }
  labelOrder = ["sigma_i", "sigma_h",
    "Lx", "Ly", "Lz", "Bz", "eta", "betad",
    "res", "ppc", "pert", "dm1", "extra"]
  if simKinds == "pairLowSigmah":
    labelOrder[0] = labelOrder[1]
    labelOrder[1] = "sigma_i"
  def addLab(labelParts, labelDict, quantity): #{
    if (quantity not in labelsWithSameVals) and (
      quantity not in excludeFromLabel):
      if labelDict[quantity] is not None and formatStrs[quantity] != "":
        addStr = formatStrs[quantity] % labelDict[quantity]
        if addStr != "":
          labelParts.append(addStr)
  #}
  maxLabelLen = 0
  simLabels = []
  for labelDict in labelQs: #{
    labelParts = []
    for q in labelOrder: #{
      addLab(labelParts, labelDict, q)
    #}
    label = ", ".join(labelParts)
    maxLabelLen = max(maxLabelLen, len(label))
    simLabels.append(label)
  #}
  sameParts = []
  commonLabelDict = dict()
  for q in labelOrder: #{
    if q in labelsWithSameVals:
      commonLabelDict[q] = labelQs[0][q]
    if q in labelsWithSameVals: #{
      if formatStrs[q]!="": #{
        if labelQs[0][q] is not None:
          addStr = formatStrs[q] % labelQs[0][q]
          if addStr != "":
            sameParts.append(addStr)
      #}
    #}
  #}
  commonLabel = ', '.join(sameParts)
  if splitCommonLabel: #{
    maxLen = splitCommonLabel if isinstance(
      splitCommonLabel, int) else maxLabelLen+4
    ir = 0
    i = 0
    while i < len(commonLabel): #{
      if (ir > maxLen and commonLabel[i]==','):
        commonLabel = commonLabel[:i] + "\n" + commonLabel[i+2:]
        ir = 0
      ir += 1
      i += 1
    #}
  #}
  # Determine whether labels are the same, followed by "-2", "-3", etc.
  # This allows callers to label simulations with the same parameters
  #  with the same color, etc.
  configurationNumbers = [(i,0,1) for i in range(len(simLabels))
                         ] + [len(simLabels)]
  if combineSame and len(simLabels) > 0: #{
    def getExtraAndNum(extraStr): #{
      mo = re.match("(.*)-([0-9][0-9]*)", extraStr)
      num = 0
      if mo:
        extraStr2 = mo.group(1)
        num = int(mo.group(2))
      else:
        extraStr2 = extraStr
      return extraStr2, num
    #}
    nUnique = 0
    configurationNumbers = []
    for sdi, sd in enumerate(labelQs): #{
      if (sdi==0): #{
        configurationNumbers.append((nUnique,0,1))
        nUnique += 1
      else: #}{
        same = True
        for key in sd: #{
          if key != "extra": #{
            if sdPrev[key] != sd[key]:
              same = False
              #print "sim", simLabels[sdi], "differs by key=", key,
              #print sdPrev[key], "vs", sd[key]
              break
          else: #}{
            extraPrev, numPrev = getExtraAndNum(sdPrev[key])
            extra, num = getExtraAndNum(sd[key])
            if extra != extraPrev:
              #print "sim", simLabels[sdi], "differs by extra",
              #print sdPrev[key], "vs", sd[key], ":",
              #print extraPrev, numPrev, extra, num
              same = False
              break
          #}
        #}
        if same: #{
          print("sim", simLabels[sdi], "is the same as prev.")
          cn = configurationNumbers[-1]
          n = cn[2]+1
          for m in range(n-1):
            cn = configurationNumbers[sdi-1-m]
            configurationNumbers[sdi-1-m] = (cn[0], cn[1], n)
          configurationNumbers.append((nUnique-1, n-1, n))
        else: #}{
          configurationNumbers.append((nUnique,0,1))
          nUnique += 1
        #}
      #}
      sdPrev = sd
      #print simLabels[sdi], "->", configurationNumbers[sdi]
    #}
    # add a (num) to label of first simulation in a row, indicating
    # how many simulations have the same configuration
    for si, cn in enumerate(configurationNumbers): #{
      if (cn[2] > 1 and cn[1] == 0):
        simLabels[si] = simLabels[si] + " (%i)" % cn[2]
    #}
    configurationNumbers.append(nUnique)
    #print configurationNumbers
  #}
  if splitSingleConfiguration and configurationNumbers[-1] == 1: #{
    n = len(configurationNumbers) - 1
    ncn = []
    for m in range(n): #{
      ncn.append((configurationNumbers[m][1],0,1))
    #}
    ncn.append(n)
    configurationNumbers = ncn
  #}
  return (labelQs, simLabels, configurationNumbers,
          commonLabelDict, commonLabel, simKinds)
#}


def getEnergiesVsTime(decimated="defaultFalse", energies = None,
  getUnrB = False, params=None): #{
  """
  returns a dictionary of various energies at each time-step
  (should be run from the data/ directory)
  and also the the dictionary will contain
  energyStepInterval, the number of steps between energy records

  if decimated=True, then this will use the decimated files;
  if decimated=False, then this will not use the decimated files;
  if decimated="defaultFalse" then this will first try to use the
    undecimated energy files, but if not present, will use
    the decimated.
  if decimated="defaultTrue" then this will first try to use the
    decimated energy files, but if not present, will use
    the undecimated.
  if decimated = integer, will find files decimated with that integer
    (default decimation = 20, every 20th energy record is saved)
  """

  nDec = 1
  fa = ""
  if decimated: #{
    nDec = 1
    if decimated == True:
      nDec = 20 # default
    elif decimated == "defaultFalse":
      if not os.path.exists("Eem.dat"):
        nDec = 20
        print("Using decimated energies because undecimated records can't be found.")
    elif isinstance(decimated, int):
      nDec = decimated
    else:
      msg = "decimated = " + str(decimated) + " is not recognized"
      raise ValueError(msg)
    if nDec > 1:
      fa = "Trunc%i" % nDec
  #}

#  def addEnergy(eDict, eStr, deStr = None): #{
#    if deStr is None:
#      deStr = eStr
#    eDict[deStr] =numpy.loadtxt(".././data/E%s_electrons%s.dat" % fa)
#  #}

  #===============================================================================
  # Magnetic and electric energies

  if os.path.exists("Eemc%s.dat" % fa):
    fname = "Eemc%s.dat" % fa
  else:
    fname = "Eem%s.dat" % fa
    if not os.path.exists("Eem%s.dat" % fa):
      msg = "Neither Eemc%s.dat or Eem%s.dat exist in dir " % (fa,fa)
      msg += os.getcwd()
      raise ValueError(msg)
  try:
    data=numpy.loadtxt(fname)
  except:
    # some compilers (cray) separate entries in Eem.dat with commas
    loadtxtKwargs = {"delimiter":','}
    data=numpy.loadtxt(fname, **loadtxtKwargs)

  if fname=="Eemc%s.dat" % fa:
    emag=data[:,:3].sum(axis=1)
    eelc=data[:,3:].sum(axis=1)
    emagx = data[:,0]
    emagy = data[:,1]
    emagz = data[:,2]
    emagt = data[:,:2].sum(axis=1)
    eelct = data[:,3:5].sum(axis=1)
    eelcz = data[:,5]
  else:
    emag=data[:,0]
    eelc=data[:,1]

  #===============================================================================
  # Particle kinetic energies

  # Electrons
  ekineb=numpy.loadtxt(".././data/Ekin_electrons_bg%s.dat" % fa)
  ekined=numpy.loadtxt(".././data/Ekin_electrons_drift%s.dat" % fa)

  # Ions
  ekinpb=numpy.loadtxt(".././data/Ekin_ions_bg%s.dat" % fa)
  ekinpd=numpy.loadtxt(".././data/Ekin_ions_drift%s.dat" % fa)

  #===============================================================================
  # Radiative energies

  # Note sure what the history is here.  There's some suggestion that
  # originally, rad was stored only for combined bg+drift.  But then,
  # zfileUtil as of Sep 2019 suggests that this was changed to store
  # combined and bg alone?
  # But as of Sep 2019, Zeltron3D in RAD_ENERGY always combined bg+drift,
  # writing only the combined Esyn_*ons.dat.
  # In Oct 2019, Zeltron3D writes out bg and drift separately.

  esyne = None
  esynp = None
  eicse = None
  eicsp = None
  esyneb = None
  esynpb = None
  eicseb = None
  eicspb = None
  esyned = None
  esynpd = None
  eicsed = None
  eicspd = None
  # Synchrotron
  if (os.path.exists(".././data/Esyn_electrons%s.dat" % fa)): #{
    # Electrons
    esyne=numpy.loadtxt(".././data/Esyn_electrons%s.dat" % fa)
    # Ions
    if os.path.exists(".././data/Esyn_ions%s.dat" % fa):
      esynp=numpy.loadtxt(".././data/Esyn_ions%s.dat" % fa)
    else:
      esynp=esyne
  #}
  if (os.path.exists(".././data/Eics_electrons%s.dat" % fa)): #{
    # Inverse Compton
    # Electrons
    eicse=numpy.loadtxt(".././data/Eics_electrons%s.dat" % fa)
    # Ions
    if os.path.exists(".././data/Eics_ions%s.dat" % fa):
      eicsp=numpy.loadtxt(".././data/Eics_ions%s.dat" % fa)
    else:
      eicsp=eicse
  #}
  if (os.path.exists(".././data/Esyn_electrons_bg%s.dat" % fa)): #{
    esyneb=numpy.loadtxt(".././data/Esyn_electrons_bg%s.dat" % fa)
    if (os.path.exists(".././data/Esyn_ions_bg%s.dat" % fa)): #{
      esynpb=numpy.loadtxt(".././data/Esyn_ions_bg%s.dat" % fa)
    else:
      esynpb=esyneb
    #}
  #}
  if (os.path.exists(".././data/Eics_electrons_bg%s.dat" % fa)): #{
    eicseb=numpy.loadtxt(".././data/Eics_electrons_bg%s.dat" % fa)
    if (os.path.exists(".././data/Eics_ions_bg%s.dat" % fa)): #{
      eicspb=numpy.loadtxt(".././data/Eics_ions_bg%s.dat" % fa)
    else:
      eicspb=eicseb
    #}
  #}
  if (os.path.exists(".././data/Esyn_electrons_drift%s.dat" % fa)): #{
    esyned=numpy.loadtxt(".././data/Esyn_electrons_drift%s.dat" % fa)
    if (os.path.exists(".././data/Esyn_ions_drift%s.dat" % fa)): #{
      esynpd=numpy.loadtxt(".././data/Esyn_ions_drift%s.dat" % fa)
    else:
      esynpd=esyned
    #}
  #}
  if (os.path.exists(".././data/Eics_electrons_drift%s.dat" % fa)): #{
    eicsed=numpy.loadtxt(".././data/Eics_electrons_drift%s.dat" % fa)
    if (os.path.exists(".././data/Eics_ions_drift%s.dat" % fa)): #{
      eicspd=numpy.loadtxt(".././data/Eics_ions_drift%s.dat" % fa)
    else:
      eicspd=eicsed
    #}
  #}

  if (esyneb is not None): #{
    if (esyne is not None): esyned = esyne - esyneb
    elif (esyned is not None): esyne = esyneb + esyned
  #}
  if (eicseb is not None): #{
    if (eicse is not None): eicsed = eicse - eicseb
    elif (eicsed is not None): eicse = eicseb + eicsed
  #}
  if (esynpb is not None): #{
    if (esynp is not None): esynpd = esynp - esynpb
    elif (esynpd is not None): esynp = esynpb + esynpd
  #}
  if (eicspb is not None): #{
    if (eicsp is not None): eicspd = eicsp - eicspb
    elif (eicspd is not None): eicsp = eicspb + eicspd
  #}

  #===============================================================================
  # create dictionary with energies
  d = {"B":emag,"E":eelc,
       "ekineb":ekineb,"ekined":ekined,
       "ekinpb":ekinpb,"ekinpd":ekinpd,
       "esyne":esyne,"esynp":esynp,
       "eicse":eicse,"eicsp":eicsp,
      }
  if fname=="Eemc%s.dat" % fa:
    d["Bx"] = emagx
    d["By"] = emagy
    d["Bz"] = emagz
    d["Bt"] = emagt
    d["Ez"] = eelcz
    d["Et"] = eelct

  if 1: #{ # if no radiation, set to zero rather than to None
    if (esyne is None and esyneb is None):
      esyne = 0*emag
      esyneb = 0*emag
      esyned = 0*emag
    if (esynp is None and esynpb is None):
      esynp = 0*emag
      esynpb = 0*emag
      esynpd = 0*emag
    if (eicse is None and eicseb is None):
      eicse = 0*emag
      eicseb = 0*emag
      eicsed = 0*emag
    if (eicsp is None and eicspb is None):
      eicsp = 0*emag
      eicspb = 0*emag
      eicspd = 0*emag
  #}

  if (esyne is not None): d["esyne"] = esyne
  if (esynp is not None): d["esynp"] = esynp
  if (esyneb is not None): d["esyneb"] = esyneb
  if (esyned is not None): d["esyned"] = esyned
  if (esynpb is not None): d["esynpb"] = esynpb
  if (esynpd is not None): d["esynpd"] = esynpd

  if (eicse is not None): d["eicse"] = eicse
  if (eicsp is not None): d["eicsp"] = eicsp
  if (eicseb is not None): d["eicseb"] = eicseb
  if (eicsed is not None): d["eicsed"] = eicsed
  if (eicspb is not None): d["eicspb"] = eicspb
  if (eicspd is not None): d["eicspd"] = eicspd


  #===============================================================================
  # Total energy

  # if the simulation is currently running (and in the middle of a dump),
  #  these may have different lengths
  ns = len(emag)
  for key,ed in d.items(): #{
    if ed is not None:
      ns = min(ns, len(ed))
  #}
  for key,ed in d.items(): #{
    if ed is not None:
      d[key] = ed[:ns]
  #}
  #etot=emag[:ns]+eelc[:ns]+ekineb[:ns]+ekined[:ns]+ekinpb[:ns]+ekinpd[:ns]+esyne[:ns]+esynp[:ns]+eicse[:ns]+eicsp[:ns]
  etot = emag.copy()
  for ed in (eelc,ekineb,ekined,ekinpb,ekinpd,esyne,esynp,eicse,eicsp):
    if ed is not None:
      etot += ed[:ns]
  d["etot"] = etot
  d["energyStepInterval"] = nDec
  d["steps"] = numpy.arange(len(etot)) * nDec

  # Special energies
  if (getUnrB): #{
    import scipy
    import scipy.interpolate
    if params is None:
      params = getSimParams()
    unrB, unrBsteps = getEnergyInUnreconnectedRgn(energy="unrB",
      params=params)
    needInterp = (len(d["steps"]) != len(unrBsteps) or
      (numpy.array(d["steps"]) != numpy.array(unrBsteps)).any())
    if needInterp: #{
      def interp(ra): #{
        rafn = scipy.interpolate.interp1d(unrBsteps, ra,
          fill_value=(ra[0], ra[-1]), bounds_error = False)
        rai = rafn(d["steps"])
        return rai
      #}
    else: #}{
      def interp(ra):
        return rai
    #}
    d["unrBsteps"] = unrBsteps
    d["unrB"] = interp(unrB)
    for nrg in ("unrBt", "unrBx", "unrBy", "unrBz"): #{
      unrBw, unrBsteps = getEnergyInUnreconnectedRgn(energy=nrg,
        params=params)
      d[nrg] = interp(unrBw)
    #}
  #}
  return d
#}

def getFieldFilename(fileName): #{
  """
  searches subdirectories fields, currents, densities for fileName;
  raises exception if not found
  (fileName should not include the extension; e.g., "Bx_0" not "Bx_0.h5")
  """
  zName = zFileName(fileName)[0]
  if zName is None:
    zName = zFileName("./" + fileName)[0]
  if zName is None:
    zName = zFileName("./fields/" + fileName)[0]
  if zName is None:
    zName = zFileName("./currents/" + fileName)[0]
  if zName is None:
    zName = zFileName("./densities/" + fileName)[0]
  if zName is None:
    zName = zFileName("./elsasserFields/" + fileName)[0]
  if zName is None:
    msg = "File " + fileName + " not found "
    msg += "(searched in: ./, ./fields, ./currents, ./densities, ./elsasserFields)"
    msg += " from directory " + os.getcwd()
    # a common error:
    if len(fileName) > 2 and fileName[-3:] == ".h5":
      msg += "\n  Note: the file extension should not be included."
    raise IOError(msg)
  return zName
#}

def getBstreamFilename(fileNameOrStep): #{
  """
  searches subdirectories fields
  raises exception if not found
  (fileName should not including the extension; e.g., "Bx_0" not "Bx_0.h5")
  """
  if isinstance(fileNameOrStep, str): #{
    fileName = fileNameOrStep
  else: #}{
    step = int(fileNameOrStep)
    fileName = "BstreamDgn_%i" % step
  #}
  zName = zFileName(fileName)[0]
  if zName is None:
    zName = zFileName("./" + fileName)[0]
  if zName is None:
    zName = zFileName("./fields/" + fileName)[0]
  if zName is None:
    msg = "File " + fileName + " not found "
    msg += "(searched in: ./, ./fields)"
    msg += " from directory " + os.getcwd()
    # a common error:
    if len(fileName) > 2 and fileName[-3:] == ".h5":
      msg += "\n  Note: the file extension should not be included."
    raise IOError(msg)
  return zName
#}

def getBstreamFilenames(errIfNoFiles = True): #{
  """
  assumes cwd is data/ or data/fields

  returns (files, dumps)
    - a list of BstreamDgn filenames, in order
    - a numpy.array of dumps, in order
  """
  prefix = ""
  if os.path.exists("fields"):
    prefix = "fields/"
  pat = prefix + "BstreamDgn_*"
  files = glob.glob(pat)
  if (len(files) == 0):
    msg = "There are no files matching %s.h5 in %s" % (pat, os.getcwd())
    #raise OSError.FileNotFoundError, msg
    raise ValueError(msg)
  dumps = numpy.array([int(s.split("_")[-1].split(".")[0]) for s in files])
  order = numpy.argsort(dumps)
  ofiles = [files[ind] for ind in order]
  dumps = dumps[order]
  return (ofiles, dumps)
#}

def getFieldShape(fileName): #{
  """
  returns (fieldShape, time, step, coords)
  where fieldShape is the shape of the numpy array containing the field,
  and time and step are the time and step when the field was saved,
  and coords = [xs, ys, [zs]], the coordinates of the field grid.

  If slices is a tuple of slice objects, this returns the field values
  for those objects.  (slices shoud be given in [xSlices, ySlices, ...]
  order.)

  Ordering is always x (first coordinate), y (second), ...
  (which means the order is opposite that in the hdf5 file).
  """
  fileName = getFieldFilename(fileName)
  if isHdf5(fileName): #{
    time = readArrayFromHdf5(fileName, "time")
    step = readArrayFromHdf5(fileName, "step")
    try:
      dataSh = readArrayFromHdf5(fileName, "field", shapeOnly = True)
    except tables.exceptions.NoSuchNodeError:
      dsetName = fileName.split("/")[-1].split("_")[0]
      dataSh = readArrayFromHdf5(fileName, dsetName, shapeOnly = True)
    except tables.exceptions.NoSuchNodeError:
      print("ERROR: there is no group named 'field' or '" + dsetName + "'")
      raise
    dataSh = dataSh[::-1]
    ndim = len(dataSh)
    coords = [readArrayFromHdf5(fileName, "axis%icoords" % i)
              for i in range(ndim-1,-1,-1)]
  else: #}{
    raise NotImplementedError("getFieldShape is not yet implemented for text output")
  #}
  return (dataSh, time, step, coords)
#}

def getField(fileName, slices = None, fieldFactors = None): #{
  """
  returns (field, time, step, coords)
  where field[i,j,...] is a numpy array containing the field at
    x = i*dx, y = j*dy, ...
  and time and step are the time and step when the field was saved,
  and coords = [xs, ys, [zs]], the coordinates of the field grid.

  If the fileName has "+" in it, this assumes that the "+" separates
  actual filenames with fields of the same dimensions, and they should be
  added together.
  If fieldFactors is given, it should be a list of numbers, one per
  filename (i.e., the number of "+" symbols plus 1), with a factor
  by which the field should be multiplied.

  If slices is a tuple of slice objects, this returns the field values
  for those objects.  (slices shoud be given in [xSlices, ySlices, ...]
  order.)

  Ordering is always x (first coordinate), y (second), ...
  (which means the order is opposite that in the hdf5 file).
  """
  if "+" in fileName: #{
    data = None
    fileNames = fileName.split("+")
    fni = len(fileNames) - 1
    for fn in fileNames[::-1]: #{
      (data1, time, step, coords) = getField(fn, slices = slices)
      if fieldFactors is not None:
        data1 *= fieldFactors[fni]
      if data is None:
        data = data1
      else:
        data += data1
      fni -= 1
    #}
  else: #}{
    fileName = getFieldFilename(fileName)
    # print("Loading " + fileName + " .h5 file")
    if isHdf5(fileName): #{
      time = readArrayFromHdf5(fileName, "time")
      step = readArrayFromHdf5(fileName, "step")
      if slices is None:
        rSlices = None
      else:
        rSlices = tuple(reversed(list(slices)))

      dsetName = "field"
      mo = re.search(r".*/([^/]*)_[0-9]*.h5", fileName)
      adsetName = None
      if mo:
        adsetName = mo.group(1)
      try:
        data = readArrayFromHdf5(fileName, dsetName, slices = rSlices,
          alternateDatasetName = adsetName)
      except tables.exceptions.NoSuchNodeError:
        dsetName = fileName.split("_")[0]
        data = readArrayFromHdf5(fileName, dsetName, slices = rSlices,
          alternateDatasetName = adsetName)
      except tables.exceptions.NoSuchNodeError:
        print("Error: there is no group named 'field' or '" + dsetName + "'")
        raise
      if fieldFactors is not None:
        data *= fieldFactors[0]
      ndim = len(data.shape)
      if ndim == 2:
        data = numpy.transpose(data)
      else: #3D
        data = numpy.transpose(data, axes = (2,1,0))
      coords = [readArrayFromHdf5(fileName, "axis%icoords" % i,
                 slices = None if rSlices is None else rSlices[i:i+1])
                for i in range(ndim-1,-1,-1)]
      #truncate coords if necessary (usually just losing the upper bound)
      for d in range(len(coords)): #{
        if (len(coords[d])>data.shape[d]):
          coords[d] = coords[d][:data.shape[d]]
      #}
    else: #}{
      raise NotImplementedError("getField is not yet implemented for text output")
    #}
  #}
  # return (remove_ghost_zones(data), time, step, coords)
  return (data, time, step, coords)
#}

def getBstreamDgnField(step, dsetName, slices = None, numSmooths=0,
  params = None): #{
  """
  returns (dataset, time, step, coords)
  where dataset[i,j,...] is a numpy array
  and time and step are the time and step when the field was saved,
  and coords = [xs, ys, [zs]], the coordinates of the field grid.

  Currently not implemented:
  If the fileName has "+" in it, this assumes that the "+" separates
  actual filenames with fields of the same dimensions, and they should be
  added together.
  If fieldFactors is given, it should be a list of numbers, one per
  filename (i.e., the number of "+" symbols plus 1), with a factor
  by which the field should be multiplied.

  If slices is a tuple of slice objects, this returns the field values
  for those objects.  (slices shoud be given in [xSlices, ySlices, ...]
  order, even if the dataset has, e.g., ny=1 because it's an x-z slice)

  Ordering is always x (first coordinate), y (second), ...
  (which means the order is opposite that in the hdf5 file).

  numSmooths > 0 will perform that many 1/4-1/2-1/4 smoothing operations
    (only works if slices is None)
  """
  if 1: #{
    fileName = getBstreamFilename("BstreamDgn_%i" % int(step))
    timeDict = getTime(step=step, params = params)
    time = timeDict["time"]
    if slices is None:
      rSlices = None
    else:
      if numSmooths > 0:
        raise ValueError("cannot smooth and slice at the same time")
      rSlices = tuple(reversed(list(slices)))

    #mo = re.search(r".*/([^/]*)_[0-9]*.h5", fileName)
    try:
      data = readArrayFromHdf5(fileName, dsetName, slices = rSlices,
        alternateDatasetName = None)
    except tables.exceptions.NoSuchNodeError:
      print("Error: there is no group named '" + dsetName + "'", end=' ')
      print("in file", fileName)
      raise
    ndim = len(data.shape)
    if ndim == 2:
      data = numpy.transpose(data)
    else: #3D
      data = numpy.transpose(data, axes = (2,1,0))
    coords = [readArrayFromHdf5(fileName, "axis%icoords" % i,
               slices = None if rSlices is None else rSlices[i:i+1])
              for i in range(ndim-1,-1,-1)]
    if (numSmooths > 0): #{
      if (ndim==2):
        data = smooth2dPeriodic(data, numSmooths=numSmooths)
      elif data.shape[1]==1:
        data[:,0,:] = smooth2dPeriodic(data[:,0,:], numSmooths=numSmooths)
      else:
        msg = "Smooth hasn't been implemented except for ZX datasets"
        raise ValueError(msg)
    #}
  #}
  return (data, time, step, coords)
#}

def getBstreamDgnArray(step, dsetName, slices = None): #{
  """
  returns dataset
  where dataset is a 1D numpy array
  """
  if 1: #{
    fileName = getBstreamFilename("BstreamDgn_%i" % step)
    step = int(fileName[:-3].split("_")[-1])
    try:
      data = readArrayFromHdf5(fileName, dsetName, slices = slices,
        alternateDatasetName = None)
    except tables.exceptions.NoSuchNodeError:
      print("Error: there is no group named '" + dsetName + "'", end=' ')
      print("in file", fileName, "in", os.getcwd())
      raise
  #}
  return data
#}

def getExtField(fieldName, step, slices=None, params = None): #{
  """
  returns (field, time, step, coords)
  where field[i,j,...] is a numpy array containing the field at
    x = i*dx, y = j*dy, ...
  and time and step are the time and step when the field was saved,
  and coords = [xs, ys, [zs]], the coordinates of the field grid.

  The field can come directly from one file, field+"_%i" % step, e.g.,
  field = "Bx" or "Jz_electrons"
  but it can also be special:

  Species fields:
  (Below, [species] is one of
      {electrons_bg,electrons_drift,ions,ft,electrons,ions})
    vel[xyz]_{electrons,ions}: returns v calculated from J/rho
    Debye_[species]:
      Debye lengths from density and temps.
      A very simple (simplistic) calculation, assuming relativistic temperatures,
      ignoring drifts.
    avgRadIC_[species]:
      IC radiation assuming all particles have the average energy in their
      cell, divided by Uph:
      i.e., (4/3) sigma_T c avgGamma^2
    avgRadICrel_[species]:
      avgRadIC but normalized by the average energy density (for all species
      combined)
    density{2/3}_[species]:
      simply the summed density (mapxy[z]...)
      use 2 for 2D, 3 for 3D

  Other fields
    Br, Bp: Br and Bphi, where phi=0 is the (+)x-axis, z is cylinder axis
    Jmag, Jmag_{electrons,ions}: |J|
    ExB_[xyz]
    vExB_[xyz] returns ExB/B^2
    EdotJ, JdotJ_{electrons,ions}: E.J_species
    vel[xyz]_{electrons,ions}:
      calculates velocity from J_ and rho_ fields, in v/c units
    P_{electrons,ions}_{bg,drift}: 1/3 trace of pressure tensor,
      i.e., (Pxx+Pyy+Pzz)/3
    Brms: calculates (Bx^2 + By^2 + Bz^2)^(1/2)
    deltaBrms: calculates (Brms^2 - B0^2)^(1/2)
    vrms: calculates (vx^2 + vy^2 + vz^2)^(1/2)
    Elsaesser variables: Plus, Minus [xyz], Energy plus/minus, and ratios.

  Other fields from BstreamDgn_[step].h5:
    unreconnectedFluxSep:
      the separation (in y) between unreconnected flux vs. x,z
      - this exists only at iy=NY/4 and iy=3*NY/4
    BxFwhm:
      the separation (in y) vs. x,z between the places where Bx is
      half of its upstream value
      - this exists only at iy=NY/4 and iy=3*NY/4

  If slices is a tuple of slice objects, this returns the field values
  for those objects.  (slices shoud be given in [xSlices, ySlices, ...]
  order.)

  Ordering is always x (first coordinate), y (second), ...
  (which means the order is opposite that in the hdf5 file).
  """
  step = int(step)
  def getSp(fldNm,params): #{
    if fldNm[0] == "J" and fldNm[1] in "xyz": #{
      speciesMap = {
        "_electrons":["electrons"],
        "_ions":["ions"],
        "":["electrons","ions"],
      }
    else: #}{
      # baseSpecies = ["electrons_bg", "ions_bg"]
      baseSpecies = ["electrons", "ions"]
      speciesMap = {
        "_electrons_bg":baseSpecies[0:1],
        "_ions_bg":baseSpecies[1:2],
        "_electrons":baseSpecies[0:1],
        "_ions":baseSpecies[1:2],
        "_bg":baseSpecies[:],
        "":baseSpecies[:],
        }
      if "thde" in params: #{
        # baseSpecies = ["electrons_bg", "electrons_drift", "ions_bg", "ions_drift"]
        baseSpecies = ["electrons", "ions"]
        speciesMap = {
          "_electrons_bg":baseSpecies[0:1],
          "_electrons_drift":baseSpecies[1:2],
          "_ions_bg":baseSpecies[2:3],
          "_ions_drift":baseSpecies[3:4],
          "_electrons":baseSpecies[0:2],
          "_ions":baseSpecies[2:4],
          "_bg":baseSpecies[0::2],
          "_drift":baseSpecies[1::2],
          "":baseSpecies[:],
          }
      #}
    #}
    beg = fldNm.split("_")[0]
    return speciesMap[fldNm[len(beg):]]
  #}
  if strBegin(fieldName,"chargeDensity"):
    if (params is None):
      params, units = getSimParams()
    (eDens, t, n, coords) = getField("mapxyz_%s_bg_%i" % ("electrons", step),
                                     slices=slices)
    (iDens, t, n, coords) = getField("mapxyz_%s_bg_%i" % ("ions", step),
                                     slices=slices)
    # don't remove ghost zones since charge density is very similar everywhere
    # data = remove_ghost_zones(Consts.e*(iDens - eDens))
    data = (Consts.e*(iDens - eDens))
  elif strBegin(fieldName,"density2") or strBegin(fieldName,"density3"): #{
    if (params is None):
      params, units = getSimParams()
    species = [fieldName.split("_")[1]]
    densTotal = None
    dim = 2 if (fieldName.split("_")[0][-1]=="2") else 3
    z = "z" if dim==3 else ""
    if species[0] == "total":
      (eDens, t, n, coords) = getField("mapxy%s_%s_bg_%i" % (z,"electrons", step),
        slices=slices)
      (iDens, t, n, coords) = getField("mapxy%s_%s_bg_%i" % (z,"ions", step),
        slices=slices)
      densTotal = eDens + iDens
    else:
      for sp in species: #{
        (spDens, t, n, coords) = getField("mapxy%s_%s_bg_%i" % (z,sp, step),
          slices=slices)
        if densTotal is None:
          densTotal = spDens
        else:
          densTotal += spDens
      #}
    # densTotal[densTotal == 0.] = 1.
    # data = remove_ghost_zones(densTotal)
    data = densTotal
  elif fieldName=="P" or strBegin(fieldName,"P_"): #}{
    # Pxx+Pyy+Pzz
    if (params is None):
      params, units = getSimParams()
    redStr = ""
    while (fieldName[-1]=="R"):
      redStr += "R"
      fieldName = fieldName[:-1]
    species = getSp(fieldName, params)
    p = None
    for sp in species: #{
      for c in "xyz": #{
        (sppc, t, n, coords) = getField("P%s%s_%s%s_%i" % (
          c,c,sp, redStr,step),
          slices=slices)
        if p is None:
          p = sppc
        else:
          p += sppc
      #}
    #}
    p *= (1./3.)
    data = remove_ghost_zones(p)
  elif strBegin(fieldName,"veffrms2"): #}{
    (Prms, t, n, coords) = getExtField("Uprms", step,
                                       slices=slices)
    (Eint, t, n, coords) = getExtField("internalEnergyDensity", step,
    slices=slices)
      #}
    #}
    data = Prms**2*Consts.c**2/(Eint**2 + Prms**2*Consts.c**2)
  elif strBegin(fieldName,"bulkLorentzFactor"): #}{
    (veff2, t, n, coords) = getExtField("veffrms2", step,
                                       slices=slices)
      #}
    #}
    data = 1.0/numpy.sqrt(1.0 - veff2)
  elif strBegin(fieldName,"Uprms"): #}{
    # total Up only, i.e. NOT Uprms_electrons or Uprms_ions
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 1 and name_split[-1] == "restFrame":
      restFrame = True
    for c in "xyz": #{
      field_name = "Up%s_%s" % (c, "total")
      if restFrame:
        field_name += "_restFrame"
      (momc, t, n, coords) = getExtField(field_name, step,
          slices=slices)
      if (c=="x"):
        Uprms2 = momc*momc
      else:
        Uprms2 += momc*momc
      #}
    #}
    data = numpy.sqrt(Uprms2)
  elif strBegin(fieldName, "deltaUprms"):
    (Uprms, t, n, coords) = getExtField("Uprms", step,
                                       slices=slices)
    data = Uprms - numpy.mean(Uprms)
  elif strBegin(fieldName, "dUprmsOverEint"):
    (Uprms, t, n, coords) = getExtField("Uprms", step,
                                       slices=slices)
    dUprms = Uprms - numpy.mean(Uprms)
    (Eint, t, n, coords) = getExtField("internalEnergyDensity", step,
                                       slices=slices)
    data = dUprms/Eint

  elif strBegin(fieldName, "dUprms2OverEint"):
    (Uprms, t, n, coords) = getExtField("Uprms", step,
                                       slices=slices)
    dUprms = Uprms - numpy.mean(Uprms)
    (Eint, t, n, coords) = getExtField("internalEnergyDensity", step,
                                       slices=slices)
    data = dUprms**2.0/Eint

  elif strBegin(fieldName,"Up"): #}{
    restFrame = False
    name_split = fieldName[4:].split("_")
    if len(name_split) > 1 and name_split[1] == "restFrame":
      restFrame = True
    species = name_split[0]
    c = fieldName[2]
    if species not in ("electrons", "ions", "total"): #{
      raise ValueError("field options are Up[xyz]_electrons and Up[xyz]_ions and Up[xyz]_total")
    #}
    if c not in "xyz":
      raise ValueError("field options are Up[xyz]_electrons and Up[xyz]_ions")
    if (params is None):
      params, units = getSimParams()
    if species == "total":
      (momc_e, t, n, coords) = getField("Up%s_%s_bg_%i" % (
            c,"electrons", step),
            slices=slices)
      (momc_i, t, n, coords) = getField("Up%s_%s_bg_%i" % (
            c,"ions", step),
            slices=slices)
      data = momc_e + momc_i
    else:
      (momc, t, n, coords) = getField("Up%s_%s_bg_%i" % (
            c,species, step),
            slices=slices)
      data = momc
      #}
    #}
    data = remove_ghost_zones(data)
    if restFrame:
      import reduce_data_utils as redu
      ph = guess_path_handler()

      trimmed_name = fieldName[:-10]
      kwargs = {"fields_to_vavg":[trimmed_name]}

      vavgs = redu.retrieve_variables_vol_means_stds(ph, **kwargs)[0]
      times = redu.retrieve_time_values(ph)
      tind = numpy.where((times[:, 0] == step))[0][0]
      mean_value = vavgs[trimmed_name][tind]
      # print("Subtracting " + trimmed_name + " mean value: {:.2e}".format(mean_value))
      data = data - mean_value
      # print("Error for " + fieldName + " at timestep {}: {:.2}%".format(step, numpy.nanmean(data)*100))
  elif strBegin(fieldName, "larmorRadius"): #}{
    # first get \bar\gamma (avgPtclEnergy/mc2)
    (avgEnergy, t, n, coords) = getExtField("avgPtclEnergy", step, slices=slices)
    # Now also need Brms
    Brms = getExtField("Brms", step, slices=slices)[0]
    # larmor radius!
    data = avgEnergy/(Brms*Consts.e)
  elif strBegin(fieldName, "skinDepth"): #}{
    # first get \bar\gamma (avgPtclEnergy/mc2)
    (avgEnergy, t, n, coords) = getExtField("avgPtclEnergy", step, slices=slices)
    # Now also need total density
    n_total = getExtField("density3_total", step, slices=slices)[0]
    # plasma skin depth!
    # avoid divide by zero -- this isn't sure -- maybe rho = -1e121e-33,
    #   but any problems should be very rare -- rarer than rho=0.
    data = remove_ghost_zones(numpy.sqrt(avgEnergy/(4*numpy.pi*Consts.e**2*n_total + 1.1211111111e-30)))
  elif strBegin(fieldName,"magnetization"): #}{
    # NOTE: this is VVZ 2020's definition: B2/4pi/h, where
    # h = enthalpy = n0 gamma mc^2 + P = 4/3 n0 gamma mc^2.
    # The best thing to do would be to actually calculate
    # h from the pressure, but here we assume the latter
    # equation holds since it is relativistic.
    # XX change this?

    # first get \bar\gamma (avgPtclEnergy/mc2)
    (avgEnergy, t, n, coords) = getExtField("avgPtclEnergy", step, slices=slices)
    # Now also need Brms
    Brms = getExtField("Brms", step, slices=slices)[0]
    # Also need total number density
    n_total = getExtField("density3_total", step, slices=slices)[0]
    # avoid divide by zero -- this isn't sure -- maybe rho = -1e121e-33,
    #   but any problems should be very rare -- rarer than rho=0.
    data = 3.0*Brms**2/(16.0*numpy.pi*n_total*avgEnergy + 1.1211111111e-30)

  elif strBegin(fieldName,"avgPtclEnergy"): #}{
    if (params is None):
      params, units = getSimParams()
    species = getSp(fieldName, params)
    UeTotal = None
    for sp in species: #{
      (Ue, t, n, coords) = getField("Ue_%s_bg_%i" % (sp, step), slices=slices)
      dim = len(Ue.shape)
      zs = "" if dim==2 else "z"
      (density, t, n, coords) = getField("mapxy%s_%s_bg_%i" % (zs, sp, step),
        slices=slices)
      if UeTotal is None:
        UeTotal = Ue
        densTotal = density
      else:
        UeTotal += Ue
        densTotal += density
    #}
    # UeTotal[densTotal==0] = 1.1211111111e-30
    # densTotal[densTotal == 0.] = 1.
    data = UeTotal / densTotal
  elif strBegin(fieldName,"fluidEnergyDensity"): #}{
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 1 and name_split[-1] == "restFrame":
      restFrame = True
    if (params is None):
      params, units = getSimParams()
    species = ["ions", "electrons"]
    UeTotal = None
    for sp in species: #{
      (Ue, t, n, coords) = getField("Ue_%s_bg_%i" % (sp, step), slices=slices)
      dim = len(Ue.shape)
      zs = "" if dim==2 else "z"
      if UeTotal is None:
        UeTotal = Ue
      else:
        UeTotal += Ue
    #}
    # filter?
    data = remove_ghost_zones(UeTotal)
    if restFrame:
      import reduce_data_utils as redu
      # Here, we take the energy in the rest frame to be
      # Ef - <P>c, where <P> is the magnitude of the net momentum
      ph = guess_path_handler()
      trimmed_name = fieldName[:-10]
      kwargs = {"fields_to_vavg":[trimmed_name]}

      vavgs = redu.retrieve_variables_vol_means_stds(ph, **kwargs)[0]
      times = redu.retrieve_time_values(ph)
      tind = numpy.where((times[:, 0] == step))[0][0]

      net_momentum_magnitude = numpy.sqrt(vavgs["Upx_total"][tind]**2.0 + vavgs["Upy_total"][tind]**2.0 + vavgs["Upz_total"][tind]**2.0)
      data = data - net_momentum_magnitude*Consts.c

  elif strBegin(fieldName,"internalEnergyDensity"): #}{
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 1 and name_split[-1] == "restFrame":
      restFrame = True
    eName = "fluidEnergyDensity"
    pName = "Uprms"
    if restFrame:
      eName += "_restFrame"
      pName += "_restFrame"
    (fluidE, t, n, coords) = getExtField(eName, step, slices=slices)
    (fluidMom, t, n, coords) = getExtField(pName, step, slices=slices)

    data = numpy.sqrt(fluidE**2 - fluidMom**2 * Consts.c**2)
  elif strBegin(fieldName,"bulkEnergyDensity"): #}{
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 1 and name_split[-1] == "restFrame":
      restFrame = True
    Ef_name = "fluidEnergyDensity"
    Ei_name = "internalEnergyDensity"
    if restFrame:
      Ef_name += "_restFrame"
      Ei_name += "_restFrame"
    (fluidE, t, n, coords) = getExtField(Ef_name, step, slices=slices)
    (fluidInt, t, n, coords) = getExtField(Ei_name, step, slices=slices)

    data = fluidE - fluidInt
  elif fieldName in ("Er", "Br") or fieldName in ("Ep","Bp"): #}{
    if (params is None):
      params, units = getSimParams()
    fName = fieldName[0] + "x"
    (Fx, t, n, coords) = getField(fName + "_%i" % step, slices=slices)
    fName = fieldName[0] + "y"
    (Fy, t, n, coords) = getField(fName + "_%i" % step, slices=slices)
    dxMin =  numpy.diff(coords[0]).min() if (len(coords[0]) > 1
      ) else coords[0]
    dyMin =  numpy.diff(coords[1]).min() if (len(coords[1]) > 1
      ) else coords[1]
    dMin = min(dxMin,dyMin)
    x0 = 0.5*(params["xmin"]+params["xmax"])
    y0 = 0.5*(params["ymin"]+params["ymax"])
    xs, ys = numpy.meshgrid(coords[0]-x0, coords[1]-y0, indexing='ij')
    if (fieldName[1]=="r"): #r_x, r_y
      hatx = xs / numpy.sqrt(xs**2 + ys**2 + 1e-16*dMin**2)
      haty = ys / numpy.sqrt(xs**2 + ys**2 + 1e-16*dMin**2)
    else: # phi_x, phi_y
      hatx = -ys / numpy.sqrt(xs**2 + ys**2 + 1e-16*dMin**2)
      haty = xs / numpy.sqrt(xs**2 + ys**2 + 1e-16*dMin**2)
    if (len(Fx.shape) > len(hatx.shape)):
      # assume x and y are first two axes, so if Fx has a third axis (z),
      #  it's the last one
      hatx = hatx[...,None]
      haty = haty[...,None]
    data = remove_ghost_zones(hatx*Fx + haty*Fy)
  elif fieldName in ("Emag", "Bmag", "EmagR", "BmagR", "EmagRR", "BmagRR"): #}{
    for c in "xyz": #{
      fName = fieldName[0] + c + fieldName[4:]
      (Fc, t, n, coords) = getField(fName + "_%i" % step, slices=slices)
      if (c=="x"):
        Fsqr = Fc*Fc
      else:
        Fsqr += Fc*Fc
    #}
    data = remove_ghost_zones(numpy.sqrt(Fsqr))
  elif strBegin(fieldName,"Jmag"): #}{
    if (params is None):
      params, units = getSimParams()
    species = getSp(fieldName, params)
#    #allSpecies = ["electrons_bg", "electrons_drift", "ions_bg", "ions_drift"]
#    allSpecies = ["electrons", "ions"]
#    fnl = 4
#    if (len(fieldName) == fnl):
#      species = allSpecies
#    #elif fieldName[fnl:] == "electrons":
#    #  species = ["electrons_bg", "electrons_drift"]
#    #elif fieldName[fnl:] == "ions":
#    #  species = ["ions_bg", "ions_drift"]
#    else:
#      species = [fieldName[fnl+1:]]
#      if fieldName[fnl]!="_" or species[0] not in allSpecies:
#        msg = "invalid field '" + fieldName + "': valid options are "
#        msg += "'Jmag', 'Jmag_electrons', 'Jmag_ions'"
#        #, 'electrons_bg', 'electrons_drift', "
#        #msg += "'ions_bg', 'ions_drift'"
#        raise ValueError, msg
#
    JmagSqr = None
    for c in "xyz": #{
      for sp in species: #{
        JcName = "J" + c + "_" + sp
        (Jc, t, n, coords) = getField(JcName + "_%i" % step, slices=slices)
        if JmagSqr is None:
          JmagSqr = Jc*Jc
        else:
          JmagSqr += Jc*Jc
      #}
    #}
    data = remove_ghost_zones(numpy.sqrt(JmagSqr))
  elif strBegin(fieldName,"Jtot"): #}{
    c = fieldName[-1]
    if c not in "xyz":
      raise ValueError("field options are Jtot_[xyz]")
    JcName_e = "J" + c + "_" + "electrons"
    (Jc_e, t, n, coords) = getField(JcName_e + "_%i" % step, slices=slices)
    JcName_i = "J" + c + "_" + "ions"
    (Jc_i, t, n, coords) = getField(JcName_i + "_%i" % step, slices=slices)
    data = remove_ghost_zones(Jc_e + Jc_i)
  elif strBegin(fieldName, "alfvenSpeedRms"):
    # Note is normalized to c already.
    # Calculates the *magnitude*, not direction
    # Calculate from the magnetization
      (sigmac, t, n, coords) = getExtField("magnetization", step,
                                       slices=slices)
      data = numpy.sqrt(sigmac/(1.0 + sigmac)) # VVZ 2020 def, NOT VVZ 2018
  elif strBegin(fieldName,"vel"): #}{
    restFrame = False
    if (len(fieldName) < 9):
      raise ValueError("field options are vel[xyz]_electrons and vel[xyz]_ions")
    c = fieldName[3]
    if c not in "xyz":
      raise ValueError("field options are vel[xyz]_electrons and vel[xyz]_ions")
    name_split = fieldName[5:].split("_")
    species = name_split[0]
    if len(name_split) > 1 and name_split[1] == "restFrame":
      restFrame = True
    redStr = ""
    while species[-1]=="R":
      redStr += "R"
      species = species[:-1]
    if species not in ("electrons", "ions", "total"): #{
      raise ValueError("field options are vel[xyz]_electrons and vel[xyz]_ions and vel[xyz]_total")
    #}
    if species == "total":
      JcName_e = "J" + c + "_" + "electrons" + redStr
      rhoName_e = "rho_" + "electrons" + redStr
      (Jc_e, t, n, coords) = getField(JcName_e + "_%i" % step, slices=slices)
      (rho_e, t, n, coords) = getField(rhoName_e + "_%i" % step, slices=slices)
      JcName_i = "J" + c + "_" + "ions" + redStr
      rhoName_i = "rho_" + "ions" + redStr
      (Jc_i, t, n, coords) = getField(JcName_i + "_%i" % step, slices=slices)
      (rho_i, t, n, coords) = getField(rhoName_i + "_%i" % step, slices=slices)
      # avoid divide by zero -- this isn't sure -- maybe rho = -1e121e-33,
      #   but any problems should be very rare -- rarer than rho=0.
      data = (Jc_i - Jc_e)/((rho_i - rho_e)*Consts.c + 1.1211111111e-30)
    else:
      JcName = "J" + c + "_" + species + redStr
      rhoName = "rho_" + species + redStr
      (Jc, t, n, coords) = getField(JcName + "_%i" % step, slices=slices)
      (rho, t, n, coords) = getField(rhoName + "_%i" % step, slices=slices)
      # avoid divide by zero -- this isn't sure -- maybe rho = -1e121e-33,
      #   but any problems should be very rare -- rarer than rho=0.
      data = Jc/(rho * Consts.c + 1.1211111111e-30)
    data = remove_ghost_zones(data)
    if restFrame:
      import reduce_data_utils as redu
      trimmed_name = fieldName[:-10]
      kwargs = {"fields_to_vavg":[trimmed_name]}

      ph = guess_path_handler()

      vavgs = redu.retrieve_variables_vol_means_stds(ph, **kwargs)[0]
      times = redu.retrieve_time_values(ph)
      tind = numpy.where((times[:, 0] == step))[0][0]
      mean_value = vavgs[trimmed_name][tind]
      # print("Subtracting " + trimmed_name + " mean value: {:.2e}".format(mean_value))
      data = data - mean_value
  elif strBegin(fieldName,"ExB"): #}{
    c = {"x":0, "y":1, "z":2}[fieldName[-1]]
    c1 = (c+1)%3
    c2 = (c+2)%3
    for i in range(2): #{
      if (i==1):
        c1, c2 = c2, c1
      Ename = "E" + "xyz"[c1]
      Bname = "B" + "xyz"[c2]
      (Ec1, t, n, coords) = getField(Ename + "_%i" % step, slices=slices)
      (Bc2, t, n, coords) = getField(Bname + "_%i" % step, slices=slices)
      if i==0:
        ExBc = Ec1*Bc2
      else:
        ExBc += -Ec1*Bc2
    #}
    data = ExBc
  elif strBegin(fieldName,"JxB"): #}{
    if (params is None):
      params, units = getSimParams()
    species = getSp(fieldName, params)
    c = {"x":0, "y":1, "z":2}[fieldName[3]]
    c1 = (c+1)%3
    c2 = (c+2)%3
    JxBc = None
    for i in range(2): #{
      if (i==1):
        c1, c2 = c2, c1
      Jc1 = None
      for sp in species: #{
        Jname = "J" + "xyz"[c1] + "_" + sp
        (J, t, n, coords) = getField(Jname + "_%i" % step, slices=slices)
        if Jc1 is None:
          Jc1 = J
        else:
          Jc1 += J
      #}
      Bname = "B" + "xyz"[c2]
      (Bc2, t, n, coords) = getField(Bname + "_%i" % step, slices=slices)
      if JxBc is None:
        JxBc = Jc1*Bc2
      else:
        JxBc += -Jc1*Bc2
    #}
    data = JxBc
  elif strBegin(fieldName,"vExB"): #}{
    c = {"x":0, "y":1, "z":2}[fieldName[-1]]
    c1 = (c+1)%3
    c2 = (c+2)%3
    for i in range(2): #{
      if (i==1):
        c1, c2 = c2, c1
      Ename = "E" + "xyz"[c1]
      Bname = "B" + "xyz"[c2]
      (Ec1, t, n, coords) = getField(Ename + "_%i" % step, slices=slices)
      (Bc2, t, n, coords) = getField(Bname + "_%i" % step, slices=slices)
      if i==0:
        ExBc = Ec1*Bc2
        BmagSqr = Bc2*Bc2
      else:
        ExBc += -Ec1*Bc2
        BmagSqr += Bc2*Bc2
    #}
    Bname = "B" + "xyz"[c]
    (Bc, t, n, coords) = getField(Bname + "_%i" % step, slices=slices)
    BmagSqr += Bc*Bc
    # avoid divide-by-zero
    BsqrMax = BmagSqr.max()
    BmagSqr += BsqrMax*1e-32
    data = ExBc/BmagSqr
  elif strBegin(fieldName,"EdotJf"): #}{
    for c in "xyz": #{
      Ename = "E" + c
      (Ec, t, n, coords) = getField(Ename + "_%i" % step,
        slices=slices)
      JcName = "J" + c + "_external"
      (Jc, t, n, coords) = getField(JcName + "_%i" % step,
                                    slices=slices)
      if (c=="x"):
        JfdotE = Jc*Ec
      else:
        JfdotE += Jc*Ec
      #}
    #}
    data = JfdotE
  elif strBegin(fieldName,"EdotJ"): #}{
    #allSpecies = ["electrons_bg", "electrons_drift", "ions_bg", "ions_drift"]
    allSpecies = ["electrons", "ions"]
    redStr = ""
    while (fieldName[-1]=="R"):
      redStr += "R"
      fieldName = fieldName[:-1]
    fnl = 5
    if (len(fieldName) == fnl):
      species = allSpecies
    #elif fieldName[fnl:] == "electrons":
    #  species = ["electrons_bg", "electrons_drift"]
    #elif fieldName[fnl:] == "ions":
    #  species = ["ions_bg", "ions_drift"]
    else:
      species = [fieldName[fnl+1:]]
      if fieldName[fnl]!="_" or species[0] not in allSpecies:
        msg = "invalid field '" + fieldName + "': valid options are "
        msg += "'EdotJ[R...R]', 'EdotJ_electrons[R...R]', 'EdotJ_ions[R...R]'"
        #, 'electrons_bg', 'electrons_drift', "
        #msg += "'ions_bg', 'ions_drift'"
        raise ValueError(msg)

    for c in "xyz": #{
      Ename = "E" + c
      (Ec, t, n, coords) = getField(Ename + redStr + "_%i" % step,
        slices=slices)
      for sp in species: #{
        JcName = "J" + c + "_" + sp
        (Jc, t, n, coords) = getField(JcName + redStr + "_%i" % step,
          slices=slices)
        if (c=="x"):
          JdotE = Jc*Ec
        else:
          JdotE += Jc*Ec
      #}
    #}
    data = JdotE
  elif strBegin(fieldName,"VdotB"): #}{
    restFrame = False
    name_split = fieldName[6:].split("_")
    if len(name_split) > 1 and name_split[1] == "restFrame":
      restFrame = True
    species = name_split[0]

    for c in "xyz": #{
      Bname = "B" + c
      (Bc, t, n, coords) = getField(Bname + "_%i" % step,
        slices=slices)
      Bc = remove_ghost_zones(Bc)
      VcName = "vel" + c + "_" + species
      if restFrame:
        VcName += "_restFrame"
      (Vc, t, n, coords) = getExtField(VcName, step,
                                       slices=slices)
      if (c=="x"):
        VdotB = Vc*Bc
      else:
        VdotB += Vc*Bc
      #}
    #}
    data = VdotB
  elif strBegin(fieldName,"VzBz"): #}{
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 2 and name_split[-1] == "restFrame":
      restFrame = True
    species = name_split[1]

    Bname = "Bz"
    (Bz, t, n, coords) = getField(Bname + "_%i" % step,
    slices=slices)
    Bz = remove_ghost_zones(Bz)
    VzName = "velz_" + species
    if restFrame:
      VzName += "_restFrame"
    (Vz, t, n, coords) = getExtField(VzName, step,
                                       slices=slices)
    #}
    data = Vz*Bz
  elif strBegin(fieldName,"VzdBz"): #}{
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 2 and name_split[-1] == "restFrame":
      restFrame = True
    species = name_split[1]

    Bname = "Bz"
    (Bz, t, n, coords) = getField(Bname + "_%i" % step,
    slices=slices)
    Bz = remove_ghost_zones(Bz)
    params, units = getSimParams()
    B0 = params["B0"]
    dBz = Bz - B0
    VzName = "velz_" + species
    if restFrame:
      VzName += "_restFrame"
    (Vz, t, n, coords) = getExtField(VzName, step,
                                       slices=slices)
    #}
    data = Vz*dBz
  elif strBegin(fieldName,"VpBp"): #}{
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 2 and name_split[-1] == "restFrame":
      restFrame = True
    species = name_split[1]

    (Bx, t, n, coords) = getField("Bx_%i" % step,
    slices=slices)
    (By, t, n, coords) = getField("By_%i" % step,
    slices=slices)
    Bx = remove_ghost_zones(Bx)
    By = remove_ghost_zones(By)
    VxName = "velx_" + species
    VyName = "vely_" + species
    if restFrame:
      VxName += "_restFrame"
      VyName += "_restFrame"
    (Vx, t, n, coords) = getExtField(VxName, step,
                                       slices=slices)
    (Vy, t, n, coords) = getExtField(VyName, step,
                                       slices=slices)
    #}
    data = Bx*Vx + By*Vy
  elif strBegin(fieldName,"electromagneticEnergyDensity"): #}{
    (Erms2, t, n, coords) = getExtField("Erms2", step, slices=slices)
    (Brms2, t, n, coords) = getExtField("Brms2", step, slices=slices)
    data = (Erms2+Brms2)/(8.0*numpy.pi)
    data = remove_ghost_zones(data)
  elif strBegin(fieldName,"electricEnergyDensity"): #}{
    (Erms2, t, n, coords) = getExtField("Erms2", step, slices=slices)
    data = Erms2/(8.0*numpy.pi)
    data = remove_ghost_zones(data)
  elif strBegin(fieldName,"magneticEnergyDensity"): #}{
    (Brms2, t, n, coords) = getExtField("Brms2", step, slices=slices)
    data = Brms2/(8.0*numpy.pi)
    data = remove_ghost_zones(data)
  elif strBegin(fieldName,"Erms"): #}{
    for c in "xyz": #{
      Ename = "E" + c
      (Ec, t, n, coords) = getField(Ename + "_%i" % step,
        slices=slices)
      if (c=="x"):
        Emag = Ec*Ec
      else:
        Emag += Ec*Ec
      #}
    #}
    if fieldName == "Erms2":
      data = Emag
    else:
      data = numpy.sqrt(Emag)
    data = remove_ghost_zones(data)
  elif strBegin(fieldName,"Brms"): #}{
    for c in "xyz": #{
      Bname = "B" + c
      (Bc, t, n, coords) = getField(Bname + "_%i" % step,
        slices=slices)
      if (c=="x"):
        Bmag = Bc*Bc
      else:
        Bmag += Bc*Bc
      #}
    #}
    if fieldName == "Brms2":
      data = Bmag
    else:
      data = numpy.sqrt(Bmag)
    data = remove_ghost_zones(data)
  elif strBegin(fieldName,"VdotdB"): #}{
    restFrame = False
    name_split = fieldName[7:].split("_")
    if len(name_split) > 1 and name_split[1] == "restFrame":
      restFrame = True
    species = name_split[0]
    params, units = getSimParams()
    B0 = params["B0"]

    for c in "xyz": #{
      Bname = "B" + c
      (Bc, t, n, coords) = getField(Bname + "_%i" % step,
        slices=slices)
      Bc = remove_ghost_zones(Bc)
      if c == "z":
        Bc = Bc - B0
      VcName = "vel" + c + "_" + species
      if restFrame:
        VcName += "_restFrame"
      (Vc, t, n, coords) = getExtField(VcName, step,
                                       slices=slices)
      if (c=="x"):
        VdotB = Vc*Bc
      else:
        VdotB += Vc*Bc
      #}
    #}
    data = VdotB
  elif strBegin(fieldName,"deltaBrms"): #}{
    (Brms, t, n, coords) = getExtField("Brms", step, slices=slices)
    (Bz, t, n, coords) = getField("Bz_%i" % step,
        slices=slices)
    Bz = remove_ghost_zones(Bz)
    params, units = getSimParams()
    B0=params["B0"]
    dBrms2 = Brms*Brms - 2*Bz*B0 + B0*B0
    if fieldName == "deltaBrms2":
      return dBrms2
    elif fieldName == "deltaBrms2vA":
      (vA, t, n, coords) = getExtField("alfvenSpeedRms", step, slices=slices)
      return dBrms2*vA
    else:
      return numpy.sqrt(dBrms2)
  elif strBegin(fieldName,"velp2_total"): #}{
    restFrame = False
    allSpecies = ["electrons", "ions", "total"]
    redStr = ""
    name_split = fieldName[5:].split("_")
    if len(name_split) > 1 and name_split[1] == "restFrame":
      restFrame = True
    while (fieldName[-1]=="R"):
      redStr += "R"
      fieldName = fieldName[:-1]
    fnl = 4
    if (len(fieldName) == fnl):
      species = allSpecies
    else:
      species = [name_split[0]]
      # species = [fieldName[fnl+1:]]
      if fieldName[fnl]!="_" or species[0] not in allSpecies:
        msg = "invalid field '" + fieldName + "': valid options are "
        msg += "'vrms[R...R]', 'vrms_electrons[R...R]', 'vrms_ions[R...R]'"
        #, 'electrons_bg', 'electrons_drift', "
        #msg += "'ions_bg', 'ions_drift'"
        raise ValueError(msg)

    for c in "xy": #{
      for sp in species: #{
        VcName = "vel" + c + "_" + sp
        if restFrame:
          VcName += "_restFrame"
        (Vc, t, n, coords) = getExtField(VcName + redStr, step,
          slices=slices)
        if (c=="x"):
          Vrms2 = Vc*Vc
        else:
          Vrms2 += Vc*Vc
      #}
    #}
    data = Vrms2

  elif strBegin(fieldName,"vrms"): #}{
    restFrame = False
    allSpecies = ["electrons", "ions", "total"]
    redStr = ""
    name_split = fieldName[5:].split("_")
    if len(name_split) > 1 and name_split[1] == "restFrame":
      restFrame = True
    while (fieldName[-1]=="R"):
      redStr += "R"
      fieldName = fieldName[:-1]
    fnl = 4
    if (len(fieldName) == fnl):
      species = allSpecies
    #elif fieldName[fnl:] == "electrons":
    #  species = ["electrons_bg", "electrons_drift"]
    #elif fieldName[fnl:] == "ions":
    #  species = ["ions_bg", "ions_drift"]
    else:
      species = [name_split[0]]
      # species = [fieldName[fnl+1:]]
      if fieldName[fnl]!="_" or species[0] not in allSpecies:
        msg = "invalid field '" + fieldName + "': valid options are "
        msg += "'vrms[R...R]', 'vrms_electrons[R...R]', 'vrms_ions[R...R]'"
        #, 'electrons_bg', 'electrons_drift', "
        #msg += "'ions_bg', 'ions_drift'"
        raise ValueError(msg)

    for c in "xyz": #{
      for sp in species: #{
        VcName = "vel" + c + "_" + sp
        if restFrame:
          VcName += "_restFrame"
        (Vc, t, n, coords) = getExtField(VcName + redStr, step,
          slices=slices)
        if (c=="x"):
          Vrms2 = Vc*Vc
        else:
          Vrms2 += Vc*Vc
      #}
    #}
    data = numpy.sqrt(Vrms2)
  elif strBegin(fieldName,"pelsaesserPlusEnergy"): #}{
    for c in "xy": #{
      ZcName = "elsaesserPlus" + c
      (Zc, t, n, coords) = getExtField(ZcName, step,
                                       slices=slices)
      if (c=="x"):
        Zrms2 = Zc*Zc
      else:
        Zrms2 += Zc*Zc
    data = Zrms2
  elif strBegin(fieldName,"pelsaesserMinusEnergy"): #}{
    for c in "xy": #{
      ZcName = "elsaesserMinus" + c
      (Zc, t, n, coords) = getExtField(ZcName, step,
                                       slices=slices)
      if (c=="x"):
        Zrms2 = Zc*Zc
      else:
        Zrms2 += Zc*Zc
    data = Zrms2
  elif strBegin(fieldName,"pelsaesserDifference"): #}{
    ZmName = "pelsaesserMinusEnergy"
    (Zm, t, n, coords) = getExtField(ZmName, step,
                                    slices=slices)
    ZpName = "pelsaesserPlusEnergy"
    (Zp, t, n, coords) = getExtField(ZpName, step,
                                    slices=slices)
    data = Zp - Zm
  elif strBegin(fieldName,"pelsaesserEnergy"): #}{
    ZmName = "pelsaesserMinusEnergy"
    (Zm, t, n, coords) = getExtField(ZmName, step,
                                    slices=slices)
    ZpName = "pelsaesserPlusEnergy"
    (Zp, t, n, coords) = getExtField(ZpName, step,
                                    slices=slices)
    data = Zm + Zp
  elif strBegin(fieldName,"elsaesserPlusRms"): #}{
    for c in "xyz": #{
      ZcName = "elsaesserPlus" + c
      (Zc, t, n, coords) = getExtField(ZcName, step,
                                       slices=slices)
      if (c=="x"):
        Zrms2 = Zc*Zc
      else:
        Zrms2 += Zc*Zc
      #}
    #}
    data = numpy.sqrt(Zrms2)
  elif strBegin(fieldName,"elsaesserMinusRms"): #}{
    for c in "xyz": #{
      ZcName = "elsaesserMinus" + c
      (Zc, t, n, coords) = getExtField(ZcName, step,
                                       slices=slices)
      if (c=="x"):
        Zrms2 = Zc*Zc
      else:
        Zrms2 += Zc*Zc
      #}
    #}
    data = numpy.sqrt(Zrms2)
  elif strBegin(fieldName,"elsaesserRatio"): #}{
    ZmName = "elsaesserMinusRms"
    (Zm, t, n, coords) = getExtField(ZmName, step,
                                       slices=slices)
    ZpName = "elsaesserPlusRms"
    (Zp, t, n, coords) = getExtField(ZpName, step,
                                       slices=slices)
      #}
    #}
    # avoid divide by zero -- this isn't sure -- maybe rho = -1e121e-33,
    #   but any problems should be very rare -- rarer than rho=0.
    data = Zp/(Zm + 1.1211111111e-30)
  elif strBegin(fieldName,"elsaesserEnergyRatio"): #}{
    ZmName = "elsaesserEnergyMinus"
    (Zm, t, n, coords) = getExtField(ZmName, step,
                                       slices=slices)
    ZpName = "elsaesserEnergyPlus"
    (Zp, t, n, coords) = getExtField(ZpName, step,
                                       slices=slices)
      #}
    #}
    data = (Zp/(Zm + 1.1211111111e-30))**2
  elif strBegin(fieldName,"elsaesserEnergy"): #}{
    if fieldName == "elsaesserEnergyPlus":
      ZName = "elsaesserPlusRms"
    elif fieldName == "elsaesserEnergyMinus":
      ZName = "elsaesserMinusRms"
    (Zd, t, n, coords) = getExtField(ZName, step,
                                     slices=slices)
      #}
    #}
    data = Zd**2
  elif strBegin(fieldName,"elsaesser"): #}{
    # Attempt to read from file first
    if not os.path.exists('./elsasserFields'):
      os.makedirs("elsasserFields")
    fileName = "elsasserFields/" + fieldName + "_%i" % step
    h5FileName = fileName + '.h5'
    if (os.path.exists(h5FileName)): #{
      (data, t, n, coords) = getField(fileName, slices=slices)
    else:
      c = {"x":0, "y":1, "z":2}[fieldName[-1]]
      vname = "vel" + "xyz"[c] + "_total"
      Bname = "B" + "xyz"[c]
      (Vc, t, n, coords) = getExtField(vname, step,
                      slices=slices)
      Bc = remove_ghost_zones(getField(Bname + "_%i" % step, slices=slices)[0])

      params, units = getSimParams()
      # sigmaDict = getSimSigmas(params)
      # sigma = sigmaDict["sigma"] # note this is the convention in Zhdankin+2020, NOT 2018
      # vA = sigmaDict["vAoverC"]
      # Note Alfven speed (and velocity) is normalized to c already
      alfvenSpeed = getExtField("alfvenSpeedRms", step, slices=slices)[0]

      # Only want fluctuating part, so subtract background field
      B0 = params["B0"]
      if c == 2:
        Bc = Bc - B0

      if strBegin(fieldName, "elsaesserPlus"):
        data = Vc + Bc/B0*alfvenSpeed
      elif strBegin(fieldName, "elsaesserMinus"):
        data = Vc - Bc/B0*alfvenSpeed
      # Save to new .h5 file
      # print("Saving " + fieldName + "_" + str(step) + " to .h5 file.")
      writeArrayToHdf5(data, h5FileName, 'w', "field")
      writeArrayToHdf5(t, h5FileName, 'a', "time")
      writeArrayToHdf5(step, h5FileName, 'a', "step")
      writeArrayToHdf5(coords[0], h5FileName, 'a', "axis0coords")
      writeArrayToHdf5(coords[1], h5FileName, 'a', "axis1coords")
      writeArrayToHdf5(coords[2], h5FileName, 'a', "axis2coords")

  elif strBegin(fieldName,"vBalignment"): #}{
    species = fieldName.split("_")[-1]
    vname = "vrms_" + species
    dBname = "deltaBrms"
    vdotdBname = "VdotdB_" + species

    (Vrms, t, n, coords) = getExtField(vname, step,
                                       slices=slices)
    (dBrms, t, n, coords) = getExtField(dBname, step,
                                       slices=slices)
    (vdotdBc, t, n, coords) = getExtField(vdotdBname, step,
                                       slices=slices)

    if strBegin(fieldName, "vBalignmentAngle"):
      data = numpy.arccos(vdotdBc/(Vrms * dBrms + 1.1211111111e-30))
    else:
      data = vdotdBc/(Vrms * dBrms + 1.1211111111e-30)
  elif strBegin(fieldName, "zEnergyDensity"):
    data = None
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 1 and name_split[-1] == "restFrame":
      restFrame = True
    # calculate kinetic energy in the z-direction
    for species in ["electrons", "ions"]:
      velName = "velz_" + species
      if restFrame:
        velName += "_restFrame"
      (Vz, t, n, coords) = getExtField(velName, step,
                                       slices=slices)
      (rho, t, n, coords) = getExtField("density3_" + species, step,
                                          slices=slices)
      if data is None:
        data = rho*Vz**2.0
      else:
        data += rho*Vz**2.0
    data = data*0.5
  elif strBegin(fieldName, "perpEnergyDensity"):
    data = None
    restFrame = False
    name_split = fieldName.split("_")
    if len(name_split) > 1 and name_split[-1] == "restFrame":
      restFrame = True
    # calculate kinetic energy in the z-direction
    for species in ["electrons", "ions"]:
      velxName = "velx_" + species
      velyName = "vely_" + species
      if restFrame:
        velxName += "_restFrame"
        velyName += "_restFrame"
      (Vx, t, n, coords) = getExtField(velxName, step,
                                       slices=slices)
      (Vy, t, n, coords) = getExtField(velyName, step,
                                       slices=slices)
      (rho, t, n, coords) = getExtField("density3_" + species, step,
                                        slices=slices)
      if data is None:
        data = rho*(Vx**2.0 + Vy**2.0)
      else:
        data += rho*(Vx**2.0 + Vy**2.0)
    data = data*0.5

  elif strBegin(fieldName,"Debye"): #}{
    # calculate inverse Debye length squared per cell
    # Get energy density (total for species, including rest mass).
    if (params is None):
      params, units = getSimParams()
    species = getSp(fieldName, params)
    invDebye = None
    for sp in species: #{
      print("Assuming relativistically-hot plasma...")
      (Ue, t, n, coords) = getField("Ue_%s_bg_%i" % (sp, step), slices=slices)
      dim = len(Ue.shape)
      zs = "" if dim==2 else "z"
      (density, t, n, coords) = getField("mapxy%s_%s_bg_%i" % (zs, sp, step),
        slices=slices)
      # assume temp is 1/3 of average energy, which is energy density / density
      Ue[density==0] = 1.
      idSqr = 4.*numpy.pi*Consts.e**2 * density**2 / (1./3. * Ue)
      idSqr[density==0.] == 0.
      if invDebye is None:
        invDebye = idSqr
      else:
        invDebye += idSqr
    #}
    idm = invDebye.max()
    if idm == 0.: #{
      data = invDebye # all zeros
    else: #}{
      zeros = (invDebye == 0.)
      # in cells with no plasma, set Debye length to minimim non-zero
      #  Debye length
      if zeros.any():
        print("%g cells have zero particles" % zeros.sum())
      invDebye[zeros] = (idm if idm > 0. else 1.)
      data = 1./numpy.sqrt(invDebye)
    #}
  elif strBegin(fieldName,"avgRadIC"): #}{
    # calculate IC radiation assuming all particles have the average
    # energy in their cell.
    # N.B. Actually returns radiated power density over U_ph
    norm = strEnd(fieldName.split("_")[0],"rel")
    if (params is None):
      params, units = getSimParams()
    species = getSp(fieldName, params)
    rad = None
    totalEnergy = 0.
    for sp in species: #{
      (Ue, t, n, coords) = getField("Ue_%s_%i" % (sp, step), slices=slices)
      dim = len(Ue.shape)
      zs = "" if dim==2 else "z"
      (density, t, n, coords) = getField("mapxy%s_%s_%i" % (zs, sp, step),
        slices=slices)
      zeros = (density==0.)
      density[zeros] = 1.
      avgGamma = Ue/density / (Consts.m_e * Consts.c**2)
      radsp = 32.*numpy.pi/9.*Consts.r_e**2 * Consts.c * density*avgGamma**2
      if norm:
        totalEnergy += Ue.sum()
      if rad is None:
        rad = radsp
      else:
        rad += radsp
    #}
    if norm: #{
      cellVol = 1.
      vol = 1.
      for d in range(len(coords)): #{
        cellVol *= coords[d][1] - coords[d][0]
        vol *= coords[d][-1] - coords[d][0]
      #}
      totalEnergy *= cellVol
      rad /= (totalEnergy/vol)
    #}
    data = rad
  elif strBegin(fieldName,"unreconnectedFluxSep"
    ) or strBegin(fieldName,"BxFwhm"): #}{
    if (params is None):
      params, units = getSimParams()
    # This may be called from zPy/for3D...but sliceUtil is only in for2D.
    syspath = sys.path
    sys.path.append(os.path.dirname(__file__) + "/../for2D")
    import sliceUtil
    sys.path = syspath
    ySliceTriple = sliceUtil.sliceToTriple(slices[1], params["NY"]+1)
    #if ySlice.start is None:
    #  msg = "a ySlice must be specifed: it can start at any index in the "
    #  msg += "lower half to get the lower half layer thickness, or any in "
    #  msg += "the upper half for the upper half layer thickness."
    #  raise ValueError, msg
    iyLo = int(round((params["NY"]-1)/4))
    iyHi = int(round(3*(params["NY"]-1)/4))
    if ySliceTriple[0] != iyLo and ySliceTriple[0] != iyHi:
      msg = "the y-slice must include one cell in y, starting at either "
      msg += repr(iyLo) + " or " + repr(iyHi) + ", not iy="
      msg += repr(ySliceTriple[0])
      raise ValueError(msg)
    if ySliceTriple[1] > ySliceTriple[0] + ySliceTriple[2]:
      msg = "the y-slice must include one and only one cell in y"
      raise ValueError(msg)
    layerInd = 0 if (ySliceTriple[0] < params["NY"]/2) else 1
    bound = "flux" if fieldName[0] == "u" else "Bfwhm"
    dsetName = "y"
    dsetName += "Lower" if (layerInd==0) else "Upper"
    dsetName += "Layer"
    if bound == "flux": #{
      dsetNameLo = dsetName + "BottomZX"
      dsetNameHi = dsetName + "TopZX"
    else: #}{
      dsetNameLo = dsetName + "DnHalfBZX"
      dsetNameHi = dsetName + "UpHalfBZX"
    #}
    slices2 = [slices[0], slice(0,1), slices[2]]
    (dsetLo, t, n, coords
      ) = getBstreamDgnField(step, dsetNameLo, slices = slices2)
    dsetHi = getBstreamDgnField(step, dsetNameHi, slices = slices2)[0]
    data = dsetHi - dsetLo
  else: #}{
    (data,t,n,coords) = getField(
      fieldName + ("_%i" % step), slices=slices)
  #}
  return (data, t, n, coords)
#}

def getFieldOnLayer(fieldName, step,
  layer = 0,
  posRelToLayer = 0,
  layerBoundedByHalfB = False,
  yAvgMethod = None,
  yAvgDist = None,
  layerSmooths = 0,
  slices=None, params = None): #{
  """
  Get field values on an x-z grid corresponding to y-values that
  are above, below, or in the (lower or upper) layer.

  returns (field, time, step, coords)
  where field[i,j,...] is a numpy array containing the field at
    x = i*dx, y = j*dy, ...
  and time and step are the time and step when the field was saved,
  and coords = [xs, ys, [zs]], the coordinates of the field grid.


  slices[1] will be ignored, but the x-z grid can be restricted by
    slices[0] and slices[2].

  fieldName and step and params: as in getExtField
  layer = {0 or "lower",  1 or "upper"}
  posRelToLayer = {"below" or -1, "center" or 0, "above" or 1}
    below is the same as -1, etc.
  layerBoundedByHalfB:
    if False, then the layer bounds are unreconnected flux bounds;
    if True, then the layer bounds are where B_x is half its upstream value.
  yAvgMethod = {None: no averaging over y
               "layer": (for posRelToLayer="center" only) over whole layer
               "cells": average over yAvgDist cells
               "rhoc": average over roughly yAvgDist * rhoc
               }
  yAvgDist = the number of cells or units of rhoc over which to average
    (applies only if yAveraging is "cells" or "rhoc"
    If posRelToLayer is "center", then, the distance will be symmetric about
      the center, but truncated to be within the layer.
    If posRelToLayer is "below", then the distance will be from the layer
      bottom extending yAvgDist down.
    If posRelToLayer is "above", then the distance will be from the layer
      top extending yAvgDist up.
  """

  if layer not in ("lower", "upper", 0, 1): #{
    msg = "layer = " + str(layer) + " is invalid"
    raise ValueError(msg)
  #}
  if layer == "lower":
    layer = 0
  if layer == "upper":
    layer = 1
  if posRelToLayer not in ("below", "above", "center", -1, 0, 1): #{
    msg = "posRelToLayer = " + str(posRelToLayer) + " is invalid"
    raise ValueError(msg)
  #}
  if posRelToLayer == "below":
    posRelToLayer = -1
  if posRelToLayer == "center":
    posRelToLayer = 0
  if posRelToLayer == "above":
    posRelToLayer = 1
  if (yAvgMethod is not None) and yAvgMethod not in ("layer", "cells",
    "rhoc"): #{
    msg = "yAvgMethod = " + str(yAvgMethod) + " is invalid"
    raise ValueError(msg)
  #}
  if yAvgMethod == "layer" and posRelToLayer != 0: #{
    msg = "yAvgMethod = layer can be used only with posRelToLayer = center"
    raise ValueError(msg)
  #}

  if params is None:
    params, units = getSimParams()

  import numpy.ma

  dset = "y"
  dset += ("Lower", "Upper")[layer]
  dset += "Layer"
  if (layerBoundedByHalfB):
    dset += ("DnHalfB", "Center", "UpHalfB")[1+posRelToLayer]
  else:
    dset += ("Bottom", "Center", "Top")[1+posRelToLayer]
  dset += "ZX"

  xzSlices = slices
  if slices is None:
    #xzSlices = [slice(None), slice(1), slice(None)]
    pass
  else: # ignore y-slice
    xzSlices = [slices[0], slice(1), slices[2]]



  #ns = 0
  #if "layerSmooths" in kws:
  #  ns = kws["layerSmooths"]
  ysMid = getBstreamDgnField(step, dset,
      numSmooths=layerSmooths, slices=xzSlices, params=params)[0]
  if posRelToLayer != 0 or yAvgMethod is not None: #{
    # if posRelToLayer=0, need to get layer bounds,
    #    so averaging doesn't go beyond them
    dnStr = "DnHalfB" if layerBoundedByHalfB else "Bottom"
    dsetDn = "y" + ("Lower", "Upper")[layer] + "Layer" + dnStr + "ZX"
    ysDn = getBstreamDgnField(step, dsetDn,
      numSmooths=layerSmooths, slices=xzSlices, params=params)[0]
    upStr = "UpHalfB" if layerBoundedByHalfB else "Top"
    dsetUp = "y" + ("Lower", "Upper")[layer] + "Layer" + upStr + "ZX"
    ysUp = getBstreamDgnField(step, dsetUp,
      numSmooths=layerSmooths, slices=xzSlices, params=params)[0]
  #}

  if posRelToLayer == 0:
    ys = ysMid
  elif posRelToLayer == -1:
    ys = ysDn
  elif posRelToLayer == 1:
    ys = ysUp
  if yAvgMethod is None: #{
    ysLo = ys
    ysHi = ys
  elif yAvgMethod == "layer": #}{
    ysLo = ysDn
    ysHi = ysUp
  else: #}{
    rhoc=Consts.c/params["omegac"]
    if yAvgMethod == "cells":
      yAvgDist = params["dy"]*yAvgDist
    else: # yAvgMethod=="rhoc"
      yAvgDist = rhoc*yAvgDist
    # yAvgDist now in cm
    if posRelToLayer == 0:
      ysLo = numpy.maximum(ys - 0.5*yAvgDist, ysDn)
      ysHi = numpy.minimum(ys + 0.5*yAvgDist, ysUp)
    elif posRelToLayer == -1:
      ysLo = ys - yAvgDist
      ysHi = ys
    elif posRelToLayer == 1:
      ysLo = ys
      ysHi = ys + yAvgDist
  #}

  jsLo = numpy.int32(numpy.round((ysLo - params["ymin"])/params["dy"]))
  jsHi = numpy.int32(numpy.round((ysHi - params["ymin"])/params["dy"])) + 1
  jMin = jsLo.min()
  jMax = jsHi.max()

  # :TODO: read in chunks

  fSlices = slices
  if fSlices is not None: # ignore y
    fSlices = (slices[0], slice(jMin,jMax), slices[2])
  (fdata, t, n, coords) = getExtField(
    fieldName, step, slices=fSlices, params = params)

  # Following is difficult with chunking
  if fdata.shape[0] < ys.shape[0]: # assume fdata is missing upper bound
    ys = ys[:fdata.shape[0]]
    ysLo = ysLo[:fdata.shape[0]]
    ysHi = ysHi[:fdata.shape[0]]
    jsLo = jsLo[:fdata.shape[0]]
    jsHi = jsHi[:fdata.shape[0]]
  if fdata.shape[2] < ys.shape[2]: # assume fdata is missing upper bound
    ys = ys[:,:,:fdata.shape[2]]
    ysLo = ysLo[:,:,:fdata.shape[2]]
    ysHi = ysHi[:,:,:fdata.shape[2]]
    jsLo = jsLo[:,:,:fdata.shape[2]]
    jsHi = jsHi[:,:,:fdata.shape[2]]

  # average over (selected range in) y
  jsLo -= jMin
  jsHi -= jMin

  # mask array
  #mfData = fData.view(numpy.ma.MaskedArray)
  js = numpy.meshgrid(*list(map(numpy.arange, fdata.shape)), indexing='ij')[1]
  mfdata = numpy.ma.array(fdata)
  mfdata.mask = numpy.logical_or(js<jsLo, js>=jsHi)

  data = mfdata.mean(axis=1).data
  # make data a 3D field, just as getField would return
  sh3d = (data.shape[0],1,data.shape[1])
  data = numpy.reshape(data, sh3d)
  return (data, t, n, coords)
#}

def getExtOrLayerField(fieldName, step,
  slices=None, params = None,
  **layerFieldKws): #{
  """
  layerFieldKws can contain arguments for getFieldOnLayer;
  if layerFieldKws is empty, this just calls getExtField
  layer = 0,
  posRelToLayer = 0,
  yAvgMethod = None,
  yAvgDist = None,
  layerSmooths = 0,
  """
  if len(layerFieldKws) == 0:
    res = getExtField(fieldName, step, slices=slices, params=params)
  else:
    res = getFieldOnLayer(fieldName, step, slices=slices, params=params,
      **layerFieldKws)
  return res
#}

def getEnergyInUnreconnectedRgn(energy=None, params=None, cacheFile=None): #{
  """
  should be run from data/ directory
  returns (steps, Ub)
    where steps is an array of the timestep at which Ub is known,
    and Ub the energy (or energies at the timesteps)

    If energy is None, then
      Ub is a 3D array of shape (numSteps, 4, 3)
      where Ub[n,q,c] is the energy in unreconnected magnetic field
      in quarter q (0 = below lower layer,
                    1 = above lower layer but below sim center, ...)
      in magnetic field component c (0=B_x, 1=B_y, 2=B_z)
    If energy is:
      unrB{x,y,z}, unrBt, unrB
      any of the above + h{0,1} or + q{0-3}
      then
      Ub is a 1D array of length numSteps, containing
      the energy in the specified component ( t-> B_x^2 + B_y^2 )
      summed over all 4 quarters, are just the one half, or
      just for a single quarter.

  """
  if (not os.path.exists("input_params.dat")): #{
    msg = "This must be run from the simulation data/ directory"
    raise ValueError(msg)
  #}
  if cacheFile is None:
    cacheFile = "bEnergyInUnreconnected.dat"
  if (os.path.exists("extra")):
    # This is for file systems (ALCF) where we can't write new files, but
    #  can create links.  Here, one should (in the data dir) do, e.g.:
    #  ln -s /someWriteDirectory... extra
    cacheFile = "extra/" + cacheFile
  if (os.path.exists(cacheFile)): #{
    try:
      ra = numpy.loadtxt(cacheFile, skiprows = 1)
    except:
      print("Error in ", os.getcwd())
      raise
    steps = numpy.int64(ra[:,0])
    n = ra.shape[0]
    Ub = numpy.reshape(ra[:,2:], (n,4,3))
  else: #}{
    if (params is None):
      params, units = getSimParams()
    bstrmFiles, steps = getBstreamFilenames()
    n = len(steps)
    if (n==0):
      msg = "No BstreamDgn_*.h5 were found: are you in data/?"
      raise ValueError(msg)
    Ub = numpy.empty((n,4,3), dtype=numpy.float64)
    for i, s in enumerate(steps): #{
      try:
        data = getBstreamDgnArray(s, "sumUnreconnectedBcSqr")
      except:
        print(("Warning: could not get dataset sumUnreconnectedBcSqr from"
          + " file for step " + str(s) + " in " + os.getcwd() +
          " -- inserting all 0s"))
        data = numpy.zeros((4,3))
      Ub[i] = numpy.reshape(data, (4,3))

    #}
    if 1: #{
      Ls, Ns = getSimLengths(params=params, zeroLzIn2d=False)
      dxs = Ls/Ns
      dV = numpy.prod(dxs)
      factor = dV/(8.*Consts.pi)
      Ub *= factor
    #}
    ra = numpy.empty((n,14), dtype=numpy.float64)
    ra[:,0] = steps
    ra[:,1] = [getTime(step=s, params=params)["tcOverLx"] for s in steps]
    ra[:,2:] = numpy.reshape(Ub, (n,12))
    header = ("step LxLightCrossingTime " +
      "unreconnectedBxEnergyQ1 unrByEnergyQ1 unrBzEnergyQ1 " +
      "unrBxEnergyQ2 unrByEnergyQ2 unrBzEnergyQ2 " +
      "unrBxEnergyQ3 unrByEnergyQ3 unrBzEnergyQ3 " +
      "unrBxEnergyQ4 unrByEnergyQ4 unrBzEnergyQ4 ")
    if (n>0):
      try:
        pass
        numpy.savetxt(cacheFile, ra, header = header)
      except:
        print("Error in ", os.getcwd())
        raise
  #}
  if (energy is not None): #{
    comps = {"x":(0,), "y":(1,), "z":(2,), "t":(0,1), "":(0,1,2)}
    parts = {"":(0,1,2,3), "h0":(0,1), "h1":(2,3),
               "q0":(0,), "q1":(1,), "q2":(2,), "q3":(3,)}
    allowed = ["unrB" + s1 + s2 for s1 in comps for s2 in parts]
    if energy not in allowed: #{
      msg = "energy specifier " + energy + " is invalid; please "
      msg += "use one of: " + repr(allowed)
      raise ValueError(msg)
    #}
    k = energy[4:]
    cStr = ""
    if (len(k) > 0):
      cStr = k[0]
      k = k[1:]
    ps = parts[k]
    cs = comps[cStr]
    res = 0. * Ub[:,0,0]
    for q in ps:
      for c in cs:
        res[:] += Ub[:,q,c]
    Ub = res
  #}
  return (Ub, steps)
#}

def getFieldLine(fileOrFieldName, step = None,
  lineDir = None, otherDirs = None, otherIndices = None,
  lineSlice = slice(None),
  ix=None, iy=None, iz=None,
  includeExtFields = True): #{
  """
  returns (field, time, step, coords, lineDir, otherDirs, otherIndices)
    where field is a slice of the field in filename
  Specify either:
    lineDir = 0, 1, or 2
    otherDirs = the directions that aren't lineDir
    otherIndices = the indices of otherDirs specifying the line
  or specify
    2 of 3: ix, iy, iz
  and, optional, lineSlice to specify part of the line
  """
  ndim = 3
  if lineDir is None: #{
    if (iz is None and (ix is None or iy is None)): #{
      ndim = 2
      iz = 0
      lineDir = 0 if ix is None else 1
      otherDirs = [1 - lineDir]
      otherIndices = [(ix, iy)[otherDirs[0]]]
    elif (ix is None): #}{
      lineDir = 0
      otherDirs = [1,2]
      otherIndices = [iy, iz]
    elif (iy is None): #}{
      lineDir = 1
      otherDirs = [2,0]
      otherIndices = [iz, ix]
    else: #}{
      lineDir = 2
      otherDirs = [0,1]
      otherIndices = [ix, iy]
    #}
  else: #}{
    ndim = len(otherDirs) + 1
  #}
  slices = [slice(None), slice(None), slice(None)]
  slices[lineDir] = lineSlice
  for i in range(0, ndim-1):
    slices[otherDirs[i]] = slice(otherIndices[i], otherIndices[i]+1)
  if step is None: #{
    try:
      it = int(fileOrFieldName.split("_")[-1].split(".")[0])
      fileName = fileOrFieldName.split(".")[0]
      fieldName = "_".join(fileOrFieldName.split("_")[:-1])
    except:
      msg = "Error: was expecting a filenames of the form name_something_34.h5"
      msg += " or name_something_34, but got " + repr(fileOrFieldName)
      print(msg)
      raise
  else: #}{
    it = step
    fieldName = fileOrFieldName
    fileName = fieldName + "_%i" % it
  #}
  parts = fileName.split("_")
  if lineDir < 2 and "Az" in parts: #{
    # special case for a line-out of Az
    bName = "B" + "yx"[lineDir]
    bFileParts = [(bName if s=="Az" else s) for s in parts]
    bFileName = "_".join(bFileParts)
    (bdata, time, step, coords) = getField(bFileName, slices=slices)
    if lineDir == 0:
      bdata = bdata[:,0]
      data = bdata.cumsum()
    else:
      bdata = bdata[0,:]
      bdata *= -1.
    if len(bdata.shape) > 1: bdata = bdata[:,0]
    # Get rid of end point, but add 0 at beginning for cumsum
    bdata[:-1] = bdata[1:]
    bdata[0] = 0.
    data = bdata.cumsum()
    ds = coords[lineDir]
    ds = numpy.array(list(ds) + [ds[-1]-ds[-2]])
    data *= numpy.diff(ds)
    data -= data.min()
  else: #}{
    if includeExtFields:
      (data, time, step, coords) = getExtField(fieldName, it, slices=slices)
    else:
      (data, time, step, coords) = getField(fileName, slices=slices)
    if ndim == 2:
      if lineDir == 0:
        data = data[:,0]
      else:
        data = data[0,:]
    else:
      if lineDir == 0:
        data = data[:,0,0]
      elif lineDir == 1:
        data = data[0,:,0]
      else:
        data = data[0,0,:]
  #}
  return (data, time, step, coords, lineDir, otherDirs, otherIndices)
#}


def getFieldSliceXY(fileName, iz, slices = None): #{
  """
  returns (field, time, step, coords)
  for 3D fields only
  """
  if slices is None:
    slices = [slice(None), slice(None), slice(iz,iz+1)]
  else:
    slices = [slices[0], slices[1], slice(iz,iz+1)]
  (data, time, step, coords) = getField(fileName, slices=slices)
  return (data[:,:,0], time, step, coords)
#}

def getFieldSliceXZ(fileName, iy): #{
  """
  returns (field, time, step, coords)
  for 3D fields only
  """
  slices = [slice(None), slice(iy,iy+1), slice(None)]
  (data, time, step, coords) = getField(fileName, slices=slices)
  return (data[:,0,:], time, step, coords)
#}

def getFieldSliceYZ(fileName, ix, slices = None): #{
  """
  returns (field, time, step, coords)
  for 3D fields only
  slices is [sliceForY, sliceForZ]
  """
  if slices is None:
    slices = [slice(ix,ix+1), slice(None), slice(None)]
  else:
    slices = [slice(ix, ix+1), slices[0], slices[1]]
  (data, time, step, coords) = getField(fileName, slices=slices)
  return (data[0,:,:], time, step, coords)
#}

def getFieldSlice(fileOrFieldName, step = None,
  axis0=0, axis1=1, i2=0, i2minusPlus = 0, slices = None, params=None,
  includeExtFields = True, layerFieldKws=None): #{
  """
  returns (field, time, step, coords)
  for 3D fields only
  slices is [sliceForAxis0, sliceForAxis1]
  i2 is the index of axis2 specifying the slice
  i2minusPlus is an int iDelta, or a pair of ints (iMinus, iPlus)
    - the "slice" will be averaged over i2-iMinus:i2+iPlus+1
      or i2-iDelta:i2+iDelta+1

  If step is None, assumes the first argument is the fileName;
  if step is given, assumes the first argument is the fieldName
  """
  if layerFieldKws is not None:
    if axis0==1 or axis1==1:
      msg = "Neither axis0 nor axis1 can be 1 if requesting a layerField"
      raise ValueError(msg)
    if (i2 != 0):
      msg = "i2 must be 0 if requesting a layerField"
      raise ValueError(msg)
    if (i2minusPlus != 0) and (i2minusPlus != (0,0)):
      msg = "i2minusPlus = " + str(i2minusPlus)
      msg += " must be 0 if requesting a layerField"
      raise ValueError(msg)
  if axis0 < 0 or axis0 > 2:
    msg = "axis0 = %i must be 0, 1, or 2" % axis0
    raise ValueError(msg)
  if axis1 < 0 or axis1 > 2:
    msg = "axis1 = %i must be 0, 1, or 2" % axis1
    raise ValueError(msg)
  if (axis0 == axis1):
    msg = "axis0 = %i cannot be the same as axis1" % axis0
    raise ValueError(msg)
  axis2 = (axis0+1)%3
  if axis2 == axis1: axis2 = (axis2+1)%3
  slicesA = [slice(None), slice(None), slice(None)]
  if (i2minusPlus is not None): #{
    if isinstance(i2minusPlus, int):
      i2m = i2minusPlus
      i2p = i2minusPlus
    else:
      (i2m, i2p) = i2minusPlus
  #}
  slicesA[axis2] = slice(i2-i2m,i2+i2p+1)

  if slices is not None: #{
    if slices[0] is not None:
      slicesA[axis0] = slices[0]
    if slices[1] is not None:
      slicesA[axis1] = slices[1]
  #}
  if step is None: #{
    try:
      it = int(fileOrFieldName.split("_")[-1].split(".")[0])
      fileName = fileOrFieldName.split(".")[0]
      fieldName = "_".join(fileOrFieldName.split("_")[:-1])
    except:
      msg = "Error: was expecting a filenames of the form name_something_34.h5"
      msg += " or name_something_34, but got " + repr(fileOrFieldName)
      print(msg)
      raise
  else: #}{
    it = step
    fieldName = fileOrFieldName
    fileName = fieldName + "_%i" % it
  #}
  if (layerFieldKws is not None):
    print("layer fields")
    (data, time, step, coords) = getFieldOnLayer(
      fieldName, it, slices=slicesA, params=params, **layerFieldKws)
  elif includeExtFields:
    (data, time, step, coords) = getExtField(
      fieldName, it, slices=slicesA, params=params)
  else:
    (data, time, step, coords) = getField(fileName, slices=slicesA)
  sdata = data.mean(axis=axis2)
#  if axis2 == 0:
#    sdata = data[0]
#  elif axis2 == 1:
#    sdata = data[:,0]
#  else:
#    sdata = data[:,:,0]
  if axis0 > axis1:
    sdata = sdata.transpose()
  #print axis0, axis1, axis2, sdata.shape
  return (sdata, time, step, coords)
#}


def getNumParticles(fileName): #{
  """
  """
  if isHdf5(fileName): #{
    shape = arrayShapeFromHdf5(fileName, "particles")
    res = shape[0]
  else: #}{
    raise NotImplementedError("getParticles is not yet implemented for text output")
  #}
  return res
#}

def getParticles(fileName, slices = None): #{
  """
  returns (data, time, step)
  where data is the particles data - x,y,z,ux,uy,uz,w
    (sliced appropriately)
  """
  if isHdf5(fileName): #{
    time = readArrayFromHdf5(fileName, "time")
    step = readArrayFromHdf5(fileName, "step")
    data = readArrayFromHdf5(fileName, "particles", slices = slices)
  else: #}{
    raise NotImplementedError("getParticles is not yet implemented for text output")
  #}
  return (data, time, step)
#}

def getParticlesInChunks(fileName, dataColSlice = slice(None)): #{
  """
  yield (data, time, steps) from getParticles, but for consecutive groups
  of particles
  """
  np = getNumParticles(fileName)
  ptclsPerChunk = int(3e6)
  for piStart in range(0, np, ptclsPerChunk): #{
    piEnd = min(np, piStart + ptclsPerChunk)
    slices = [slice(piStart, piEnd), dataColSlice]
    print("yielding chunk", slices)
    yield getParticles(fileName, slices=slices)
  #}
#}

def getPtclEnergySpectrum(it, spec, sym, normalize = True,
  half = None): #{
  """
  returns (dN/d|u|, uBinEdges, step)
  working dir should be data/

  unless dNdg = true, in which case, (dN/dgamma, gammaBinEdges, step)

  normalize = True returns dNdu such that int dNdu du = 1
    but if half = "lower" or "upper", then int dNdu du = 0.5
  normalize = "gamma" returns dNdu such that int dN/dgamma dgamma = 1.
  """
  name = "./spectrum_%s_%s_%s" % (spec, sym, it)
  (fileName, fileType) = zFileName(name, errorIfNotFound = True)
  if fileType == "h5": #{
    uBinEdges = readArrayFromHdf5(fileName, "axis0coords")
    dNdbg = readArrayFromHdf5(fileName, "field")
    step = readArrayFromHdf5(fileName, "step")
  else: #}{
    dNdbg = numpy.loadtxt(fileName)
    uBinEdges = numpy.loadtxt("./u.dat")
    step = it
  #}
  if (half is not None): #{
    (dNdbgLo, uBinEdgesLo, stepLo
      ) = getPtclAngularSpectrumIntegratedOverAngle(it, spec, sym)
    diff = dNdbg - dNdbgLo
    if (abs(diff) <= 1e-4 * dNdbg).all(): #{
      msg = "The spectrum and angular spectrum are for the same particles, so the spectrum "
      msg += "for the 2 halves cannot be separated.  Early versions of Zeltron"
      msg += " store all particles in both spectra and angular spectra; "
      msg += " then only lower-half particles were stored in both; "
      msg += " maybe later versions will store all in the spectra, "
      msg += " and the lower-half in the angular spectra."
      print(msg)
      #raise ValueError, msg
    #}
    if half == "lower":
      dNdbg = dNdbgLo
    else:
      dNdbg -= dNdbgLo
  #}
  dNdg = None
  gBinEdges = None
  normFactor = 1.
  if (normalize == 'gamma'): #{
  # normalize so integral f(gamma) d gamma = 1
    gBinEdges = numpy.sqrt(uBinEdges**2 + 1)
    uCtrs = 0.5*(uBinEdges[1:] + uBinEdges[:-1])
    gCtrs = 0.5*(gBinEdges[1:] + gBinEdges[:-1])
    dNdg = dNdbg * gCtrs/uCtrs
    dg = numpy.diff(gBinEdges)
    normFactor = (dNdg * dg).sum()
  elif normalize: #}{
    du = numpy.diff(uBinEdges)
    normFactor = (dNdbg * du).sum()
    if 0: #{
      import matplotlib
      import matplotlib.pyplot as plt
      plt.loglog(uBinEdges[:-1], dNdbg)
      plt.show()
      raise RuntimeError
    #}
  #}
  if (normFactor != 1.):
    if half is not None: normFactor *= 2.
    dNdbg /= normFactor
#  if (returndNdg):
#    if gBinEdges is None:
#      gBinEdges = numpy.sqrt(uBinEdges**2 + 1)
#    if dNdg is None:
#      uCtrs = 0.5*(uBinEdges[1:] + uBinEdges[:-1])
#      gCtrs = 0.5*(gBinEdges[1:] + gBinEdges[:-1])
#      dNdg = dNdbg * gCtrs/uCtrs
#
  return (dNdbg, uBinEdges, step)
#}

def getPtclMaxEnergySpectrum(spec, normalize = True): #{
  """
  returns (dN/d|u|_max, uBinEdges) from tracked particles
  working dir should be data/
  |u|_max is the maximum over all time
  u = beta*gamma

  spec = a tracked species

  unless dNdg = true, in which case, (dN/dgamma, gammaBinEdges, step)

  normalize = True returns dNdu such that int dNdu du = 1
  normalize = "gamma" returns dNdu such that int dN/dgamma dgamma = 1.
  """
  (stepOfMaxU, maxU) = getOrbitMaxU(spec) # **kwargs
  #(suBinEdges, suBinSelectProb) = getOrbitSubsetSelection(spec)
  w = getOrbitSubsetWeights(spec)
  logMaxU = numpy.log(maxU)
  # can't auto-estimate weighted data
  dN, lnBinEdges = numpy.histogram(logMaxU, bins = "auto")
  if (isinstance(w, numpy.ndarray) and len(w) > 1):
    dN, lnBinEdges = numpy.histogram(logMaxU, bins = lnBinEdges, weights=w)

  uBinEdges = numpy.exp(lnBinEdges)
  dNdbg = dN/numpy.diff(uBinEdges)


  dNdg = None
  gBinEdges = None
  normFactor = 1.
  if (normalize == 'gamma'): #{
  # normalize so integral f(gamma) d gamma = 1
    gBinEdges = numpy.sqrt(uBinEdges**2 + 1)
    uCtrs = 0.5*(uBinEdges[1:] + uBinEdges[:-1])
    gCtrs = 0.5*(gBinEdges[1:] + gBinEdges[:-1])
    dNdg = dNdbg * gCtrs/uCtrs
    dg = numpy.diff(gBinEdges)
    normFactor = (dNdg * dg).sum()
    dNdg /= normFactor
  elif normalize: #}{
    du = numpy.diff(uBinEdges)
    normFactor = (dNdbg * du).sum()
  #}
  if (normFactor != 1.):
    dNdbg /= normFactor
#  if (returndNdg):
#    if gBinEdges is None:
#      gBinEdges = numpy.sqrt(uBinEdges**2 + 1)
#    if dNdg is None:
#      uCtrs = 0.5*(uBinEdges[1:] + uBinEdges[:-1])
#      gCtrs = 0.5*(gBinEdges[1:] + gBinEdges[:-1])
#      dNdg = dNdbg * gCtrs/uCtrs
#
  return (dNdbg, uBinEdges)
#}


def getPtclAngularSpectrum(it, spec, sym): #{
  """
  returns (dN/d|u| dphi dlambda[u,phi,lambda],
           uBinEdges, phiBinEdges, lambdaBinEdges, step)
  working dir should be data/
  """
  name = "./angular_%s_%s_%s" % (spec, sym, it)
  (fileName, fileType) = zFileName(name, errorIfNotFound = True)
  if fileType == "h5": #{
    uBinEdges = readArrayFromHdf5(fileName, "axis0coords")
    phiBinEdges = readArrayFromHdf5(fileName, "axis1coords")
    lambdaBinEdges = readArrayFromHdf5(fileName, "axis2coords")
    dN = readArrayFromHdf5(fileName, "field")
    step = readArrayFromHdf5(fileName, "step")
  else: #}{
    step = it
    uBinEdges = numpy.loadtxt("./u.dat")
    phiBinEdges = numpy.loadtxt("./phi.dat")
    lambdaBinEdges = numpy.loadtxt("./lambda.dat")
    # following is a 2D array
    dN2d = numpy.loadtxt(fileName)
    dN = numpy.reshape(dN2d, (len(uBinEdges)-1, len(phiBinEdges)-1,
      len(lambbdaBinEdges)-1))
  #}
  return (dN, uBinEdges, phiBinEdges, lambdaBinEdges, step)
#}

def getPtclAngularSpectrumIntegratedOverAngle(it, spec, sym): #{
  """
  returns (dN/d|u|, uBinEdges, step)
  obtained from the angular spectrum
  working dir should be data/
  """
  (dNdAll, uBinEdges, phiBinEdges, lambdaBinEdges, step
    ) = getPtclAngularSpectrum(it, spec, sym)
  dSinPhi = numpy.diff(numpy.sin(numpy.pi/180. * phiBinEdges))
  dLrad = (numpy.pi/180.) * numpy.diff(lambdaBinEdges)

  dNdAll *= dSinPhi[None, :, None]
  dNdAll *= dLrad[None, None, :]

  dNdu = dNdAll.sum(axis=2).sum(axis=1) / (4.*numpy.pi)
  return (dNdu, uBinEdges, step)
#}

def getRadSpectrum(it, spec, sym, radType): #{
  """
  Returns the radiation spectrum for particles in the lower half of the
  simulation (zeltron calculated radiated spectra only for lower-half
  particles).
  returns (eps f(eps), epsBinEdges, epsBinCtrs, step)
  working dir should be data/
  where f(eps) d eps = the total radiated power (ergs/s) emitted
    in photons with energies in [eps, eps + d eps].
    I.e., f(eps) = eps * dN/(dt deps)
    where dN/(dt deps) is the number of photons (per time) emitted in
    energy bin [eps, eps + deps]
  and epsBinEdges and epsBinCntrs are in eV
  (N.B. epsBinCntrs are the logarithmic centers, not halfway between the
  nearest epsBinedges.)

  it = timestep
  spec = species ("electrons" or "ions" or 'both')
  sym = "bg" or "drift"
  radType = "IC" or "syn"
  """

  if radType not in ("syn", "IC"):
    raise ValueError("radType must be 'syn' or 'IC'")
  if spec == 'both': #{
    (f, binEdges, step) = getRadSpectrum(it, "electrons", sym, radType)
    (f2, binEdges, step) = getRadSpectrum(it, "ions", sym, radType)
    f += f2
    res = (f, binEdges, step)
  else: #}{

    fname = "synchrotron" if (radType[0] in "sS") else "invcompton"
    name = "./" + fname + "_iso_" + spec + "_" + sym + repr(it)
    (fileName, fileType) = zFileName(name, errorIfNotFound = True)
    if fileType == "h5": #{
      raise RuntimeError('not implemented for hdf5')
      #uBinEdges = readArrayFromHdf5(fileName, "axis0coords")
      #dNdbg = readArrayFromHdf5(fileName, "field")
      #step = readArrayFromHdf5(fileName, "step")
    else: #}{
      nuCtrs = numpy.loadtxt("./nu.dat")
      try:
        nuBinEdges = numpy.loadtxt("./nuEdges.dat")
      except:
      # zeltron doesn't bin values into nu-bins; rather, it calculates
      # a value at each nuCtr to represent the bin, without defining the bin.
      #nuCtrs are log spaced, so determine bin edges accordingly.
        nuBinEdges = nuCtrs[0] * (nuCtrs[-1]/nuCtrs[0])**(
          (numpy.arange(len(nuCtrs)+1)-0.5)/(len(nuCtrs)-1.)
          )
      epsBinCtrs = nuCtrs * (Consts.h / Consts.evtoerg) # eV
      epsBinEdges = nuBinEdges * (Consts.h / Consts.evtoerg)
      step = it
      nuFnu=numpy.loadtxt(fileName)
      # N.B. zeltron's nu f(nu) d nu is actually
      # eps f(eps) d eps /4pi = (h nu)^2 dN / (dt deps) /4pi where eps = h nu
      epsf = 4.*numpy.pi * nuFnu
    #}
    res = (epsf, epsBinEdges, epsBinCtrs, step)
  #}
  return res
#}

def getOrbitSpecies(species): #{
  """
  converts common species names to that used by the tracker
  """
  spDict = {
    "electrons":"Electrons",
    "ions":"Ions",
    "Electrons":"Electrons",
    "Ions":"Ions",
    "electronsSubset":"ElectronsSubset",
    "ElectronsSubset":"ElectronsSubset",
    "ionsSubset":"IonsSubset",
    "IonsSubset":"IonsSubset",
    }
  return spDict[species]
#}

def getOrbitFilename(species, cid, orbitDump, addPrefix=True): #{
  fname = "tracked" + getOrbitSpecies(species
    ) + "_cid" + repr(cid) + "_" + repr(orbitDump) + ".h5"
  if addPrefix:
    if os.path.exists("./orbits"):
      prefix = "./orbits/"
      fname = prefix + fname
  return fname
#}

def getOrbitDir(): #{
  """
  return the directory (with trailing "/") containing the tracked particle
  data.
  """
  prefix = "./"
  if os.path.exists("./orbits"):
    prefix = "./orbits/"
  return prefix
#}

def getOrbitInfoCacheDir(infoCacheDir = None): #{
  """
  Return a directory in which orbit data can be cached to speed future
  reading of tracked particle data.
  Usually this is just ./orbits (or if it doesn't exist, ./) directory,
  but sometimes we want to be able to analyze data in a directory owned
  by someone else, in which case infoCacheDir can be specified to be
  an owned directory.
  """
  if infoCacheDir is None:
    prefix = getOrbitDir()
  else:
    prefix = infoCacheDir + "/"
  return prefix
#}

def getOrbitInfo(infoCacheDir = None, maxNumCollectors = None,
  species = None): #{
  """
  return (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
    stepsPerOrbitDump)

    trackedSpecies = a list of strings of species that were tracked
    ntp = the number of tracked particles
    numData = the number of numbers recorded for each particle
      (as of June 2016, this is always 13)
    times = the times at which data was recorded for each tracked particle
    timeSteps = the timesteps at which data was recorded

  species = None -- is good for the original trajectories where ions
    and electrons have the same info;
    but for Subset orbits, should be, e.g., electronsSubset or ionsSubset

  The following should be used only by zfileUtil:
    ntpPerCollector = the number of particles in each group of particles;
      each group is saved in a file with a different cid (collector ID)
      (cids always run from 0 to numCollectors-1)
    stepsPerOrbitDump = the number of orbit steps per orbit-dump
      (a new orbit dump file is saved at every checkpoint)
  infoCacheDir is a directory in which this can save a file with
    information extracted from all the orbits files so that extraction
    can be avoided upon reanalysis.  Normally it puts it in the orbits
    directory, but in case one doesn't have write perms....
  maxNumCollectors can be set to limit the number of orbits files
    considered -- this is mainly for faster testing.
  """
  prefix = getOrbitDir()
  if species is not None:
    sp = getOrbitSpecies(species)
    tfiles = glob.glob(prefix + "tracked" + sp + "_cid*_*.h5")
  else:
    tfiles = glob.glob(prefix + "tracked*_cid*_*.h5")
    tfile = [f for f in tfiles if "Subset_cid" not in f]
  # determine the species, collectors (cid=collector ID), orbit-dumps
  speciesSet = set()
  collectors = set()
  odumps = set()
  for f in tfiles:
    fp = f.split("_")
    speciesSet.add(fp[0][len(prefix+"tracked"):])
    collectors.add(int(fp[1][3:]))
    odumps.add(int(fp[2][:-3]))
  trackedSpecies = sorted(list(speciesSet))
  collectors = sorted(list(collectors))
  odumps = sorted(list(odumps))
  numCollectors = len(collectors)
  if maxNumCollectors is not None: #{
    if (maxNumCollectors < len(collectors)):
      numCollectors = maxNumCollectors
      collectors = collectors[:numCollectors]
  #}
  numOdumps = len(odumps)
  # choose one species and get #ptcls
  if species is None:
    sp = trackedSpecies[0]
  dFirst = odumps[0]
  ntpPerCollector = []
  ntp = 0
  # Note: with many collectors, this loop can take a very long time
  # so save temporary data file so we do it only once
  arrayShapeFile = getOrbitInfoCacheDir(infoCacheDir = infoCacheDir)
  arrayShapeFile += "tracked" + sp + "FileInfo.dat"
  print("arrayShapeFile =", arrayShapeFile)
  if os.path.exists(arrayShapeFile):  #{
    raShapeInfo = numpy.int64(numpy.loadtxt(arrayShapeFile))
    if len(raShapeInfo.shape) == 1:
      raShapeInfo = numpy.reshape(raShapeInfo, (1, raShapeInfo.size))
    if raShapeInfo.shape[0] != len(collectors): #{
      msg = "The file '" + arrayShapeFile + "' doesn't have "
      msg += repr(len(collectors)) + " lines (=the number of collectors); "
      msg += "please delete the file and re-run the analysis, which "
      msg += "should regenerate the file correctly."
      raise ValueError(msg)
    #}
    for row in raShapeInfo:
      (nTimes, ntpi, numData) = row[1:4]
      ntpPerCollector.append(ntpi)
      ntp += ntpi
  else: #}{
    raShapeInfo = []
    for ci in range(len(collectors)):
      filename = prefix + getOrbitFilename(sp, ci, dFirst, addPrefix=False)
      (nTimes, ntpi, numData) = arrayShapeFromHdf5(filename, "particles")
      raShapeInfo.append((ci, nTimes, ntpi, numData))
      ntpPerCollector.append(ntpi)
      ntp += ntpi
    foundFile = False
    try: #{ # make sure file permissions
      with open(arrayShapeFile, 'w') as f: #{
        foundFile = True
      #}
    except IOError as exc:
      print(exc)
      print("Warning: zfileUtil.getOrbitInfo is unable to write to file " + arrayShapeFile)
      arrayShapeFile = os.path.expanduser('~')
      arrayShapeFile += "/tracked" + sp + "FileInfoi7281xz4.dat"
      print("  attempting to save file under name " + arrayShapeFile)
      with open(arrayShapeFile, 'w') as f: #{
        foundFile = True
      #}
      # if above generates exception, let it be raised.
      print("  ...attempt succeeded: please rename the file to exclude ")
      print("     7281xz4; then move it to the orbits directory, or ")
      print("     in the future call getOrbitInfo with infoCacheDir = ")
      print("     the directory where the file can be found.")
    #}
    with open(arrayShapeFile, 'w') as f: #{
      f.write("# collector nTimes nParticles nDataPerParticle\n")
      for row in raShapeInfo: #{
        f.write(" ".join([repr(x) for x in row]) + "\n")
      #}
    #}
  #}
  # choose one species/collector and get times
  stepsPerOrbitDump = []
  times = []
  timeSteps = []
  for di in range(dFirst, dFirst+numOdumps):
    filename = prefix + getOrbitFilename(sp, 0, di, addPrefix=False)
    times.append(readArrayFromHdf5(filename, "times"))
    timeSteps.append(readArrayFromHdf5(filename, "timeSteps"))
    stepsPerOrbitDump.append(len(times[-1]))
  times = numpy.hstack(times)
  timeSteps = numpy.hstack(timeSteps)
  return (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
    stepsPerOrbitDump)
#}

def getOrbitDataDesc(): #{
  columns = ["x", "y", "z", "ux", "uy", "uz", "weight",
    "Ell", "Bpp", "Fr", "Fe",
    "tag", "timestep"]
  colDict = dict(list(zip(columns, list(range(len(columns))))))
  return (columns, colDict)
#}

def fixOrbitSlices(ptclSlice = slice(None), stepSlice = slice(None),
  dataSlice = slice(None), orbitInfo = None, infoCacheDir = None,
  species = None): #{
  """
  convert slices (with default values) into triples with known values
  However, ptclSlice can just be a 1D array
  """
  import sliceUtil
  if orbitInfo is None:
    orbitInfo = getOrbitInfo(infoCacheDir = infoCacheDir, species=species)
  (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
    stepsPerOrbitDump) = orbitInfo
  if isinstance(ptclSlice, slice):
    ptclSpec = sliceUtil.sliceToTriple(ptclSlice, ntp)
  else: # should be a 1D list/array
    # create the smallest slice (with step 1) that holds all in ptclSlice
    if len(ptclSlice) == 0:
      ptclSpec = [0,0,1]
    else:
      ptclSpec = sliceUtil.sliceToTriple(
        slice(ptclSlice.min(), ptclSlice.max()+1), len(ptclSlice))
  stepSpec = sliceUtil.sliceToTriple(stepSlice, len(times))
  dataSpec = sliceUtil.sliceToTriple(dataSlice, numData)
  return (ptclSpec, stepSpec, dataSpec)
#}

def getOrbitsAboveThresholdU(species, ptclSlice = slice(None),
  thresholdU = 0., orbitInfo = None, infoCacheDir = None,
  nomGBperChunk = None): #{
  """
  ptclSlice may be a slice describing particles to get, or it
    may be a list of ptclIndices

  returns a list of ptclIndices that are in ptclSlice that at some
  point in their trajectories reach beta*gamma >= thresholdU.
  However, if all particles specified by ptclSlice are above threshold,
  this can just return ptclSlice (which may be either a slice or a list
  of ptclIndices).
  """
  if thresholdU > 0.: #{
    (stepOfMaxU, maxU) = getOrbitMaxU(species, orbitInfo = orbitInfo,
      infoCacheDir = infoCacheDir, nomGBperChunk = nomGBperChunk)
    keep = (maxU >= thresholdU)
    if ptclSlice == slice(None): #{
      if not keep.all():
        ptclSlice = numpy.arange(maxU.size)[keep]
    else: #}{
    # oddly, the following code works both
    #  if isinstance(ptclSlice, slice) or if ptclSlice is a list of indices
      keep2 = numpy.zeros((maxU.size,), dtype=numpy.bool)
      keep2[ptclSlice] = True
      numToKeep2 = keep2.sum()
      keep = numpy.logical_and(keep, keep2)
      numToKeep = keep.sum()
      if (numToKeep < numToKeep2):
        ptclSlice = numpy.arange(maxU.size)[keep]
    #}
  #}
  return ptclSlice
#}

def getOrbits(species, ptclSlice = slice(None), stepSlice = slice(None),
  dataSlice = slice(None), thresholdU = 0.,
  orbitInfo = None, infoCacheDir = None, returnSubsetWeights = False): #{
  """
  return (data, times, timeSteps)
      or (data, times, timeSteps, subsetWeights) [if returnSubsetWeights=True]
    data = an array of shape (nt, np, numData)
      where nt = number of timesteps,
            np = number of particles,
            numData = number of numbers recorded for each particle (at one step)
            weights = the inverse selection probability, in case of
              working with a subset of tracked particles.
    and times and timeSteps are the times/steps corresponding to the
      requested stepSlice (N.B., if FTRACK=1, then the timeSteps are
      essentially the same as stepSlice).
  species: "Electrons" or "Ions"
  ptclSlice: a slice describing which particles to get
    or a 1d list/tuple/array of particle indices
  stepSlice: a slice describing which tracked steps to get
    (if FTRACK=1, this is the same as which timesteps to get)
  dataSlice: a slice describing which particle data to get
  thresholdU: only return particles (specified by ptclSlice) that achieve
    beta*gamma >= thresholdU at some point in their trajectory
    (not just stepSlice, in case that is less than everything)
  orbitInfo: can be set to the results of getOrbitInfo() to avoid
    multiple calls to that function
  """
  import sliceUtil
  if orbitInfo is None:
    #print species, "Subset in species =", "Subset" in species
    orbitInfo = getOrbitInfo(infoCacheDir = infoCacheDir,
      species = species)
  (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
    stepsPerOrbitDump) = orbitInfo
  ptclSlice = getOrbitsAboveThresholdU(species, ptclSlice=ptclSlice,
    thresholdU = thresholdU, orbitInfo = orbitInfo,
    infoCacheDir = infoCacheDir)
  (ptclSpec, stepSpec, dataSpec) = fixOrbitSlices(ptclSlice=ptclSlice,
    stepSlice=stepSlice, dataSlice=dataSlice, orbitInfo=orbitInfo,
    species = species)
  nt = sliceUtil.tripleSize(stepSpec)
  nd = sliceUtil.tripleSize(dataSpec)
  if isinstance(ptclSlice, slice): #{
    ptclIndices = None
    np = sliceUtil.tripleSize(ptclSpec)
  else: #}{
    ptclIndices = ptclSlice
    np = len(ptclIndices)
  #}
  if (np > ntp):
    np = ntp

  # get data
  data = numpy.empty((nt, np, nd))
  weights = 1.
  doWeights = (returnSubsetWeights and "Subset" in species)
  if doWeights: #{
    weights = numpy.empty((np,))
  #}
  nti = 0
  dumpStart = 0
  for di, stepsInDump in enumerate(stepsPerOrbitDump): #{
    dumpStop = dumpStart + stepsInDump
    # figure out which timesteps we need from this orbitDump
    dumpTriple = sliceUtil.tripleOverlap(stepSpec, dumpStart, dumpStop)
    numSteps = sliceUtil.tripleSize(dumpTriple)

    npi = 0
    collectorStart = 0
    for ci, ptclsInCollector in enumerate(ntpPerCollector): #{
      filename = getOrbitFilename(species, ci, di)
      collectorStop = collectorStart + ptclsInCollector
      # figure out which ptcls we need from this collector
      if ptclIndices is None: #{
        collectorTriple = sliceUtil.tripleOverlap(
          ptclSpec, collectorStart, collectorStop)
        numPtcls = sliceUtil.tripleSize(collectorTriple)
      # and get requested data
        #print filename, "%i:%i, %i:%i" % (nti, nti+numSteps, npi, npi+numPtcls)
        #print "  -> ", dumpTriple, collectorTriple
        if numSteps*numPtcls > 0:
          data[nti:nti+numSteps, npi:npi+numPtcls, :] = readArrayFromHdf5(
            filename, "particles",
            slices = (slice(*dumpTriple), slice(*collectorTriple), dataSlice))
          if doWeights: #{
            globalIndices = numpy.arange(collectorStart, collectorStop)[
              slice(*collectorTriple)]
            w = getOrbitSubsetWeights(
              species, ptclSlice = globalIndices)
            weights[npi:npi+numPtcls] = w
          #}
          npi += numPtcls
      else: #}{
        (globalIndices, collectorTriple, localIndices
          ) = sliceUtil.tripleIndicesOverlap(ptclSpec, collectorStart,
          collectorStop, ptclIndices)
        numSlicedPtcls = sliceUtil.tripleSize(collectorTriple)
        numPtcls = len(localIndices)
        if numSteps*numPtcls > 0: #{
          if (numSlicedPtcls > 10 * numPtcls): #{
            listOfPtclSlices = sliceUtil.indicesToSlices(localIndices)
            wholeRaParts = []
            for s in listOfPtclSlices: #{
              part = readArrayFromHdf5(
                filename, "particles",
                slices = (slice(*dumpTriple), s, dataSlice))
              npPart = part.shape[1]
              data[nti:nti+numSteps, npi:npi+npPart, :] = part
              npi += npPart
            #}
          else: #}{
            part = readArrayFromHdf5(
              filename, "particles",
              slices = (slice(*dumpTriple), slice(*collectorTriple),
                dataSlice))
            data[nti:nti+numSteps, npi:npi+numPtcls, :] = part[
              :, localIndices, :]
            npi += numPtcls
          #}
          if doWeights: #{
            wres = getOrbitSubsetWeights(
              species, ptclSlice = globalIndices)
            weights[npi-numPtcls:npi] = wres
          #}
        #}
      #}

      collectorStart = collectorStop
    #}
    dumpStart = dumpStop
    nti += numSteps
  #}
  # get times
  times = times[stepSlice]
  timeSteps = timeSteps[stepSlice]
  if returnSubsetWeights:
    return (data, times, timeSteps, weights)
  else:
    return (data, times, timeSteps)
#}

def getOrbitsInChunks(species, ptclSlice = slice(None), stepSlice = slice(None),
  dataSlice = slice(None), thresholdU = 0., orbitInfo = None, nomGBperChunk = None,
  infoCacheDir = None, returnSubsetWeights = False): #{
  """
  Same as getOrbits, except that instead of returning all the orbits,
    it returns them in chunks (each chunk contains the complete time
    history for a subset of particles).
  With this function one can easily process large amounts of
    tracked-particle data without running out of memory.

  This function should be used by iteration, e.g.,

  for (data, times, timeSteps) in getOrbitsInChunks(species,...):
    # analyze data, etc.

  return (data, times, timeSteps)
      or (data, times, timeSteps, subsetWeights) [if returnSubsetWeights=True]
    data = an array of shape (nt, np, numData)
      where nt = number of timesteps,
            np = number of particles,
            numData = number of numbers recorded for each particle (at one step)
            weights = the inverse selection probability, in case of
              working with a subset of tracked particles.
    and times and timeSteps are the times/steps corresponding to the
      requested stepSlice (N.B., if FTRACK=1, then the timeSteps are
      essentially the same as stepSlice).
  species: "Electrons" or "Ions"
  ptclSlice: a slice describing which particles to get
    or a 1d list/tuple/array of particle indices
  stepSlice: a slice (or list of slices) describing which tracked steps to
    get.
    Note that the first step at which data is stored is step FTRACK;
    so stepSlice = slice(0,5,11) will return data from
    timeSteps (FTRACK, 5*FTRACK, 10*FTRACK).
  dataSlice: a slice describing which particle data to get
  thresholdU: only return particles (specified by ptclSlice) that achieve
    beta*gamma >= thresholdU at some point in their trajectory
    (not just stepSlice, in case that is less than everything)
  orbitInfo: can be set to the results of getOrbitInfo() to avoid
    multiple calls to that function

  nomGBperChunk: the approximate size, in GB, for each chunk;
    the actual chunk size can vary quite bit (by a factor of 2)
    from this.
  """
  import sliceUtil
  if orbitInfo is None:
    orbitInfo = getOrbitInfo(infoCacheDir = infoCacheDir,
    species = species)
  (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
    stepsPerOrbitDump) = orbitInfo

  ptclSlice = getOrbitsAboveThresholdU(species, ptclSlice=ptclSlice,
    thresholdU = thresholdU, orbitInfo = orbitInfo,
    infoCacheDir = infoCacheDir, nomGBperChunk = nomGBperChunk)

  if isinstance(stepSlice, list) or isinstance(stepSlice, tuple): #{
    multipleStepSlices = True
    nt = 0
    for ss in stepSlice: #{
      if isinstance(ss, int): #{
        ss = slice(ss, ss+1)
      #}
      (ptclSpec, stepSpec, dataSpec) = fixOrbitSlices(ptclSlice=ptclSlice,
        stepSlice=ss, dataSlice=dataSlice, orbitInfo=orbitInfo,
        species = species)
      nt += sliceUtil.tripleSize(stepSpec)
    #}
  else: #}{
    multipleStepSlices = False
    (ptclSpec, stepSpec, dataSpec) = fixOrbitSlices(ptclSlice=ptclSlice,
      stepSlice=stepSlice, dataSlice=dataSlice, orbitInfo=orbitInfo,
      species = species)
    nt = sliceUtil.tripleSize(stepSpec)
  #}

  np = sliceUtil.tripleSize(ptclSpec)
  nd = sliceUtil.tripleSize(dataSpec)
  if (nomGBperChunk is None): #{
    if isinstance(ptclSlice, slice):
      nomGBperChunk = 0.5
    elif len(ptclSlice) < 0.25*np:
      nomGBperChunk = 1.6
    elif len(ptclSlice) < 0.5*np:
      nomGBperChunk = 1.
    else:
      nomGBperChunk = 0.5
  #}

  #print "getOrbitsInChunks:", "ptclSpec=", ptclSpec, "ptclSlice=", ptclSlice
  #print "nt, np, nd =", nt, np, nd
  gbPerPtcl = nt*nd*8. / 2.**30
  if ("Subset" in species):
    gbPerPtcl /= 2
  maxPtclsPerChunk = max(1., nomGBperChunk/gbPerPtcl)
  #print "maxPperChunk =", maxPtclsPerChunk
  nomNumChunks = max(1, int(0.8 + np / maxPtclsPerChunk))
  #print "nomNumChunks =", nomNumChunks
  maxPtclsPerChunk = np / nomNumChunks
  #print "maxPperChunk =", maxPtclsPerChunk
  print("Getting <=", maxPtclsPerChunk, "ptcls/chunk in approx.", nomNumChunks, "chunks")

  pStart = ptclSpec[0]
  pStep = ptclSpec[2]
  numPtclsLeft = np
  while pStart < ptclSpec[1]: #{
    nToTake = numPtclsLeft
    if (numPtclsLeft > maxPtclsPerChunk): #{
      nToTake = maxPtclsPerChunk
    #}
    pStop = pStart + pStep * nToTake
    if isinstance(ptclSlice, slice): #{
      chunkPtclSlice = slice(pStart,pStop,pStep)
    else: #}{ # ptclSlice is a list of indices
      keep = numpy.logical_and(ptclSlice>=pStart, ptclSlice<pStop)
      chunkPtclSlice = ptclSlice[keep]
    #}
    #print "ptclSpec =", ptclSpec, "numLeft=", numPtclsLeft, "nToTake=", nToTake
    #print "pStart/pStop/pStep", pStart, pStop, pStep, pStep*nToTake
    if multipleStepSlices: #{
      data = []
      times = []
      timeSteps = []
      for ss in stepSlice: #{
        (data1, times1, timeSteps1) = getOrbits(species, ptclSlice=chunkPtclSlice,
          stepSlice=ss, dataSlice=dataSlice, orbitInfo=orbitInfo,
          returnSubsetWeights = returnSubsetWeights)
        data.append(data1)
        times.append(times1)
        timeSteps.append(times1)
      #}
      yield (numpy.vstack(data), numpy.hstack(times),
        numpy.hstack(timeSteps))
    else: #}{
      yield getOrbits(species, ptclSlice=chunkPtclSlice,
        stepSlice=stepSlice, dataSlice=dataSlice, orbitInfo=orbitInfo,
        returnSubsetWeights = returnSubsetWeights)
    #}
    numPtclsLeft -= nToTake
    pStart = pStop
  #}
#}

if 0: #{ // test the getOrbits function
  (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
    stepsPerOrbitDump) = getOrbitInfo()
  print(trackedSpecies, len(times), ntp)
  pSlice = slice(2,None,4003)
  sSlice = slice(3,None,1150)
  dSlice =  slice(10,None)
  for x in getOrbits("Electrons",
    ptclSlice = pSlice,
    stepSlice = sSlice,
    dataSlice = dSlice):
    print(x)
  chi = 0
  for y in getOrbitsInChunks("Electrons",
    ptclSlice = pSlice,
    stepSlice = sSlice,
    dataSlice = dSlice, nomGBperChunk=2.3e-7):
    #print "chunk", chi
    for x in y:
      print(x)
    chi += 1
  raise RuntimeError("halting after test")
#}

def getOrbitSubsetSelection(species): #{
  """
  return (uBinEdges, uBinSelectProb) for tracked..._cid*_*.h5
    where ... = species
    and uBinEdges is a list of bin edges and
    uBinSelectProb is the probability with which the original tracked
    particles were selected
  """
  if "Subset" in species: #{
    fname = getOrbitFilename(species, 0, 0)
    uBinEdges = readArrayFromHdf5(fname, "uBinEdges")
    uBinSelectProb = readArrayFromHdf5(fname, "uBinSelectProb")
  else: #}{
    uBinEdges = numpy.array([0.,1e37])
    uBinSelectProb = numpy.array([1.])
  #}
  return (uBinEdges, uBinSelectProb)
#}

def getOrbitSubsetWeights(species, ptclSlice = slice(None)): #{
  """
  return an array of weights for each tracked particle
  (or just a number of all weigths are the same)

  ptclSlice may be a slice or a list of indices
  """
  w = 1.
  if "Subset" in species:
    (stepOfMaxU, maxU) = getOrbitMaxU(species) # **kwargs
    (suBinEdges, suBinSelectProb) = getOrbitSubsetSelection(species)
    w = suBinSelectProb[0]
    if (suBinSelectProb != w).any(): #{
      maxU = maxU[ptclSlice]
      suBins = numpy.digitize(maxU, suBinEdges)
      suBins[suBins==0] = 1
      suBins -= 1
      w = 1./suBinSelectProb[suBins]
    #}
  return w
#}

def getOrbitMaxU(species, orbitInfo = None, infoCacheDir = None,
  nomGBperChunk = None, overwrite = False): #{
  """
  Return (stepOfMaxU, maxU)
    2 arrays with as many elements as tracked particles,
    with stepOfMaxU being the timestep at which each particle reached its
    max value of beta*gamma, and maxU being that max value.
  This information will be retrieved from a cached file if present,
    or if not present, will be calculated from the tracked particle data
    and written to a cache file:
    [infoCacheDir]/tracked[species]MaxU.dat
  """
  infoCacheDir = getOrbitInfoCacheDir(infoCacheDir = infoCacheDir)
  cacheFile = infoCacheDir + "tracked" + {
    'e':'E', 'p':'P', 'i':'I'}[species[0]] + species[1:] + "MaxU.dat"
  if (os.path.exists(cacheFile) and not overwrite): #{
    ra = numpy.loadtxt(cacheFile)
    if (ra.shape == (0,)):
      msg = "the file " + cacheFile + " appears to be corrupted;"
      msg += " please delete it so it can be regenerated."
      raise ValueError(msg)
    step = numpy.int32(ra[:,0])
    uMax = ra[:,1]
  else: #}{ # need to create file first
    print("Creating/calculating", cacheFile)
    # Make sure we can create the file
    # (so an error will be emitted now rather than after calculating everything)
    bakCacheFile = cacheFile
    while (os.path.exists(bakCacheFile)):
      bakCacheFile += ".bak"
    if os.path.exists(cacheFile):
      shutil.move(cacheFile, bakCacheFile)
    with open(cacheFile, 'w') as f: #{
      f.write("# stepOfMaxBetaGamma maxBetaGamma (for each particle)\n")
    #}

    if orbitInfo is None:
      orbitInfo = getOrbitInfo(infoCacheDir = infoCacheDir,
      species = species)
    (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
      stepsPerOrbitDump) = orbitInfo

    step = numpy.empty((ntp,), dtype=numpy.int32)
    uMax = numpy.empty((ntp,))

    uxCol = 3

    npi = 0
    dataSlice = slice(uxCol,uxCol+4)
    for (data, times, timeSteps) in getOrbitsInChunks(
      species, dataSlice = dataSlice,
      #ptclSlice = slice(5000), # for debugging only
      orbitInfo = orbitInfo, infoCacheDir = infoCacheDir,
      nomGBperChunk = nomGBperChunk): #{
      # when changing the number of columns, also have to change
      #  addition to data.shape[1] below, calculating uMax
      numPtcls = data.shape[1]
      ux = data[:,:,0]
      uy = data[:,:,1]
      uz = data[:,:,2]
      #w = data[0,:,3]
      uSqr = ux*ux + uy*uy + uz*uz
      stepOfMaxU = numpy.argmax(uSqr, axis=0)
      #uMax = numpy.array((data.shape[1], 3))
      step[npi:npi+numPtcls] = stepOfMaxU
      uMax[npi:npi+numPtcls] = uSqr[stepOfMaxU, numpy.arange(numPtcls)]
      npi += numPtcls
    #}
    uMax = numpy.sqrt(uMax)
    # header is prepended with "# "
    numpy.savetxt(cacheFile, numpy.hstack((step[:,None],uMax[:,None])),
      fmt=("%i","%.18g"),
      header = "stepOfMaxBetaGamma maxBetaGamma (for each particle)")
  #}
  return (step, uMax)
#}

if 0: #{ // test the getOrbitsMaxU function
  step, uMax = getOrbitMaxU("electrons", orbitInfo = None, infoCacheDir = None,
    overwrite = False)
  print(step[:10])
  print(uMax[:10])
  raise RuntimeError
#}

def saveHighEnergyOrbits(species, orbitInfo = None, infoCacheDir = None,
  nomGBperChunk = None, overwrite = False, nomGBperFile = 0.5,
  perc = 0.01): #{
  """
  Extracts samples of particles from the orbits files, keeping
  all particles with energy in the percentil "perc" (e.g., if perc=0.01,
  all particles in the top 1% are kept), and random selection uniform per
  logarithmic u (beta gamma) bin below that and with approximately the
  same number of particles as in the top 1%.

  perc = the target percentage for high-energy particles; e.g., if
    perc=0.01, then this will try to save all particles in the top 1%
    (in terms of maximum energy) -- but then it will also try to save
    a similar number of lower-energy particles representing (statistically)
    the rest of the tracked particles.
  """
  #perc = 0.01
  tspecies = {'e':'E', 'p':'P', 'i':'I'}[species[0]] + species[1:]
  savedir = ""
  if os.path.exists("./orbits/"):
    savedir = "./orbits/"
  newFileNameBase = savedir
  newFileNameBase += "tracked"+tspecies+"Subset_cid"
  infoCacheDir = getOrbitInfoCacheDir(infoCacheDir = infoCacheDir)
  cacheFile = infoCacheDir + "tracked" + tspecies + "MaxU.dat"
  if 1: #{ # need to create file first
    print("Creating/calculating", cacheFile)
    print(" and ", tspecies)
    if orbitInfo is None:
      orbitInfo = getOrbitInfo(infoCacheDir = infoCacheDir,
        species = species)
    (trackedSpecies, ntp, numData, times, timeSteps, ntpPerCollector,
      stepsPerOrbitDump) = orbitInfo

    # We want to do this in one pass so get an estimate of the distribution

    if (os.path.exists(cacheFile)): #{
      print("Using MaxU from file", cacheFile)
      ra = numpy.loadtxt(cacheFile)
      if (ra.shape == (0,)):
        msg = "the file " + cacheFile + " appears to be corrupted;"
        msg += " please delete it so it can be regenerated."
        raise ValueError(msg)
      step = numpy.int32(ra[:,0])
      uMax = ra[:,1]
      (dN, loguBinEdges) = numpy.histogram(numpy.log(uMax), bins = 'auto')
      uBinEdges = numpy.exp(loguBinEdges)
      dN = numpy.array(dN, dtype=numpy.float64)
    else: #}{
      print("Estimating MaxU from spectra")
      # estimate dN from spectra
      sfileglob = "spectrum_%s_bg_*.h5" % species
      sfiles = glob.glob(sfileglob)
      if len(sfiles) == 0: #{
        msg = "No files were found matching " + sfileglob
        msg += ".  Either change to the data directory, or "
        msg += "create the ...MaxU.dat file, which will take longer "
        msg += "but work better."
        raise ValueError(msg)
      #}
      ds = sorted([int(s.split("_")[-1][:-3]) for s in sfiles])
      dN = None
      uOnePercentMax = -1.
      uOnePercentMaxInd = -1
      for d in ds: #{
        (dNdu, uBinEdges, step) = getPtclEnergySpectrum(
          d, species, "bg", normalize = False)
        dNp = dNdu * numpy.diff(uBinEdges)
        dNp /= dNp.sum()
        uOnePercentInd = numpy.where(dNp.cumsum() < 1.-perc)[0][-1]
        uOnePercent = uBinEdges[uOnePercentInd]
        #print "d=%i, u1%%=%g" % (d, uOnePercent)
        if dN is None: #{
          dN = dNp
        #}
        if uOnePercent > uOnePercentMax: #{
          uOnePercentMax = uOnePercent
          uOnePercentMaxInd = uOnePercentInd
          dN = dNp
        #}
      #}
    #}

    # Make sure we can create the file
    # (so an error will be emitted now rather than after calculating everything)
    bakCacheFile = cacheFile
    while (os.path.exists(bakCacheFile)):
      bakCacheFile += ".bak"
    if os.path.exists(cacheFile):
      shutil.move(cacheFile, bakCacheFile)
    with open(cacheFile, 'w') as f: #{
      f.write("# stepOfMaxBetaGamma maxBetaGamma (for each particle)\n")
    #}

    # Estimate the energy above which 1% of tracked particles.
    N = dN.sum()
    dN /= N
    uOnePercentInd = numpy.where(dN.cumsum() < 1.-perc)[0][-1]
    uOnePercent = uBinEdges[uOnePercentInd]
    # probability of keeping a particle in a given bin
    # (add one for particles with energy above the highest been)
    keepProb = numpy.empty((dN.size+1,))
    # keep all 1% particles
    keepProb[uOnePercentInd:] = 1.
    # and the number of all other kept particles should be similar to
    # the top 1%
    Nmost = dN[:uOnePercentInd].sum() # presumbably Nmost ~ 0.99
    nonEmptyBins = (dN[:uOnePercentInd] > 0).sum()
    # try to get about the same number per bin
    numSavePerBin = perc*ntp/nonEmptyBins
    numExpectedPerBin = ntp*dN
    keepProb[:uOnePercentInd] = numSavePerBin / (
      numExpectedPerBin[:uOnePercentInd] + 0.1)
    keepProb[:-1][dN==0] = 1.
    keepProb[keepProb > 1.] = 1.

    if 0: #{
      print("u1% =", uOnePercent)
      print("us=", uBinEdges[::5])
      print("dN=", dN[::5])
      print("keepProb=", keepProb[::5])
    #}


    # Aim for 2 GB files max (4 B/number for single precision float32)
    numPtclsPerFile = int(nomGBperFile * 2.**30 / (numData*len(times) * 4.))
    print("Saving all particles with u > %g" % uOnePercent)
    print("Max ptcls per file = %g" % numPtclsPerFile)
    # file
    cid = numpy.array([0])
    ptclsInTmpData = numpy.array([0])
    tmpData = numpy.empty((len(times), numPtclsPerFile, numData),
      dtype = numpy.float32)
    tmpIndices = numpy.empty((numPtclsPerFile,), dtype=numpy.int32)
    def addPtcls(newData, indices, times=None, timeSteps=None,
      selectProb = None,
      uBinEdges = None,
      forceSave = False): #{
      #global cid
      #global ptclsInTmpData
      #global tmpData
      #global tmpIndices
      numToAdd = 0 if len(newData)==0 else newData.shape[1]
      pitd = ptclsInTmpData[0]
      while numToAdd > 0 or forceSave: #{
        if numToAdd > 0: #{
          numToAdd = min(numToAdd, numPtclsPerFile - pitd)
          tmpData[:,pitd:pitd+numToAdd,:
            ] = newData[:,:numToAdd,:]
          tmpIndices[pitd:pitd+numToAdd
            ] = indices[:numToAdd]
          pitd += numToAdd
          newData = newData[:,numToAdd:,:]
        #}
        #print "pitd=", pitd, "numPtclsPerFile=", numPtclsPerFile, "force=", forceSave
        if (pitd == numPtclsPerFile or forceSave): #{
          # save file and start a new one
          h5FileName = newFileNameBase + repr(cid[0]) + "_0.h5"
          if (os.path.exists(h5FileName)): #{
            raise ValueError("file " + h5FileName + " already exists.")
          #}
          # Sort particles in energy-order, because that might help
          # in reading the data later.
          ux = tmpData[:,:pitd,3]
          uy = tmpData[:,:pitd,4]
          uz = tmpData[:,:pitd,5]
          uSqr = ux*ux + uy*uy + uz*uz
          stepOfMaxU = numpy.argmax(uSqr, axis=0)
          uSqrMax = numpy.sqrt(uSqr[stepOfMaxU, numpy.arange(pitd)])
          order = numpy.argsort(uSqrMax)
          writeArrayToHdf5(tmpData[:,order,:],
            h5FileName, 'w', "particles")
          writeArrayToHdf5(tmpIndices[order],
            h5FileName, 'a', "particleIndices")
          if times is not None:
            writeArrayToHdf5(times, h5FileName, 'a', "times")
          if timeSteps is not None:
            writeArrayToHdf5(timeSteps, h5FileName, 'a', "timeSteps")
          if selectProb is not None:
            writeArrayToHdf5(selectProb, h5FileName, 'a', "uBinSelectProb")
          if uBinEdges is not None:
            writeArrayToHdf5(uBinEdges, h5FileName, 'a', "uBinEdges")
          pitd = 0
          cid[0] += 1
          if forceSave and numToAdd==0:
            forceSave = False
        #}
        numToAdd = 0 if len(newData)==0 else newData.shape[1]
      #}
      ptclsInTmpData[0] = pitd
    #}

    stepWithMaxU = numpy.empty((ntp,), dtype=numpy.int32)
    uMax = numpy.empty((ntp,))
    savePtclInd = []
    weights = []
    numInUBinOrig = numpy.zeros((len(uBinEdges)-1,))
    numInUBinSelected = numInUBinOrig.copy()

    uxCol = 3

    npi = 0
    #dataSlice = slice(uxCol,uxCol+4)
    for (data, times, timeSteps) in getOrbitsInChunks(
      species, #ptclSlice = slice(5000), # for debugging only
      orbitInfo = orbitInfo, infoCacheDir = infoCacheDir,
      nomGBperChunk = nomGBperChunk): #{
      # when changing the number of columns, also have to change
      #  addition to data.shape[1] below, calculating uMax
      numPtcls = data.shape[1]
      ux = data[:,:,3]
      uy = data[:,:,4]
      uz = data[:,:,5]
      #w = data[0,:,6]
      uSqr = ux*ux + uy*uy + uz*uz
      stepOfMaxU = numpy.argmax(uSqr, axis=0)
      #uMax = numpy.array((data.shape[1], 3))
      #print stepOfMaxU.shape, uSqr.shape, uMax.shape, npi, npi+numPtcls, numPtcls
      stepWithMaxU[npi:npi+numPtcls] = stepOfMaxU
      uMax[npi:npi+numPtcls] = numpy.sqrt(uSqr[stepOfMaxU, numpy.arange(numPtcls)])
      numInUBinOrig += numpy.histogram(uMax[npi:npi+numPtcls],
        bins=uBinEdges)[0]

      binOfMaxU = numpy.digitize(uMax[npi:npi+numPtcls], uBinEdges)
      binOfMaxU[binOfMaxU==0] = 1
      binOfMaxU -= 1
      j = numpy.where(binOfMaxU==binOfMaxU.max())[0][0]
      #print keepProb[binOfMaxU][:10]
      r = numpy.random.random((numPtcls,))
      keep = numpy.logical_or(uMax[npi:npi+numPtcls] >= uOnePercent,
               r <= keepProb[binOfMaxU])
      #print "u>u1%: ", numpy.arange(npi,npi+numPtcls)[uMax[npi:npi+numPtcls] >= uOnePercent]
      #print "r<p  : ", numpy.arange(npi,npi+numPtcls)[r < keepProb[binOfMaxU] ]
      weights += list(1./keepProb[binOfMaxU])
      savePtclInds = list(numpy.arange(npi, npi+numPtcls)[keep])
      #print "save Ptcls: ", savePtclInds
      #print "save umax: ", uMax[npi:npi+numPtcls][keep]
      numInUBinSelected += numpy.histogram(uMax[npi:npi+numPtcls][keep],
        bins=uBinEdges)[0]
      addPtcls(data[:,keep,:], savePtclInds, times=times, timeSteps=timeSteps,
        selectProb = keepProb, uBinEdges = uBinEdges)
      pctDone = int((npi+numPtcls)*100/ntp)
      if 1 and (int(npi*100/ntp) < pctDone): #{
        # print progress
        print("%i%%" % pctDone, end=' ')
      #}
      npi += numPtcls
    #}
    addPtcls(numpy.array([]), [], times = times, timeSteps = timeSteps,
      selectProb = #numpy.float64(numInUBinSelected)/numInUBinOrig
      keepProb
      ,
      uBinEdges = uBinEdges,
      forceSave = True)

    if 1: #{
      import matplotlib
      detectDisplay()
      import matplotlib.pyplot as plt
      setDefaultPlot()
      uBinCtrs = 0.5*(uBinEdges[1:] + uBinEdges[:-1])
      red = (numInUBinOrig > 0)
      plt.loglog(uBinCtrs[red], numInUBinOrig[red], 'o',
        mfc='none', mec='b', ms=8, mew=2, label='orig')
      plt.loglog(uBinCtrs[red], numInUBinSelected[red],
        '+', mfc='none', mec='r', ms=7, mew = 2, label='selected')
      leg = plt.legend(loc='best', labelspacing=0.01)
      leg.set_zorder(0)
      plt.subplots_adjust(bottom=0.14)
      plt.xlabel("$u$")
      plt.ylabel("number of particles in bin")
      plt.savefig(savedir + "tracked" + tspecies + "SubsetSelection.png")
      #plt.show()
    #}
    # header is prepended with "# "
    numpy.savetxt(cacheFile,
      numpy.hstack((stepWithMaxU[:,None],uMax[:,None])),
      fmt=("%i","%.18g"),
      header = "stepOfMaxBetaGamma maxBetaGamma (for each particle)")
  #}
#}

if 0: #{
  saveHighEnergyOrbits("electrons")
  raise RuntimeError
#}
def get_heatEnergyDensity(step): #}{
  eName = "fluidEnergyDensity"
  fluidE = getExtField(eName, step)[0]
  fluidE = numpy.nanmean(fluidE)

  # Do <p>^2 instead of <p^2>
  for c in "xyz": #{
    field_name = "Up%s_%s" % (c, "total")
    momc = getExtField(field_name, step)[0]
    momc = numpy.nanmean(momc)
    if (c=="x"):
      Up2rms = momc*momc
    else:
      Up2rms += momc*momc

  heatEnergyDensity = numpy.sqrt(fluidE**2 - Up2rms * Consts.c**2)
  return heatEnergyDensity

def get_list_of_simulation_names(directory):
  initial_sim_names = os.listdir(directory)
  sim_names = []

  for sim_dir in initial_sim_names:
    if 'data' in os.listdir(directory + sim_dir) or 'fields' in os.listdir(directory + sim_dir):
      print("Adding " + sim_dir + " to reducible simulations.")
      sim_names.append(sim_dir)

  return sim_names


def get_list_of_sim_names_with_phrases(directory, phrases, quiet=False):
  initial_sim_names = os.listdir(directory)
  sim_names = []

  for sim_dir in initial_sim_names:
    if all(phrase in sim_dir for phrase in phrases) and "tar.gz" not in sim_dir:
      if 'data' in os.listdir(directory + sim_dir) or 'fields' in os.listdir(directory + sim_dir):
        if len(os.listdir(directory + sim_dir + '/data/')) != 0:
          if not quiet:
            print("Adding " + sim_dir + " to reducible simulations.")
          sim_names.append(sim_dir)

  if len(sim_names) == 0:
    for sim_dir in initial_sim_names:
      if all(phrase in sim_dir for phrase in phrases):
        if 'phys_params.dat' in os.listdir(directory + sim_dir) and 'input_params.dat' in os.listdir(directory + sim_dir):
          if not quiet:
            print("Adding " + sim_dir + " to reducible simulations.")
          sim_names.append(sim_dir)


  return sim_names

if __name__ == "__main__": #{
  pass
#}

def remove_ghost_zones(data):
  params, units = getSimParams()
  if data.shape[0] > 2:
    if numpy.allclose(data[0, :, :], data[-1, :, :]) or data.shape[0] == params["NX"]:
      data = data[1:, :, :]
      # print("Removed x ghost zones")
  if data.shape[1] > 2:
    if numpy.allclose(data[:, 0, :], data[:, -1, :]) or data.shape[1] == params["NY"]:
      data = data[:, 1:, :]
      # print("Removed y ghost zones")
  if data.shape[2] > 2:
    if numpy.allclose(data[:, :, 0], data[:, :, -1]) or data.shape[2] == params["NZ"]:
      data = data[:, :, 1:]
      # print("Removed z ghost zones")
  return data

def guess_path_handler():
  """
  A major kludge. Not reliable.
  """
  import reduce_data_utils as redu
  from path_handler_class import path_handler
  raw_data_path = os.getcwd() + "/"
  sim_name = guess_sim_name()
  kwargs = {}

  if "06165/ahankla" in raw_data_path:
    # assume on stampede2. This is all major kludge
    config = "stampede2"
  else:
    reduced_data_path = raw_data_path + "../../data_reduced/" + sim_name + "/"
    figures_path = raw_data_path + "../../figures/" + sim_name + "/"
    kwargs["path_to_reduced_data"] = reduced_data_path
    kwargs["path_to_figures"] = figures_path
    kwargs["path_to_raw_data"] = raw_data_path
    config = "lia_hp"

  ph = path_handler(sim_name, config, **kwargs)
  return ph

def guess_sim_name():
  """
  A major kludge. Not reliable.
  """
  split_path = os.getcwd().split("/")
  if split_path[-1] == "data":
    sim_name = split_path[-2]
  else:
    sim_name = split_path[-1]
  return sim_name
