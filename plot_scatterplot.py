import numpy as np
import sys
sys.path.append("./modules/")
import os
from raw_data_utils import *
from reduce_data_utils import *
from path_handler_class import *
from scipy import interpolate
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl

def plot_scatterplot(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    times_to_plot = kwargs.get("times_to_plot", retrieve_time_values(ph)[:, -10:])
    scatter_vars = kwargs.get("scatter_variables", ["ExB_z", "Upz_total"])
    xy_average = kwargs.get("xy_average", False)
    vol_average = kwargs.get("vol_average", True)
    time_average = kwargs.get("time_average", False)
    fit_scatterplot = kwargs.get("fit_scatterplot", False)
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        times_string = "t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True
    else:
        times_string = "tAll"

    if vol_average:
        xy_average = False

    x_values = None
    y_values = None
    x_norm = rdu.getNormValue(scatter_vars[0], simDir=ph.path_to_reduced_data)
    y_norm = rdu.getNormValue(scatter_vars[1], simDir=ph.path_to_reduced_data)
    x_abbrev = rdu.getFieldAbbrev(scatter_vars[0], True)
    y_abbrev = rdu.getFieldAbbrev(scatter_vars[1], True)

    # Figure set-up
    if "figure_name" in kwargs and kwargs["figure_name"] == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel(x_abbrev + x_norm[1])
    plt.ylabel(y_abbrev + y_norm[1])
    title_str = ph.sim_name + "\n" + times_string + "\n t avg: " + str(time_average) + ", xy avg: " + str(xy_average) + ", vol avg: " + str(vol_average)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')

    if not vol_average:
        os.chdir(ph.path_to_raw_data)
        for time in times_to_plot:
            (quantity_x, t, n, coords) = rdu.getExtField(scatter_vars[0], int(time))
            quantity_y = rdu.getExtField(scatter_vars[1], int(time))[0]

            quantity_x = quantity_x/x_norm[0]
            quantity_y = quantity_y/y_norm[0]

            if quantity_x.shape != quantity_y.shape:
                quantity_x = rdu.remove_ghost_zones(quantity_x)
                quantity_y = rdu.remove_ghost_zones(quantity_y)

            if xy_average:
                quantity_x = np.mean(quantity_x, axis=(0, 1))
                quantity_y = np.mean(quantity_y, axis=(0, 1))

            if not time_average:
                plt.scatter(quantity_x, quantity_y, color='blue')
            else:
                if x_values is None:
                    x_values = quantity_x
                    y_values = quantity_y
                else:
                    x_values += quantity_x
                    y_values += quantity_y
        if time_average:
            x_values = x_values/np.size(times_to_plot)
            y_values = y_values/np.size(times_to_plot)
            plt.scatter(x_values, y_values)
    else:
        (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs, fields_to_vavg=scatter_vars)
        times_in_LC = retrieve_time_values(ph)[:, 2]
        quantity_x = vol_means[scatter_vars[0]]/x_norm[0]
        quantity_y = vol_means[scatter_vars[1]]/y_norm[0]
        if t_limited:
            tinitial_ind = (np.abs(tinitial - times_in_LC)).argmin()
            tfinal_ind = (np.abs(tfinal - times_in_LC)).argmin()
            times_in_LC = times_in_LC[tinitial_ind:tfinal_ind]
            quantity_x = quantity_x[tinitial_ind:tfinal_ind]
            quantity_y = quantity_y[tinitial_ind:tfinal_ind]
        print(quantity_x.shape)
        colors = plt.cm.get_cmap('cividis')
        sc = plt.scatter(quantity_x, quantity_y, c=times_in_LC, cmap=colors)
        cbar = plt.colorbar(sc)
        cbar.set_label("$tc/L$")
        if fit_scatterplot:
            linear_model = np.polyfit(quantity_x, quantity_y, 1)
            linear_model_func = np.poly1d(linear_model)
            fine_quantity_x = np.linspace(np.min(quantity_x), np.max(quantity_x), 100, endpoint=True)
            plt.plot(fine_quantity_x, linear_model_func(fine_quantity_x), 'k--')
            title_str += "\n Slope: {:.2f}. Intercept: {:.2f}".format(*linear_model_func)
            print(linear_model_func)

    plt.title(title_str)
    plt.legend(frameon=False)
    plt.tight_layout()

    fig_dir = ph.path_to_figures + "scatterplots/" + scatter_vars[0] + "-" + scatter_vars[1] + "/"
    if not os.path.isdir(fig_dir):
        os.makedirs(fig_dir)
    fig_name = fig_dir + times_string
    if xy_average:
        fig_name += '_xyAvg'
    if vol_average:
        fig_name += '_volAvg'
    if time_average:
        fig_name += '_tAvg'
    if fit_scatterplot:
        fig_name += '_fit'
    fig_name += ".png"
    figure_name = kwargs.get("figure_name", fig_name)

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        plt.gca().grid(False, which='both')
        plt.savefig(figure_name.replace(".png", "_nogrid.png"))
    plt.close()


if __name__ == '__main__':
    stampede2 = False
    system_config = "lia_hp"
    overwrite_data = False
    scatter_variables = ["ExB_z", "Upz_total"]
    time_average = False
    xy_average = False
    vol_average = True
    fit_scatterplot = True

    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    # sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    # sim_name = rdu.get_list_of_sim_names_with_phrases("/mnt/d/imbalanced_turbulence/data_reduced/", ["768cube", "data", "64ppc"])[0]
    # sim_name = rdu.get_list_of_sim_names_with_phrases("/mnt/d/imbalanced_turbulence/data_reduced/", ["768cube", "dcorr029", "A075"])[-1]
    sim_name = rdu.get_list_of_sim_names_with_phrases("/mnt/d/imbalanced_turbulence/data_reduced/", ["384cube", "dcorr029", "8xyz", "im10", "A075", "seedstudy3"])[-1]

    if stampede2:
        system_config = "stampede2"
    ph = path_handler(sim_name, system_config)
    if vol_average:
        xy_average = False

    times_to_plot = retrieve_time_values(ph)[:, 0]
    tinitial = 0.0
    tfinal = 20.0
    # times_to_plot = times_to_plot[-20:]
    # times_to_plot = [5400]

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["times_to_plot"] = times_to_plot
    kwargs["scatter_variables"] = scatter_variables
    kwargs["time_average"] = time_average
    kwargs["xy_average"] = xy_average
    kwargs["vol_average"] = vol_average
    kwargs["tinitial"] = tinitial
    kwargs["tfinal"] = tfinal
    kwargs["fit_scatterplot"] = fit_scatterplot

    plot_scatterplot(ph, **kwargs)
