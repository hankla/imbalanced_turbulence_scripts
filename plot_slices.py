"""
This script will plot slices of every given quantity over all given times.
Defaults are just to plot all non restframe quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - from create_slices-2d_cross-helicity: need to implement subtracting vel/B means
"""
import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *

def plot_slice(ph, time, **kwargs):
    """
    ph: path_handler
    """
    # ------------------------------------------
    fields_to_plot = kwargs.get("fields_to_slice", ["ExB_z"])
    overwrite = kwargs.get("overwrite", True)
    sim_name = ph.sim_name
    ix = kwargs.get("ix", 0)
    iy = kwargs.get("iy", 0)
    iz = kwargs.get("iz", 0)
    logScale = kwargs.get("logScale", False)
    cmin = kwargs.get("cmin", None)
    cmax = kwargs.get("cmax", None)
    # ------------------------------------------

    os.chdir(ph.path_to_raw_data)
    figpath = ph.path_to_figures

    print("Plotting " + sim_name + " slices at time step {:d}".format(int(time)))

    for field in fields_to_plot:
        print("Plotting field: " + field)
        # --- z slice
        figdir = figpath + "slices-2d/z-slice/" + field + "/"
        if logScale:
            figdir += "logScale/"
        if cmin is not None or cmax is not None:
            figdir += "cLim/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)
        figname = figdir + field + "_t{:05d}".format(int(time)) + "_iz{}".format(iz)
        if logScale:
            figname += "log"
        # figname += sim_name
        plot_field2d(field, int(time), None, None, iz, save=figname, overwrite=overwrite, logScale=logScale, saveExts=["png"], cmin=cmin, cmax=cmax)
        # plot_field2d(field, int(time), None, None, iz, save=figname, overwrite=overwrite, titleplus=("\n " + sim_name), logScale=logScale, saveExts=["pdf"], cmin=cmin, cmax=cmax)
        plt.close()

        # # --- y slice
        # figdir = figpath + "slices-2d/y-slice/" + field + "/"
        # if logScale:
            # figdir += "logScale/"
        # if not os.path.isdir(figdir):
            # os.makedirs(figdir)
        # figname = figdir + field + "_t{:04d}".format(int(time)) + "_iy{}".format(iy)
        # if logScale:
            # figname += "log"
        # # figname += sim_name
        # print("saving " + figname)
        # plot_field2d(field, int(time), None, iy, None, save=figname, overwrite=overwrite, titleplus=("\n " + sim_name), logScale=logScale)
        # plt.close()
#
        # # --- x slice
        # figdir = figpath + "slices-2d/x-slice/" + field + "/"
        # if logScale:
            # figdir += "logScale/"
        # if not os.path.isdir(figdir):
            # os.makedirs(figdir)
        # figname = figdir + field + "_t{:04d}".format(int(time)) + "_ix{}".format(ix)
        # if logScale:
            # figname += "log"
        # # figname += sim_name
        # plot_field2d(field, int(time), ix, None, None, save=figname, overwrite=overwrite, titleplus=("\n " + sim_name), logScale=logScale)
        # plt.close()


if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"

    sim_name = "test_data"
    sim_name = "Jext_test"
    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"

    # kwargs
    overwrite = True
    ix = 0; iy = 0; iz = 0

    zVars = rdu.Zvariables.options
    fields_to_plot = zVars
    fields_to_plot = ["elsaesserPlusx", "elsaesserPlusy", "velx_total", "pelsaesserDifference", "pelsaesserEnergy"]
    fields_to_plot = ["elsaesserPlusy", "elsaesserMinusy"]
    # fields_to_plot = ["EdotJf", "Jfx", "Jfy", "Jfz", "Ex", "Ey", "Ez"]


    # --------------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, system_config)

    time_steps = retrieve_time_values(ph)[:, 0]
    # times_to_plot = time_steps[20:21]
    times_to_plot = time_steps

    for time in times_to_plot:
        plot_slice(ph, time, fields_to_plot=fields_to_plot,
                   ix=ix, iy=iy, iz=iz, overwrite=overwrite)
