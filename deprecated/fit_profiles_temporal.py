import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from scipy.optimize import curve_fit

"""
This script will plot profiles of Ex and By over the chosen times.
In addition, it will fit the profiles to a sine in order to extract
the phase difference between the magnetic and electric fields,
the amplitude ratio of electric/magnetic fields,
and the phase over time, which can be used to find omega.

TO DO:
- make compatible with not 0,0,0 indices
"""

def fit_profiles_temporal(ph, **kwargs):
    tstart_ind = kwargs.get("tstart_ind", None)
    tend_ind = kwargs.get("tend_ind", None)
    figdir = ph.path_to_figures + "profiles-1d_temporal/"
    figdir += "fits/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    profile_data = retrieve_profiles_temporal(ph, **kwargs)
    selected_fields = kwargs.get("selected_fields", ["Ex", "By", "vely_total"])
    def cos_func(t, a, omega, phi, c):
        return a*np.cos(t*omega+phi)+c
    bounds = ([-np.inf, -np.inf, 0, -np.inf], [np.inf, np.inf, 2.0*np.pi, np.inf])
    p0dict = kwargs.get("p0dict", {k:None for k in selected_fields})
    times_in_LC = retrieve_time_values(ph)[:, 2]
    if tend_ind is not None:
        times_in_LC = times_in_LC[:tend_ind]
    if tstart_ind is not None:
        times_in_LC = times_in_LC[tstart_ind:]

    for field in selected_fields:
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        figname = figdir + field + "_x0y0z0_" + ph.sim_name + ".png"

        selected_profile = np.array(profile_data[field]["x0y0z0"])
        if tend_ind is not None:
            selected_profile = selected_profile[:tend_ind]
        if tstart_ind is not None:
            selected_profile = selected_profile[tstart_ind:]
        fit_params, cov = curve_fit(f=cos_func, xdata=times_in_LC, ydata=selected_profile/norm_value[0], bounds=bounds, p0=p0[field])

        plt.figure()
        plt.plot(times_in_LC, selected_profile/norm_value[0], markersize=3, marker='o')
        plt.plot(times_in_LC, cos_func(times_in_LC, *fit_params), 'k--', label="Fit")
        titstr = ph.sim_name + "\n Temporal profile of " + field + " at x=0, y=0, z=0"
        titstr += "\n Fit parameters: amp={:.2f}".format(fit_params[0]) + norm_value[1][1:]
        titstr += ", omega={:.2f} c/L, phase={:.2f} (for cosine), offset={:.2f}".format(*fit_params[1:])
        if tstart_ind is not None:
            titstr += "\n starting at t={:.2f} L/c".format(times_in_LC[0])
        if tend_ind is not None:
            titstr += ", ending at t={:.2f} L/c".format(times_in_LC[-1])
        plt.xlabel("$tL/c$")
        ylabel = field_abbrev + norm_value[1]
        plt.title(titstr)
        plt.ylabel(ylabel)
        plt.tight_layout()

        print("Saving figure: " + figname)
        plt.savefig(figname, bbox_inches="tight")
        plt.close()




if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    # sim_name = "zeltron_96cube_balanced_s0"
    # sim_name = "zeltron_96cube_single-mode_s0"
    # sim_name = "zeltron_96cube_balanced_master"
    # sim_name = "zeltron_96cube_single-mode_master"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_standing"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_travelling"
    # sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    # sim_name = "zeltron_96cube_z-mode-decay-dcorr0sigma05A025_s0-Nd60"

    # kwargs
    ix = 0; iy = 0; iz = 0
    overwrite = False
    to_show = False

    # ------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    times_in_LC = retrieve_time_values(ph)[:, 2]
    tstart_ind = (np.abs(times_in_LC - 5.0)).argmin()
    # tend_ind = times_in_LC.size
    tend_ind = (np.abs(times_in_LC - 30)).argmin()

    p0 = {}
    # p0["By"] = ([0.2, 4, np.pi/2, 0])
    # p0["Ex"] = None
    # p0["vely_total"] = ([0.2, 5, np.pi/4, 0])
    p0["By"] = None
    p0["Ex"] = ([0.2, 4, 3*np.pi/2, 0])
    p0["vely_total"] = None

    # ----------------------------
    # No editing from here on.
    # ----------------------------
    kwargs = {}
    kwargs["selected_fields"] = ["Ex", "By", "vely_total"]
    kwargs["to_output"] = True
    kwargs["overwrite_data"] = overwrite
    kwargs["ix"] = ix
    kwargs["iy"] = iy
    kwargs["iz"] = iz
    kwargs["tstart_ind"] = tstart_ind
    kwargs["tend_ind"] = tend_ind

    fit_profiles_temporal(ph, **kwargs)
    # compare_phase_over_time(ph, **kwargs)
