#!/bin/bash
#SBATCH --time=05:00:00
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=plot_nodrivezmode
#SBATCH --output=/work/06165/ahankla/stampede2/imbalanced_turbulence/sbatch_scripts/logs/out_compare_plot_vol-avg_over_time.%j
#SBATCH --error=/work/06165/ahankla/stampede2/imbalanced_turbulence/sbatch_scripts/logs/err_compare_plot_vol-avg_over_time.%j
#SBATCH -A TG-PHY140041

module purge
module load intel/18.0.2
module load python3/3.7.0

script='compare_plot_vol-avg_over_time'

scriptpath="/work/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work/06165/ahankla/stampede2/imbalanced_turbulence/sbatch_scripts/logs/log_${script}.txt"
fname="${script}.py"

cd ${scriptpath}
pwd
date
module list

python3 -u ${fname} > ${logpath}

date
