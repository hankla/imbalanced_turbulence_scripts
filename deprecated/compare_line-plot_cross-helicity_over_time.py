import numpy as np
import sys
sys.path.append("./modules/")
sys.path.append("./modules/zPy/for3D/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from utils import get_times
from plot_field2d import plot_field2d
import zfileUtil as zf
import pickle

# config = "zeltron_96cube_balanced_s0"
# config = "zeltron_96cube_single-mode_s0"
# config = "zeltron_96cube_single-mode_no-forcing_standing"
# config = "zeltron_96cube_single-mode_no-forcing_travelling"
config = "zeltron_96cube_balanced_master"
# config = "zeltron_96cube_single-mode_master"
# configs = ["zeltron_96cube_single-mode_no-forcing_standing", "zeltron_96cube_single-mode_no-forcing_travelling", "zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_s0"]
# labels = ["Single (standing) mode, sine forcing", "Single (travelling) mode, sine forcing", "Single mode, OLA forcing", "Balanced (8 modes), OLA forcing"]
configs = ["zeltron_96cube_single-mode_no-forcing_travelling", "zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_s0"]
labels = ["Single mode, sine forcing", "Single mode, OLA forcing", "Balanced (8 modes), OLA forcing"]


figpath = "/mnt/d/imbalanced_turbulence/figures/compare/"
field = "cross-helicity"
linestyles = ["-", ":", "-."]

plt.figure()
plt.xlabel("$tc/L$")
plt.ylabel("$H_c/E$")
# plt.ylabel("Cross-helicity normalized to maximum")
plt.gca().axhline([0], color="black", linestyle="--")
titstr = ""
for i, config in enumerate(configs):
    print("Loading " + config)
    datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    ch_means_p = datarpath +  "cross-helicity_vavg_means.p"
    te_means_p = datarpath +  "total-energy_vavg_means.p"
    if os.path.exists(ch_means_p):
        mean_cross_helicity = pickle.load(open(ch_means_p, "rb"))
        mean_total_energy = pickle.load(open(te_means_p, "rb"))
    else:
        print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    tlcpath = datarpath + "timeInLc.p"
    if not os.path.exists(tlcpath):
        print("ERROR: PICKLE TIME VALUES FIRST!!")
    else:
        timesLC = np.array(pickle.load(open(tlcpath, "rb")))

    normed_ch = np.array(mean_cross_helicity["vol_avg"])/np.array(mean_total_energy["vol_avg"])
    # norm = np.max(np.abs(mean_cross_helicity["vol_avg"]))
    # mean = np.mean(mean_cross_helicity["vol_avg"])
    meanch = np.mean(normed_ch)
    titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanch)
    plt.plot(timesLC, normed_ch, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))



titstr += r"$4\langle v\cdot B\rangle/\langle v^2+B^2\rangle$" + " \n"

plt.title(titstr)
plt.legend()
plt.tight_layout()
figdir = figpath + "volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + "compare_" + field
print("saving " + figname)
plt.savefig(figname)


plt.figure()
plt.xlabel("$tc/L$")
plt.ylabel("$H_c/E$")
# plt.ylabel("Cross-helicity normalized to maximum")
plt.gca().axhline([0], color="black", linestyle="--")
titstr = ""
for i, config in enumerate(configs):
    print("Loading " + config)
    datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    chf_means_p = datarpath +  "cross-helicity-flucs_vavg_means.p"
    tef_means_p = datarpath +  "total-energy-flucs_vavg_means.p"
    if os.path.exists(ch_means_p):
        mean_cross_helicity_f = pickle.load(open(chf_means_p, "rb"))
        mean_total_energy_f = pickle.load(open(tef_means_p, "rb"))
    else:
        print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    tlcpath = datarpath + "timeInLc.p"
    if not os.path.exists(tlcpath):
        print("ERROR: PICKLE TIME VALUES FIRST!!")
    else:
        timesLC = np.array(pickle.load(open(tlcpath, "rb")))

    normed_chf = np.array(mean_cross_helicity_f["vol_avg"])/np.array(mean_total_energy_f["vol_avg"])
    meanchf = np.mean(normed_chf)
    titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanchf)
    plt.plot(timesLC, normed_chf, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))



titstr += r"$4\langle v\cdot (B-B_0)\rangle/\langle v^2+(B-B_0)^2\rangle$" + " \n"

plt.title(titstr)
plt.legend()
plt.tight_layout()
figdir = figpath + "volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + "compare_" + field + "-flucs"
print("saving " + figname)
plt.savefig(figname)

plt.figure()
plt.xlabel("$tc/L$")
plt.ylabel("$H_c/E$")
# plt.ylabel("Cross-helicity normalized to maximum")
plt.gca().axhline([0], color="black", linestyle="--")
titstr = ""
for i, config in enumerate(configs):
    print("Loading " + config)
    datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    chvf_means_p = datarpath +  "cross-helicity-vflucs_vavg_means.p"
    tevf_means_p = datarpath +  "total-energy-vflucs_vavg_means.p"
    if os.path.exists(chvf_means_p):
        mean_cross_helicity_vf = pickle.load(open(chvf_means_p, "rb"))
        mean_total_energy_vf = pickle.load(open(tevf_means_p, "rb"))
    else:
        print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    tlcpath = datarpath + "timeInLc.p"
    if not os.path.exists(tlcpath):
        print("ERROR: PICKLE TIME VALUES FIRST!!")
    else:
        timesLC = np.array(pickle.load(open(tlcpath, "rb")))

    normed_chvf = np.array(mean_cross_helicity_vf["vol_avg"])/np.array(mean_total_energy_vf["vol_avg"])
    meanchvf = np.mean(normed_chvf)
    titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanchvf)
    plt.plot(timesLC, normed_chvf, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))



titstr += r"$4\langle (v-\langle v\rangle) \cdot (B-B_0)\rangle/\langle (v-\langle v\rangle)^2+(B-B_0)^2\rangle$" + " \n"

plt.title(titstr)
plt.legend()
plt.tight_layout()
figdir = figpath + "volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + "compare_" + field + "-vflucs"
print("saving " + figname)
plt.savefig(figname)

plt.figure()
plt.xlabel("$tc/L$")
plt.ylabel("$H_c/E$")
# plt.ylabel("Cross-helicity normalized to maximum")
plt.gca().axhline([0], color="black", linestyle="--")
titstr = ""
for i, config in enumerate(configs):
    print("Loading " + config)
    datarpath = "/mnt/d/imbalanced_turbulence/data/reduced/" + config + "/"
    chv_means_p = datarpath +  "cross-helicity-v_vavg_means.p"
    tev_means_p = datarpath +  "total-energy-v_vavg_means.p"
    if os.path.exists(chv_means_p):
        mean_cross_helicity_v = pickle.load(open(chv_means_p, "rb"))
        mean_total_energy_v = pickle.load(open(tev_means_p, "rb"))
    else:
        print("Error: run create_slices-2d_cross-helicity_over_time.py first!!")
    tlcpath = datarpath + "timeInLc.p"
    if not os.path.exists(tlcpath):
        print("ERROR: PICKLE TIME VALUES FIRST!!")
    else:
        timesLC = np.array(pickle.load(open(tlcpath, "rb")))

    normed_chv = np.array(mean_cross_helicity_v["vol_avg"])/np.array(mean_total_energy_v["vol_avg"])
    meanchv = np.mean(normed_chv)
    titstr += labels[i] + " , mean Hc/E: {:.2e}\n".format(meanchv)
    plt.plot(timesLC, normed_chv, label=labels[i], ls=linestyles[i], marker="o", markersize=3)
    # plt.gca().axhline([mean/norm], ls="--", label=None, color="C{}".format(i))



titstr += r"$4\langle (v-\langle v\rangle) \cdot B\rangle/\langle (v-\langle v\rangle)^2+B^2\rangle$" + " \n"

plt.title(titstr)
plt.legend()
plt.tight_layout()
figdir = figpath + "volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figname = figdir + "compare_" + field + "-v"
print("saving " + figname)
plt.savefig(figname)
