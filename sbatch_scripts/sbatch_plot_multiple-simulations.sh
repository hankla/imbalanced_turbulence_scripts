#!/bin/bash
#SBATCH --time=40:00:00
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=1536spectra
# #SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_multiple-simulations4.%j
# #SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_multiple-simulations4.%j
# #SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_multiple-simulationsParallel.%j
# #SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_multiple-simulationsParallel.%j
# #SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_volume-avgs.%j
# #SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_volume-avgs.%j
# #SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_pvx-frac.%j
# #SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_pvx-frac.%j
#SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_spectra.%j
#SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_spectra.%j
# #SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_length_scales.%j
# #SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_length_scales.%j
#SBATCH -A TG-PHY140041
#SBATCH --mail-type=ALL
#SBATCH --mail-user=lia.hankla@gmail.com
# #SBATCH --begin=now+3hours

module purge
module load intel/18.0.2
module load python3/3.7.0

# script='plot_multiple-simulations4'
# script='parallel_plot_multiple-simulations'
# script='plot_volume_averages'
script='plot_magnetic_energy_spectrum'
# script='plot_positive_vx_fraction_over_energy'
# script='plot_length_scales_over_time'

scriptpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/log_${script}.txt"
fname="${script}.py"

cd ${scriptpath}
pwd
date
module list

python3 -u ${fname} > ${logpath}

date
