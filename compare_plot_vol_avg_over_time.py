import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import *
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - cross helicity fluctuations
"""

def compare_plot_vol_avg_over_time(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    cut_initial = kwargs.get("cut_initial", False)
    plot_std = kwargs.get("plot_std", False)
    match_times = kwargs.get("match_times", False)
    max_scaling = kwargs.get("max_scaling", False)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = kwargs.get("fields_to_vavg", ["velz_total", "VpBp_total", "deltaBrms"])
    linear_fit = kwargs.get('linear_fit', False)
    plot_decay_time = kwargs.get("plot_decay_time", True)
    ylims = kwargs.get("ylims", None)
    log_yscale = kwargs.get("log_yscale", False)
    tequivalent = False
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_limited = True
    if "final_Einj" in kwargs:
        final_Einj = kwargs.get("final_Einj")
        if "tequivalent" in kwargs and kwargs["tequivalent"]:
            t_save_str = "_t{:d}-teq".format(int(tinitial))
            tequivalent = True
            t_limited = True

    sim_set = comparison(category)
    configs = sim_set.configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/volAvg-over-time/"
    if match_times:
        figdir += "matched_times/"
    if cut_initial:
        figdir += "noT0/"
    if max_scaling:
        figdir += "scaled_to_max/"
    if log_yscale:
        figdir += "log_yscale/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figbase = figdir
    if plot_std:
        figbase += "std_"
    else:
        figbase += "mean_"

    labels = sim_set.labels
    colors = sim_set.colors

    smallest_end_time = None
    maxval = None

    for field in fields_to_plot:
        linestyles = itertools.cycle(sim_set.linestyles)
        markers = itertools.cycle(sim_set.markers)
        norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)[1]
        if "VpBp" in field:
            norm_value = "$/B_0v_A(t)$"
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.figure()
        plt.xlabel("$tc/L$")
        if plot_std:
            ylabel = "standard deviation of " + field_abbrev + norm_value
        else:
            ylabel = r"$\langle$" + field_abbrev + norm_value + r"$\rangle$"
        plt.ylabel(ylabel)
        # plt.ylabel("Cross-helicity normalized to maximum")
        # plt.gca().axhline([0], color="black", linestyle="--")
        figure_name = figbase + field.strip("_")
        if linear_fit:
            figure_name += "_linear-fit"
        if t_limited:
            figure_name += t_save_str
        if "final_Einj" in kwargs and not tequivalent:
            figure_name += "_plotEinj"
        figure_name += ".png"

        for i, sim_name in enumerate(configs):
            if stampede2:
                ph = path_handler(sim_name, "stampede2")
            else:
                ph = path_handler(sim_name, system_config)

            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            times_in_LC = retrieve_time_values(ph)[:, 2]
            if smallest_end_time is None or times_in_LC[-1] < smallest_end_time:
                smallest_end_time = times_in_LC[-1]
            (vol_avgs, vol_stds) = retrieve_variables_vol_means_stds(ph, fields_to_vavg=[field])
            norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)[0]
            if "VpBp" in field:
                # normalize to vA(t), then take the mean
                if not os.path.exists(ph.path_to_reduced_data + "magnetization.dat"):
                    plot_length_scales(ph)
                mag_values = np.loadtxt(ph.path_to_reduced_data + "magnetization.dat", delimiter=",", skiprows=1)
                magnetization = mag_values[:, 1]

                # should be able to jump every FDUMP outputs
                jump = int(params["FDUMP"])
                magnetization = magnetization[::jump]

                # in case of going longer than needed
                magnetization = magnetization[:vol_avgs[field].size]
                alfven_in_c = np.sqrt(magnetization/(magnetization + 1.0))
                norm_value = (params["B0"]*alfven_in_c, "$/B_0v_A(t)$")[0]

            if plot_std:
                field_data = vol_stds[field]
            else:
                field_data = vol_avgs[field]

            if field_data is not None:
                # if field == "cumInjectedEnergyDensity":
                    # field_data = -1.0*field_data
                if i == 0:
                    maxval = np.max(field_data)/norm_value

                if cut_initial:
                    times_in_LC = times_in_LC[1:]
                    field_data = field_data[1:]

                if max_scaling:
                    field_data = (field_data/norm_value)/np.max(np.abs(field_data/norm_value))

                if "final_Einj" in kwargs:
                    einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
                    einj_cum = np.cumsum(einj)
                    dt_in_s = params["dt"]
                    Lx = params["xmax"] - params["xmin"]
                    Ly = params["ymax"] - params["ymin"]
                    Lz = params["zmax"] - params["zmin"]
                    volume = Lx*Ly*Lz
                    mag_energy_density = (params["B0"])**2.0/(8.0*np.pi)
                    einj_norm = einj_cum/volume/mag_energy_density
                    dt_in_LC = dt_in_s*rdu.Consts.c/Lz
                    einj_times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
                    einj_times_in_LC = np.insert(einj_times_in_LC, 0, 0)
                    einj_times_in_LC = einj_times_in_LC[:-1]
                    equivalent_time_ind = (np.abs(-1.0*einj_norm - final_Einj)).argmin()
                    einj_equiv_time = einj_times_in_LC[equivalent_time_ind]
                    equivalent_time_ind = (np.abs(times_in_LC - einj_equiv_time)).argmin()
                    equivalent_time = times_in_LC[equivalent_time_ind]
                    value_at_teq = field_data[equivalent_time_ind]
                    if not np.isscalar(norm_value):
                        norm_at_teq = norm_value[equivalent_time_ind]
                    else:
                        norm_at_teq = norm_value

                if t_limited:
                    tinitial_ind = (np.abs(tinitial - times_in_LC)).argmin()
                    if tequivalent:
                        tfinal_ind = equivalent_time_ind
                    else:
                        tfinal_ind = (np.abs(tfinal - times_in_LC)).argmin()
                    times_in_LC = times_in_LC[tinitial_ind:tfinal_ind]
                    field_data = field_data[tinitial_ind:tfinal_ind]
                    if not np.isscalar(norm_value):
                        norm_value = norm_value[tinitial_ind:tfinal_ind]

                plt.plot(times_in_LC, field_data/norm_value, label=labels[i], ls=next(linestyles), marker=next(markers), markersize=4, markevery=int(field_data.size/10)+1, color=colors[i], path_effects=[mpe.Stroke(linewidth=1.8, foreground='k'), mpe.Normal()])
                if "final_Einj" in kwargs and not tequivalent:
                    print(equivalent_time)
                    plt.plot(equivalent_time, value_at_teq/norm_at_teq, label=None, color='red', markersize=8, marker='x', path_effects=[mpe.Stroke(linewidth=1.8, foreground='k'), mpe.Normal()])
                if linear_fit:
                    linear_model = np.polyfit(times_in_LC, field_data/norm_value, 1)
                    linear_model_vals = np.poly1d(linear_model)(times_in_LC)
                    plt.plot(times_in_LC, linear_model_vals, label=None, ls=next(linestyles), marker=next(markers), markersize=4, markevery=int(field_data.size/10)+1, color=colors[i])
                if ylims is not None:
                    plt.ylim(ylims)
                if log_yscale:
                    plt.yscale('log')
                if "NDECAY" in params and plot_decay_time:
                    NDECAY = params["NDECAY"]
                    time_inds = retrieve_time_values(ph)[:, 0]
                    tind = (np.abs(time_inds - NDECAY)).argmin()
                    end_time = retrieve_time_values(ph)[tind, 2]
                    plt.gca().axvline([end_time], color='black', ls='-.')

        # plt.gca().axhline([maxval/2.5], color='tab:orange', ls='--', label="A=0.1A0 linear prediction")
        # plt.gca().axhline([maxval/6.25], color='tab:orange', ls=':', label="A=0.1A0 quadratic prediction")
        # plt.gca().axhline([maxval/5.0], color='tab:green', ls='--', label="A=0.05A0 linear prediction")
        # plt.gca().axhline([maxval/25], color='tab:green', ls=':', label="A=0.05A0 quadratic prediction")


        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        titstr = ""
        if cut_initial:
            titstr += "$t=0$ removed"
        if match_times:
            titstr += " xlim adjusted"
        titstr += "\n" + category
        plt.title(titstr + "\n")

        # --------------------------------------------
        #       Special legends time!
        # --------------------------------------------
        if "8m" in category:
            from mpl_toolkits.axes_grid1.inset_locator import inset_axes
            # First, a colorbar for the balance parameter values.
            n = len(sim_set.configs)
            cmap = plt.cm.get_cmap('viridis_r')
            balance_param_values = np.sort(np.unique(np.array(get_comparison_Avar_as_Bvar(category, "imbalance", "deltaBrms_mean")[0])))
            colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=cmap))
            colorbar1.set_label(r"Balance Parameter $\xi$")
            colorbar1.ax.set_yticklabels(["{:.2f}".format(x) for x in balance_param_values])

            if category == "8mImbalanceAll-comparison":
                # Next, have a legend for dcorr 0 vs. nonzero
                legend_elements = [matplotlib.lines.Line2D([0], [0], color='black', ls=sim_set.linestyles[0], label="$\gamma_D=0.0$",marker=sim_set.markers[0], markersize=4),
                                matplotlib.lines.Line2D([0], [0], color='black', ls=sim_set.linestyles[1], label="$\gamma_D=0.29\omega_A$", marker=sim_set.markers[1], markersize=4)]
                plt.gca().legend(handles=legend_elements, frameon=False)
        else:
            if sim_set.cbar_label is not None:
                colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
                colorbar1.set_label(sim_set.cbar_label)
                colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
            legend_elements = sim_set.legend_elements
            if not legend_elements:
                plt.legend(frameon=False, ncol=sim_set.ncol)
            else:
                plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)

        plt.tight_layout()
        if match_times:
            plt.xlim([0.0, smallest_end_time])
        if t_limited and not tequivalent:
            plt.xlim([tinitial, tfinal])
        # plt.tight_layout()
        metadata={'Author':'Lia Hankla', 'Subject':category, 'Keywords':'Git commit: ' + ph.get_current_commit()}
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.title('')
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight', metadata=metadata)

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    # category = "xiAllboxSizeExtremes-comparison"
    category = "768Imbalance-comparison"
    # category = "8xyzIm00Mf-comparison"
    # category = "modeNum-comparison"
    fields_to_plot = ["cumInjectedEnergyDensity", "internalEnergyDensity", "internalEnergyDensity_efficiency"]
    # fields_to_plot = ["magneticEnergyDensity", "Brms2", "turbulentEnergyDensity_efficiency", "netEnergyDensity_efficiency", "internalEnergyDensity_efficiency", "electromagneticEnergyDensity_efficiency", "netEnergyDensity", "ExB_z", "Upz_total", "turbulentEnergyDensity", "electromagneticEnergyDensity", "magneticEnergyDensity"]
    # fields_to_plot = ["magneticEnergyDensity", "Upz_total", "relativisticVz2"]
    fields_to_plot = fields_to_plot[:1]
    cut_at_tequivalent = False
    plot_final_Einj = True

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    kwargs["fields_to_vavg"] = fields_to_plot
    kwargs["tinitial"] = 0.0
    kwargs["tfinal"] = 20.0
    # kwargs["ylims"] = [-0.55, 0.55]
    # kwargs["ylims"] = [-0.7, 0.7]
    # kwargs["ylims"] = [0, 0.2]

    kwargs["tequivalent"] = False
    # get minimum final Einj
    if plot_final_Einj:
        stats = get_comparison_stats(category, **kwargs)
        final_einj_densities = stats.loc["einj_density_at_20_Lc"]
        final_Einj = final_einj_densities.min()
        kwargs["final_Einj"] = final_Einj
        kwargs["tequivalent"] = cut_at_tequivalent

    compare_plot_vol_avg_over_time(category, **kwargs)
