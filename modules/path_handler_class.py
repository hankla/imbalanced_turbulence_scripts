# Class to handle building paths
# Default is stampede2
import os
import subprocess

class path_handler:
    def __init__(self, sim_name, setup="stampede2", **kwargs):
        """
        path_handler is set up to know some default settings.
        Add to defaults using the setup below.
        Non-standard paths can be added in the kwargs.
        """
        self.sim_name = sim_name
        self.setup = setup

        # -------- Add setup information here
        if setup == "lia_hp":
            path_to_reduced_data = "/mnt/d/imbalanced_turbulence/data_reduced/" + sim_name + "/"
            path_to_raw_data = "/mnt/d/imbalanced_turbulence/data_raw/" + sim_name + "/"
            path_to_figures = "/mnt/d/imbalanced_turbulence/figures/" + sim_name + "/"
            path_to_scripts = "/mnt/d/imbalanced_turbulence/scripts/"

        elif setup == "jila_laptop":
            path_to_reduced_data = "/run/media/amha5924/toroidal_dynamo/imbalanced_turbulence/data_reduced/" + sim_name + "/"
            path_to_raw_data = "/run/media/amha5924/toroidal_dynamo/imbalanced_turbulence/data_raw/" + sim_name + "/"
            path_to_figures = "/run/media/amha5924/toroidal_dynamo/imbalanced_turbulence/figures/" + sim_name + "/"
            path_to_scripts = "/run/media/amha5924/toroidal_dynamo/imbalanced_turbulence/scripts/"

        elif setup == "stampede2":
            path_to_reduced_data = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/data_reduced/" + sim_name + "/"
            path_to_raw_data = "/scratch/06165/ahankla/imbalanced_turbulence/" + sim_name + "/data/"
            path_to_figures = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/figures/" + sim_name + "/"
            path_to_scripts = "/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"

        # -------------------------------------
        # If paths are non-standard, set them here using kwargs.
        # Otherwise, use the defaults from above
        # --------------------------------------------
        self.path_to_reduced_data = kwargs.get('path_to_reduced_data', path_to_reduced_data)
        self.path_to_raw_data = kwargs.get('path_to_raw_data', path_to_raw_data)
        self.path_to_figures = kwargs.get('path_to_figures', path_to_figures)
        self.path_to_scripts = kwargs.get('path_to_scripts', path_to_scripts)

        # -------------------------------------
        # Create the directories if need be
        # -------------------------------------
        if not os.path.exists(self.path_to_reduced_data):
            # only make reduced data path if raw path exists
            if not os.path.exists(self.path_to_raw_data):
                print("ERROR in creating path to reduced data: path to raw data doesn't exist!! This implies simulation name is incorrect.")
                print(self.path_to_raw_data)
            else:
                os.makedirs(self.path_to_reduced_data)
                if not os.path.exists(self.path_to_figures):
                    os.makedirs(self.path_to_figures)
        elif not os.path.exists(self.path_to_figures):
            # only make figures path if reduced path exists
            os.makedirs(self.path_to_figures)

        # --------------------------------
        # Set various other paths
        # --------------------------------
        # Path for file containing time values in various units (step, seconds, light-crossing)
        self.times_path = self.path_to_reduced_data + "time_values.txt"
        # Path for file containing coordinate values
        self.coords_path = self.path_to_reduced_data + "coords.txt"

        # Paths for 1D (volume-averaged) data
        self.output_vars_vol_means_path = self.path_to_reduced_data + "output-vars_volume-means.p"
        self.output_vars_vol_stds_path = self.path_to_reduced_data + "output-vars_volume-stds.p"
        # Paths for profile data
        self.spatial_profiles_path = self.path_to_reduced_data + "spatial_profiles.p"
        self.spatial_profiles_avg_path = self.path_to_reduced_data + "spatial_profiles_avg.p"
        self.temporal_profiles_path = self.path_to_reduced_data + "temporal_profiles.p"
        self.temporal_profiles_avg_path = self.path_to_reduced_data + "temporal_profiles_xyavg.p"

    def get_flux_variable_path(self, quantity, ix, iy, iz):
        if ix is not None:
            quantity_str = quantity + "x%i" % ix
        elif iy is not None:
            quantity_str = quantity + "y%i" % iy
        elif iz is not None:
            quantity_str = quantity + "z%i" % iz
        flux_path = self.path_to_reduced_data + "flux_" + quantity_str + ".txt"
        return flux_path


    def get_pvz_fraction_over_time_path(self):
        pvz_fraction_path = self.path_to_reduced_data + "pvz_fraction_over_time.p"
        return pvz_fraction_path


    def get_pvxy_fraction_over_time_path(self):
        pvxy_fraction_path = self.path_to_reduced_data + "pvxy_fraction_over_time.p"
        return pvxy_fraction_path

    def get_gamma_centers_path(self):
        return self.path_to_reduced_data + "gamma_centers.txt"


    def get_pspectrum_path(self, quantity, time):
        pspectrum_path = self.path_to_reduced_data + "pspectrum_" + quantity + "_t{}.txt".format(int(time))
        return pspectrum_path

    def get_current_commit(self):
        current_dir = os.getcwd()
        os.chdir(self.path_to_scripts)
        git_commit = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip().decode('ASCII')
        os.chdir(current_dir)
        return git_commit
