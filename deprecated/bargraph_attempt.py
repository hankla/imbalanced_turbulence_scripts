import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plot_length_scales_over_time import plot_length_scales
import matplotlib.patheffects as mpe
from compare_handler import *
from mpl_toolkits.mplot3d import Axes3D
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""

def plot_bargraph(category, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    t_save_str = None
    t_tit_str = ""
    if "tinitial" in kwargs and "tfinal" in kwargs:
        tinitial = kwargs.get("tinitial")
        tfinal = kwargs.get("tfinal")
        t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
        t_tit_str = "\n Averaged from t={:.2f} - {:.2f} L/c".format(tinitial, tfinal)

    ph0 = path_handler(comparison(category).configs[0], "lia_hp")

    (xi_values, internalEnergy) = get_comparison_Avar_as_Bvar(category, "imbalance", "internalEnergyDensity_efficiency_mean", **kwargs)
    netEnergy = get_comparison_Avar_as_Bvar(category, "imbalance", "netEnergyDensity_efficiency_mean", **kwargs)[1]
    EMEnergy = get_comparison_Avar_as_Bvar(category, "imbalance", "electromagneticEnergyDensity_efficiency_mean", **kwargs)[1]
    turbulentEnergy = get_comparison_Avar_as_Bvar(category, "imbalance", "turbulentEnergyDensity_efficiency_mean", **kwargs)[1]

    xi_values = xi_values[::-1]
    internalEnergy = internalEnergy[::-1]
    EMEnergy = EMEnergy[::-1]
    turbulentEnergy = turbulentEnergy[::-1]
    netEnergy = netEnergy[::-1]
    # full_values = np.vstack([internalEnergy, EMEnergy])
    # full_values = np.vstack([full_values, turbulentEnergy])
    # full_values = np.vstack([full_values, netEnergy])

    # set up figure info
    figdir = ph0.path_to_figures + "../compare/" + category + "/energy-partition/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "energy-partition_bar"
    if t_save_str is not None:
        figname += t_save_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    if figure_name == "show":
        matplotlib.use("TkAgg")
    # fig = plt.figure()
    # ax1 = fig.add_subplot(111, projection='3d')
# 
    # xpos = np.array(xi_values)
    # ypos = np.arange(0,4)
    # xposM, yposM = np.meshgrid(xpos, ypos)
    # zpos = full_values.ravel()
# 
    # dx = 0.1
    # dy = 0.5
# 
    # ax1.bar3d(xposM.ravel(), yposM.ravel(), zpos*0, dx, dy, zpos)
    # ax1.set_xlabel("xi values")
    # ax1.set_ylabel("energy type")
    # ax1.w_xaxis.set_ticks(xpos + dx/2.)
    # ax1.w_xaxis.set_ticklabels(xi_values)
    # ax1.w_yaxis.set_ticks(ypos + dy/2.)
    # ax1.w_yaxis.set_ticklabels(["int", "EM", "turb", "net"])
    # # ax1.w_xaxis.set_ticklabels(xi_values)
# 
    # # ax2.plot(times_in_LC, field_std/norm_value[0], markersize=3, marker="o", linestyle="-.", color=ax2color)
    plt.figure()
    barWidth = 0.85
    barPos = np.arange(len(xi_values))
    plt.bar(barPos, internalEnergy, width=barWidth, label=r"$\Delta\mathcal{E}_{\rm int}/\mathcal{E}_{\rm inj}$")
    plt.bar(barPos, netEnergy, bottom=internalEnergy, width=barWidth, label=r"$\Delta\mathcal{E}_{\rm net}/\mathcal{E}_{\rm inj}$")
    plt.bar(barPos, turbulentEnergy, bottom=[i+j for i,j in zip(internalEnergy, netEnergy)], width=barWidth, label=r"$\Delta\mathcal{E}_{\rm turb}/\mathcal{E}_{\rm inj}$")
    plt.bar(barPos, EMEnergy, bottom=[i+j+k for i,j,k in zip(internalEnergy, netEnergy, turbulentEnergy)], width=barWidth, label=r"$\Delta\mathcal{E}_{\rm EM}/\mathcal{E}_{\rm inj}$")
    plt.gca().set_xticklabels([0, 0.0, 0.25, 0.5, 0.75, 1.0])
    plt.ylim([0, 1])
    # plt.gca().axhline([1.0], color='black', linestyle='--')
    plt.xlabel(r"Balance parameter $\xi$")
    # plt.gca().set_yscale('log')
    titstr = category + t_tit_str
    plt.title(titstr)
    # plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)

    for i in np.arange(len(xi_values)):
        plt.gca().annotate("{:.2f}".format(internalEnergy[i]), (i+0.1, internalEnergy[i]), va='top')
        plt.gca().annotate("{:.2f}".format(turbulentEnergy[i]), (i+0.1, turbulentEnergy[i] + netEnergy[i]+internalEnergy[i]), va='top')
        plt.gca().annotate("{:.2f}".format(EMEnergy[i]), (i+0.1, EMEnergy[i] + turbulentEnergy[i]+netEnergy[i] + internalEnergy[i]), va='top')
    plt.gca().annotate(r"$\Delta\mathcal{E}_{\rm int}/\mathcal{E}_{\rm inj}$", (-0.4, 0.01), va='bottom')
    plt.gca().annotate(r"$\Delta\mathcal{E}_{\rm net}/\mathcal{E}_{\rm inj}$", (-0.4, internalEnergy[0]), va='bottom')
    plt.gca().annotate(r"$\Delta\mathcal{E}_{\rm turb}/\mathcal{E}_{\rm inj}$", (-0.4, netEnergy[0] + internalEnergy[0]), va='bottom')
    plt.gca().annotate(r"$\Delta\mathcal{E}_{\rm EM}/\mathcal{E}_{\rm inj}$", (-0.4, turbulentEnergy[0] + netEnergy[0] + internalEnergy[0]), va='bottom')

    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        plt.savefig(figure_name)
    plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    category = "768Imbalance-comparison"
    overwrite_data = False
    plot_decay_time = False

    tinitial = 19.0 # L/c
    tfinal = 20.0 # L/c

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["tinitial"] = tinitial
    kwargs["tfinal"] = tfinal
    kwargs["reload_all_sims"] = True

    # --------------------
    plot_bargraph(category, **kwargs)
