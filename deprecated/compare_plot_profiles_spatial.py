import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import comparison
import itertools
"""
This script will plot the spatial profilesof every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

Useful for diagnostics. Not included in Hankla+2021.
"""
def compare_plot_profiles_spatial(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = kwargs.get("fields_to_profile", ["Ex", "By", "vely_total"])
    dim = kwargs.get("dim", "z")
    average_2d = kwargs.get("average_2d", False)

    configs = comparison(category).configs
    labels = comparison(category).labels
    ix = 0; iy = 0; iz = 0
    # sim_name0 = "zeltron_96cube_single-mode_master"
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    times_to_plot = kwargs.get("times_to_profile", retrieve_time_values(ph0)[:, 0])

    figbase = ph0.path_to_figures + "../compare/" + category + "/profiles-1d_spatial/" + dim + "-profile/"
    figure_name = kwargs.get("figure_name", None)

    colors = comparison(category).colors

    smallest_end_time = None

    coords = retrieve_coords(ph0)
    x_axes = {}
    x_axes["z"] = coords[2]
    x_axes["y"] = coords[1]
    x_axes["x"] = coords[0]
    if average_2d:
        if dim == "x": profstr = "yAzA"
        elif dim == "y": profstr = "xAzA"
        elif dim == "z": profstr = "xAyA"
    else:
        if dim == "x": profstr = "iy{}iz{}".format(iy,iz)
        elif dim == "y": profstr = "ix{}iz{}".format(ix, iz)
        elif dim == "z": profstr = "ix{}iy{}".format(ix, iy)
    (params, units) = rdu.getSimParams(simDir=ph0.path_to_reduced_data)
    consts = rdu.Consts
    rho = consts.c/params["omegac"]

    if figure_name == "show":
        matplotlib.use("TkAgg")

    for field in fields_to_plot:
        linestyles = itertools.cycle(comparison(category).linestyles)
        markers = itertools.cycle(comparison(category).markers)
        figdir = figbase + field + "/"
        if average_2d:
            figdir += "2d-averaged/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)
        norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        x_axis = np.array(x_axes[dim])

        for time in times_to_plot:
            plt.figure()
            plt.xlabel("$" + dim + r"/\rho$") # units
            ylabel = field_abbrev + norm_value[1]

            for i, sim_name in enumerate(configs):
                ph = path_handler(sim_name, system_config)
                profile_data = retrieve_profiles_spatial(ph, [time], fields_to_profile=[field], average_2d=average_2d)
                chosen_profile = profile_data[field]["{:d}".format(int(time))][profstr.replace("i", '')]
                if chosen_profile.size != x_axis.size:
                    if x_axis.size == params["N" + dim.capitalize()]:
                        x_axis = x_axis[1:]
                if i == 0:
                    maxval = np.max(chosen_profile)/norm_value[0]

                plt.plot(x_axis/rho, chosen_profile/norm_value[0], label=labels[i], marker=next(markers), ls=next(linestyles), markersize=2, markevery=int(chosen_profile.size/10), color=colors[i] )

            # plt.gca().axhline([maxval/2.5], color='tab:orange', ls='--', label="A=0.1A0 linear prediction")
            # plt.gca().axhline([-maxval/2.5], color='tab:orange', ls='--', label="A=0.1A0 linear prediction")
            # plt.gca().axhline([maxval/6.25], color='tab:orange', ls=':', label="A=0.1A0 quadratic prediction")
            # plt.gca().axhline([maxval/5.0], color='tab:green', ls='--', label="A=0.05A0 linear prediction")
            # plt.gca().axhline([-maxval/5.0], color='tab:green', ls='--', label="A=0.05A0 linear prediction")
            # plt.gca().axhline([maxval/25], color='tab:green', ls=':', label="A=0.05A0 quadratic prediction")
            plt.legend()
            plt.gca().grid(color='.9', ls='--')
            plt.gca().tick_params(top=True, right=True, direction='in', which='both')
            figname = figdir + field + "_t{}".format(int(time)) + "_" + profstr + ".png"
            figure_name = kwargs.get("figure_name", figname)
            titstr = field + " " + dim + "-profile"
            if average_2d:
                titstr += ", 2d averaged"
            else:
                if dim == "x": titstr += " at y={}, z={}".format(iy, iz)
                elif dim == "y": titstr += " at x={}, z={}".format(ix, iz)
                elif dim == "z": titstr += " at x={}, y={}".format(ix, iy)
            titstr += ", t={}".format(time)
            titstr += "\n" + category
            plt.title(titstr)
            plt.tight_layout()
            if figure_name != "show":
                print("Saving figure " + figure_name)
                plt.savefig(figure_name, bbox_inches="tight")
                plt.gca().grid(False, which='both')
                plt.savefig(figure_name.replace(".png", "_nogrid.png"))
                plt.close()
    if figure_name == "show":
        plt.show()

if __name__ == '__main__':
    category = "dcorr-comparison"
    category = "im09-comparison"
    # category = "ppc-comparison"
    category = "A0-comparison"
    # category = "res-comparison"
    # category = "boxSize-comparison"
    system_config = "lia_hp"
    stampede2 = False
    match_times = True
    to_show = True
    average_2d = True

    ph0 = path_handler(comparison(category).configs[0], system_config)
    times_to_plot = retrieve_time_values(ph0)[:, 0]
    times_to_plot = times_to_plot[:2]


    # -------------------------------------------------
    kwargs = {}
    kwargs["system_config"] = system_config
    kwargs["match_times"] = match_times
    kwargs["times_to_plot"] = times_to_plot
    kwargs["system_config"] = system_config
    kwargs["stampede2"] = stampede2
    kwargs["average_2d"] = average_2d
    if to_show:
        kwargs["figure_name"] = "show"

    compare_plot_profiles_spatial(category, **kwargs)
