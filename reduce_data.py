import sys
sys.path.append('./modules/')
from reduce_data_utils import *
import raw_data_utils as rdu
from path_handler_class import *
import matplotlib.pyplot as plt
import shutil

def reduce_data(sim_name, ph, **kwargs):
    overwrite = kwargs.get("overwrite", False)

    # ---------------------------------------------
    # First step is to export all the time values
    times = retrieve_time_values(ph, overwrite)
    times_in_LC = times[:, 2]

    # ---------------------------------------------
    # Second step is to make sure the simulation parameters
    # are accessible from the reduced data path
    phyparam_path = ph.path_to_raw_data + "phys_params.dat"
    new_phyparam_path = ph.path_to_reduced_data + "phys_params.dat"
    inputparam_path = ph.path_to_raw_data + "input_params.dat"
    new_inputparam_path = ph.path_to_reduced_data + "input_params.dat"
    shutil.copyfile(phyparam_path, new_phyparam_path)
    shutil.copyfile(inputparam_path, new_inputparam_path)

    # ---------------------------------------------
    # Now, take volume averages of all regular output variables.
    # The values will be output to a .txt file.
    output_variable_vol_avg = retrieve_variables_vol_avg(ph, overwrite)
    # print(output_variable_vol_avg.keys())

    # ---------------------------------------------
    # Now, find the fluxes for common quantities
    # The values will be output to a .txt file.
    for quantity in rdu.Zvariables.flux_options:
        retrieve_flux_variable(ph, quantity, None, None, 0)
        retrieve_flux_variable(ph, quantity, None, 0, None)
        retrieve_flux_variable(ph, quantity, 0, None, None)

    # Check time steps match
    if not (output_variable_vol_avg["time step"] == times[:, 0]).all():
        print("ERROR")
    # ---------------------------------------------
    # Quick example of how to use extracted data
    # bx_mean = output_variable_vol_avg["Bx mean"]
    # bx_std = output_variable_vol_avg["Bx std"]
    # plt.plot(times_in_LC, bx_mean)
    # plt.xlabel("$tc/L$")
    # plt.ylabel(r"$\langle B_x\rangle$")
    # plt.fill_between(times_in_LC, bx_mean - bx_std, bx_mean + bx_std, alpha=0.5)
    # plt.show()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    overwrite = False

    # sim_name = "zeltron_96cube_single-mode_s0"
    # sim_name = "zeltron_96cube_balanced_s0"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_travelling"
    sim_name = "zeltron_96cube_balanced_master"
    # sim_name = "zeltron_96cube_single-mode_master"

    # --------------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    reduce_data(sim_name, ph, overwrite=overwrite)
