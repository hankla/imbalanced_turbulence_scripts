import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import scipy.fftpack as fftpack
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *

"""
This script will plot the injected energy (J dot E) at each time step for a given simulation.
There are also some functionalities like trying to fit the injected energy to, e.g.,
a sine. These are commented out because they don't work at the moment.
"""

def plot_injected_energy(ph, **kwargs):
    if os.path.exists(ph.path_to_raw_data):
        os.chdir(ph.path_to_raw_data)
    elif os.path.exists(ph.path_to_reduced_data + "Eem.dat"):
        os.chdir(ph.path_to_reduced_data)
    else:
        print("Eem.dat not available. Injected energy NOT plotted!")
        return
    sim_name = ph.sim_name

    figdir = ph.path_to_figures + "energy-conservation/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "injected-energy-at-each-time.png"
    figure_name = kwargs.get("figure_name", figname)

    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    B0 = params["B0"]
    einj = np.loadtxt("Einj.dat")

    consts = rdu.Consts
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    Lz = params["zmax"] - params["zmin"]
    Ly = params["ymax"] - params["ymin"]
    Lx = params["xmax"] - params["xmin"]
    volume = Lx*Ly*Lz

    dt_in_s = params["dt"]
    dt_in_LC = dt_in_s*consts.c/Lz
    times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
    times_in_LC = np.insert(times_in_LC, 0, 0)
    times_in_LC = times_in_LC[:-1]
    times = times_in_LC

    # confirmed this matches emdata at time 0
    mag_energy_density = B0**2/(8*np.pi)
    initial_ME = mag_energy_density * Lx*Ly*Lz
    # normalize injected energy to initial magnetic energy
    einj_norm = einj/initial_ME

    # make energy plot
    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel('$tc/L_z$', fontsize=18)
    plt.ylabel(r'$E_{inj}/(B_0^2/8\pi~V)$', fontsize=14)
    titstr = sim_name
    plt.plot(times, einj_norm, lw=2, ls='-', label="Einj")
    plt.gca().axhline([0.0], color="black", ls="--")

    plt.title(titstr)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"
    overwrite_data = False

    sim_name = "test_data"
    sim_name = "Jext_test"

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, system_config)

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data

    plot_injected_energy(ph, **kwargs)
