
import numpy
import math
import zfileUtil
zfileUtil.detectDisplay()
import matplotlib.pyplot as plt
import sys

c=2.99792458e10

def plot_field_xz(it,iy,field):

    if it=='':
       it='0'

    if iy=='':
       iy='0'

    if field=='':
       field='Bx'

    #===============================================================================
    # Parameters of the simulation

    (params, units) = zfileUtil.getSimParams()

    # Nominal cyclotron frequency
    rho=c/params["omegac"]

    #===============================================================================
    # The grid
    fieldFile = field + "_" + it
    (mapxz, time, step, coords) = zfileUtil.getFieldSliceXZ(fieldFile, 
      int(iy))
    x1d=coords[0]
    z1d=coords[2]
    x, z = numpy.meshgrid(x1d, z1d, indexing='ij')

    norm = (1., "")
    if len(field) == 2 and field[0] in "BE":
      norm = (params["B0"], r"$/B_0$")
    elif len(field) > 8 and field[:6]=="mapxyz":
      nd = params["Drift. dens."]
      nb = params["density ratio"] * nd
      if field[-2:] == "bg":
        norm = (nb, r"$/n_{be}$")
      elif field[-5:] == "drift":
        norm = (nb, r"$/n_{be}$")

    # The field
    vMin=None
    vMax=None
    if 1:
      vMin = mapxz.min()/norm[0]
      vMax = mapxz.max()/norm[0]
      if (vMin < 0 and vMax > 0 and vMax+vMin < vMax/3.):
        vMax = max(vMax, -vMin)
        vMin = -vMax
    plt.pcolormesh(x/rho,z/rho,mapxz/norm[0],vmin=vMin,vmax=vMax,
      cmap = plt.get_cmap("seismic"),
      )
    plt.xlabel(r'$x/\rho$',fontsize=18)
    plt.ylabel(r'$z/\rho$',fontsize=18)
    plt.xlim([numpy.min(x/rho),numpy.max(x/rho)])
    plt.ylim([numpy.min(z/rho),numpy.max(z/rho)])
    
    plt.colorbar().ax.set_ylabel(field+norm[1],fontsize=18)
    
    plt.title("time step="+it+"/ iy="+iy+"/ Field="+field,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

if __name__ == "__main__": #{
  if len(sys.argv) != 4: #{
    print "usage: python plot_field_xz.py timestep y-index field"
    print " e.g., python plot_field_xz.py 300 480 Bx"
  else: #}{
    plot_field_xz(sys.argv[1],sys.argv[2],sys.argv[3])
  #}
#}
