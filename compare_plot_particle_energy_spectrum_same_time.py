import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import optimize
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import comparison, get_comparison_Avar_as_Bvar,get_comparison_stats
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe
from fractions import Fraction

small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)

"""
This script will plot the particle distribution of every simulation in the comparison
at the specified times...either the same light-crossing time or (if tequivalent) at
equivalent times where the same amount of energy has been injected to each simulation.

The particle distribution can be compensated to \gamma^compensate_index, where
compensate_index is a Fraction.
"""


def compare_plot_particle_energy_spectrum_same_time(category, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    species = kwargs.get("species", "electrons")
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    time_to_plot = kwargs.get("time_to_plot", 76)
    tequivalent = False
    to_compensate = kwargs.get("to_compensate", False)
    if to_compensate:
        compensate_index = kwargs.get("compensate_index", Fraction(3,1))
    # NOTE this times must be indices! Since this script plots the same times.
    # The L/c should be the same but the timestep will be different for box sizes
    if "final_Einj" in kwargs and time_to_plot == "teq":
        tequivalent = True
        time_string = "teq"
        final_Einj = kwargs.get("final_Einj")
    else:
        time_to_plot = int(time_to_plot)
        time_string = "t{:d}".format(time_to_plot)

    if species not in ['electrons', 'ions']:
        print("Error: species must be one of electrons or ions")
        return

    configs = comparison(category).configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/particle_energy_spectrum_same_time/" + species + "/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + species + "_" + time_string
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    if to_compensate:
        comp_str = str(compensate_index).replace('/','')
        figname += "_compensated" + comp_str
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    labels = comparison(category).labels
    colors = itertools.cycle(comparison(category).colors)
    linestyles = itertools.cycle(comparison(category).linestyles)
    markers = itertools.cycle(comparison(category).markers)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    plt.figure()
    plt.xlabel(r'$\gamma$')
    ylabel = r"$\gamma$"
    if to_compensate:
        ylabel += r"$^{" + str(1 + Fraction(compensate_index)) + "}$"
    ylabel += "$~f(\gamma)$"
    plt.ylabel(ylabel)
    if ylims is not None:
        plt.ylim(ylims)
    else:
        if to_compensate:
            plt.ylim([1e2, 1e7])
        else:
            plt.ylim([1e-10, 1e-2])
    if xlims is not None:
        plt.xlim(xlims)
    else:
        plt.xlim([10, 3e5])
    title = species + " spectrum" + "\n"
    equiv_string = "at equivalent times "

    consts = rdu.Consts
    for i, sim_name in enumerate(configs):
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, system_config)

        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        Lx = params["xmax"] - params["xmin"]
        Ly = params["ymax"] - params["ymin"]
        Lz = params["zmax"] - params["zmin"]
        times_in_LC = retrieve_time_values(ph)[:, 2]

        # Get equivalent time step
        if tequivalent:
            einj = np.loadtxt(ph.path_to_reduced_data + "Einj.dat")
            einj_cum = np.cumsum(einj)
            dt_in_s = params["dt"]
            volume = Lx*Ly*Lz
            mag_energy_density = (params["B0"])**2.0/(8.0*np.pi)
            einj_norm = einj_cum/volume/mag_energy_density
            dt_in_LC = dt_in_s*rdu.Consts.c/Lz
            einj_times_in_LC = np.cumsum(dt_in_LC*np.ones(einj.shape))
            einj_times_in_LC = np.insert(einj_times_in_LC, 0, 0)
            einj_times_in_LC = einj_times_in_LC[:-1]
            equivalent_time_ind = (np.abs(-1.0*einj_norm - final_Einj)).argmin()
            einj_equiv_time = einj_times_in_LC[equivalent_time_ind]
            equivalent_time_ind = (np.abs(times_in_LC - einj_equiv_time)).argmin()
            # print(equivalent_time_ind)
            time_to_plot = equivalent_time_ind
            equiv_string += "{:.2f}, ".format(times_in_LC[equivalent_time_ind])
        else:
            time_in_LC = times_in_LC[time_to_plot]
        os.chdir(ph.path_to_reduced_data + "particle_spectra/")
        timesteps = retrieve_time_values(ph)[:, 0]
        n = int(timesteps[time_to_plot])

        # load spectrum
        (dNdg, uBinEdges, step
        ) = rdu.getPtclEnergySpectrum(n, species, "bg", "gamma")
        # gamma-array
        gEdges = np.sqrt(uBinEdges**2+1.)
        gamma_values = 0.5*(gEdges[:-1] + gEdges[1:])
        # cut off zero values
        end_ind = np.where(dNdg == 0)[0][0]
        gamma_values = gamma_values[:end_ind]
        dNdg = dNdg[:end_ind]
        # plt.loglog(gamma_values, ys, lw=2, ls=next(linestyles), marker=next(markers), label = labels[i], color=next(colors))
        if to_compensate:
            plot_dNdg = gamma_values*dNdg*(gamma_values)**(compensate_index)
        else:
            plot_dNdg = gamma_values*dNdg
        plt.loglog(gamma_values, plot_dNdg, lw=2, label = labels[i], color=next(colors), path_effects=[mpe.Stroke(linewidth=3, foreground='k'), mpe.Normal()])


    # Fit Maxwell-Juettner, only low- to medium-energies
    max_fit_ind = dNdg.argmax()
    max_params, max_e = optimize.curve_fit(maxwellJuettner, gamma_values[:max_fit_ind+10], dNdg[:max_fit_ind+10])
    max_fine_gamma = np.linspace(gamma_values[0], gamma_values[-1], 1000)
    print(max_params)
    fit_maxwellian = maxwellJuettner(max_fine_gamma, *max_params)

    plt.gca().axvline([3.0*max_params[1]], color='green', linestyle='-.')
    cut_gamma_values = gamma_values[45:-14]
    if not to_compensate:
        plt.loglog(max_fine_gamma, max_fine_gamma*fit_maxwellian, 'k--')
        # Plot \gamma f(gamma) \propto \gamma \gamma^{-3}
        # plt.loglog(cut_gamma_values, 1.5e5*cut_gamma_values**(-2.7), 'k:')
        if tequivalent:
            plt.loglog(cut_gamma_values, 5.5e5*cut_gamma_values**(-2.0), 'k:')
        else:
            plt.loglog(cut_gamma_values, 1.5e6*cut_gamma_values**(-2.0), 'k:')
            # Use below for late times only
            # plt.loglog(cut_gamma_values, 1.0e4*cut_gamma_values**(-1.7), 'k-.')
    else:
        # cut_gamma_values = gamma_values[58:-10]
        plt.gca().axhline([1e6], color='black', linestyle=':')
        plt.gca().loglog(cut_gamma_values, 1.2e5*cut_gamma_values**(-1.7+compensate_index), color='black', linestyle='-.')

    gamma_max = Lz*consts.e*params["B0"]/(2.0*consts.m_e*consts.c**2.0)
    plt.gca().axvline([gamma_max], color='green', linestyle=':')

    title += category + "\n"
    if tequivalent:
        title += equiv_string
    else:
        title += r"$t=${:.2f} $L/c$".format(time_in_LC)
    plt.title(title)
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')

    sim_set = comparison(category)
    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False, ncol=sim_set.ncol)
    else:
        plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)

    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()


def maxwellJuettner(gamma_values, A, Theta):
    # NOTE: need gamma^2 because getPtclAngularSpectrumIntegratedOverAngle does not
    # include the radius^2 when integrating over 3D
    return A*gamma_values**2.0/Theta**3.0*np.exp(-gamma_values/Theta)


if __name__ == '__main__':
    # ------- inputs ---------
    category = "xiAllboxSizeAll-comparison"
    category = "xi10boxSize-comparison"
    category = "768Imbalance-comparison"
    # category = "modeNum-comparison"
    stampede2 = False
    system_config = "lia_hp"
    species = 'electrons'
    plot_final_Einj = True
    to_compensate = False
    compensate_index = Fraction(2,1)
    if to_compensate:
        xlims = [5e2, 2e5]
        ylims = [1.0e5, 1e7]
    else:
        xlims = [5e1, 2e5]
        ylims = [1.0e-5, 1e-0]

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["species"] = species
    kwargs["time_to_plot"] = 30
    kwargs["stampede2"] = stampede2
    kwargs["system_config"] = system_config
    kwargs["to_compensate"] = to_compensate
    kwargs["compensate_index"] = compensate_index

    # get minimum final Einj
    if plot_final_Einj:
        stats = get_comparison_stats(category, **kwargs)
        final_einj_densities = stats.loc["einj_density_at_20_Lc"]
        final_Einj = final_einj_densities.min()
        kwargs["final_Einj"] = final_Einj
        kwargs["time_to_plot"] = "teq"

    compare_plot_particle_energy_spectrum_same_time(category, **kwargs)
    compare_plot_particle_energy_spectrum_same_time(category, **kwargs, xlims=xlims, ylims=ylims)
