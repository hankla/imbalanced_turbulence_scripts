import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import comparison
import matplotlib.pylab as pl
import itertools
"""
This script will plot the temporal profiles of every given quantity
at the location (0, 0, 0) or averaged over two axes.

Useful for diagnostics. Not used in Hankla+ 2021.
"""

def compare_plot_profiles_temporal(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    cut_initial = kwargs.get("cut_initial", False)
    match_times = kwargs.get("match_times", False)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = kwargs.get("fields_to_profile", ["Ex", "By", "vely_total"])
    max_scaling = kwargs.get("max_scaling", False)
    average_2d = kwargs.get("average_2d", False)

    configs = comparison(category).configs
    labels = comparison(category).labels
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    times_to_plot = kwargs.get("times_to_plot", retrieve_time_values(ph0)[:,0])

    figbase = ph0.path_to_figures + "../compare/" + category + "/profiles-1d_temporal/"
    figure_name = kwargs.get("figure_name", None)
    if average_2d:
        figbase += "2d_averaged/"
    if match_times:
        figbase += "matched_times/"
    if cut_initial:
        figbase += "noT0/"
    if max_scaling:
        figbase += "scaled_to_max/"
    if not os.path.isdir(figbase):
        os.makedirs(figbase)

    colors = comparison(category).colors

    smallest_end_time = None
    if figure_name == "show":
        matplotlib.use("TkAgg")

    for field in fields_to_plot:
        linestyles = itertools.cycle(comparison(category).linestyles)
        markers = itertools.cycle(comparison(category).markers)
        norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.figure()
        plt.xlabel("$tc/L$")
        ylabel = field_abbrev + norm_value[1]
        plt.ylabel(ylabel)

        for i, sim_name in enumerate(configs):
            ph = path_handler(sim_name, system_config)

            times_in_LC = retrieve_time_values(ph)[:, 2]
            if smallest_end_time is None or times_in_LC[-1] < smallest_end_time:
                smallest_end_time = times_in_LC[-1]
            profile_data = retrieve_profiles_temporal(ph, fields_to_profile=[field], average_2d=average_2d)
            if average_2d:
                chosen_profile = np.array(profile_data[field]["xAyAz0"])
            else:
                chosen_profile = np.array(profile_data[field]["x0y0z0"])

            if cut_initial:
                times_in_LC = times_in_LC[1:]
                chosen_profile = chosen_profile[1:]

            if max_scaling:
                max_value = np.max(chosen_profile/norm_value[0])
                chosen_profile = chosen_profile/max_value

            if chosen_profile.size == times_in_LC.size + 1:
                chosen_profile = chosen_profile[:-1]
            plt.plot(times_in_LC, chosen_profile/norm_value[0], label=labels[i], marker=next(markers), ls=next(linestyles), markersize=2, color=colors[i] )

        if match_times:
            plt.xlim([0.0, smallest_end_time])
        plt.legend()
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        figname = figbase + field + ".png"
        figure_name = kwargs.get("figure_name", figname)
        titstr = ""
        if cut_initial:
            titstr += "$t=0$ removed"
        if match_times:
            titstr += " xlim adjusted"
        if max_scaling:
            titstr += " scaled to max value "
        if average_2d:
            titstr += "\n profile at $z=0$, averaged over $x$ and $y$, over time"
        else:
            titstr += "\n profile at $x=0$, $y=0$, $z=0$ over time"
        titstr += "\n" + category
        plt.title(titstr)
        plt.tight_layout()
        if figure_name != "show":
            print("Saving " + figname)
            plt.savefig(figname, bbox_inches="tight")
            plt.gca().grid(False, which='both')
            plt.savefig(figure_name.replace(".png", "_nogrid.png"))
            plt.close()
    if figure_name == "show":
        plt.show()


if __name__ == '__main__':
    category = "dcorr-comparison"
    category = "im09-comparison"
    # category = "ppc-comparison"
    category = "A0-comparison"
    # category = "res-comparison"
    # category = "boxSize-comparison"
    system_config = "lia_hp"
    stampede2 = False
    match_times = True
    to_show = False
    average_2d = True

    # -------------------------------------------------
    kwargs = {}
    kwargs["system_config"] = system_config
    kwargs["match_times"] = match_times
    kwargs["average_2d"] = average_2d
    if to_show:
        kwargs["figure_name"] = "show"

    compare_plot_profiles_temporal(category, **kwargs)
