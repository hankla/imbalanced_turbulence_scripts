
import numpy
import math
from mayavi.mlab import *
from mayavi import mlab
import sys

def plot3D_bulk(it,spec):

    if it=='':
       it='0'

    if spec=='':
       spec='electrons'
       
    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]

    #===============================================================================
    # The grid
    x=numpy.loadtxt(".././data/x.dat")
    y=numpy.loadtxt(".././data/y.dat")
    z=numpy.loadtxt(".././data/z.dat")

    # Particle density
    nd=numpy.loadtxt(".././data/densities/mapxyz_"+spec+"_drift"+it+".dat")
    nb=numpy.loadtxt(".././data/densities/mapxyz_"+spec+"_bg"+it+".dat")

    # Particle flux component Fx
    Fxd=numpy.loadtxt(".././data/densities/Fx_"+spec+"_drift"+it+".dat")
    Fxb=numpy.loadtxt(".././data/densities/Fx_"+spec+"_bg"+it+".dat")

    # Particle flux component Fy
    Fyd=numpy.loadtxt(".././data/densities/Fy_"+spec+"_drift"+it+".dat")
    Fyb=numpy.loadtxt(".././data/densities/Fy_"+spec+"_bg"+it+".dat")

    # Particle flux component Fz
    Fzd=numpy.loadtxt(".././data/densities/Fz_"+spec+"_drift"+it+".dat")
    Fzb=numpy.loadtxt(".././data/densities/Fz_"+spec+"_bg"+it+".dat")
    
    #===============================================================================
    
    nx=len(x)
    ny=len(y)/2
    nz=len(z)

    dx,ptx=x[1]-x[0],nx*1j
    dy,pty=y[1]-y[0],ny*1j
    dz,ptz=z[1]-z[0],nz*1j

    x,y,z=numpy.ogrid[-dx:dx:ptx,-dy:dy:pty,-dz:dz:ptz]
    
    bulkx=numpy.empty((nx,ny,nz))
    bulky=numpy.empty((nx,ny,nz))
    bulkz=numpy.empty((nx,ny,nz))
    gam_bulk=numpy.empty((nx,ny,nz))

    for ix in range(0,nx):
        for iy in range(0,ny):
            for iz in range(0,nz):
            
                ntot=nd[iy+iz*ny*2,ix]+nb[iy+iz*ny*2,ix]
                Fxtot=Fxd[iy+iz*ny*2,ix]+Fxb[iy+iz*ny*2,ix]
                Fytot=Fyd[iy+iz*ny*2,ix]+Fyb[iy+iz*ny*2,ix]
                Fztot=Fzd[iy+iz*ny*2,ix]+Fzb[iy+iz*ny*2,ix]
                
                bulkx[ix,iy,iz]=Fxtot/ntot/2.9979246e+10
                bulky[ix,iy,iz]=Fytot/ntot/2.9979246e+10
                bulkz[ix,iy,iz]=Fztot/ntot/2.9979246e+10
                
                bulk_tot=numpy.sqrt(bulkx[ix,iy,iz]*bulkx[ix,iy,iz]+bulky[ix,iy,iz]*bulky[ix,iy,iz]+bulkz[ix,iy,iz]*bulkz[ix,iy,iz])
                
                gam_bulk[ix,iy,iz]=1.0/numpy.sqrt(1.0-bulk_tot*bulk_tot)
    
    #===============================================================================
    # Plot gamma_tot
    
    contour3d(bulkz,contours=10,transparent=True,opacity=0.9)
    mlab.outline()
    mlab.colorbar(title='Beta',orientation='vertical',nb_labels=10)
    mlab.axes()
    mlab.title(it+"/"+spec)
    
    #===============================================================================
    
    mlab.show()
    
    #===============================================================================

plot3D_bulk(sys.argv[1],sys.argv[2])
