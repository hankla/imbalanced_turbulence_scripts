import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from path_handler_class import *
from reduce_data_utils import *
import raw_data_utils as rdu
from compare_handler import *

"""
This script will plot the fraction of particles with vz > 0
"""

def plot_positive_vz_fraction_over_energy(ph, **kwargs):
    sim_name = ph.sim_name
    overwrite_data = kwargs.get("overwrite_data", False)
    to_tavg = kwargs.get("to_tavg", False)
    vz_over_avg = kwargs.get("vz_over_avg", True)
    xlims = kwargs.get("xlim", None)
    ylims = kwargs.get("ylim", None)

    gamma_start_ind = kwargs.get("gamma_start_ind", 0)
    gamma_end_ind = kwargs.get("gamma_end_ind", -1)

    # Times in L/c
    tinitial = kwargs.get("tinitial", 0.0)
    tfinal = kwargs.get("tfinal", 20.0)
    titstr = sim_name + "\n"

    # ------------------------------------------------
    time_values = retrieve_time_values(ph)
    times_in_LC = time_values[:, 2]
    tinitial_ind = (np.abs(tinitial - times_in_LC)).argmin()
    tfinal_ind = (np.abs(tfinal - times_in_LC)).argmin()
    colors = pl.cm.cividis(np.linspace(0, 1, tfinal_ind - tinitial_ind))
    t_save_str = "_t{:d}-{:d}".format(int(tinitial), int(tfinal))
    t_tit_str = ""
    if to_tavg:
        t_tit_str += "Averaged "
    t_tit_str += "from t={:.2f} - {:.2f} L/c".format(tinitial, tfinal)
    titstr += t_tit_str

    # Grab gamma bins
    gamma_centers = load_gamma_centers(ph)

    # make energy plot
    figdir = ph.path_to_figures + "positive_vz_fraction/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    if gamma_end_ind == -1:
        gamma_end_ind = np.size(gamma_centers)
    figname = figdir + "positive_vz_fraction_over_Energy"
    figname += t_save_str
    if to_tavg:
        figname += "Avg"
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)
    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()


    # Extract relevant info from pvz dictionary
    pvz_dict = retrieve_pvz_fraction_over_time(ph, overwrite_data)
    total_particles = np.zeros(gamma_centers.size)
    total_pvz_particles = np.zeros(gamma_centers.size)
    total_pvz_fraction_over_energy = np.zeros(gamma_centers.size)
    for tind in np.arange(tinitial_ind, tfinal_ind):
        for gamma_ind in np.arange(0, gamma_centers.size - 1):
            gamma_str = "gamma{:d}".format(gamma_ind)
            total_particles[gamma_ind] = pvz_dict[gamma_str][tind, 1]
            total_pvz_particles[gamma_ind] = pvz_dict[gamma_str][tind, 0]

        pvz_fraction_over_energy = total_pvz_particles/total_particles
        if not to_tavg:
            plt.plot(gamma_centers, pvz_fraction_over_energy, color=colors[tind - tinitial_ind])
        else:
            total_pvz_fraction_over_energy += pvz_fraction_over_energy
    if to_tavg:
        plt.plot(gamma_centers, total_pvz_fraction_over_energy/(tfinal_ind - tinitial_ind))
    else:
        plt.colorbar(matplotlib.cm.ScalarMappable(cmap='cividis'))

    plt.gca().axhline([0.5], color='black', ls='--')
    plt.gca().grid(color='.9', ls='--')
    plt.xscale('log')
    if ylims is not None:
        plt.ylim(ylims)
    if xlims is not None:
        plt.xlim(xlims)

    plt.title(titstr)
    plt.xlabel(r"$\gamma$")
    ylabel = "Fraction of particles with"
    if vz_over_avg:
        ylabel += r" $v_z>\langle v_z\rangle$"
    else:
        ylabel += r" $v_z > 0$"
    plt.ylabel(ylabel)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()


if __name__ == '__main__':

    # ------- inputs ---------
    system_config = "lia_hp"
    # system_config = "stampede2"
    overwrite_data = False
    vz_over_avg = False

    # Times in L/c
    tinitial = 10.0
    tfinal = 20.0

    to_tavg = True
    xlims = None
    ylims = None
    # xlims = [1e2, 2e3]
    ylims = [0, 1]

    # sim_name = "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s2"
    # sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    sim_name = "zeltron_384cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD360-seedstudy5"
    configs = [sim_name]
    # category = "768Imbalance-comparison"
    # configs = comparison(category,reduced_data_path="/work2/06165/ahankla/stampede2/imbalanced_turbulence/data_reduced/", setup="stampede2").configs[-1:]
    # configs = [comparison(category).configs[0], comparison(category).configs[-1]]

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["tinitial"] = tinitial
    kwargs["tfinal"] = tfinal
    kwargs["to_tavg"] = to_tavg
    kwargs["xlim"] = xlims
    kwargs["ylim"] = ylims
    kwargs["vz_over_avg"] = vz_over_avg

    for sim_name in configs:
        ph = path_handler(sim_name, system_config)
        print(sim_name)
        plot_positive_vz_fraction_over_energy(ph, **kwargs)
