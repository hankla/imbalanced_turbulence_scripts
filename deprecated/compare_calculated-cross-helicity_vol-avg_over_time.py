import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""




DEFUNCT
DO NOT USE UNTIL REVISED





This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""
stampede2 = False

configs = ["zeltron_96cube_single-mode_master", "zeltron_96cube_single-mode_s0", "zeltron_96cube_balanced_master", "zeltron_96cube_balanced_s0"]
labels = ["Single mode (run 1)", "Single mode (run 2)", "Balanced (run 1)", "Balanced (run 2)"]
# configs = ["zeltron_96cube_single-mode_master", "zeltron_96cube_balanced_master"]
# labels = ["Single mode, OLA forcing", "Balanced (8 modes), OLA forcing"]
colors = ["C01", "C01", "C02", "C02"]
linestyles = ["-", ":", "-.", "--"]
markers = ["o", "x", "s", "d"]

sim_name0 = "zeltron_96cube_single-mode_master"
ph0 = path_handler(sim_name0, "lia_hp")
consts = rdu.Consts
(params, units) = rdu.getSimParams(simDir=ph0.path_to_reduced_data)
B0=params["B0"]

figdir = ph0.path_to_figures + "../compare/volAvg-over-time/"
if not os.path.isdir(figdir):
    os.makedirs(figdir)
figure_name = figdir + "deltaVdotB"

for i, sim_name in enumerate(configs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")
    vol_avgs = retrieve_variables_vol_avg(ph)
    times_in_LC = retrieve_time_values(ph)[:, 2]


    # ----------------------------------------------
    # Now calculate <v dot dB> = < v dot B > - B0<v_z>
    vB = vol_avgs["VdotB_total mean"]
    vz = vol_avgs["velz_total mean"]
    full_vdB = vol_avgs["VdotdB_total mean"]
    calculated_vdB = vB - B0*vz
    # Check if equal
    print(np.isclose(full_vdB, calculated_vdB))

    # Now < dv dot B > = < dv dot dB > = < v dot dB > - <v> dot <dB>
    vx = vol_avgs["velx_total mean"]
    vy = vol_avgs["vely_total mean"]
    dBx = vol_avgs["Bx mean"]
    dBy = vol_avgs["By mean"]
    dBz = vol_avgs["Bz mean"] - B0
    vdB = vx*dBx + vy*dBy + vz*dBz
    dvdB = full_vdB - vdB

    plt.plot(times_in_LC, dvdB/B0, label=labels[i], ls=linestyles[i], marker=markers[i], markersize=4, color=colors[i])

    plt.xlabel("$tc/L$")
    plt.ylabel(r"$\langle \delta v\cdot B\rangle$")

plt.legend()
plt.axhline([0.0], c="k", ls="--")
plt.gca().grid(color='.9', ls='--')
plt.tight_layout()
plt.savefig(figure_name)
root_name = figure_name.split("/")[-1]
if not os.path.isdir(figdir + "pdfs/"):
    os.makedirs(figdir + "pdfs/")
plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

plt.gca().grid(False, which='both')
if not os.path.isdir(figdir + "pdfs/nogrids/"):
    os.makedirs(figdir + "pdfs/nogrids/")
plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
if not os.path.isdir(figdir + "nogrids/"):
    os.makedirs(figdir + "nogrids/")
plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
plt.show()
