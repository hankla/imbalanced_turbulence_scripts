import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from plot_profiles_spatial import plot_profiles_spatial

"""
This script will plot profiles of Ex and By over the chosen times.
In addition, it will fit the profiles to a sine in order to extract
the phase difference between the magnetic and electric fields,
the amplitude ratio of electric/magnetic fields,
and the phase over time, which can be used to find omega.

TO DO:
- make compatible with not 0,0,0 indices
"""


def compare_phase_over_time(ph, **kwargs):
    """
    This script will take the amplitude and phase from Ex-By profiles
    (note must run the fits first!)
    and plot the amplitude ratio and the phase difference over time.
    These plots are useful to check analytic predictions of e.g.
    Alfven waves.

TO DO:
    - retrieve data instead of reading entire slices
    - savefig vs. show
    """
    to_show = kwargs.get("to_show", False)
    output_values_path = ph.path_to_reduced_data + "Ex-By-vy_profile_fits.txt"
    if not os.path.exists(output_values_path):
        print("ERROR: must plot profiles first! set plot_profiles_fit = True")
    output_values = np.loadtxt(output_values_path, skiprows=1, delimiter=",")
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    consts = rdu.Consts
    rho = consts.c/params["omegac"]
    Ex_norm_value = rdu.getNormValue("Ex", simDir=ph.path_to_reduced_data)
    Ex_field_abbrev = rdu.getFieldAbbrev("Ex", True)
    By_norm_value = rdu.getNormValue("By", simDir=ph.path_to_reduced_data)
    By_field_abbrev = rdu.getFieldAbbrev("By", True)
    vy_norm_value = rdu.getNormValue("vely_total", simDir=ph.path_to_reduced_data)
    vy_field_abbrev = rdu.getFieldAbbrev("vely_total", True)

    time_steps = output_values[:, 0]
    time_values = retrieve_time_values(ph) # LC: [:, 2]
    indices = np.where(time_values[:, 0] == time_steps)[0]
    times_in_LC = time_values[indices, 2]

    ex_amp = output_values[:, 1]
    by_amp = output_values[:, 3]
    vy_amp = output_values[:, 5]
    ex_phase = output_values[:, 2]
    by_phase = output_values[:, 4]
    vy_phase = output_values[:, 6]
    exby_phase_diff = np.abs(ex_phase - by_phase)
    exvy_phase_diff = np.abs(ex_phase - vy_phase)
    exby_amp_ratio = ex_amp/by_amp
    exvy_amp_ratio = ex_amp/vy_amp

    ls = ["-", "--", ":"]

    if to_show:
        matplotlib.use('TkAgg')
    figdir = ph.path_to_figures + "Ex-By-vy_predictions/"
    if not os.path.exists(figdir):
        os.makedirs(figdir)

    plt.figure()
    plt.plot(times_in_LC, exby_phase_diff, ls=ls[0], label=r"$|\phi_{E_x}-\phi_{B_y}|$")
    plt.plot(times_in_LC, exvy_phase_diff, ls=ls[1], label=r"$|\phi_{E_x}-\phi_{v_y}|$")
    plt.xlabel("$tc/L$")
    plt.ylabel(r"Phase Difference")
    plt.title(ph.sim_name)
    plt.legend()
    plt.tight_layout()
    figname_phasediff = figdir + "Ex-By-vy_phase-difference_over_time.png"
    if not to_show:
        print("saving: " + figname_phasediff)
        plt.savefig(figname_phasediff)


    plt.figure()
    plt.plot(times_in_LC, ex_phase, label=r"$E_x$", ls=ls[0])
    plt.plot(times_in_LC, by_phase, label=r"$B_y$", ls=ls[1])
    plt.plot(times_in_LC, vy_phase, label=r"$v_y$", ls=ls[2])
    plt.xlabel("$tc/L$")
    plt.ylabel("Phase")
    plt.title(ph.sim_name)
    plt.legend()
    plt.tight_layout()
    figname_phase = figdir + "Ex-By-vy_phase_over_time.png"
    if not to_show:
        print("saving: " + figname_phase)
        plt.savefig(figname_phase)

    plt.figure()
    plt.plot(times_in_LC, exby_amp_ratio, ls=ls[0], label=r"$E_x/B_y$")
    plt.plot(times_in_LC, exvy_amp_ratio, ls=ls[1], label=r"$E_x/v_y$")
    plt.xlabel("$tc/L$")
    plt.ylabel(r"Amplitude ratio")
    plt.title(ph.sim_name)
    plt.legend()
    plt.tight_layout()
    figname_ampratio = figdir + "Ex-By-vy_amp-ratio_over_time.png"
    if not to_show:
        print("saving: " + figname_ampratio)
        plt.savefig(figname_ampratio)

    plt.figure()
    plt.plot(times_in_LC, ex_amp, ls=ls[0], color="tab:blue", label=Ex_field_abbrev+Ex_norm_value[1])
    plt.plot(times_in_LC, by_amp, ls=ls[1], color="tab:orange", label=By_field_abbrev+By_norm_value[1])
    plt.plot(times_in_LC, vy_amp, ls=ls[2], color="tab:green", label=vy_field_abbrev+vy_norm_value[1])
    plt.xlabel("$tc/L$")
    plt.title(ph.sim_name)
    plt.legend()
    plt.tight_layout()
    figname_amp = figdir + "Ex-By-vy_amp_over_time.png"
    if not to_show:
        print("saving: " + figname_amp)
        plt.savefig(figname_amp)

    if to_show:
        plt.show()


if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    # sim_name = "zeltron_96cube_balanced_s0"
    # sim_name = "zeltron_96cube_single-mode_s0"
    # sim_name = "zeltron_96cube_balanced_master"
    # sim_name = "zeltron_96cube_single-mode_master"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_standing"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_travelling"
    # sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    sim_name = "zeltron_96cube_z-mode-decay-dcorr0sigma05A025_s0-Nd60"

    # kwargs
    ix = 0; iy = 0; iz = 0
    overwrite = False
    to_show = False
    plot_profiles_fit  = True

    # ------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    time_steps = retrieve_time_values(ph)[:, 0]
    times_to_plot = time_steps[:]

    # ----------------------------
    # No editing from here on.
    # ----------------------------
    kwargs = {}
    kwargs["selected_fields"] = ["Ex", "By", "vely_total"]
    kwargs["to_output"] = True
    kwargs["to_fit"] = True
    kwargs["overwrite_data"] = overwrite
    kwargs["ix"] = ix
    kwargs["iy"] = iy
    kwargs["iz"] = iz
    kwargs["to_show"] = to_show

    if plot_profiles_fit:
        plot_profiles_spatial(ph, times_to_plot, **kwargs)
    compare_phase_over_time(ph, **kwargs)
