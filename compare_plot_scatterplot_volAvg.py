import numpy as np
import sys
sys.path.append("./modules/")
import os
from raw_data_utils import *
from compare_handler import *
from reduce_data_utils import *
from path_handler_class import *
from scipy import interpolate
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import matplotlib.patheffects as mpe
from scipy import integrate

def compare_plot_scatterplot_volAvg(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    scatter_vars = kwargs.get("scatter_variables", ["ExB_z", "Upz_total"])
    sim_set = comparison(category)
    configs = sim_set.configs
    system_config = kwargs.get("system_config", "lia_hp")
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    tinitial = kwargs.get("tinitial", 0.0)
    tfinal = kwargs.get("tfinal", 20.0)
    times_string = "t{:d}-{:d}".format(int(tinitial), int(tfinal))
    fit_scatterplot = kwargs.get("fit_scatterplot", False)
    same_norm = kwargs.get("same_norm", False)

    sim_name0 = configs[0]
    ph0 = path_handler(sim_name0, system_config)
    x_norm = rdu.getNormValue(scatter_vars[0], simDir=ph0.path_to_reduced_data)
    y_norm = rdu.getNormValue(scatter_vars[1], simDir=ph0.path_to_reduced_data)
    x_abbrev = rdu.getFieldAbbrev(scatter_vars[0], True)
    y_abbrev = rdu.getFieldAbbrev(scatter_vars[1], True)

    # Figure set-up
    if "figure_name" in kwargs and kwargs["figure_name"] == "show":
        matplotlib.use("TkAgg")
    fig1 = plt.figure()
    ax1= plt.gca()
    fig2 = plt.figure()
    ax2= plt.gca()
    title_str = category + "\n Vol avg"
    ax1.grid(color='.9', ls='--')
    ax1.grid(which='minor', color='.9', ls='--')
    ax1.tick_params(top=True, right=True, direction='in', which='both')
    ax2.grid(color='.9', ls='--')
    ax2.grid(which='minor', color='.9', ls='--')
    ax2.tick_params(top=True, right=True, direction='in', which='both')

    if fit_scatterplot:
        x_fits = np.array([])
        y_fits = np.array([])

    for i, sim_name in enumerate(configs):
        ph = path_handler(sim_name, system_config)
        params, units = getSimParams(simDir=ph.path_to_reduced_data)
        Lz = params["zmax"] - params["zmin"]
        sigmaDict = rdu.getSimSigmas(params)
        sigma = sigmaDict["sigma"]
        vA0 = np.sqrt(sigma/(sigma + 1.0))
        ylabel = r"$\langle$" + y_abbrev + r"$\rangle c^2/(\langle$" + x_abbrev + r"$\rangle$"
        if not os.path.exists(ph.path_to_reduced_data + "magnetization.dat"):
            plot_length_scales(ph)
        mag_values = np.loadtxt(ph.path_to_reduced_data + "magnetization.dat", delimiter=",", skiprows=1)
        magnetization = mag_values[:, 1]
        vA_over_time = np.sqrt(magnetization/(magnetization + 1.0))
        all_times_LC = mag_values[:, 0]
        tA_vals = integrate.cumtrapz(vA_over_time, all_times_LC)

        B0 = params["B0"]
        (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs, fields_to_vavg=scatter_vars)
        quantity_x = vol_means[scatter_vars[0]]/x_norm[0] # ExB
        quantity_y = vol_means[scatter_vars[1]]/y_norm[0] # Upz
        if same_norm:
            g0 = B0**2.0/(4*np.pi*rdu.Consts.c)
            mom_density = quantity_x*x_norm[0]/(4*np.pi*rdu.Consts.c) # ExB/(4pi c)
            quantity_x = mom_density/g0
            quantity_y = quantity_y * y_norm[0]/g0
            x_abbrev = r"$S_z$"
            y_norm = (y_norm[0], r"$/g_0$")
            x_norm = (x_norm[0], r"$/c^2g_0$")

        times_in_LC = retrieve_time_values(ph)[:, 2]
        tinitial_ind = (np.abs(tinitial - times_in_LC)).argmin()
        tfinal_ind = (np.abs(tfinal - times_in_LC)).argmin()
        times_in_LC = times_in_LC[tinitial_ind:tfinal_ind]
        quantity_x = quantity_x[tinitial_ind:tfinal_ind]
        quantity_y = quantity_y[tinitial_ind:tfinal_ind]
        to_VA = []
        tA_vals_sparse = []
        for t in times_in_LC:
            t_ind = (np.abs(mag_values[:, 0] - t)).argmin()
            to_VA.append(vA_over_time[t_ind])
            tA_vals_sparse.append(tA_vals[t_ind])
        to_VA = np.array(to_VA)
        tA_vals_sparse = np.array(tA_vals_sparse)
        # Normalize to vA(t)
        # ylabel += "$t v_A(t)/L)$"
        # to_VA = 1.0
        # Normalize to vA0
        ylabel += "$t v_{A0}/L)$"
        to_VA = vA0
        times = to_VA * times_in_LC

        # Normalize to tA = Alfven age
        # times = tA_vals_sparse
        # ylabel += "$t_A(t))$"

        if fit_scatterplot:
            x_fits = np.append(x_fits, quantity_x)
            y_fits = np.append(y_fits, quantity_y)

        plt.figure(fig1.number)
        plt.scatter(quantity_x, quantity_y, color=sim_set.colors[i], marker=sim_set.markers[i], edgecolors='black', linewidths=0.5)
        # plt.scatter(quantity_x, quantity_y, color=sim_set.colors[i], marker=sim_set.markers[i], path_effects=[mpe.Stroke(linewidth=1.8, foreground='k'), mpe.Normal()])
        plt.figure(fig2.number)
        window_len = 9
        cut_ind = int(window_len/2)
        window = 'hanning'
        to_plot = quantity_y/(quantity_x*times)
        to_plot = smooth(to_plot, window_len, window)
        # Truncate to avoid weird window effects
        to_plot = to_plot[cut_ind:-cut_ind]
        # quantity_y = smooth(quantity_y, window_len, window)[cut_ind:-cut_ind]
        # quantity_x = smooth(quantity_x, window_len, window)[cut_ind:-cut_ind]
        # to_plot = quantity_y/(quantity_x*times)

        # times_in_LC = times_in_LC[1:-1]

        plt.plot(times_in_LC, to_plot, color=sim_set.colors[i], marker=sim_set.markers[i], path_effects=[mpe.Stroke(linewidth=2, foreground='k'), mpe.Normal()], markevery=int(to_plot.size/10))
        # plt.plot(times_in_LC, to_plot, color=sim_set.colors[i], lw=2)

    plt.title(title_str)
    plt.figure(fig1.number)
    if fit_scatterplot:
        linear_model = np.polyfit(np.array(x_fits), np.array(y_fits), 1)
        linear_model_func = np.poly1d(linear_model)
        fine_quantity_x = np.linspace(np.min(x_fits), np.max(x_fits), 100, endpoint=True)
        plt.plot(fine_quantity_x, linear_model_func(fine_quantity_x), 'k--')
        title_str += "\n Slope: {:.2f}. Intercept: {:.2f}".format(*linear_model_func)

    if sim_set.variable == "imbalance":
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    plt.title(title_str)
    plt.xlabel(r"$\langle$" + x_abbrev + r"$\rangle$" + x_norm[1])
    plt.ylabel(r"$\langle$" + y_abbrev + r"$\rangle$" + y_norm[1])
    plt.legend(frameon=False)
    if xlims is not None:
        plt.xlim(xlims)
    if ylims is not None:
        plt.ylim(ylims)
    plt.tight_layout()

    plt.figure(fig2.number)
    if sim_set.variable == "imbalance":
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    # ax2.axhline([0.0], color='black', ls="--")
    plt.xlabel(r"$tc/L$")
    plt.ylabel(ylabel)
    plt.legend(frameon=False)
    # if xlims is not None:
        # plt.xlim(xlims)
    # if ylims is not None:
    plt.xlim([times_in_LC[0], times_in_LC[-1]])
    plt.ylim([0.0, 2.5])
    plt.ylim([0.0, 1.8])
    plt.tight_layout()

    figdir = ph.path_to_figures + "../compare/" + category + "/scatterplots/" + scatter_vars[0] + "-" + scatter_vars[1] + "/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    fig_name = figdir + times_string
    fig_name += '_volAvg'
    if same_norm:
        fig_name += "_sameNorm"
    if xlims is not None:
        fig_name += "_xL"
        if ylims is not None:
            fig_name += "yL"
    elif ylims is not None:
        fig_name += "_yL"
    if fit_scatterplot:
        fig_name += '_fit'
    figure_name2 = fig_name + "_test.png"
    fig_name += ".png"
    figure_name = kwargs.get("figure_name", fig_name)

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.figure(fig1.number)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')

        plt.figure(fig2.number)
        plt.savefig(figure_name2, bbox_inches='tight')
        root_name = figure_name2.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

    plt.close()

    # plt.figure()
    # plt.plot(times_in_LC, tA_vals_sparse, label=r"$t_A(t)$")
    # plt.plot(times_in_LC, times_in_LC*vA0, 'k--', label=r"$tv_{A0}/L$")
    # plt.xlabel(r"$tc/L$")
    # plt.legend()
    # plt.gca().grid(color='.9', ls='--')
    # plt.gca().grid(which='minor', color='.9', ls='--')
    # plt.show()


def smooth(x, window_len=5, window='hanning'):
    s = np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    if window == 'flat': # moving average
        w=np.ones(window_len, 'd')
    else:
        w=eval('np.'+window+'(window_len)')

    y = np.convolve(w/w.sum(), s, mode='valid')
    return y


if __name__ == '__main__':
    overwrite_data = False
    scatter_variables = ["ExB_z", "Upz_total"]
    category = "768Imbalance-comparison"
    # category = "384im025seed-comparison"
    # category = "xiAllboxSizeExtremes-comparison"

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["scatter_variables"] = scatter_variables
    kwargs["xlims"] = [-0.7, 0.7]
    kwargs["ylims"] = [-4.5, 4.5]
    # kwargs["ylims"] = [-2.5, 2.5]
    # kwargs["ylims"] = [-0.2, 0.2]
    kwargs["fit_scatterplot"] = True
    kwargs["tinitial"] = 5.0
    kwargs["tfinal"] = 20.0
    kwargs["same_norm"] = True

    compare_plot_scatterplot_volAvg(category, **kwargs)
