#!/bin/bash
#SBATCH --time=15:00:00
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --job-name=vavg768elsasser
#SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_elsaesser_quantities.%j
#SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_elsaesser_quantities.%j
#SBATCH -A TG-PHY160032
#SBATCH --mail-type=ALL
#SBATCH --mail-user=lia.hankla@gmail.com
# #SBATCH --begin=now+3hours

module purge
module load intel/18.0.2
module load hdf5/1.10.4
module load python3/3.7.0

# script='plot_volume_averages'
script='save_elsaesser_quantities'
# script='plot_positive_vx_fraction_over_energy'
# script='plot_length_scales_over_time'

scriptpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/log_${script}_im00.txt"
fname="${script}.py"

cd ${scriptpath}
pwd
date
module list

python3 -u ${fname} > ${logpath}

date
