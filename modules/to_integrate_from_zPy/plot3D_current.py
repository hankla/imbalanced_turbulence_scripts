
import numpy
import math
from mayavi.mlab import *
from mayavi import mlab
import sys

def plot3D_current(it,current,spec):

    if it=='':
       it='0'

    if current=='':
       current='Jx'

    if spec=='':
       spec='electrons'

    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]
    
    #===============================================================================
    # The grid
    x=numpy.loadtxt(".././data/xfield.dat")
    y=numpy.loadtxt(".././data/yfield.dat")
    z=numpy.loadtxt(".././data/zfield.dat")
   
    # The current
    map_temp=numpy.loadtxt(".././data/currents/"+current+"_"+spec+"_"+it+".dat")

    nx=len(x)
    ny=len(y)/2
    nz=len(z)

    dx,ptx=x[1]-x[0],nx*1j
    dy,pty=y[1]-y[0],ny*1j
    dz,ptz=z[1]-z[0],nz*1j

    x,y,z=numpy.ogrid[-dx:dx:ptx,-dy:dy:pty,-dz:dz:ptz]

    mapxyz=numpy.empty((nx,ny,nz))

    for ix in range(0,nx):
        for iy in range(0,ny):
            for iz in range(0,nz):   
                mapxyz[ix,iy,iz]=map_temp[iy+iz*ny*2,ix]
         
    contour3d(mapxyz,contours=10,transparent=True,opacity=0.9)
    mlab.outline()
    mlab.colorbar(title=current,orientation='vertical',nb_labels=10)
    mlab.axes()
    mlab.title(it+"/"+current+"/"+spec)
    
    #===============================================================================

    mlab.show()
    
    #===============================================================================

plot3D_current(sys.argv[1],sys.argv[2],sys.argv[3])
