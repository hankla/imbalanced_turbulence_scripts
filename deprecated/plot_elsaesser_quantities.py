import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
This script will compare the volume-averaged conserved quantities:
energy and cross-helicity. From Schekochihin 2020 Eq. 3.4
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""

def plot_elsaesser_quantities(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    fields_to_vavg = ["pelsaesserMinusEnergy", "pelsaesserPlusEnergy", "pelsaesserDifference", "VpBp_total"]
    kwargs["fields_to_vavg"] = fields_to_vavg
    overwrite_data = kwargs.get("overwrite_data", False)
    plot_decay_time = kwargs.get("plot_decay_time", True)
    (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    sim_name = ph.sim_name

    consts = rdu.Consts
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)

    sigmaDict = rdu.getSimSigmas(params)
    B0 = params["B0"]
    sigma = sigmaDict["sigma"]

    # ----------------------------------------------
    # set up figure info
    figdir = ph.path_to_figures + "volAvg-over-time/means/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)

    # ----------------------------------------------
    # Plot the comparison of the +- energies first.
    figname = figdir + 'pelsaesserEnergyComparison.png'
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    norm_value = rdu.getNormValue("pelsaesserEnergy", simDir=ph.path_to_reduced_data)
    # Body of the plot.
    plt.plot(times_in_LC, vol_means["pelsaesserMinusEnergy"]/norm_value[0], markersize=3, marker="o", label=r"$\langle |Z_\perp^-|^2\rangle/c^2$")
    plt.gca().axhline([np.mean(vol_means["pelsaesserMinusEnergy"])], ls='--', color='black', label="Mean: {:.3f}".format(np.mean(vol_means["pelsaesserMinusEnergy"])))
    plt.plot(times_in_LC, vol_means["pelsaesserPlusEnergy"]/norm_value[0], markersize=3, marker="o", label=r"$\langle |Z_\perp^+|^2\rangle$/c^2")
    plt.gca().axhline([np.mean(vol_means["pelsaesserPlusEnergy"])], ls=':', color='black', label="Mean: {:.3f}".format(np.mean(vol_means["pelsaesserPlusEnergy"])))

    plt.gca().set_xlabel("$tc/L$")
    plt.gca().tick_params(axis='y')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name)
    plt.legend()

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        plt.savefig(figure_name)
    plt.close()

    # ----------------------------------------------
    # Now plot the ratio of the +- energies
    figname = figdir + 'pelsaesserEnergyRatio.png'
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    ratio = vol_means["pelsaesserPlusEnergy"]/vol_means["pelsaesserMinusEnergy"]
    plt.plot(times_in_LC, ratio, markersize=3, marker="o", label=r"$\langle |Z_\perp^+|^2\rangle/\langle |Z_\perp^-|^2\rangle$", ls='None')
    plt.gca().axhline([np.mean(ratio)], ls='--', color='black', label="Mean: {:.2f}".format(np.mean(ratio)))

    plt.gca().set_xlabel("$tc/L$")
    plt.gca().tick_params(axis='y')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name)
    plt.legend()

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        plt.savefig(figure_name)
    plt.close()

    # ----------------------------------------------
    # Now to check the normalization/calculation:
    # vperp dot Bperp should be the same as the elsaesser energies' difference
    # (Schekochihin 2020 eq. 3.4)
    figname = figdir + 'cross-helicity.png'
    figure_name = kwargs.get("figure_name", figname)

    # Body of the plot.
    sigmaDict = rdu.getSimSigmas(params)
    sigma = sigmaDict["sigma"]
    vA = np.sqrt(sigma/(sigma + 1.0))
    difference = vol_means["pelsaesserDifference"]
    cross_helicity = difference/4.0
    norm_value = rdu.getNormValue("VpBp_total", simDir=ph.path_to_reduced_data)
    VpBp = vol_means["VpBp_total"]/norm_value[0]*vA

    plt.plot(times_in_LC, cross_helicity, markersize=3, marker="o", label=r"$(\langle |Z_\perp^+|^2\rangle-\langle |Z_\perp^-|^2\rangle)/4$")
    plt.plot(times_in_LC, VpBp, markersize=3, marker="o", label=r"$\langle v_\perp\cdot B_\perp\rangle\times v_{A,0}/B_0c$")
    # plt.gca().axhline([np.mean(ratio)], ls='--', color='black', label="Mean: {:.2f}".format(np.mean(ratio)))

    plt.gca().set_xlabel("$tc/L$")
    plt.gca().tick_params(axis='y')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name)
    plt.legend()

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"
    overwrite_data = False
    only_means = True
    only_stds = False
    plot_decay_time = True

    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    # sim_name = "test_data"

    if stampede2:
        system_config = "stampede2"

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["only_means"] = only_means
    kwargs["only_stds"] = only_stds
    kwargs["plot_decay_time"] = plot_decay_time

    # --------------------
    ph = path_handler(sim_name, system_config)
    plot_elsaesser_quantities(ph, **kwargs)
