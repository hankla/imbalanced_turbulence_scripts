import numpy as np
import sys
sys.path.append("./modules/")
import os
from raw_data_utils import *
from reduce_data_utils import *
from path_handler_class import *
from scipy import interpolate
import matplotlib.pyplot as plt
import matplotlib.pylab as pl

def plot_elsasser_energy_spectrum(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    times_to_plot = kwargs.get("times_to_plot", retrieve_time_values(ph)[:, 0])
    cut_initial = kwargs.get("cut_initial", False)
    larmor_radius_time_evolution = np.loadtxt(ph.path_to_reduced_data + "larmor_radius.dat", delimiter=",", skiprows=1)

    if cut_initial:
        times_to_plot = times_to_plot[1:]
    # N = int(np.size(times_to_plot)/10) + 1
    # if np.size(times_to_plot) < 10:
        # N = 1
    # times_to_plot = [int(t) for t in times_to_plot[::N]]
    times_to_plot = [int(t) for t in times_to_plot]
    actual_times_plotted = []
    LC_times_plotted = []
    coords = retrieve_coords(ph)
    time_values = retrieve_time_values(ph)
    nx = coords[0].size - 1
    ny = coords[1].size - 1
    # number of modes comes from reduce_perp_spectrum
    number_of_modes = int(np.min([nx, ny])/3)

    # functions for converting between integers and wavenumbers
    def nvalsTokvals(nvals):
        # return 2.0*np.pi*nvals/number_of_modes
        return 2.0*np.pi/nx*nvals

    def kvalsTonvals(kvals):
        # return number_of_modes*kvals/(2.0*np.pi)
        return nx*kvals/(2.0*np.pi)

    # Figure set-up
    if "figure_name" in kwargs and kwargs["figure_name"] == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel(r"$k_\perp\rho_e(t)}$")
    plt.ylabel("Elsasser energy spectrum")
    title_str = ph.sim_name + "\n"
    plt.yscale('log')
    plt.xscale('log')
    # plt.ylim([1e6, 1e9])
    # plt.xlim([6e-2, 2])
    plt.gca().grid(color='.9', ls='--')
    plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    nvals = None
    markers = ["o", "s", "X", "*", "D", "P", "v", "<", ">", "^"]
    colors = pl.cm.viridis(np.linspace(0, 1, len(times_to_plot)))

    # Loop through given times
    for i in np.arange(len(times_to_plot)):
        time = times_to_plot[i]
        t_ind = (np.abs(time_values[:, 0] - time)).argmin()
        time_in_LC = time_values[t_ind, 2]
        actual_times_plotted.append(time_values[t_ind, 0])
        LC_times_plotted.append(time_in_LC)
        # print(time_values[t_ind, 0])
        larmor_ind = (np.abs(larmor_radius_time_evolution[:, 0] - time_in_LC)).argmin()
        larmor_radius_in_cells = larmor_radius_time_evolution[larmor_ind, 1]
        # larmor_radius_in_cells = 3.0/2.0 # = NOM_LENGTH_SCALE
        perp_k_vals = (2.0*np.pi/nx)*np.arange(1, number_of_modes)*larmor_radius_in_cells
        pspectrumPlus = retrieve_perpendicular_spectrum(ph, "elsasserPlus", time, overwrite_data)
        pspectrumMinus = retrieve_perpendicular_spectrum(ph, "elsasserMinus", time, overwrite_data)

        # plt.plot(perp_k_vals, pspectrum, marker=markers[i], markersize=4, color=colors[i], label="$t = ${:.2f} $L/c$".format(time_in_LC))
        plt.plot(perp_k_vals, pspectrumPlus, color=colors[i], label="$t = ${:.2f} $L/c$".format(time_in_LC))
        plt.plot(perp_k_vals, pspectrumMinus, ls=":", color=colors[i], label="$t = ${:.2f} $L/c$".format(time_in_LC))

    # if ph.setup != "stampede2":
        # secax = plt.gca().secondary_xaxis('top', functions=(kvalsTonvals, nvalsTokvals))
        # secax.set_xlabel("Number of cells")
    plt.title(title_str)
    # plt.legend(frameon=False)
    cbar_ticks = np.array(LC_times_plotted)
    cbar_ticks = np.linspace(cbar_ticks.min(), cbar_ticks.max(), cbar_ticks.size)
    cbar_tick_labs = ["{:.2f}".format(t) for t in cbar_ticks]
    cmap = pl.cm.get_cmap('viridis', len(LC_times_plotted))
    mappable = pl.cm.ScalarMappable(cmap=cmap)
    mappable.set_clim(cbar_ticks.min(), cbar_ticks.max())
    cbar = plt.colorbar(mappable, ticks=cbar_ticks)
    cbar.set_label("$tc/L$")
    cbar.ax.set_yticklabels(cbar_tick_labs)
    plt.tight_layout()

    times_string = "t" + "t".join(map(str, map(int, times_to_plot)))
    times_string = "tCompare"

    fig_dir = ph.path_to_figures + "pspectrum_elsasser-variables/"
    if cut_initial:
        fig_dir += "cut_initial/"
    if not os.path.isdir(fig_dir):
        os.makedirs(fig_dir)
    fig_name = fig_dir + "pspectrum_elsasserVariables_" + times_string
    fig_name += ".png"
    figure_name = kwargs.get("figure_name", fig_name)

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches='tight')
        plt.gca().grid(False, which='both')
        plt.savefig(figure_name.replace(".png", "_nogrid.png"))
    plt.close()


if __name__ == '__main__':
    stampede2 = True
    vladimir_data = False
    system_config = "lia_hp"
    overwrite_data = False

    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    # sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    # sim_name = rdu.get_list_of_sim_names_with_phrases("/mnt/d/imbalanced_turbulence/data_reduced/", ["768cube", "im10"])[0]
    sim_name = rdu.get_list_of_sim_names_with_phrases("/scratch/06165/ahankla/imbalanced_turbulence/", ["768cube", "dcorr029", "A075", "im00"])[0]

    if stampede2:
        system_config = "stampede2"
        ph = path_handler(sim_name, system_config)
    else:
        ph = path_handler(sim_name, system_config)

    if stampede2 and vladimir_data:
        vvz_path = "/scratch/02831/zhdankin/data_from_mira/"
        sim_names = ["data_1536cube_track_64ppc"]
        ph = path_handler(sim_names[0], system_config, path_to_raw_data=vvz_path + sim_names[0] + "/")

    times_to_plot = []
    time_values = retrieve_time_values(ph)
    for t in time_values[:, 0]:
        t_ind = (np.abs(time_values[:, 0] - t)).argmin()
        LC_time = time_values[t_ind, 2]
        if LC_time > 8.7 and LC_time < 10.0:
            times_to_plot.append(int(t))
    # print(times_to_plot)

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["times_to_plot"] = times_to_plot

    plot_elsasser_energy_spectrum(ph, **kwargs)
