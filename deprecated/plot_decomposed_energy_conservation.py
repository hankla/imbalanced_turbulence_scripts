import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *

"""
This script will plot the kinetic energy and compare to the injected
energy to find the error in energy conservation.
"""


def plot_energy_conservation(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    consts = rdu.Consts
    times = kwargs.get("times", None)
    sim_name = ph.sim_name
    linestyle = kwargs.get("linestyle", "-")
    figpath = ph.path_to_figures + "energy-conservation/"
    figdir = figpath
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "decomposed_kinetic-energy.png"
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.gca().set_yscale('log')
    os.chdir(ph.path_to_raw_data)
    (params, units) = rdu.getSimParams()
    B0=params["B0"]
    Lx = params["xmax"] - params["xmin"]
    Ly = params["ymax"] - params["ymin"]
    Lz = params["zmax"] - params["zmin"]
    volume = Lz*Ly*Lz

    background_magDens = B0**2/(8*numpy.pi)
    background_Emag = background_magDens * volume

    #===============================================================================
    # Fluid kinetic energy from distribution function
    ekin_i = numpy.loadtxt("Ekin_ions_bg.dat")
    ekin_e = numpy.loadtxt("Ekin_electrons_bg.dat")
    total_kinetic_energy = ekin_i + ekin_e

    Npoints = ekin_e.size
    times = numpy.linspace(0, times_in_LC[-1], Npoints)
    plt.xlabel('$tc/L$',fontsize=18)

    plt.plot(times, total_kinetic_energy/background_Emag, lw=2, color="magenta", label=r"$E_{\mathrm{kin}}/E_{\mathrm{mean}}$", marker="X", markersize=6, markevery=int(Npoints/10))

    #===============================================================================
    # kinetic energy calculated from rho*v^2
    (vol_avgs, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
    perp_energy = vol_avgs["perpEnergyDensity"]*volume
    z_energy = vol_avgs["zEnergyDensity"]*volume
    rest_mass_energy = params["Drift. dens."]*vol_avgs["avgPtclEnergy"]

    z_kinetic_energy = z_energy - rest_mass_energy
    perp_kinetic_energy = perp_energy - rest_mass_energy

    plt.plot(times_in_LC, z_kinetic_energy/background_Emag, lw=2, color="red", label=r"$E_{\mathrm{kin,z}}/E_{\mathrm{mean}}$", marker="o", markersize=6, ls=linestyle)
    plt.plot(times_in_LC, perp_kinetic_energy/background_Emag, lw=2, color="blue", label=r"$E_{\mathrm{kin,\perp}}/E_{\mathrm{mean}}$", marker="s", markersize=6, ls=linestyle)
    plt.plot(times_in_LC, (z_kinetic_energy + perp_kinetic_energy)/background_Emag, lw=2, color="green", label=r"$E_{\mathrm{kin,\perp+\parallel}}/E_{\mathrm{mean}}$", marker="X", markersize=6, ls=linestyle)

    plt.title(ph.sim_name, fontsize=20)
    # plt.ylim([0.0001, 30])
    plt.gca().grid(color='.9', ls='--')
    plt.legend()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figname)
        plt.savefig(figure_name)
    plt.close()

    print(np.mean(z_kinetic_energy + perp_kinetic_energy))
    print(np.mean(total_kinetic_energy))


if __name__ == '__main__':
    stampede2 = False
    system_config = "jila_laptop"
    kwargs = {}
    kwargs["figure_name"] = "show"
    kwargs["fields_to_vavg"] = ["zEnergyDensity", "perpEnergyDensity", "avgPtclEnergy"]
    kwargs["overwrite_data"] = False

    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, system_config)

    times = retrieve_time_values(ph)
    plot_energy_conservation(ph, **kwargs)
