
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_synchrotron_anis(it,il,ip,spec,sym):

    if it=='':
       it='0'

    if il=='':
       il='0'
       
    if ip=='':
       ip='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # nu, phi and lambda arrays
    nu=numpy.loadtxt(".././data/nu.dat")
    eps1=nu*4.1356701e-15
    phi=numpy.loadtxt(".././data/phi.dat")
    lbd=numpy.loadtxt(".././data/lambda.dat")
    
    # Synchrotron spectrum
    map_temp=numpy.loadtxt(".././data/synchrotron_"+spec+"_"+sym+it+".dat")
    nuFnu=numpy.empty(len(nu))
    
    for ie in range(0,len(nu)):
        nuFnu[ie]=map_temp[int(ip)+ie*len(phi),int(il)]
    
    plt.plot(eps1,nuFnu,color='blue',lw=2)
    plt.xscale('log')
    plt.yscale('log')

    plt.xlabel(r'$\epsilon_1$ [eV]',fontsize=20)
    plt.ylabel(r'$\nu F_{\nu}$ [A.U.]',fontsize=20)
    plt.xlim([numpy.min(eps1),numpy.max(eps1)])
    plt.ylim([1e-4*numpy.max(nuFnu),3.0*numpy.max(nuFnu)])
    
    plt.title("time step="+it+"/ Species="+spec+"/ Sym="+sym,fontsize=18)
      
    print "Lambda=",lbd[int(il)],", Phi=",phi[int(ip)]
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_synchrotron_anis(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
