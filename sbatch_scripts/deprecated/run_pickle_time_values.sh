#!/bin/bash
#SBATCH --time=00:05:00
#SBATCH --partition=development
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=pickle_time
#SBATCH --output=/work/06165/ahankla/stampede2/imbalanced_turbulence/run_scripts/logs/out_pickle_time_values.%j
#SBATCH --error=/work/06165/ahankla/stampede2/imbalanced_turbulence/run_scripts/logs/err_pickle_time_values.%j
#SBATCH -A TG-PHY140041

module purge
module load intel/18.0.2
module load python3/3.7.0

script='pickle_time_values'

scriptpath="/work/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work/06165/ahankla/stampede2/imbalanced_turbulence/run_scripts/logs/log_${script}.txt"
fname="${script}.py"

cd ${scriptpath}
pwd
date
module list

python3 -u ${fname} > ${logpath}

date
