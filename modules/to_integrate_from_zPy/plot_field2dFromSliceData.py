from __future__ import division
from __future__ import print_function
import numpy
import matplotlib
import matplotlib.pyplot as plt

def readData(h5FileName, datasetName, slices = None): #{
  """
  Returns a (numpy float64) array of the field values
  from an hdf5 dataset

  If slices is set, this will retrieve the requested slice of a field;
  e.g., if the field is 4x5x3, then 
  slices = (slice(None), slice(2,5), slice(None)) will return a 
  4x3x3 subset.

  If getDescription, then this returns (array, string) where the
  string is the value of the "description" attribute
  (set by writeArrayToHdf5)
  """
  import tables
  import warnings
  try:
    file = tables.open_file(h5FileName,'r')
    getNode = file.get_node
  except:
    try:
      warnings.simplefilter('ignore', UserWarning)
      file = tables.openFile(h5FileName,'r')
      getNode = file.getNode
      warnings.simplefilter('default', UserWarning)
    except:
      msg = "\n\nError: Failed to open hdf5 file: '" + h5FileName + "'"
      msg += " (is it an hdf5 file?)\n"
      raise ValueError(msg)

  groupName = '/' + datasetName
  node = getNode(groupName)
  
  try:
    if (slices == None):
      res = numpy.array( node )
    else:
      res = numpy.array( node[tuple(slices)] )
  except:
    file.close()
    msg = "\n\nError: Failed to open dataset '" + groupName + "'"
    msg += " in hdf5 file: '" + h5FileName + "'\n"
    raise ValueError(msg)

  if (1): #{ # get attribs
    attribs = dict()
    # attribs that already have the correct type
    for attStr in ("fieldFile", "command", "directory",
      "axis0", "axis0label", "axis1", "axis1label",
      "iSlice", "i2minusPlus", 
      "norm", "normStr", "normQtyStr", "time", "step", "rho"):
      if hasattr(node._v_attrs, attStr):
        attribs[attStr] = getattr(node._v_attrs, attStr)
    # attribs that are python expressions
    for attStr in ("hSlice", "vSlice", "commandKws",
      "layerFieldKws", "timeDict", "params", "units",):
      if hasattr(node._v_attrs, attStr):
        attr = getattr(node._v_attrs, attStr)
        #print(attStr, attr, type(attr))
        attribs[attStr] = eval(getattr(node._v_attrs, attStr))
    print("attribs = ", attribs)
  #}

  file.close()

  return (res, attribs)

  
#}

def valOrDef(d, key, default): #{
  if key in d:
    res = d[key]
  else:
    res = default
  return res
#}

def plot(sliceFile): #{

  (field, attrib) = readData(sliceFile, "field")
  print(field.shape)
  (hCoords, hCoordsAttrib) = readData(sliceFile, "hAxisCoords")
  print(hCoords.shape)
  (vCoords, vCoordsAttrib) = readData(sliceFile, "vAxisCoords")
  print(vCoords.shape)

  rho = hCoordsAttrib["rho"]

  h1d = hCoords/rho
  v1d = vCoords/rho
  data = field/attrib["norm"]
  
  cmin = valOrDef(attrib["commandKws"], "cmin", None)
  cmax = valOrDef(attrib["commandKws"], "cmax", None)
  print(cmin, cmax)

  print(attrib["commandKws"])
  
  p = plt.pcolormesh(h1d, v1d, data.transpose(), 
    vmin=cmin, vmax=cmax,cmap="seismic" if cmin < 0 else "inferno")
  plt.colorbar(p)
  plt.title(sliceFile)
  plt.show()

#}

if __name__ == "__main__": #{
  import sys
  args = sys.argv[1:]
  plot(args[0])
#}

