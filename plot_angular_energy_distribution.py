import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from path_handler_class import *
from reduce_data_utils import *
import raw_data_utils as rdu

"""
This script will plot the Aitoff projection of the particles' angular energy distribution
at a certain time step "timestep" for a range of energies (gamma_start_ind to gamma_end_ind)
"""

def plot_angular_energy_distribution(ph, **kwargs):
    os.chdir(ph.path_to_raw_data)
    sim_name = ph.sim_name

    timestep = kwargs.get("timestep", 0)
    gamma_start_ind = kwargs.get("gamma_start_ind", 0)
    gamma_end_ind = kwargs.get("gamma_end_ind", -1)
    fixed_clims = False
    if 'vmin' in kwargs or 'vmax' in kwargs:
        fixed_clims = True

    # ------------------------------------------------
    time_values = retrieve_time_values(ph)
    tind = np.where(timestep == time_values[:, 0])[0][0]
    time_in_LC = time_values[tind, 2]
    titstr = ph.sim_name + "\n" + r"$t={:.2f}~L/c$".format(time_in_LC)

    # NOTE these angles are in degrees
    # polar_angle runs from -90 to 90 and has half as many steps as azi_angle; also called phi
    # azi_angle runs from -180 to 180; also called lambda
    (N_electrons, ubins, polar_angle, azi_angle, step) = rdu.getPtclAngularSpectrum(timestep, 'electrons', 'bg')
    N_ions = rdu.getPtclAngularSpectrum(timestep, 'ions', 'bg')[0]
    N_particles = N_electrons + N_ions
    gamma_bins = np.sqrt(1. + ubins**2)
    # convert to radians
    polar_angle = np.pi/180.0*polar_angle
    azi_angle = np.pi/180.0*azi_angle

    # now split up the particles into the desired energy bins
    gamma_start_value = "%2.2e " % gamma_bins[int(gamma_start_ind)]
    gamma_end_value = "%2.2e " % gamma_bins[int(gamma_end_ind)]
    titstr += "\n" + gamma_start_value + r"< $\gamma$ < " + gamma_end_value + "\n"
    energy_slice = N_particles[gamma_start_ind:gamma_end_ind, :, :]
    energy_slice = np.sum(energy_slice, axis=0)

    # now normalize to total number of particles
    total_num_particles = np.sum(energy_slice)
    energy_slice = energy_slice/total_num_particles

    vmin = kwargs.get('vmin', np.nanmax([1e-6, np.nanmin(energy_slice)]))
    vmax = kwargs.get('vmax', np.nanmax([1e-6, np.nanmax(energy_slice)]))

    figdir = ph.path_to_figures + "angular_energy_distribution/t{}/".format(timestep)
    if fixed_clims:
        figdir += "fixed_clims/"
        figdir += "min{:.2e}max{:.2e}/".format(vmin, vmax)
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    if gamma_end_ind == -1:
        gamma_end_ind = np.size(gamma_bins)
    figname = figdir + "angular_energy_distribution_gammaInd{}-{}.png".format(gamma_start_ind, gamma_end_ind)
    figure_name = kwargs.get("figure_name", figname)

    # make energy plot
    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    ax = plt.gca(projection='mollweide')

    lognorm = colors.LogNorm(vmin=vmin, vmax=vmax)
    plt.pcolormesh(azi_angle, polar_angle, energy_slice, norm=lognorm, cmap='viridis')
    plt.grid(alpha=0.4)
    plt.colorbar(shrink=0.5)

    plt.title(titstr)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = True
    system_config = "lia_hp"
    overwrite_data = False
    timestep = 2910
    timestep = 0
    # gamma_start_ind = 0; gamma_end_ind = -1
    gamma_start_ind = 0; gamma_end_ind = 40
    gamma_start_ind = 40; gamma_end_ind = 50
    # gamma_start_ind = 80; gamma_end_ind = -1

    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    sim_name = "zeltron_256cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD120-s2"
    sim_name = "zeltron_384cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD360-seedstudy5"

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, system_config)

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["timestep"] = timestep
    kwargs["gamma_start_ind"] = gamma_start_ind
    kwargs["gamma_end_ind"] = gamma_end_ind

    kwargs["vmin"] = 1.2e-4
    kwargs["vmax"] = 2.2e-4

    plot_angular_energy_distribution(ph, **kwargs)
