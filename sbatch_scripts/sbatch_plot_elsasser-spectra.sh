#!/bin/bash
#SBATCH --time=08:00:00
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --job-name=im00spectra
#SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_elsasser_spectra.%j
#SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_elsasser_spectra.%j
# #SBATCH --output=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/out_plot_length_scales.%j
# #SBATCH --error=/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/err_plot_length_scales.%j
#SBATCH -A TG-PHY160032
#SBATCH --mail-type=ALL
#SBATCH --mail-user=lia.hankla@gmail.com
# #SBATCH --begin=now+3hours

# Order of the module load is important!
module purge
module load intel/18.0.2
module load hdf5/1.10.4
module load python3/3.7.0

# script='plot_volume_averages'
script='plot_elsasser_energy_spectrum'
# script='plot_positive_vx_fraction_over_energy'
# script='plot_length_scales_over_time'

scriptpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work2/06165/ahankla/stampede2/imbalanced_turbulence/scripts/sbatch_scripts/logs/log_${script}_im00.txt"
fname="${script}.py"

cd ${scriptpath}
pwd
date
module list

python3 -u ${fname} > ${logpath}

date
