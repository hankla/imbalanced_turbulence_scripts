import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
from scipy import optimize
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from scipy.signal import argrelextrema
from compare_handler import *
"""
This script will plot temporal profiles at the location (0, 0, 0)
of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - extend to other locations
"""

def fit_particle_energy_spectrum(ph, time_index, **kwargs):
    # use time_index rather than time step so can easily translate
    # to other box sizes.
    # ------------------------------------------
    sim_name = ph.sim_name
    to_show = kwargs.get("to_show", False)
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    species = kwargs.get("species", "electrons")
    (SimParams, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    consts = rdu.Consts
    # ------------------------------------------
    if to_show:
        matplotlib.use('TkAgg')
    if species not in ['electrons', 'ions']:
        print("Error: species must be one of electrons or ions")
        return

    time_string = "t{:d}".format(time_index)
    figbase = ph.path_to_figures + "particle_energy_spectrum/fit/"
    if not os.path.exists(figbase):
        os.makedirs(figbase)
    figname = figbase + species + "_" + time_string
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    figname += ".png"

    # --------------------------------------------
    os.chdir(ph.path_to_reduced_data + "particle_spectra/")
    time_values = retrieve_time_values(ph)
    time_in_LC = time_values[time_index, 2]
    timestep = int(time_values[time_index, 0])

    # dNdg is f(gamma)
    (dNdg, uBinEdges, step
    ) = rdu.getPtclEnergySpectrum(timestep, species, "bg", "gamma")
    gEdges = np.sqrt(uBinEdges**2+1.)
    gamma_values = 0.5*(gEdges[1:] + gEdges[:-1])

    # cut off zero values
    end_ind = np.where(dNdg == 0)[0][0]
    gamma_values = gamma_values[:end_ind]
    dNdg = dNdg[:end_ind]

    # Fit to Maxwell-Juettner
    max_params, max_e = optimize.curve_fit(maxwellJuettner, gamma_values, dNdg)
    max_fine_gamma = np.linspace(gamma_values[0], gamma_values[-1], 1000)
    fit_maxwellian = maxwellJuettner(max_fine_gamma, *max_params)
    print(max_params)

    # data_max_gamma = gamma_values[dNdg.argmax()]
    # fit_max_gamma = max_fine_gamma[fit_maxwellian.argmax()]


    log_gamma = np.log10(gamma_values)
    log_spectrum = np.log10(dNdg)

    # Find where to fit the power-law: start from the spectrum max
    max_index = dNdg.argmax()
    # Power-law should end before the pile-up starts, i.e. at the next inflection point
    # which we'll find as the local extrema of the derivative
    spectrum_derivative = np.gradient(log_spectrum[max_index:], log_gamma[max_index:], edge_order=2)
    maxima_inds = argrelextrema(spectrum_derivative, np.greater)
    if maxima_inds[0].size > 1:
        inflection_index = maxima_inds[0][0] + max_index
    else:
        inflection_index = -2
    gamma_to_fit = log_gamma[max_index:inflection_index+1]
    spectrum_to_fit = log_spectrum[max_index:inflection_index+1]
    params, e = optimize.curve_fit(power_law, gamma_to_fit, spectrum_to_fit)
    fine_gamma = np.linspace(gamma_to_fit[0], gamma_to_fit[-1], 1000)
    fit_power_law = power_law(fine_gamma, *params)

    # --------------------------------------
    plt.figure()
    plt.xlabel(r'$\gamma$',fontsize=20)
    plt.ylabel(r'$\frac{dN}{d\gamma}$',fontsize=20)
    if ylims is not None:
        plt.ylim(ylims)
    else:
        plt.ylim([1e-12, 1e-2])
    if xlims is not None:
        plt.xlim(xlims)
    else:
        plt.xlim([10, 2e5])
    title = ph.sim_name + "\n" + species + " spectrum at "
    title += ("$t=${:.2f} $L/c$".format(time_in_LC))
    title += "\n Fitted power-law index: {:.2f}".format(params[1])
    title += "\n Best fit " + r"$\Theta$: {:.2f} (i.e. $\bar\gamma=${:.2f})".format(max_params[1], 3.0*max_params[1])
    print("Fitted power-law index: {:.2f}".format(params[1]))

    plt.loglog(gamma_values, dNdg)
    plt.loglog(max_fine_gamma, fit_maxwellian, 'k--')
    plt.loglog(gamma_values[inflection_index], dNdg[inflection_index], 'r.')
    plt.loglog(gamma_values[max_index], dNdg[max_index], 'r.')
    # plt.loglog(gamma_values[dNdg.argmax()], dNdg.max(), 'ro')
    # plt.loglog(max_fine_gamma[fit_maxwellian.argmax()], fit_maxwellian.max(), 'go')
    plt.loglog(10**(fine_gamma), 10**(fit_power_law), 'g:')

    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title)
    plt.tight_layout()
    if to_show:
        plt.show()
    else:
        print("Saving figure " + figname)
        plt.savefig(figname, bbox_inches='tight')
    plt.close()


def power_law(gamma_values, y0, slope):
    return slope*gamma_values+y0


def maxwellJuettner(gamma_values, A, Theta):
    # NOTE: need gamma^2 because getPtclAngularSpectrumIntegratedOverAngle does not
    # include the radius^2 when integrating over 3D
    return A*gamma_values**2.0/Theta**3.0*np.exp(-gamma_values/Theta)

if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    sim_name = "zeltron_512cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD240-s210"
    # sim_name = "zeltron_512cube_8xyz-Jxyz_im00sigma05dcorr029omega035A075phisame_PPC32-FD240-s21"

    category = "768Imbalance-comparison"
    sim_name = comparison(category).configs[-1]

    # kwargs
    overwrite = False
    to_show = True
    time_index = 0
    time_index = 30
    time_index = 76
    # time_index = 60
    xlims = [10, 1e5]
    ylims = [1.0e-3, 1]
    xlims = None
    ylims = None

    # ------------
    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, "lia_hp")

    kwargs = {}
    kwargs["to_show"] = to_show
    kwargs["xlims"] = xlims
    kwargs["ylims"] = ylims
    fit_particle_energy_spectrum(ph, time_index, **kwargs)
