import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import *
from plot_length_scales_over_time import plot_length_scales
import itertools
import matplotlib.patheffects as mpe
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - cross helicity fluctuations
"""

def compare_plot_opacity_over_time(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = ["Upz_total", "ExB_z"]
    linear_fit = kwargs.get('linear_fit', False)
    ylims = kwargs.get("ylims", None)

    sim_set = comparison(category)
    configs = sim_set.configs
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/volAvg-over-time/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figbase = figdir

    labels = sim_set.labels
    colors = sim_set.colors
    linestyles = itertools.cycle(sim_set.linestyles)
    markers = itertools.cycle(sim_set.markers)
    # norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)[1]
    # field_abbrev = rdu.getFieldAbbrev(field, True)

    plt.figure()
    plt.xlabel("$tc/L$")
    # ylabel = r"$\langle$" + field_abbrev + norm_value + r"$\rangle$"
    ylabel = "Fraction of injected flux that is absorbed"
    plt.ylabel(ylabel)
    # plt.ylabel("Cross-helicity normalized to maximum")
    plt.gca().axhline([0], color="black", linestyle="--")
    figure_name = figbase + "opacity"
    figure_name += ".png"

    for i, sim_name in enumerate(configs):
        if stampede2:
            ph = path_handler(sim_name, "stampede2")
        else:
            ph = path_handler(sim_name, system_config)

        (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        times_in_LC = retrieve_time_values(ph)[:, 2]
        (vol_avgs, vol_stds) = retrieve_variables_vol_means_stds(ph, fields_to_vavg=fields_to_plot)
        # norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)[0]

        Upz = vol_avgs["Upz_total"]
        ExBz = vol_avgs["ExB_z"]

        fraction_absorbed = np.diff(Upz)*4.0*np.pi*rdu.Consts.c/(ExBz[:-1])
        fraction_absorbed = np.isfinite(fraction_absorbed)
        norm_value = 1

        plt.plot(times_in_LC[:-1], fraction_absorbed/norm_value, label=labels[i], ls=next(linestyles), marker=next(markers), markersize=4, markevery=int(fraction_absorbed.size/10)+1, color=colors[i], path_effects=[mpe.Stroke(linewidth=1.8, foreground='k'), mpe.Normal()])
        print(ph.sim_name)
        print(np.nanmean(fraction_absorbed))

    if ylims is not None:
        plt.ylim(ylims)
    plt.gca().grid(color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    titstr = ""
    titstr += "\n" + category
    plt.title(titstr + "\n")

    if sim_set.cbar_label is not None:
        colorbar1 = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=sim_set.cmap), ticks=sim_set.cbar_ticks)
        colorbar1.set_label(sim_set.cbar_label)
        colorbar1.ax.set_yticklabels(sim_set.cbar_tick_labs)
    legend_elements = sim_set.legend_elements
    if not legend_elements:
        plt.legend(frameon=False, ncol=sim_set.ncol)
    else:
        plt.gca().legend(handles=legend_elements, ncol=sim_set.ncol, frameon=False)

    plt.tight_layout()

    metadata={'Author':'Lia Hankla', 'Subject':category, 'Keywords':'Git commit: ' + ph.get_current_commit()}
    print("Saving figure " + figure_name)
    plt.savefig(figure_name, bbox_inches='tight')
    root_name = figure_name.split("/")[-1]
    if not os.path.isdir(figdir + "pdfs/"):
        os.makedirs(figdir + "pdfs/")
    plt.title('')
    plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight', metadata=metadata)

    plt.gca().grid(False, which='both')
    if not os.path.isdir(figdir + "pdfs/nogrids/"):
        os.makedirs(figdir + "pdfs/nogrids/")
    plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
    if not os.path.isdir(figdir + "nogrids/"):
        os.makedirs(figdir + "nogrids/")
    plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    # category = "xiAllboxSizeExtremes-comparison"
    category = "768Imbalance-comparison"
    # category = "8xyzIm00Mf-comparison"
    # category = "modeNum-comparison"

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    kwargs["tinitial"] = 0.0
    kwargs["tfinal"] = 20.0
    kwargs["ylims"] = [-1, 1.0]

    compare_plot_opacity_over_time(category, **kwargs)
