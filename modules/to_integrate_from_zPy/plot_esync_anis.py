
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_esync_anis(it,il,ip,spec,sym):

    if it=='':
       it='0'

    if il=='':
       il='0'
       
    if ip=='':
       ip='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # nu, phi and lambda arrays
    nu=numpy.loadtxt(".././data/nu.dat")
    eps1=nu*4.1356701e-15
    
    phi=numpy.loadtxt(".././data/phi.dat")
    lbd=numpy.loadtxt(".././data/lambda.dat")
    
    # Spectrum
    map_temp=numpy.loadtxt(".././data/ecut_"+spec+"_"+sym+it+".dat")
    nuFnu=numpy.empty(len(eps1))
    
    for ie in range(0,len(eps1)):
        nuFnu[ie]=map_temp[int(ip)+ie*len(phi),int(il)]*eps1[ie]*eps1[ie]
    
    plt.plot(eps1,nuFnu,color='blue',lw=2)
    plt.xscale('log')
    plt.yscale('log')

    plt.xlabel(r'$\epsilon_{cut}$ [eV]',fontsize=20)
    plt.ylabel(r'$\epsilon^2_{cut} \frac{dN}{d\epsilon_{cut}}$ [A.U.]',fontsize=20)
    plt.xlim([numpy.min(eps1),numpy.max(eps1)])

    miny=1e-4*numpy.max(nuFnu)
    maxy=3.0*numpy.max(nuFnu)

    plt.ylim([miny,maxy])

    plt.plot([1.6e8,1.6e8],[miny,maxy],ls='--',lw=2,color='red')
    
    plt.title("time step="+it+"/ Species="+spec+"/ Sym="+sym,fontsize=18)
         
    print "Lambda=",lbd[int(il)],", Phi=",phi[int(ip)]
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_esync_anis(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
