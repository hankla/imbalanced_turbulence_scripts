import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plot_length_scales_over_time import plot_length_scales
import matplotlib.patheffects as mpe
"""
This script will plot the volume average of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...
"""

def plot_ExBUpz_volume_average(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    fields_to_plot = ["ExB_z", "Upz_total"]
    (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs, fields_to_vavg=fields_to_plot)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    sim_name = ph.sim_name

    # set up figure info
    figdir = ph.path_to_figures + "volAvg-over-time/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "ExBUpz_volume_averages"
    figure_name = kwargs.get("figure_name", figname)
    # Body of the plot.
    if figure_name == "show":
        matplotlib.use("TkAgg")

    colors = ["blue", "red"]
    # ----------------------------------------------
    # Now plot
    print("Plotting volume means and standard deviations for " + ph.sim_name)
    for i, field in enumerate(fields_to_plot):
        field_mean = vol_means[field]
        field_mean = field_mean/np.max(np.abs(field_mean))
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.plot(times_in_LC, field_mean, color=colors[i], path_effects=[mpe.Stroke(linewidth=2,foreground='k'), mpe.Normal()], label=field_abbrev, marker='o', markersize=2)

    plt.legend()
    plt.xlabel("$tc/L$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        plt.savefig(figure_name)
    plt.close()

if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    vladimir_data = False
    system_config = "lia_hp"
    overwrite_data = False
    only_means = True
    only_stds = False
    plot_decay_time = False

    sim_name = "zeltron_768cube_8xyz-Jxyz_im10sigma05dcorr029omega035A075phisame_PPC32-FD360-s310"
    sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    if not stampede2 and vladimir_data:
        red_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
        sim_name = rdu.get_list_of_sim_names_with_phrases(red_dir, ["1024cube", "data"])[0]

    kwargs = {}
    kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["plot_decay_time"] = plot_decay_time

    # --------------------
    if stampede2:
        system_config = "stampede2"

    if stampede2 and vladimir_data:
        vvz_path = "/scratch/02831/zhdankin/data_from_mira/"
        sim_name = "data_768cube_track_64ppc"
        ph = path_handler(sim_name, system_config, path_to_raw_data=vvz_path + sim_name + "/")
    else:
        ph = path_handler(sim_name, system_config)

    plot_ExBUpz_volume_average(ph, **kwargs)
