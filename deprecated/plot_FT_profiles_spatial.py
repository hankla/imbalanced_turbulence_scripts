import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from scipy.fftpack import fft, fftfreq, fftshift
from matplotlib.ticker import FixedLocator
import matplotlib.pylab as pl

"""
This script will plot the fourier transform of temporal profiles at the location (0, 0, 0)
of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - extend to other locations
"""

def plot_FT_profiles_spatial(ph, **kwargs):
    # ------------------------------------------
    fields_to_plot = kwargs.get("fields_to_profile", rdu.Zvariables.options)
    sim_name = ph.sim_name
    log_yscale = kwargs.get("log_yscale", False)
    to_show = kwargs.get("to_show", False)
    dim = kwargs.get("dim", "z")
    ix = kwargs.get("ix", 0)
    iy = kwargs.get("iy", 0)
    iz = kwargs.get("iz", 0)
    times_to_plot = kwargs.get("times_to_profile", retrieve_time_values(ph)[:, 0])
    cut_initial = kwargs.get("cut_initial", True)
    average_2d = kwargs.get("average_2d", False)
    average_over_time = kwargs.get("average_over_time", 1)
    # ------------------------------------------
    if to_show:
        matplotlib.use('TkAgg')
    figdirbase = ph.path_to_figures + "profiles-1d_spatial/"
    os.chdir(ph.path_to_reduced_data)

    if cut_initial and 0 in times_to_plot:
        times_to_plot = times_to_plot[1:]

    if times_to_plot.size > 8:
        N = int(np.size(times_to_plot)/8) + 1
        times_to_plot = times_to_plot[::N]

    all_timesteps = retrieve_time_values(ph)[:, 0]
    initial_times_to_plot = times_to_plot
    times_to_plot = []

    # Now, add times to average around
    for time in initial_times_to_plot:
        tind = np.where(time == all_timesteps)[0][0]

        if tind - average_over_time < 0:
            times_to_plot = np.array(all_timesteps[:tind+average_over_time+1])
        else:
            new_times = np.array(all_timesteps[tind-average_over_time:tind+average_over_time+1])
            if time == initial_times_to_plot[0]:
                times_to_plot = new_times
            else:
                times_to_plot = np.hstack((times_to_plot, new_times))

    times_to_plot = np.unique(times_to_plot)
    # print(initial_times_to_plot)
    # print(times_to_plot)

    coords = retrieve_coords(ph)
    if dim == "z":
        dimstr = "$k_z$"
        secstr = r"$\lambda_z$"
        x_values = coords[2]
        if average_2d:
            profstr = "xAyA"
            loadstr = "xAyA"
        else:
            profstr = "ix{}iy{}".format(ix, iy)
            loadstr = "x{}y{}".format(ix, iy)
    elif dim == "y":
        dimstr = "$k_y$"
        secstr = r"$\lambda_y$"
        x_values = coords[1]
        if average_2d:
            profstr = "xAzA"
            loadstr = "xAzA"
        else:
            profstr = "ix{}iz{}".format(ix, iz)
            loadstr = "x{}z{}".format(ix, iz)
    else:
        dimstr = "$k_x$"
        secstr = r"$\lambda_x$"
        x_values = coords[0]
        if average_2d:
            profstr = "yAzA"
            loadstr = "yAzA"
        else:
            profstr = "iy{}iz{}".format(iy,iz)
            loadstr = "y{}z{}".format(iy, iz)
    (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
    Consts = rdu.Consts
    time_values = retrieve_time_values(ph)

    def kToLambda(kvals):
        return 2*np.pi/kvals
    def lambdaTok(lambdavals):
        return 2*np.pi/lambdavals

    print("Plotting " + sim_name + " Fourier transform of spatial profiles")

    profile_data = retrieve_profiles_spatial(ph, times_to_plot, **kwargs)
    x_interval = x_values[1] - x_values[0]
    times_string = "t" + "t".join(map(str, map(int, initial_times_to_plot)))+"-ta{:d}".format(average_over_time)
    if len(initial_times_to_plot) > 2:
        colors = pl.cm.viridis(np.linspace(0, 1, len(initial_times_to_plot)))
    else:
        colors = ["tab:blue", "tab:orange"]
        # colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple", "tab:brown"]

    for field in fields_to_plot:
        figdir = figdirbase + dim + "-profile/" + field + "/"
        if average_2d:
            figdir += "2d-averaged/"
        figdir += "FT/"
        if log_yscale:
            figdir += "log_yscale/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)
        figname = figdir + field + "_" + profstr + "_" + times_string + ".png"
        print(figname)
        plt.figure()
        for i, time in enumerate(initial_times_to_plot):
            t_ind = np.where(time_values[:, 0] == time)[0][0]
            time_in_LC = time_values[t_ind, 2]
            norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
            field_abbrev = rdu.getFieldAbbrev(field, True)

            tind = np.where(time == all_timesteps)[0][0]
            if tind - average_over_time < 0:
                times_to_average = all_timesteps[:tind+average_over_time+1]
            else:
                times_to_average = all_timesteps[tind-average_over_time:tind+average_over_time+1]

            profile = None
            counter = 0
            for t in times_to_average:
                if profile is None:
                    profile = profile_data[field]["{:d}".format(int(t))][loadstr]
                    counter += 1
                else:
                    profile += profile_data[field]["{:d}".format(int(t))][loadstr]
                    counter += 1

            profile = np.array(profile)/norm_value[0]/counter

            N = profile.size
            wavenumbers = fftfreq(N, x_interval)
            profile_FT = fft(profile)

            # convert to k instead of 1/lambda
            wavenumbers = 2*np.pi*fftshift(wavenumbers)
            # normalize to rhoe
            rhoe = Consts.c/params['omegac']
            wavenumbers = wavenumbers * rhoe
            profile_FT_abs_norm = 1.0/N * np.abs(fftshift(profile_FT))**2.0

            plt.plot(wavenumbers[N//2:], profile_FT_abs_norm[N//2:], marker="o", lw=2, label="$t=${:.2f} $L/c$".format(time_in_LC), color=colors[i], markersize=2)
        if ph.setup == "lia_hp":
            secax = plt.gca().secondary_xaxis('top', functions=(kToLambda, lambdaTok))
            xlabel = r"Wavelength " + secstr + r"$/\rho_e$"
            secax.set_xlabel(xlabel)
        if log_yscale:
            plt.yscale('log')
        plt.xscale('log')
        titstr = sim_name + "\n FT of spatial " + dim + "-profile taken at " + profstr + " averaged over {:d} adjacent data dumps\n".format(1+2*average_over_time) + field_abbrev + norm_value[1]
        plt.title(titstr)
        plt.ylabel("$|\mathcal{F}($" + field_abbrev + norm_value[1] + "$)|^2$")
        plt.xlabel(r"Wavenumber " + dimstr + r"$\rho_e$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.legend()
        plt.tight_layout()

        if to_show:
            plt.show()
        else:
            print("Saving figure: " + figname)
            plt.savefig(figname, bbox_inches="tight")

        plt.close()




if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"

    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im00sigma05dcorr0omega035A075phi0_PPC32-FD30-long"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"
    sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"

    omegaD = 0.6/np.sqrt(3.0)
    # omegaD = None

    # kwargs
    overwrite = False
    to_show = False
    cut_initial = True
    average_2d = True
    average_over_time = 0 # note this is on either side i.e. with do time - 1, time, time + 1 for value of 1

    zVars = rdu.Zvariables.options
    # fields_to_plot = zVars[:]
    fields_to_plot = ["Bx", "By", "velx_total", "vely_total"]

    if stampede2:
        system_config = "stampede2"
    ph = path_handler(sim_name, system_config)
    times_to_profile = retrieve_time_values(ph)[:, 0]
    times_to_profile = times_to_profile[::10]

    kwargs = {}
    kwargs["fields_to_profile"] = fields_to_plot
    kwargs["to_show"] = to_show
    kwargs["overwrite"] = overwrite
    kwargs["cut_initial"] = cut_initial
    kwargs["times_to_profile"] = times_to_profile
    kwargs["average_2d"] = average_2d
    kwargs["average_over_time"] = average_over_time
    if omegaD is not None: kwargs["omegaD"] = omegaD
    plot_FT_profiles_spatial(ph, **kwargs, log_yscale=True)
    # plot_FT_profiles_spatial(ph, **kwargs, log_yscale=False)
