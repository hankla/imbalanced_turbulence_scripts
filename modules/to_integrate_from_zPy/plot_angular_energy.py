
import numpy
import math
import matplotlib.pyplot as plt
import sys

import zfileUtil

def plot_angular_energy(it,ig,spec,sym):

    if it=='':
       it='0'

    if ig=='':
       ig='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # The grid, and energy-dependent particle angular distribution
    (map_temp, u, phi, lbd, step) = zfileUtil.getPtclAngularSpectrum(
      it, spec, sym)
    gam = numpy.sqrt(1. + u**2)

    angular = map_temp[ig,:,:]

    lbd=lbd*math.pi/180.0
    phi=phi*math.pi/180.0
    
    plt.subplot(111,projection="aitoff")
    plt.pcolormesh(lbd,phi,angular,vmin=0.)
    plt.grid("on",color='white',lw=1)
    
    gam1="%2.2e " % gam[int(ig)]
    gam2="%2.2e " % gam[int(ig)+1]
    titre=gam1+r"< $\gamma$ < "+gam2
    
    plt.text(0.0,1.9,titre,horizontalalignment='center',fontsize=18)
         
    plt.colorbar().ax.set_ylabel(r'$n/n_0$',fontsize=18)

    plt.show()
    
    #===============================================================================

plot_angular_energy(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
