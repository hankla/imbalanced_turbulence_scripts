import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
"""
This script will compare the volume-averaged conserved quantities:
energy and cross-helicity. From Schekochihin 2020 Eq. 3.4

Useful diagnostic, not used in Hankla+ 2021.
"""

def plot_energy_breakdown(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    fields_to_vavg = ["fluidEnergyDensity", "internalEnergyDensity", "netEnergyDensity", "turbulentEnergyDensity"]
    kwargs["fields_to_vavg"] = fields_to_vavg
    overwrite_data = kwargs.get("overwrite_data", False)
    overwrite_Eheat = kwargs.get("overwrite_Eheat", False)
    plot_decay_time = kwargs.get("plot_decay_time", True)
    (vol_means, vol_stds) = retrieve_variables_vol_means_stds(ph, **kwargs)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    sim_name = ph.sim_name


    # ----------------------------------------------
    # set up figure info
    figdir = ph.path_to_figures + "energy-partition/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)

    # ----------------------------------------------
    # Plot the comparison of the +- energies first.
    figname = figdir + 'energy_breakdown.png'
    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")

    plt.figure()
    for field in fields_to_vavg:
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        plt.plot(times_in_LC, vol_means[field]/norm_value[0], markersize=3, label=field_abbrev + norm_value[1])
    summed_energy = vol_means["netEnergyDensity"] + vol_means["turbulentEnergyDensity"] + vol_means["internalEnergyDensity"]
    plt.plot(times_in_LC, summed_energy/norm_value[0], markersize=3, label="Sum", ls="--", color="black")

    plt.gca().set_xlabel("$tc/L$")
    plt.gca().tick_params(axis='y')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name)
    plt.legend(frameon=False)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving " + figure_name)
        plt.savefig(figure_name)
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
    plt.close()


    # -----------------------------------
    figname = figdir + 'energy_breakdown_sum.png'
    figure_name = kwargs.get("figure_name", figname)

    plt.figure()
    for field in fields_to_vavg[:1]:
        norm_value = rdu.getNormValue(field, simDir=ph.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)
        plt.plot(times_in_LC, vol_means[field]/norm_value[0], markersize=3, label=field_abbrev + norm_value[1])
    summed_energy = vol_means["netEnergyDensity"] + vol_means["turbulentEnergyDensity"] + vol_means["internalEnergyDensity"]
    plt.plot(times_in_LC, summed_energy/norm_value[0], markersize=3, label="Sum", ls="--", color="black")

    plt.gca().set_yscale("log")
    plt.gca().set_xlabel("$tc/L$")
    plt.gca().tick_params(axis='y')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(sim_name)
    plt.legend(frameon=False)

    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        plt.savefig(figure_name)
        plt.gca().grid(False, which='both')
        plt.savefig(figure_name.replace(".png", "_nogrid.png"))
    plt.close()



if __name__ == '__main__':

    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"
    overwrite_data = False
    only_means = True
    only_stds = False
    plot_decay_time = True

    sim_name = "zeltron_96cube_mz-mode-dcorr0sigma05A025_s0-Nd60"
    # sim_name = "test_data"

    if stampede2:
        system_config = "stampede2"

    kwargs = {}
    # kwargs["figure_name"] = "show"
    kwargs["overwrite_data"] = overwrite_data
    kwargs["only_means"] = only_means
    kwargs["only_stds"] = only_stds
    kwargs["plot_decay_time"] = plot_decay_time

    # --------------------
    ph = path_handler(sim_name, system_config)
    plot_energy_breakdown(ph, **kwargs)
