
import numpy
import math
import matplotlib
import zfileUtil
zfileUtil.detectDisplay()
import matplotlib.pyplot as plt
import sys

def plot_field_zy(it,ix,field, **kws):
    consts = zfileUtil.Consts

    reduceRes = 1
    if "reduceRes" in kws:
      reduceRes = kws["reduceRes"]
    if it=='':
       it='0'

    if ix=='':
       ix='0'

    if field=='':
       field='Bx'

    #===============================================================================
    # Parameters of the simulation

    (params, units) = zfileUtil.getSimParams()

    iyLo = None
    iyHi = None
    if "iymin" in kws: iyLo = kws["iymin"]
    if "iymax" in kws: iyHi = kws["iymax"]

    #===============================================================================
    # The grid
    # add time step to field
    fieldFile = "+".join([f + "_" + it for f in field.split("+")])
    slices = None
    if reduceRes > 1:
      #slices = (slice(None, None, reduceRes), slice(iyLo, iyHi, reduceRes))
      slices = (slice(iyLo, iyHi, reduceRes), slice(None, None, reduceRes))
    elif iyLo is not None or iyHi is not None:
      #slices = (slice(None), slice(iyLo, iyHi))
      slices = (slice(iyLo, iyHi), slice(None))
    (mapxy, time, step, coords) = zfileUtil.getFieldSliceYZ(
      fieldFile, int(ix), slices = slices)
    mapxy = mapxy.transpose()
    z1d=coords[2]
    y1d=coords[1]
    z, y = numpy.meshgrid(z1d, y1d, indexing='ij')
    
    rho=consts.c/params["omegac"]
    B0=params["B0"]
    nd = params["Drift. dens."]
    nb = params["density ratio"] * nd
    norm = (1., "")
    if '/' in field:
      field = field.split("/")[-1]
    if len(field) == 2 and field[0] in "BE":
      norm = (B0, r"$/B_0$")
    elif len(field) > 8 and field[:6]=="mapxyz":
      if field[-2:] == "bg":
        norm = (nb, r"$/n_{be}$")
      elif field[-5:] == "drift":
        norm = (nb, r"$/n_{be}$")
    elif field[0] == "m": #}{
      norm = (nb, "n_{be}")
    elif len(field)>3 and field[:3]=="JxB": #}{
      norm = (nb*consts.e*consts.c * B0,"(e n_{be} c B_0)")
    elif field[0] == "J" or field[:-1]=="dblRedJ": #}{
      norm = (nb*consts.e*consts.c, "$(e n_{be} c)$")
    #}

    # The field
    #vmin=-1
    #vmax=1
    vmin = None
    vmax = None
    if ("vmin" in kws): vmin = kws["vmin"]
    if ("vmax" in kws): vmax = kws["vmax"]
    actualMin = mapxy.min()/norm[0]
    actualMax = mapxy.max()/norm[0]
    if (vmin is None): 
      vmin = actualMin
    if (vmax is None): vmax = actualMax
    if (vmin < 0 and vmax > 0 and vmax+vmin < vmax/3.):
      if "vmax" not in kws: vmax = max(vmax, -vmin)
      if "vmin" not in kws: vmin = -vmax
      print vmin, vmax

    ymin = numpy.min(y)/rho
    ymax = numpy.max(y)/rho
    if "ymin" in kws: ymin = kws["ymin"]
    if "ymax" in kws: ymax = kws["ymax"]
    xRange = (z.max()-z.min())/rho
    yRange = ymax - ymin
    if abs(yRange/xRange -1.) > 0.2: #{
      rc = matplotlib.rcParams
      figsize = list(rc["figure.figsize"])
      ratio = max(0.75, min(2, yRange/xRange))
      figsize[1] = figsize[1] * ratio
      plt.figure(figsize=figsize)
    #}
    #vmin=-2.
    #vmax=2.
    cnorm = None
    logScale = 0
    if "logScale" in kws and kws["logScale"]: #{
      if (vmin <= 0):
        logScale = 2
        # Need to think more about how to do this
        #vAbsMin = abs(x).min()/rho
        #vAbsMax = abs(x).max()/rho
        #linthresh = min(max(vAbsMax/1e4, numpy.sqrt(vAbsMin*vAbsMax)), 
        #                min(vAbsMax, vmax/10.)
        linthresh = 0.1
        linscale = 0.1
        cnorm = matplotlib.colors.SymLogNorm(
          linthresh=linthresh, linscale = linscale,
          vmin=vmin, vmax=vmax) 
      else:
        logScale = 1
        cnorm = matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax) 
        if vmin >= actualMin: 
          mapxy[mapxy < norm[0]*vmin] = norm[0]*vmin
    #}
    cbExtend = "neither"
    if actualMin < vmin - 0.05*(vmax-vmin) or (logScale and
      actualMin < vmin*0.95):
      cbExtend = 'min'
    if actualMax > vmax + 0.05*(vmax-vmin):
      cbExtend = 'max' if cbExtend=='neither' else 'both'
    p = plt.pcolormesh(z/rho,y/rho,mapxy/norm[0],vmin=vmin,vmax=vmax,
      norm = cnorm,
      cmap = plt.get_cmap("seismic" if vmin < 0 else "hot"),
      )
    if abs(yRange/ratio - 1.) < 0.25:
      plt.gca().set_aspect("equal")
    plt.xlabel(r'$z/\rho_c$')
    plt.ylabel(r'$y/\rho_c$')
    plt.xlim([numpy.min(z/rho),numpy.max(z/rho)])

    plt.ylim(ymin, ymax)
    #plt.xlim(0,160)
    #plt.ylim(30,50)
    
    plt.colorbar(p,extend=cbExtend).ax.set_ylabel(field+norm[1])

#    overlayAz = ("withAz" in kws and kws["withAz"])
#    if overlayAz: #{
#      # need to find path of zPy/for2d containing  plot_Az.py
#      # If this doesn't work, then need zPy/for2D in PYTHONPATH
#      import os
#      dir3d = os.path.dirname(__file__)
#      if len(dir3d) > 5 and dir3d[-5:]=="for3D":
#        dir2d = dir3d[:-2] + "2D"
#        import sys
#        sys.path.append(dir2d)
#      try:
#        import plot_Az
#      except:
#        msg = "The script needs plot_Az.py to plot magnetic field lines. "
#        msg += "Please add the directory containing that to the "
#        msg += "environment variable PYTHONPATH -- it's probably "
#        msg += "something like .../zPy/for2D"
#        print msg
#        raise #ValueError, msg
#      ccolor = 'b' if vmin >= 0 else 'g'
#      if isinstance(kws["withAz"], str): #{
#        ccolor = kws["withAz"]
#      #}
#      (Az, timeAz, stepAz, xAz, yAz, izAz) = plot_Az.get_Az(it, iz)
#      if iyLo is not None or iyHi is not None:
#        Az = Az[:, slice(iyLo, iyHi)]
#        yAz = yAz[slice(iyLo, iyHi-1)]
#      numLevels = 40
#      if "AzLevels" in kws:
#        numLevels = kws["AzLevels"]
#      cp = plt.contour(xAz/rho, yAz/rho, Az[:-1,:-1].transpose()/B0, 
#        numLevels, 
#        lw=1.0,
#        linestyles='-',
#        alpha = 0.6,
#        #levels = numpy.arange(0.,0.8e8,0.04e8)
#        colors=ccolor)
#    #}
    
    title = "time step="+it+"/ ix="+ix+"/ Field="+field
    Lx = params["xmax"] - params["xmin"]
    Lz = params["zmax"] - params["zmin"]
    dt = params["dt"]
    title = field 
    is2D = (params["NX"] <= 2)
    if (is2D):
      title += r", $tc/L_z=$%.3g" % (int(it)*dt * consts.c / Lz)
    else:
      title += r", $tc/L_x=$%.3g" % (int(it)*dt * consts.c / Lx)
    if (not is2D):
      is2D = False
      dx = params["dx"]
      title += r", $x/\rho_c=$%.3g" %  (int(ix)*dx/rho)
    plt.title(title)
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.18)
    
    if 1: # set window title
      import sys
      wt = zfileUtil.getShortCwd()
      wt += ' ' + ' '.join(sys.argv[1:])
      plt.gcf().canvas.set_window_title(wt)

    save = False
    if "save" in kws and kws["save"]: #{
      save = True
      defaultName = field 
      if not is2D:
        defaultName += "_ix%s" % ix
      defaultName += "_%s" % it
      #kws["save"] = name
      zfileUtil.saveFig(defaultName, **kws)
    #}
    
    #===============================================================================

    if (not save) or ("show" in kws and kws["show"]):
      plt.show()
    
    #===============================================================================

if __name__ == "__main__": #{
  args, kwargs = zfileUtil.getArgsAndKwargs(sys.argv[1:])
  if len(args) != 3:
    print "usage: python plot_field_zy.py step ix-coordinate field",
    print "[vmin=, vmax=, logScale=, save=, overwrite=]"
    print "[ymin=inRhoC, ymax=inRhoC, splitY=#rhocAroundLayer]"
    print " e.g., python plot_field_zy.py 3000 128 Bz"
    print "  field can be the sum of fields, like Jz_electrons+Jz_ions"
  else:
    zfileUtil.setDefaultPlot()
    plot_field_zy(args[0],args[1],args[2], **kwargs)
#}
