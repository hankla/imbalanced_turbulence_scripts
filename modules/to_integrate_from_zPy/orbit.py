
import numpy
import math
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys

def plot_orbit(ip,spec):

    if ip=='':
       ip='1'
       
    if spec=='':
       spec='electrons'

    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]

    # Number of timesteps
    NT=params2[9]

    # Duration of a timestep
    dt=params2[20]

    # Upstream magnetic field
    B0=params1[2]

    # Boundaries of the particle energy array
    gmin=params2[21]
    gmax=params2[22]

    dir='../data/orbits/'

    #===============================================================================
    # Particle's data

    data=numpy.loadtxt(dir+"orb_particle_"+spec+ip+".dat")
    x=data[:,0]/rho
    y=data[:,1]/rho
    z=data[:,2]/rho
    ux=data[:,3]
    uy=data[:,4]
    uz=data[:,5]
    Ell=data[:,7]
    Bpp=data[:,8]
    Fr=data[:,9]
    Fe=data[:,10]
    
    gam=numpy.sqrt(1.0+ux*ux+uy*uy+uz*uz)

    #===============================================================================
    # Sample of points along the trajectory
    
    i1=math.floor(1.0*(len(x)-1)/5.0)
    i2=math.floor(2.0*(len(x)-1)/5.0)
    i3=math.floor(3.0*(len(x)-1)/5.0)
    i4=math.floor(4.0*(len(x)-1)/5.0)
    
    x1=x[i1]
    y1=y[i1]
    z1=z[i1]
    
    x2=x[i2]
    y2=y[i2]
    z2=z[i2]
    
    x3=x[i3]
    y3=y[i3]
    z3=z[i3]
    
    x4=x[i4]
    y4=y[i4]
    z4=z[i4]
    
    #===============================================================================
    # xy-plane

    plt.figure(1)
    plt.subplot(211)
    plt.plot(x,y,color='blue',lw=2)
    
    plt.plot(x[0],y[0],'bo',markersize=8,color='green')
    plt.plot(x1,y1,'bo',markersize=8,color='red')
    plt.plot(x2,y2,'bo',markersize=8,color='red')
    plt.plot(x3,y3,'bo',markersize=8,color='red')
    plt.plot(x4,y4,'bo',markersize=8,color='red')
    plt.plot(x[len(x)-1],y[len(y)-1],'bo',markersize=8,color='magenta')
    
    plt.xlabel(r'$x/\rho$',fontsize=18)
    plt.ylabel(r'$y/\rho$',fontsize=18)

    #===============================================================================
    # zy-plane

    plt.subplot(212)
    plt.plot(z,y,color='blue',lw=2)
    
    plt.plot(z[0],y[0],'bo',markersize=8,color='green')
    plt.plot(z1,y1,'bo',markersize=8,color='red')
    plt.plot(z2,y2,'bo',markersize=8,color='red')
    plt.plot(z3,y3,'bo',markersize=8,color='red')
    plt.plot(z4,y4,'bo',markersize=8,color='red')
    plt.plot(z[len(z)-1],y[len(y)-1],'bo',markersize=8,color='magenta')
    
    plt.xlabel(r'$z/\rho$',fontsize=18)
    plt.ylabel(r'$y/\rho$',fontsize=18)

    #===============================================================================
    # 3D orbit
    
    fig = plt.figure(2)
    ax = fig.gca(projection='3d')
    ax.plot(x,y,z,color='blue',lw=2,label='')
    
    ax.scatter(x[0],y[0],z[0],'bo',color='green')
    ax.scatter(x1,y1,z1,'bo',color='red')
    ax.scatter(x2,y2,z2,'bo',color='red')
    ax.scatter(x3,y3,z3,'bo',color='red')
    ax.scatter(x4,y4,z4,'bo',color='red')
    ax.scatter(x[len(x)-1],y[len(y)-1],z[len(z)-1],'bo',color='magenta')
    
    ax.set_xlabel(r'$x/\rho$',fontsize=18)
    ax.set_ylabel(r'$y/\rho$',fontsize=18)
    ax.set_zlabel(r'$z/\rho$',fontsize=18)
    
    #===============================================================================
    # Plot particle's properties
    fig = plt.figure(3)
    
    # Particle energy
    plt.subplot(311)
    plt.plot(gam,color='blue',lw=2)
    plt.xlabel('Time step',fontsize=18)
    plt.ylabel(r'$\gamma(t)$',fontsize=18)
    
    plt.plot(0,gam[0],'bo',markersize=8,color='green')
    plt.plot(i1,gam[i1],'bo',markersize=8,color='red')
    plt.plot(i2,gam[i2],'bo',markersize=8,color='red')
    plt.plot(i3,gam[i3],'bo',markersize=8,color='red')
    plt.plot(i4,gam[i4],'bo',markersize=8,color='red')
    plt.plot(len(gam)-1,gam[len(gam)-1],'bo',markersize=8,color='magenta')
    
    # Synchrotron critical energy
    plt.subplot(312)
    esync=1.7365152e-08*Bpp*gam*gam
    esync[0]=esync[1]
    plt.plot(esync,color='red',lw=2)
    plt.xlabel('Time step',fontsize=18)
    plt.ylabel(r'$\epsilon_{sync}(t) [eV]$',fontsize=18)
    
    plt.plot(0,esync[0],'bo',markersize=8,color='green')
    plt.plot(i1,esync[i1],'bo',markersize=8,color='red')
    plt.plot(i2,esync[i2],'bo',markersize=8,color='red')
    plt.plot(i3,esync[i3],'bo',markersize=8,color='red')
    plt.plot(i4,esync[i4],'bo',markersize=8,color='red')
    plt.plot(len(esync)-1,esync[len(esync)-1],'bo',markersize=8,color='magenta')
    
    print numpy.max(esync)/1e6,'MeV'
    
    # Frad Versus Fel   
    plt.subplot(313)

    plt.plot(Fe,color='magenta',lw=2)
    plt.plot(Fr,color='green',lw=2,ls='--')
    plt.xlabel('Time step',fontsize=18)
    plt.ylabel(r'$F_e(t), g(t)$',fontsize=18)
    plt.ylim([0e0,1.2*numpy.max(Fe)])
    
    plt.plot(0,Fe[0],'bo',markersize=8,color='green')
    plt.plot(i1,Fe[i1],'bo',markersize=8,color='red')
    plt.plot(i2,Fe[i2],'bo',markersize=8,color='red')
    plt.plot(i3,Fe[i3],'bo',markersize=8,color='red')
    plt.plot(i4,Fe[i4],'bo',markersize=8,color='red')
    plt.plot(len(Fe)-1,esync[len(Fe)-1],'bo',markersize=8,color='magenta')
    
    #===============================================================================
    
    plt.show()
    
    #===============================================================================
    
plot_orbit(sys.argv[1],sys.argv[2])
