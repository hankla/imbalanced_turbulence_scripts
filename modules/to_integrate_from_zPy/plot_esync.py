
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_esync(it,spec,sym):

    if it=='':
       it='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # Frequency-array
    nu=numpy.loadtxt(".././data/nu.dat")
    eps1=nu*4.1356701e-15
    
    # Synchrotron spectrum
    nuFnu=numpy.loadtxt(".././data/ecut_iso_"+spec+"_"+sym+it+".dat")
    nuFnu=nuFnu*eps1*eps1
    
    plt.plot(eps1,nuFnu,color='blue',lw=2)
    plt.xscale('log')
    plt.yscale('log')

    plt.xlabel(r'$\epsilon_{cut}$ [eV]',fontsize=20)
    plt.ylabel(r'$\epsilon^2_{cut} \frac{dN}{d\epsilon_{cut}}$ [A.U.]',fontsize=20)
    plt.xlim([numpy.min(eps1),numpy.max(eps1)])
    
    miny=1e-4*numpy.max(nuFnu)
    maxy=3.0*numpy.max(nuFnu)
    
    plt.ylim([miny,maxy])

    plt.plot([1.6e8,1.6e8],[miny,maxy],ls='--',lw=2,color='red')
    
    plt.title("time step="+it+"/ Species="+spec+"/ Sym="+sym,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_esync(sys.argv[1],sys.argv[2],sys.argv[3])
