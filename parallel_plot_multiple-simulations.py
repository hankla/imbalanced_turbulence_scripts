"""
This script will plot slices of every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

TO DO:
    - from create_slices-2d_cross-helicity: need to implement subtracting vel/B means
"""
import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import shutil
import multiprocessing as mp
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from plot_slices import plot_slice
# from plot_profiles_spatial import plot_profiles_spatial
# from plot_profiles_temporal import plot_profiles_temporal
from plot_volume_averages import plot_volume_average
from plot_length_scales_over_time import plot_length_scales
from plot_energy_conservation import plot_energy_conservation
# from plot_flux_over_time import plot_flux_over_time
from plot_magnetic_energy_spectrum import plot_magnetic_energy_spectrum
from plot_injected_energy import plot_injected_energy
# from plot_FT_vol_avg_over_time import plot_FT_volume_averages_over_time
# from plot_FT_profiles_temporal import plot_FT_profiles_temporal
# from plot_FT_profiles_spatial import plot_FT_profiles_spatial
from plot_particle_energy_spectrum import plot_particle_energy_spectrum
from plot_elsaesser_quantities import plot_elsaesser_quantities


def parallel_plot(simulations, system_config, **kwargs):
    pool = mp.Pool(len(simulations))
    arg = [(sim, system_config, kwargs) for sim in simulations]
    pool.map(plotting_wrapper, arg)



def plotting_wrapper(arg):
    sim, system_config, kwargs = arg
    return plot_single_simulation(sim, system_config, **kwargs)


def plot_single_simulation(sim_name, system_config, **kwargs):
    print("******************************************")
    print("******************************************")
    print("Plotting simulation " + sim_name)

    # -------------------------------------------
    # extract some kwargs
    overwrite_data = kwargs.get("overwrite_data", False)
    t_start_ind = kwargs.get("t_start_ind", 0)
    t_end_ind = kwargs.get("t_end_ind", -1)

    # -------------------------------------------
    # Set paths
    ph = path_handler(sim_name, system_config)

    time_steps = retrieve_time_values(ph, overwrite_data)[:, 0]
    times_to_plot = time_steps[t_start_ind:t_end_ind]
    # -------------------------------------------
    # Next step is to make sure the simulation parameters
    # are accessible from the reduced data path
    files_to_copy = ["phys_params.dat", "input_params.dat", "Eem.dat", "Eifluid.dat", "Eefluid.dat", "Einj.dat", "Ekin_ions_bg.dat", "Ekin_electrons_bg.dat", "divE.dat"]
    if stampede2:
        for file1 in files_to_copy:
            shutil.copyfile(ph.path_to_raw_data + file1, ph.path_to_reduced_data + file1)
        print("Files copied to reduced data folder.")


    # ---------------------------------------------
    if kwargs.get("plot_vavgs", False):
        plot_volume_average(ph, **kwargs)
        # plot_elsaesser_quantities(ph, **kwargs)

    if kwargs.get("plot_energy", False):
        plot_length_scales(ph, **kwargs)
        plot_energy_conservation(ph)
        # injected energy over time
        plot_injected_energy(ph, **kwargs)

    if kwargs.get("plot_spectra", False):
        # kinetic energy spectrum
        # need spectrum_electrons_bg_0.h5 etc for this
        # plot_particle_energy_spectrum(ph, times_to_plot=times_to_plot, species='both')
        plot_particle_energy_spectrum(ph, times_to_plot=times_to_plot, species='both', xlims=[10, 1e5], ylims=[1.0e-3, 1])
        # plot_particle_energy_spectrum(ph, times_to_plot=time_steps[0:10], species='both')
        plot_particle_energy_spectrum(ph, times_to_plot=time_steps[0:10], species='both', xlims=[10, 1e5], ylims=[1.0e-3, 1])

        # magnetic energy spectrum
        # will output to reduced data so doesn't have to be on stampede2
        plot_magnetic_energy_spectrum(ph, times_to_plot=times_to_plot)
        plot_magnetic_energy_spectrum(ph, times_to_plot=time_steps[0:10])

    # if kwargs.get("plot_profiles", False):
        # plot_profiles_spatial(ph, times_to_plot, **kwargs)
        # plot_profiles_spatial(ph, times_to_plot, **kwargs, average_2d=True)
        # plot_profiles_temporal(ph, **kwargs)
        # plot_profiles_temporal(ph, **kwargs, average_2d=True)

    # if plot_FT:
        # # Get omegaD
        # (params, X) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
        # omegaD = params["omegaD"]
#
        # # Plot volume averages first. Remember <Ex> etc doesn't mean anything! Only vz.
        # # plot_FT_volume_averages_over_time(ph, omegaD=omegaD, **kwargs)
        # plot_FT_volume_averages_over_time(ph, omegaD=omegaD, log_yscale=True, **kwargs)
        # # plot_FT_volume_averages_over_time(ph, omegaD=omegaD, log_yscale=True, only_after_decay=True, **kwargs)
        # # plot_FT_volume_averages_over_time(ph, omegaD=omegaD, log_yscale=True, only_before_decay=True, **kwargs)
        # # plot_FT_volume_averages_over_time(ph, omegaD=omegaD, log_yscale=True, detrend=True, **kwargs)
        # # Plot FT of profiles.
        # # plot_FT_profiles_temporal(ph, omegaD=omegaD, **kwargs)
        # # plot_FT_profiles_temporal(ph, omegaD=omegaD, log_yscale=True, **kwargs)
        # plot_FT_profiles_temporal(ph, omegaD=omegaD, log_yscale=True, **kwargs, average_2d=True)
        # # plot_FT_profiles_temporal(ph, omegaD=omegaD, log_yscale=True, only_before_decay=True, **kwargs)
        # # plot_FT_profiles_temporal(ph, omegaD=omegaD, log_yscale=True, only_after_decay=True, **kwargs)
#
        # # plot_FT_profiles_spatial(ph, **kwargs, log_yscale=True)
        # plotFT_profiles_spatial(ph, **kwargs, log_yscale=True, average_2d=True)
        # # plot_FT_profiles_spatial(ph, **kwargs, log_yscale=False)

    if plot_slices:
        for time in times_to_plot[::10]:
            plot_slice(ph, time, fields_to_plot=kwargs.get("fields_to_slice",["Jmag"]),
                       ix=ix, iy=iy, iz=iz, overwrite=overwrite_fig, logScale=logScale)
    # Plot fluxes
    # for quantity in rdu.Zvariables.flux_options:
        # plot_flux_over_time(ph, quantity=quantity, ix=0, iy=None, iz=None)
        # plot_flux_over_time(ph, quantity=quantity, ix=None, iy=0, iz=None)
        # plot_flux_over_time(ph, quantity=quantity, ix=None, iy=None, iz=0)


if __name__ == "__main__":
    # ----------------------------------
    #    INPUTS
    # ----------------------------------
    stampede2 = True
    system_config = "lia_hp"

    zVars = rdu.Zvariables.options + rdu.Zvariables.restFrame_options
    fields_to_vavg = zVars
    all_fields = fields_to_vavg + rdu.Zvariables.restFrame_options_NOvavg

    # fields_to_profile = ["elsaesserMinusy", "elsaesserPlusy", "velz_total", "Ex", "By", "Bx", "Bz", "vely_total", "velx_total"]
    # fields_to_profile = ["velz_total", "Ex", "By", "Bx", "Bz", "vely_total", "velx_total"]
    # fields_to_slice = ["Jmag", "Jtot_x", "Jtot_y", "Jtot_z"] + fields_to_profile
    # fields_to_slice = ["pelsaesserDifference", "VpBp_total", "Jmag", "Jtot_z", "pelsaesserMinusEnergy", "pelsaesserPlusEnergy"]
    fields_to_slice = ["VpBp_total", "Jmag"]
    # fields_to_vavg = zVars
    # fields_to_vavg = ["EdotJf", "pelsaesserEnergy", "pelsaesserDifference", "pelsaesserPlusEnergy", "pelsaesserMinusEnergy", "velz_total", "VpBp_total", "deltaBrms", "deltaBrms2", "vely_total", "velx_total", "internalEnergyDensity"]
    fields_to_vavg = ["velz_total", "VpBp_total", "deltaBrms", "deltaBrms2", "vely_total", "velx_total", "internalEnergyDensity"]
    fields_to_vavg = ["density3_electrons"]
    fields_to_vavg = ["elsaesserEnergyPlus", "elsaesserEnergyMinus"]

    t_start_ind = 0
    t_end_ind = -1

    ix = 0; iy = 0; iz = 0
    overwrite_fig = True
    overwrite_data = False
    logScale = False

    plot_vavgs = True
    plot_profiles = False
    plot_FT = False
    plot_slices = False
    plot_spectra = False
    plot_energy = False
    only_means = True
    only_stds = False

    if stampede2:
        system_config = "stampede2"
        scratch_dir = "/scratch/06165/ahankla/imbalanced_turbulence/"
        list_of_sims = []
        list_of_sims.append(rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im00"])[0])
        list_of_sims.append(rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im025"])[0])
        list_of_sims.append(rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im05"])[0])
        list_of_sims.append(rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["8xyz", "768cube", "im075"])[0])
        simulations = list_of_sims
    else:
        scratch_dir = "/mnt/d/imbalanced_turbulence/data_reduced/"
        simulations = rdu.get_list_of_sim_names_with_phrases(scratch_dir, ["Jxyz_", "2zopp"])

    # ----------------------------------
    #    PLOTTING (NO EDITS REQUIRED)
    # ----------------------------------
    kwargs = {}
    kwargs["overwrite_data"] = overwrite_data
    kwargs["logScale"] = logScale
    kwargs["overwrite_fig"] = overwrite_fig
    # kwargs["fields_to_profile"] = fields_to_profile
    kwargs["fields_to_vavg"] = fields_to_vavg
    kwargs["fields_to_slice"] = fields_to_slice
    kwargs["ix"] = ix
    kwargs["iy"] = iy
    kwargs["iz"] = iz
    kwargs["t_start_ind"] = t_start_ind
    kwargs["t_end_ind"] = t_end_ind
    kwargs["plot_vavgs"] = plot_vavgs
    kwargs["plot_energy"] = plot_energy
    kwargs["plot_spectra"] = plot_spectra
    # kwargs["plot_profiles"] = plot_profiles
    kwargs["plot_slices"] = plot_slices
    kwargs["only_means"] = only_means
    kwargs["only_stds"] = only_stds

    parallel_plot(simulations, system_config, **kwargs)
