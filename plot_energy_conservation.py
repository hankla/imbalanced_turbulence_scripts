import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *


def plot_energy_conservation(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    overwrite_data = kwargs.get("overwrite_data", False)
    times_in_LC = retrieve_time_values(ph)[:, 2]
    sim_name = ph.sim_name

    figpath = ph.path_to_figures + "energy-conservation/"
    figdir = figpath
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "energy-conservation.png"

    figure_name = kwargs.get("figure_name", figname)

    if figure_name == "show":
        matplotlib.use("TkAgg")
    energy_conservation(ph=ph, times=times_in_LC)

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figure_name)
        plt.savefig(figure_name)
    plt.close()


if __name__ == '__main__':
    stampede2 = False
    system_config = "jila_laptop"

    kwargs = {}
    # kwargs["figure_name"] = "show"

    sim_name = "zeltron_96cube_balanced_s0"
    # sim_name = "zeltron_96cube_single-mode_s0"
    # sim_name = "zeltron_96cube_balanced_master"
    # sim_name = "zeltron_96cube_single-mode_master"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_standing"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_travelling"
    # sim_name = "zeltron_96cube_single-mode_no-forcing_dcorr"
    # sim_name = "zeltron_96cube_single-mode_sforcing_zg0"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0"
    # sim_name = "zeltron_96cube_z-mode-sigma05A025_s0"
    # sim_name = "zeltron_192cube_z-mode-dcorr0sigma05A025_s0"
    # sim_name = "zeltron_48cube_L05_z-mode-dcorr0sigma05A025_s0"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0"
    sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-Nd30"
    # sim_name = "zeltron_96cube_z-mode-dcorr0sigma05A025_s0-PPC32-Nd60"

    if stampede2:
        ph = path_handler(sim_name, "stampede2")
    else:
        ph = path_handler(sim_name, system_config)

    plot_energy_conservation(ph, **kwargs)
