#!/bin/bash

execfile=${BASH_SOURCE[0]}
dir=${execfile%/setzpy3d.sh}
if [ -d "$dir" ]
then
  pushd "$dir" &>/dev/null
  export zpy3d=`pwd`
  popd &>/dev/null
else
  echo "Error: invalid directory ${dir}"
fi

