
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_zy(it,ix,spec,sym):

    if it=='':
       it='0'

    if ix=='':
       ix='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # Parameters of the simulation

    params1=numpy.loadtxt(".././data/phys_params.dat",skiprows=1)
    params2=numpy.loadtxt(".././data/input_params.dat",skiprows=1)

    # Nominal cyclotron frequency
    rho=2.9979246e+10/params1[3]

    #===============================================================================
    # The grid
    z=numpy.loadtxt(".././data/z.dat")
    y=numpy.loadtxt(".././data/y.dat")

    # The density
    map0=numpy.loadtxt(".././data/densities/mapxyz_"+spec+"_drift0.dat")
    map_temp=numpy.loadtxt(".././data/densities/mapxyz_"+spec+"_"+sym+it+".dat")
    
    mapzy=numpy.empty((len(y),len(z)))
    
    for iz in range(0,len(z)):
        for iy in range(0,len(y)):
            mapzy[iy,iz]=map_temp[iy+iz*len(y),int(ix)]

    mapzy=mapzy/numpy.max(map0)

    plt.pcolormesh(z/rho,y/rho,mapzy)
    plt.xlabel(r'$z/\rho$',fontsize=18)
    plt.ylabel(r'$y/\rho$',fontsize=18)
    plt.xlim([numpy.min(z/rho),numpy.max(z/rho)])
    plt.ylim([numpy.min(y/rho),numpy.max(y/rho)])
    
    plt.colorbar().ax.set_ylabel(r'$n/n_0$',fontsize=18)
    
    plt.title("time step="+it+"/ x="+ix+"/ Species="+spec+"/ Sym="+sym,fontsize=18)
    
    #===============================================================================

    plt.show()
    
    #===============================================================================

plot_zy(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
