import numpy as np
import sys
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
sys.path.append("./modules/")
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from scipy.fftpack import fft, fftfreq, fftshift
from compare_handler import comparison
import itertools
"""
This script will plot the spatial profilesof every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

"""

def compare_plot_FT_vol_avg_over_time(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    match_times = kwargs.get("match_times", False)
    max_scaling = kwargs.get("max_scaling", False)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = kwargs.get("fields_to_vavg", ["velz_total", "VpBp_total", "deltaBrms"])
    log_yscale = kwargs.get("log_yscale", True)
    only_after_decay = kwargs.get("only_after_decay", False)
    only_before_decay = kwargs.get("only_before_decay", False)
    ind_times_to_FT = kwargs.get("ind_times_to_FT", None) # a tuple with starting/ending indices.

    ix = 0
    iy = 0
    iz = 0

    configs = comparison(category).configs
    labels = comparison(category).labels
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)

    figdir = ph0.path_to_figures + "../compare/" + category + "/volAvg-over-time/FT/"
    if match_times:
        figdir += "matched_times/"
    if log_yscale:
        figdir += "log_yscale/"
    if max_scaling:
        figdir += "scaled_to_max/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    time_steps = retrieve_time_values(ph0)[:, 0]
    times_to_plot = kwargs.get("times_to_plot", retrieve_time_values(ph0)[:, 0])
    omegaDvalues = comparison(category).omegaD

    colors = comparison(category).colors
    smallest_end_time = None

    # First, need to find smallest time.
    for sim_name in configs:
        ph = path_handler(sim_name, system_config)

        times_in_LC = retrieve_time_values(ph)[:, 2]
        if smallest_end_time is None or times_in_LC[-1] < smallest_end_time:
            smallest_end_time = times_in_LC[-1]

    # Now, loop through fields and simulations.
    for field in fields_to_plot:
        linestyles = itertools.cycle(comparison(category).linestyles)
        markers = itertools.cycle(comparison(category).markers)
        norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)

        plt.figure()
        plt.xlabel(r"Angular Frequency $\omega~[c/L]$")
        plt.ylabel("Power [arb. units]")

        for i, sim_name in enumerate(configs):
            ph = path_handler(sim_name, system_config)
            (params, units) = rdu.getSimParams(simDir=ph.path_to_reduced_data)
            sigmaDict = rdu.getSimSigmas(params)
            vA = sigmaDict["vAoverC"]
            omegaA = 2 * np.pi * vA
            if "omegaD" in params:
                omegaD = params["omegaD"]*omegaA
            else:
                omegaD = omegaDvalues[i]*omegaA

            times_in_LC = retrieve_time_values(ph)[:, 2]
            time_interval = times_in_LC[1] - times_in_LC[0]

            vol_means = retrieve_variables_vol_means_stds(ph,
                                                        fields_to_vavg=[field
                                                        ])[0]
            if vol_means[field] is None:
                continue
            profile = np.array(vol_means[field]) / norm_value[0]

            smallest_ind = (np.abs(times_in_LC - smallest_end_time)).argmin()
            tit_add = ""
            if match_times:
                times_in_LC = times_in_LC[:smallest_ind + 1]
                profile = profile[:smallest_ind + 1]
            else:
                if ind_times_to_FT is None:
                    if "NDECAY" not in params:
                        start_ind = 0
                        end_ind = -1
                        start_time_step = 0
                        end_time_step = int(time_steps[-1])
                        tit_add = " (entire duration)"
                    elif only_after_decay:
                        NDECAY = params["NDECAY"]
                        start_ind = (np.abs(time_steps - NDECAY)).argmin()
                        end_ind = -1
                        start_time_step = int(NDECAY)
                        end_time_step = int(time_steps[-1])
                        tit_add = " (only after decay)"
                    elif only_before_decay:
                        start_ind = 0
                        NDECAY = params["NDECAY"]
                        end_ind = (np.abs(time_steps - NDECAY)).argmin()
                        start_time_step = 0
                        end_time_step = int(NDECAY)
                        tit_add = " (only before decay)"
                    else:
                        start_ind = 0
                        start_time_step = 0
                        end_time_step = int(time_steps[-1])
                        end_ind = -1
                        tit_add = " (entire duration)"
                        # tit_add = " between $t=${:.2f} and {:.2f} $L/c$".format(times_in_LC[start_ind], times_in_LC[end_ind])
                else:
                    start_time_step = 0
                    end_time_step = int(time_steps[-1])
                    start_ind = ind_times_to_FT[0]
                    end_ind = ind_times_to_FT[1]
                times_in_LC = times_in_LC[start_ind:end_ind]
                profile = profile[start_ind:end_ind]

            # Now do FT stuff
            N = profile.size
            frequencies = fftfreq(N, time_interval)
            profile_FT = fft(profile)
            frequencies = 2 * np.pi * fftshift(frequencies)
            profile_FT_abs_norm = 1.0 / N * np.abs(fftshift(profile_FT))**2.0
            if max_scaling:
                profile_FT_abs_norm = profile_FT_abs_norm / np.max(
                    profile_FT_abs_norm[N // 2:])
            plt.plot(frequencies[N // 2:],
                     profile_FT_abs_norm[N // 2:],
                     label=labels[i],
                     marker=next(markers),
                     ls=next(linestyles),
                     markersize=2,
                     color=colors[i])

        plt.gca().axvline([2.0*omegaA],
                        color="black",
                          ls="--",
                        label=r"$2\omega_A=${:.2f} $c/L$, $2\omega_D=${:.2f} $c/L$".format(2.0*omegaA, 2.0*omegaD))
        plt.gca().axvline([2.0*omegaD],
                        color="black",
                          ls="--")
        plt.gca().axvline([omegaA - omegaD],
                        color="black",
                          ls=":",
                        label=r"$\omega_A-\omega_D=${:.2f} $c/L$, $\omega_A+\omega_D=${:.2f} $c/L$".format(omegaA - omegaD, omegaA+omegaD))
        plt.gca().axvline([omegaA + omegaD],
                        color="black",
                        ls=":")

        plt.legend()
        if log_yscale:
            plt.yscale('log')
        plt.xscale('log')
        plt.gca().grid(color='.9', ls='--')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        figure_name = figdir + field + "_FT-in-time{:d}-{:d}_vol-avg.png".format(start_time_step, end_time_step)
        titstr = "\n FT of " + r"$\langle$" + field_abbrev + norm_value[
            1] + r"$\rangle$" + "\n"
        if match_times:
            titstr += " with times matched up to ${:.2f}~L/c$".format(
                smallest_end_time)
        else:
            titstr += tit_add

        titstr += "\n" + category
        plt.title(titstr)
        plt.tight_layout()
        print("Saving figure " + figure_name)
        plt.savefig(figure_name, bbox_inches="tight")
        root_name = figure_name.split("/")[-1]
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
        plt.close()

if __name__ == '__main__':
    category = "dcorr-comparison"
    category = "decay2500-comparison"
    fields_to_plot = ["deltaBrms", "VpBp_total", "velz_total", "vely_total", "velx_total"]
    match_times = False

    kwargs = {}
    kwargs["system_config"] = "lia_hp"
    kwargs["fields_to_vavg"] = fields_to_plot
    kwargs["match_times"] = match_times
    # kwargs["ind_times_to_FT"] = (10, 100)

    compare_plot_FT_vol_avg_over_time(category, **kwargs)
    compare_plot_FT_vol_avg_over_time(category, **kwargs, only_after_decay=True)
    compare_plot_FT_vol_avg_over_time(category, **kwargs, only_before_decay=True)
