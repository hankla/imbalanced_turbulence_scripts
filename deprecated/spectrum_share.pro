; MAIN PROGRAM
; PRO spectrum_share.pro
; Computes ring-integrated magnetic energy spectrum
; Written by Vladimir Zhdankin

; name of Zeltron data directory
datadir = '../data_512cube_einonrad_tratio1_beta025_prod/'
nsnap = 3000
pi = 2.0*acos(0.0)

print, 'loading magnetic fields'
file = datadir + 'fields/Bx_'+strtrim(nsnap,2)+'.h5'
file_id = H5F_OPEN(file)
dataset_idl = H5D_OPEN(file_id,'/field')
bx = h5d_read(dataset_idl)
dataset_idl = H5D_OPEN(file_id,'/axis0coords')
zvals = h5d_read(dataset_idl)
dataset_idl = H5D_OPEN(file_id,'/axis1coords')
yvals = h5d_read(dataset_idl)
dataset_idl = H5D_OPEN(file_id,'/axis2coords')
xvals = h5d_read(dataset_idl)
ntemp = size(xvals)
nx = ntemp[1]-1 ; account for guard cells
ntemp = size(yvals)
ny = ntemp[1]-1
ntemp = size(zvals)
nz = ntemp[1]-1

file = datadir + 'fields/By_'+strtrim(nsnap,2)+'.h5'
file_id = H5F_OPEN(file)
dataset_idl = H5D_OPEN(file_id,'/field')
by = h5d_read(dataset_idl)

file = datadir + 'fields/Bz_'+strtrim(nsnap,2)+'.h5'
file_id = H5F_OPEN(file)
dataset_idl = H5D_OPEN(file_id,'/field')
bz = h5d_read(dataset_idl)

nm  = min([nx,ny])/3 ; number of modes
spec_ringb = fltarr(nm) ; integrated spectrum

FOR kk = 0,nz-1 DO BEGIN

  bxff = fft(bx(0:nx-1,0:ny-1,kk))
  byff = fft(by(0:nx-1,0:ny-1,kk))
  bzff = fft(bz(0:nx-1,0:ny-1,kk))

  ; unintegrated spectrum
  Et  = (real_part(bxff)^2+imaginary(bxff)^2 $
         + real_part(byff)^2+imaginary(byff)^2 $
         + real_part(bzff)^2+imaginary(bzff)^2)/(8.0*pi)
  
  ; rearranged unintegrated spectrum (center k = 0)
  Et2 = fltarr(nx,ny)
  Et2[nx/2-1:nx-1,ny/2-1:ny-1] = Et[0:nx/2,0:ny/2] ; positive kx positive ky
  Et2[0:nx/2-2,0:ny/2-2] = Et[nx/2+1:nx-1,ny/2+1:ny-1] ; negative kx negative ky
  Et2[nx/2-1:nx-1,0:ny/2-2] = Et[0:nx/2,ny/2+1:ny-1] ; positive kx negative ky
  Et2[0:nx/2-2,ny/2-1:ny-1] = Et[nx/2+1:nx-1,0:ny/2] ; negative kx positive ky

  ; ring integrated spectrum
  Pt = fltarr(nm)
  Pt[0]=0.25*(Et2[nx/2-1,ny/2]+Et2[nx/2-1,ny/2-2]+Et2[nx/2,ny/2-1]+Et2[nx/2-2,ny/2-1])*(2.0*pi)
  kp=findgen(nm)+1.
  for nn=1, nm-1 do begin
    ks   = kp[nn]
    npk = 4*ks ; number of sampling points
    dt  = 2.0*pi/npk ; angular separation between points
    x   = (nx/2-1)+ks*cos(findgen(npk)*dt)
    y   = (ny/2-1)+ks*sin(findgen(npk)*dt)
    pk  = interpolate(Et2,x,y)
    Pt[nn]= (2.0*pi*ks)*total(pk)/npk
  endfor

  spec_ringb = spec_ringb + Pt

ENDFOR

spec_ringb = spec_ringb/nz
; wavenumbers normalized to a reference scale (e.g., rho_e)
ref_scale = 1.0 ; arbitrary
Lx = nx ; arbitrary
kvalsperpt = 2.0*pi*(1.0+findgen(nm))*ref_scale/Lx

plot0=plot(kvalsperpt,spec_ringb,$
xrange=[kvalsperpt(0),kvalsperpt(nm-1)],linestyle=0,thick=1,$
color='blue',/xlog,/ylog,ytitle='$E_{mag}(k_\perp)$',$
font_size=16,xtitle='$k_\perpd_e$',name='Magnetic')

l=legend(target=[plot0],position=[0.75,0.75])

END
