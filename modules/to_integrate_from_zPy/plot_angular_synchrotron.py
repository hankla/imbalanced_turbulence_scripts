
import numpy
import math
import matplotlib.pyplot as plt
import sys

def plot_angular_synchrotron(it,ie,spec,sym):

    if it=='':
       it='0'

    if ie=='':
       ie='0'

    if spec=='':
       spec='electrons'
       
    if sym=='':
       sym='bg'

    #===============================================================================
    # The grid
    phi=numpy.loadtxt(".././data/phi.dat")
    lbd=numpy.loadtxt(".././data/lambda.dat")
    nu=numpy.loadtxt(".././data/nu.dat")

    # The energy-dependent particle angular distribution
    map_temp=numpy.loadtxt(".././data/synchrotron_"+spec+"_"+sym+it+".dat")

    angular=numpy.empty((len(phi),len(lbd)))

    for il in range(0,len(lbd)):
        for ip in range(0,len(phi)):
            angular[ip,il]=map_temp[ip+int(ie)*len(phi),il]

    lbd=lbd*math.pi/180.0
    phi=phi*math.pi/180.0
    
    plt.subplot(111,projection="aitoff")
    plt.pcolormesh(lbd,phi,angular)
    plt.grid("on",color='white',lw=1)
    
    e1="%2.2e " % (nu[int(ie)]*4.1356701e-15)
    e2="%2.2e " % (nu[int(ie)+1]*4.1356701e-15)
    titre=e1+" [eV] "+r"< $\epsilon_1$ < "+e2+" [eV]"
    
    plt.text(0.0,1.9,titre,horizontalalignment='center',fontsize=18)

    #plt.colorbar().ax.set_ylabel(r'$n/n_0$',fontsize=18)

    plt.show()
    
    #===============================================================================

plot_angular_synchrotron(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
