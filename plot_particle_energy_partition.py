import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import raw_data_utils as rdu
from scipy import optimize
from path_handler_class import *
from reduce_data_utils import *
from plotting_utils import *
from compare_handler import *
import itertools

small_size = 12
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)


def plot_nonthermal_fraction(ph, **kwargs):
    # -----------------------------------------------------------------
    # Initial set-up, grabbing variables
    species = kwargs.get("species", None)
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)

    if species not in ['electrons', 'ions']:
        print("Error: species must be one of electrons or ions")
        return
    figdir = ph.path_to_figures + "particle_energy_partition/" + species + "/"
    if not os.path.isdir(figdir):
        os.makedirs(figdir)
    figname = figdir + "nonthermal_energy_fraction_time-evolution"
    if xlims is not None:
        figname += "_xL"
    if ylims is not None:
        figname += "_yL"
    figname += ".png"
    figure_name = kwargs.get("figure_name", figname)

    os.chdir(ph.path_to_reduced_data + "particle_spectra/")
    time_values = retrieve_time_values(ph)
    times_in_LC = time_values[:, 2]

    if figure_name == "show":
        matplotlib.use("TkAgg")
    plt.figure()
    plt.xlabel(r'$tc/L$')
    # plt.ylabel(r'$E_{\rm nonthermal}/E_{\rm total}$')
    if ylims is not None:
        plt.ylim(ylims)
    # else:
        # plt.ylim([1e-8, 1e0])
    if xlims is not None:
        plt.xlim(xlims)

    energy_fraction = []
    particle_fraction = []

    for timestep in time_values[:, 0]:
        timestep = int(timestep)
        # dNdg is f(gamma)
        (dNdg, uBinEdges, step
        ) = rdu.getPtclEnergySpectrum(timestep, species, "bg", "gamma")
        gEdges = np.sqrt(uBinEdges**2+1.)
        gamma_values = 0.5*(gEdges[1:] + gEdges[:-1])
        # cut off zero values
        end_ind = np.where(dNdg == 0)[0][0]
        gamma_values = gamma_values[:end_ind]
        dNdg = dNdg[:end_ind]

        # Fit Maxwell-Juettner
        max_params, max_e = optimize.curve_fit(maxwellJuettner, gamma_values, dNdg)
        fit_maxwellian = maxwellJuettner(gamma_values, *max_params)
        energy_in_maxwellian = fit_maxwellian * gamma_values

        # Get nonthermal particles
        nonthermal_particles = np.clip(dNdg - fit_maxwellian, a_min=0, a_max=None)
        energy_in_nonthermal = nonthermal_particles * gamma_values

        total_energy = energy_in_maxwellian + energy_in_nonthermal
        total_particles = dNdg.sum()
        nonthermal_energy_fraction = energy_in_nonthermal.sum() / total_energy.sum()
        nonthermal_particle_fraction = nonthermal_particles.sum() / total_particles
        maxwellian_particle_fraction = fit_maxwellian.sum() / total_particles

        energy_fraction.append(nonthermal_energy_fraction)
        particle_fraction.append(nonthermal_particle_fraction)

    plt.semilogy(times_in_LC, energy_fraction, label=r"$E_{\rm nonthermal}/E_{\rm total}$")
    plt.semilogy(times_in_LC, particle_fraction, label=r"$N_{\rm nonthermal}/N_{\rm total}$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(ph.sim_name + "\n")
    plt.legend(frameon=False)
    # plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    plt.tight_layout()

    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figname)
        plt.savefig(figure_name, bbox_inches='tight')
        root_name = figure_name.split("/")[-1]
        plt.title('')
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"), bbox_inches='tight')

        plt.gca().grid(False, which='both')
        if not os.path.isdir(figdir + "pdfs/nogrids/"):
            os.makedirs(figdir + "pdfs/nogrids/")
        plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"), bbox_inches='tight')
        if not os.path.isdir(figdir + "nogrids/"):
            os.makedirs(figdir + "nogrids/")
        plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"), bbox_inches='tight')
    plt.close()

    # Plot last time step's division of energy
    plt.figure()
    figname = figdir + "nonthermal_energy_fraction_tLast.png"
    plt.xlabel(r'$\gamma$')
    # plt.ylabel(r'$E_{\rm nonthermal}/E_{\rm total}$')
    plt.title(ph.sim_name + "\n $t=${:.2f} $L/c$".format(times_in_LC[-1]))
    plt.loglog(gamma_values, energy_in_nonthermal/total_energy)
    plt.axhline([1.0], ls='--', color='black')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    # plt.legend(frameon=False)
    # plt.gca().legend(bbox_to_anchor=(1.01, 1.0), frameon=False, ncol=1)
    plt.tight_layout()
    if figure_name == "show":
        plt.show()
    else:
        print("Saving figure " + figname)
        plt.savefig(figname, bbox_inches='tight')

    # plt.loglog(gamma_values, gamma_values * fit_maxwellian, label="Maxwellian only")
    # plt.loglog(gamma_values, gamma_values * (fit_maxwellian + nonthermal_particles), label="Sum")
    # plt.loglog(gamma_values, gamma_values * dNdg, label="Data", ls='--')




def maxwellJuettner(gamma_values, A, Theta):
    # NOTE: need gamma^2 because getPtclAngularSpectrumIntegratedOverAngle does not
    # include the radius^2 when integrating over 3D
    return A*gamma_values**2.0/Theta**3.0*np.exp(-gamma_values/Theta)


if __name__ == '__main__':
    # ------- inputs ---------
    stampede2 = False
    system_config = "lia_hp"
    species = 'electrons'

    # sim_name = "zeltron_96cube-L1_mode-8xyz-Jxyz_im10sigma05dcorr0omega035A075phi0_PPC32-FD30"
    category = "768Imbalance-comparison"
    sim_name = comparison(category).configs[-1]

    if stampede2:
        system_config = "stampede2"
    ph = path_handler(sim_name, system_config)
    xlims = None
    ylims = None

    kwargs = {}
    kwargs["figure_name"] = "show"
    kwargs["species"] = species
    kwargs["xlims"] = xlims
    kwargs["ylims"] = ylims

    plot_nonthermal_fraction(ph, **kwargs)
