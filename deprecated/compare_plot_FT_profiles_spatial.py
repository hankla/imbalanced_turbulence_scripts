import numpy as np
import sys
sys.path.append("./modules/")
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import raw_data_utils as rdu
from path_handler_class import *
from reduce_data_utils import *
from compare_handler import comparison
from scipy.fftpack import fft, fftfreq, fftshift
"""
This script will plot the spatial profilesof every given quantity over all given times.
Defaults are just to plot all quantities (Zvariables.options)
at all times (retrieve_time_values). This takes a while...

"""
def compare_plot_FT_profiles_spatial(category, **kwargs):
    stampede2 = kwargs.get("stampede2", False)
    log_yscale = kwargs.get("log_yscale", True)
    system_config = kwargs.get("system_config", "lia_hp")
    fields_to_plot = kwargs.get("fields_to_profile", ["Ex", "By", "vely_total"])
    average_2d = kwargs.get("average_2d", False)

    dim = kwargs.get("dim", "z")
    ix = kwargs.get("ix", 0)
    iy = kwargs.get("iy", 0)
    iz = kwargs.get("iz", 0)
    # ------------------------------------------

    configs = comparison(category).configs
    labels = comparison(category).labels
    sim_name0 = configs[0]
    if stampede2:
        system_config = "stampede2"
    ph0 = path_handler(sim_name0, system_config)
    times_to_plot = kwargs.get("times_to_profile", retrieve_time_values(ph0)[:, 0])

    figbase = ph0.path_to_figures + "../compare/" + category + "/profiles-1d_spatial/" + dim + "-profile/"
    figure_name = kwargs.get("figure_name", None)

    colors = comparison(category).colors
    linestyles = comparison(category).linestyles
    markers = comparison(category).markers

    def kToLambda(kvals):
        return 2*np.pi/kvals
    def lambdaTok(lambdavals):
        return 2*np.pi/lambdavals

    smallest_end_time = None
    # First, need to find smallest time.
    for sim_name in configs:
        ph = path_handler(sim_name, system_config)

        times_in_LC = retrieve_time_values(ph)[:, 2]
        if smallest_end_time is None or times_in_LC[-1] < smallest_end_time:
            smallest_end_time = times_in_LC[-1]

    time_values = retrieve_time_values(ph0)
    coords = retrieve_coords(ph0)
    if dim == "z":
        dimstr = "$k_z$"
        secstr = r"$\lambda_z$"
        x_values = coords[2]
        if average_2d:
            profstr = "xAyA"
            loadstr = "xAyA"
        else:
            profstr = "ix{}iy{}".format(ix, iy)
            loadstr = "x{}y{}".format(ix, iy)
    elif dim == "y":
        dimstr = "$k_y$"
        secstr = r"$\lambda_y$"
        x_values = coords[1]
        if average_2d:
            profstr = "xAzA"
            loadstr = "xAzA"
        else:
            profstr = "ix{}iz{}".format(ix, iz)
            loadstr = "x{}z{}".format(ix, iz)
    else:
        dimstr = "$k_x$"
        secstr = r"$\lambda_x$"
        x_values = coords[0]
        if average_2d:
            profstr = "yAzA"
            loadstr = "yAzA"
        else:
            profstr = "iy{}iz{}".format(iy,iz)
            loadstr = "y{}z{}".format(iy, iz)
    x_interval = x_values[1] - x_values[0]
    (params, units) = rdu.getSimParams(simDir=ph0.path_to_reduced_data)
    consts = rdu.Consts
    rho = consts.c/params["omegac"]

    if figure_name == "show":
        matplotlib.use("TkAgg")

    for field in fields_to_plot:
        figdir = figbase + field + "/"
        if average_2d:
            figdir += "2d-averaged/"
        figdir += "FT/"
        if not os.path.isdir(figdir):
            os.makedirs(figdir)
        norm_value = rdu.getNormValue(field, simDir=ph0.path_to_reduced_data)
        field_abbrev = rdu.getFieldAbbrev(field, True)

        for time in times_to_plot:
            t_ind = np.where(time_values[:, 0] == time)[0][0]
            time_in_LC = time_values[t_ind, 2]

            plt.figure()
            plt.ylabel("$|\mathcal{F}($" + field_abbrev + norm_value[1] + "$)|^2$")
            # plt.ylabel("Power [arb. units]")
            plt.xlabel(r"Wavenumber " + dimstr + r"$\rho_e$")

            for i, sim_name in enumerate(configs):
                ph = path_handler(sim_name, system_config)
                profile_data = retrieve_profiles_spatial(ph, [time], fields_to_profile=[field], average_2d=average_2d)
                profile = profile_data[field]["{:d}".format(int(time))][loadstr]

                N = profile.size
                wavenumbers = fftfreq(N, x_interval)
                profile_FT = fft(profile)
                # convert to k instead of 1/lambda
                wavenumbers = 2*np.pi*fftshift(wavenumbers)
                # normalize to rhoe
                wavenumbers = wavenumbers * rho
                profile_FT_abs_norm = 1.0/N * np.abs(fftshift(profile_FT))**2.0

                plt.plot(wavenumbers[N//2:], profile_FT_abs_norm[N//2:], marker="o", lw=2, label=labels[i], color=colors[i], markersize=2)
            if ph.setup == "lia_hp":
                secax = plt.gca().secondary_xaxis('top', functions=(kToLambda, lambdaTok))
                xlabel = r"Wavelength " + secstr + r"$/\rho_e$"
                secax.set_xlabel(xlabel)
            if log_yscale:
                plt.yscale('log')
            plt.xscale('log')

            plt.legend()
            plt.gca().grid(color='.9', ls='--')
            plt.gca().tick_params(top=True, right=True, direction='in', which='both')
            figname = figdir + field + "_t{}".format(int(time)) + "_" + profstr + ".png"
            figure_name = kwargs.get("figure_name", figname)
            titstr = "FT of " + field_abbrev + norm_value[1] + " " + dim + "-profile taken at " + profstr
            titstr += ", $t=${:.2f} $L/c$".format(time_in_LC)
            titstr += "\n" + category
            plt.title(titstr)
            plt.tight_layout()
            if figure_name != "show":
                print("Saving figure " + figure_name)
                plt.savefig(figure_name, bbox_inches="tight")
                root_name = figure_name.split("/")[-1]
                if not os.path.isdir(figdir + "pdfs/"):
                    os.makedirs(figdir + "pdfs/")
                plt.savefig(figdir + "pdfs/" + root_name.replace(".png", ".pdf"))

                plt.gca().grid(False, which='both')
                if not os.path.isdir(figdir + "pdfs/nogrids/"):
                    os.makedirs(figdir + "pdfs/nogrids/")
                plt.savefig(figdir + "pdfs/nogrids/" + root_name.replace(".png", "_nogrid.pdf"))
                if not os.path.isdir(figdir + "nogrids/"):
                    os.makedirs(figdir + "nogrids/")
                plt.savefig(figdir + "nogrids/" + root_name.replace(".png", "_nogrid.png"))
                plt.close()
    if figure_name == "show":
        plt.show()

if __name__ == '__main__':
    category = "8mImbalanceAll-comparison"
    category = "8mImbalance-comparison"
    # category = "im10-00-comparison"
    system_config = "lia_hp"
    stampede2 = False
    to_show = False
    average_2d = True

    fields_to_plot = ["Bx", "By"]
    # fields_to_plot = ["Ex", "Bx", "By", "vely_total", "velz_total"]
    ph0 = path_handler(comparison(category).configs[0], system_config)
    times_to_plot = retrieve_time_values(ph0)[:, 0]
    times_to_plot = [times_to_plot[-1]]


    # -------------------------------------------------
    kwargs = {}
    kwargs["system_config"] = system_config
    kwargs["times_to_plot"] = times_to_plot
    kwargs["system_config"] = system_config
    kwargs["stampede2"] = stampede2
    kwargs["fields_to_profile"] = fields_to_plot
    kwargs["average_2d"] = average_2d
    if to_show:
        kwargs["figure_name"] = "show"

    compare_plot_FT_profiles_spatial(category, **kwargs)
