#!/bin/bash
#SBATCH --time=00:10:00
#SBATCH --partition=development
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=plot_zmode
#SBATCH --output=/work/06165/ahankla/stampede2/imbalanced_turbulence/sbatch_scripts/logs/out_plot_energy_conservation.%j
#SBATCH --error=/work/06165/ahankla/stampede2/imbalanced_turbulence/sbatch_scripts/logs/err_plot_energy_conservation.%j
#SBATCH -A TG-PHY140041

module purge
module load intel/18.0.2
module load python3/3.7.0


scriptpath="/work/06165/ahankla/stampede2/imbalanced_turbulence/scripts/"
logpath="/work/06165/ahankla/stampede2/imbalanced_turbulence/sbatch_scripts/logs/log_plot_energy_conservation.txt"

cd ${scriptpath}
pwd
date
module list

python3 -u plot_energy_conservation.py > ${logpath}
python3 -u plot_injected-energy.py >> ${logpath}

date
